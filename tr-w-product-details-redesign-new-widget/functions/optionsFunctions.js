allOptionsSelectedF: function () {
				var allOptionsSelected = true;
				if (this.variantOptionsArray().length > 0) {
					var variantOptions = this.variantOptionsArray();
					for (var i = 0; i < variantOptions.length; i++) {
						if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
							allOptionsSelected = false;
							this.selectedSku(null);
							break;
						}
					}
				}

				return allOptionsSelected;
			},
			
			opusOption: function (data, event) {

				var sWarnOptid = $(event.currentTarget).parents().find('#storePickupWarngModal .store-warning input[type=radio]:checked').attr('id');
				if ("s-decline-upload" === sWarnOptid) {
					data.isPhotoUpload(false);
				} else if ("s-accept-upload" === sWarnOptid) {
					$('#available-today').prop('checked', false);
					$('#ship-to-home').prop('checked', true);
					data.isPhotoUpload(true);

				}
				$.Topic("opusIsPhotoUpload").publish(data.isPhotoUpload());

            },
            allOptionsSelected: function () {
				var allOptionsSelected = true;
				if (this.variantOptionsArray().length > 0) {
					var variantOptions = this.variantOptionsArray();
					for (var i = 0; i < variantOptions.length; i++) {
						if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
							allOptionsSelected = false;
							this.selectedSku(null);
							break;
						}
					}

					if (allOptionsSelected) {
						// get the selected sku based on the options selected by the user
						var selectedSKUObj = this.getSelectedSku(variantOptions);
						if (selectedSKUObj === null) {
							return false;
						}
						this.selectedSku(selectedSKUObj);
					}
					this.refreshSkuData(this.selectedSku());
				}

				return allOptionsSelected;
            },
            //this method convert the map to array of key value object and sort them based on the enum value
			//to use it in the select binding of knockout
			mapOptionsToArray: function (variantOptions, order) {
				var optionArray = [];

				for (var idx = 0, len = order.length; idx < len; idx++) {
					if (variantOptions.hasOwnProperty(order[idx])) {
						optionArray.push({
							key: order[idx],
							value: variantOptions[order[idx]],
							visible: ko.observable(true)
						});
					}
				}
				return optionArray;
            },
            
            //this method populates productVariantOption model to display the variant options of the product
			populateVariantOptions: function (widget) {
				var options = widget.productVariantOptions();
				if (options && options !== null && options.length > 0) {
					var optionsArray = [],
						productLevelOrder, productTypeLevelVariantOrder = {},
						optionValues,
						productVariantOption, variants;
					for (var typeIdx = 0, typeLen = widget.productTypes().length; typeIdx < typeLen; typeIdx++) {
						if (widget.productTypes()[typeIdx].id == widget.product().type()) {
							variants = widget.productTypes()[typeIdx].variants;
							for (var variantIdx = 0, variantLen = variants.length; variantIdx < variantLen; variantIdx++) {
								productTypeLevelVariantOrder[variants[variantIdx].id] = variants[variantIdx].values;
							}
						}
					}

					for (var i = 0; i < options.length; i++) {
						if (widget.product().variantValuesOrder[options[i].optionId]) {
							productLevelOrder = widget.product().variantValuesOrder[options[i].optionId]();
						}
						optionValues = this.mapOptionsToArray(options[i].optionValueMap, productLevelOrder ? productLevelOrder : productTypeLevelVariantOrder[options[i].optionId]);

						productVariantOption = this.productVariantModel(options[i].optionName, options[i].mapKeyPropertyAttribute, optionValues, widget, options[i].optionId);
						optionsArray.push(productVariantOption);

					}
					widget.variantOptionsArray(optionsArray);

					setTimeout(function () {
						widget.loadCarousel();
					}, 100)

				} else {
					widget.imgMetadata = widget.product().product.productImagesMetadata;
					widget.variantOptionsArray([]);
				}
			},