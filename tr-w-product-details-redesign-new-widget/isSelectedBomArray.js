function isSelectedBomArrayValid(widget, parentContext) {
    var hasZones = true;
    return widget.bomArray().length == widget.selectedBomArray().length &&
        widget.selectedBomArray().every(function (selectedBomItem) {
            return selectedBomItem.isInStock() == true;
        }) && (widget.product().productZones()!==null && widget.product().productZones().length > 0);
}