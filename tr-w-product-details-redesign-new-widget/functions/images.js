handleCycleImages: function (data, event, index, parentIndex) {
				var absoluteIndex = index + parentIndex * 4;
				// Handle left key
				if (event.keyCode == 37) {
					if (absoluteIndex == 0) {
						$('#prodDetails-imgCarousel').carousel('prev');
						$('#carouselLink' + (this.product().thumbImageURLs.length - 1)).focus();
					} else if (index == 0) {
						// Go to prev slide
						$('#prodDetails-imgCarousel').carousel('prev');
						$('#carouselLink' + (absoluteIndex - 1)).focus();
					} else {
						$('#carouselLink' + (absoluteIndex - 1)).focus();
					}
				}
				// Handle right key
				if (event.keyCode == 39) {
					if (index == 3) {
						$('#prodDetails-imgCarousel').carousel('next');
						$('#carouselLink' + (absoluteIndex + 1)).focus();
					} else if (absoluteIndex == (this.product().thumbImageURLs.length - 1)) {
						// Extra check when the item is the last item of the carousel
						$('#prodDetails-imgCarousel').carousel('next');
						$('#carouselLink0').focus();
					} else {
						$('#carouselLink' + (absoluteIndex + 1)).focus();
					}
				}
			},

			loadImageToMain: function (data, event, index) {
				this.activeImgIndex(index);
				$(document).find(".cc-viewer-pane").removeClass("hide");
				$(document).find("#display-video").addClass("hide");

				//this.mainImgUrl(this.getProductImages("L")[index]);
				return false;
			},

			assignImagesToProduct: function (pInput) {
				if (this.firstTimeRender == true) {
					this.product().primaryFullImageURL(pInput.primaryFullImageURL);
					this.product().primaryLargeImageURL(pInput.primaryLargeImageURL);
					this.product().primaryMediumImageURL(pInput.primaryMediumImageURL);
					this.product().primarySmallImageURL(pInput.primarySmallImageURL);
					this.product().primaryThumbImageURL(pInput.primaryThumbImageURL);
					this.firstTimeRender = false;
				}

				this.product().thumbImageURLs(pInput.thumbImageURLs);
				this.product().smallImageURLs(pInput.smallImageURLs);
				this.product().mediumImageURLs(pInput.mediumImageURLs);
				this.product().largeImageURLs(pInput.largeImageURLs);
				this.product().fullImageURLs([]);
				this.product().fullImageURLs(pInput.fullImageURLs);
				this.product().sourceImageURLs(pInput.sourceImageURLs);

				this.mainImgUrl(pInput.primaryFullImageURL);
				this.imgGroups(this.groupImages(pInput.thumbImageURLs));
				this.activeImgIndex(0);
				this.activeImgIndex.valueHasMutated();
            },
            // Loads the Magnifier and/or Viewer, when required
			loadImage: function () {
				if (resourcesAreLoaded) {
					var contents = $('#cc-image-viewer').html();
					if (!contents) {
						if (this.viewportWidth() > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
							this.loadMagnifier();
						} else if (this.viewportWidth() >= CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) {
							this.loadZoom();
						} else {
							//Load zoom on carousel
							this.loadCarouselZoom();
						}
					} else {
						this.loadViewer(this.handleLoadEvents.bind(this));
					}
				} else if (resourcesNotLoadedCount++ < resourcesMaxAttempts) {
					setTimeout(this.loadImage, 500);
				}
			},

			groupImages: function (imageSrc) {
				var self = this;
				var images = [];
				if (imageSrc) {
					for (var i = 0; i < imageSrc.length; i++) {
						if (i % 4 == 0) {
							images.push(ko.observableArray([imageSrc[i]]));
						} else {
							images[images.length - 1].push(imageSrc[i]);
						}
					}
				}

				return images;
			},