onConfigReady: function (widget) {

				var self = this;


				if (location.search.substr(1).indexOf("basketLineId") != -1) {
					return;
				}

				/* $("a.bv-content-btn-pages").on('click', function () {
				     var collapseButton = $('#reviews > a');
				     if ($('#review-container').hasClass('in')) {
				         collapseButton.removeAttr("data-toggle");
				     }
				 });*/

				var productModel = {
					'event': 'pdp-details-event',
					'ecommerce': {
						'detail': {
							'actionField': {
								'products': [{
									'name': widget.product().displayName(),
									'id': widget.product().id(),
									'price': widget.product().listPrice(),
									'creative': 'detailview'
								}]
							}
						},
					}
				}
				var dataLayer = [];
				dataLayer.push(productModel);

				if ((publishedSkuData.backorder && (widget.stockAvailable() === 0 || widget.itemQty() > widget.stockAvailable()))) {} else {
					widget.deliveryDateHeadingFlag(true);
					widget.backOrderFlag(false);
					var fullDate;
					if (widget.product().BWYDeliveryDates && typeof widget.product().BWYDeliveryDates == 'function') {
						if (typeof widget.product().BWYDeliveryDates() !== "undefined" && widget.product().BWYDeliveryDates() !== null) {
							fullDate = widget.product().BWYDeliveryDates() || '';
						}
						if (fullDate) {
							var index = fullDate.indexOf('-');
							var newDate = fullDate.substring(index + 1);
							newDate = newDate.replace('-', '/');
							newDate = "Estimated Arrival " + newDate;
							widget.standardDeliveryDate(newDate);
						}
					}
				}

				function getPRMDate(data) {
					var deliveryDate = data.split(',');
					var dateStr = _this.getServerTime();
					var dateTime = new Date(dateStr);
					var today = new Date();
					var time;
					Date.prototype.stdTimezoneOffset = function () {
						var jan = new Date(this.getFullYear(), 0, 1);
						var jul = new Date(this.getFullYear(), 6, 1);
						return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
					}

					Date.prototype.dst = function () {
						return this.getTimezoneOffset() < this.stdTimezoneOffset();
					}
					if (today.dst()) {
						time = dateTime.getUTCHours() - 4; //Daylight savings time
					} else {
						time = dateTime.getUTCHours() - 5; //Not daylight savings time
					}
					if (time < 15) { //If current time is before 3 PM EST
						return deliveryDate[0];
					} else {
						if (deliveryDate[1]) {
							return deliveryDate[1];
						} else {
							return deliveryDate[0];
						}
					}
				}

				if (widget.product().BWYDeliveryDates && typeof widget.product().BWYDeliveryDates == 'function') {
					if (typeof widget.product().BWYDeliveryDates() !== "undefined" && widget.product().BWYDeliveryDates() !== null) {
						widget.bwyFlag(true);
						widget.bwyFlagDate(widget.product().BWYDeliveryDates());
					} else {
						widget.bwyFlag(false);
					}
				}

				if (widget.product().AIRDeliveryDates && typeof widget.product().AIRDeliveryDates == 'function') {
					if ((typeof widget.product().AIRDeliveryDates !== "undefined" && widget.product().AIRDeliveryDates())) {
						widget.airFlag(true);
						widget.airFlagDate((widget.product().AIRDeliveryDates()));
					}
				} else if (widget.product().airDeliveryDates && typeof widget.product().airDeliveryDates == 'function') {
					if (typeof widget.product().airDeliveryDates !== "undefined" && widget.product().airDeliveryDates()) {
						widget.airFlag(true);
						widget.airFlagDate((widget.product().airDeliveryDates()));
					}
				} else {
					widget.airFlag(false);
				}
				if (widget.product().two2NDDeliveryDates && typeof widget.product().two2NDDeliveryDates == 'function') {
					if (typeof widget.product().two2NDDeliveryDates() !== "undefined" && widget.product().two2NDDeliveryDates()) {
						widget.two_ndFlag(true);
						widget.two_ndFlagDate(widget.product().two2NDDeliveryDates());
					}
				} else {
					widget.two_ndFlag(false);
				}
				if (widget.product().PRMDeliveryDates && typeof widget.product().PRMDeliveryDates == 'function') {
					if (typeof widget.product().PRMDeliveryDates() !== "undefined" && widget.product().PRMDeliveryDates()) {
						widget.prmFlag(true);
						widget.prmFlagDate(getPRMDate(widget.product().PRMDeliveryDates()));
					}
				} else {
					widget.prmFlag(false);
				}

				widgetModel = widget;
				if (widget.product().BWYBackorderDeliveryDates && typeof widget.product().BWYBackorderDeliveryDates == 'function') {
					widget.deliveryDate(widget.product().BWYBackorderDeliveryDates());
				}

				$.Topic('TR_PERSONALIZATION_DATA_UPDATED').subscribe(function (personalization_obj) {
					widget.personaliztionObj = personalization_obj;

				});
				$.Topic("REMOVE_BOM").subscribe(function (bom_url_obj) {
					var index = -1;
					for (var j = 0; j < widget.urlObjArr.length; j++) {
						if (widget.urlObjArr[j].slotnum == bom_url_obj.slotnum &&
							widget.urlObjArr[j].prodsku == bom_url_obj.prodsku) {
							index = j;
							break;
						}
					}
					if (index > -1) {
						widget.urlObjArr.splice(index, 1);
						$(".tr-w-button.preview-button").data("url", "");

					}

				});

				$.Topic("BOM_URL").subscribe(function (bom_url_obj) {
					var variantOptions = widget.variantOptionsArray(),
						sku_obj = widget.getSelectedSku(variantOptions),
						skuid, flag;

					if (widget.product().childSKUs().length == 1) {
						sku_obj = widget.product().childSKUs()[0];
						skuid = sku_obj.repositoryId();
						flag = 0;
					} else {
						skuid = sku_obj.repositoryId,
							flag = 0;
					}

					for (var j = 0; j < widget.urlObjArr.length; j++) {
						if (widget.urlObjArr[j].slotnum == bom_url_obj.slotnum &&
							widget.urlObjArr[j].prodsku == bom_url_obj.prodsku) {
							widget.urlObjArr[j] = bom_url_obj;
							flag = 1;
							break;
						}
					}

					if (flag === 0) {
						widget.urlObjArr.push(bom_url_obj);
					}
					var url_sku_id = "000" + skuid;
					var url = "https://thingsremembered.scene7.com/ir/render/ThingsRememberedRender/" + url_sku_id + "?"
					for (var i = 0; i < widget.urlObjArr.length; i++) {
						if (skuid == widget.urlObjArr[i].prodsku && widget.urlObjArr[i].color != "1302030") {
							var str = "obj=GEM/" + widget.urlObjArr[i].slotnum;
							str += "&color=" + widget.urlObjArr[i].color + "&";
							url += str;
						}
					}

					url += "cache=off&hei=400";

					if (widget.urlObjArr.length === widget.bomArray().length) {
						$(".tr-w-button.preview-button").data("url", url);
						$(".tr-e-bom").find(".main-error").hide();

						widget.urlObjArr.sort(function (a, b) {
							return Number(a.slotnum) - Number(b.slotnum);
						});

						$.Topic('TR_BOM_UPDATED').publish(widget.urlObjArr);
					}
				});


				$.Topic('SKU_OBJ_BOM').subscribe(function (product_obj) {
					publishedSkuData = product_obj;
					skuInventory = widget.stockAvailable();
					var slot_variant, curr_index,
						slot_vals = [],
						sku_id = product_obj.id;
					widget.selectedBomArray([]);


					if (product_obj.BOMKey) {
						if (widget.skuBOM[sku_id]) {
							widget.bomArray(widget.skuBOM[sku_id]);
							$(".bom-row.owl-carousel").each(function () {
								$(this).owlCarousel({
									items: 5,
									pagination: false,
									navigation: true,
									navigationText: false,
									rewindNav: false,
									itemsMobile: [479, 5],
									itemsDesktop: [1199, 5],
									itemsDesktopSmall: [980, 5],
									itemsTablet: [768, 5],
									itemsTabletSmall: false
								});
							});
							return;
						}

						var bom_variant_arr = [];

						var value = 0;

						function executeBOMAjax(value) {
							curr_index = value + 1;
							slot_variant = "slot" + curr_index + "Variant";
							slot_vals.push(product_obj[slot_variant]);
							var data = {};
							data[CCConstants.PRODUCT_IDS] = [product_obj[slot_variant]];

							ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, data, function (products) {
								var slot_bom = [];
								if (products.length > 0) {

									var descr_arr = products[0].description.split(','),
										name_arr = products[0].names.split(','),
										urls = products[0].longDescription ? products[0].longDescription.replace(/<p>/g, "") : "",
										urls_arr = urls.length > 0 ? urls.split("</p>") : [];

									if (name_arr.length != descr_arr.length ||
										descr_arr.length != urls_arr.length) {}

									for (var i = 0; i < descr_arr.length; i++) {
										var obj = {};
										obj.skuid = descr_arr[i].trim();
										obj.name = name_arr[i];

										obj.imgurl = "/file/products/" + obj.skuid + ".jpg";
								// 		console.log("change number of stones")
										obj.slot = "slot" + (value + 1);
										obj.prodsku = sku_id;
										slot_bom.push(obj);

									}
									bom_variant_arr[bom_variant_arr.length] = slot_bom;
									widget.bomArray(bom_variant_arr);
									widget.skuBOM[sku_id] = bom_variant_arr;

									var owl = $(".bom-row.owl-carousel")[$(".bom-row.owl-carousel").length - 1]
									$(".bom-row.owl-carousel").each(function () {
										$(this).owlCarousel({
											items: 5,
											pagination: false,
											navigation: true,
											navigationText: false,
											rewindNav: false,
											itemsMobile: [479, 5],
											itemsDesktop: [1199, 5],
											itemsDesktopSmall: [980, 5],
											itemsTablet: [768, 5],
											itemsTabletSmall: false
										});
									});

								}

								if (product_obj.numberOfSlots > value + 1) {
									executeBOMAjax(value + 1);
								}
							});
						}

						executeBOMAjax(value);
					}
					if ((publishedSkuData.backorder && (widget.stockAvailable() === 0 || widget.itemQty() > widget.stockAvailable()))) {

					} else {
						widget.deliveryDateHeadingFlag(true);
						widget.backOrderFlag(false);
						var fullDate = product_obj.BWYDeliveryDates || '';

						if (fullDate !== '' && fullDate !== null) {
							var index = fullDate.indexOf('-');
							var newDate = fullDate.substring(index + 1);
							newDate = newDate.replace('-', '/');
							newDate = "Estimated Arrival " + newDate;
							widget.standardDeliveryDate(newDate);
						}

						if ($(".tr-e-product-description #descr-container").height() + 10 < $(".tr-e-product-description #descr-container .tr-prod-descr-string").height()) {
							$(".tr-prod-descr-show-more").show();
						} else {
							$(".tr-prod-descr-show-more").hide();
						}

						if (typeof product_obj.BWYDeliveryDates !== "undefined" && product_obj.BWYDeliveryDates) {
							widget.bwyFlag(true);
							widget.bwyFlagDate(product_obj.BWYDeliveryDates);
						} else {
							widget.bwyFlag(false);
						}

						if (typeof product_obj.two2NDDeliveryDates !== "undefined" && product_obj.two2NDDeliveryDates) {
							widget.two_ndFlag(true);
							widget.two_ndFlagDate(product_obj.two2NDDeliveryDates);
						} else {
							widget.two_ndFlag(false);
						}
						if (typeof product_obj.AIRDeliveryDates !== "undefined" && product_obj.AIRDeliveryDates) {
							widget.airFlag(true);
							widget.airFlagDate((product_obj.AIRDeliveryDates));

						} else {
							widget.airFlag(false);
						}

						if (typeof product_obj.PRMDeliveryDates !== "undefined" && product_obj.PRMDeliveryDates) {
							widget.prmFlag(true);
							widget.prmFlagDate(getPRMDate(product_obj.PRMDeliveryDates));

						} else {
							widget.prmFlag(false);
						}
					}
					$.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
				});


				$.Topic(pubsub.topicNames.UPDATE_LISTING_FOCUS).subscribe(function (obj) {
					widget.skipTheContent(true);
				});


				$.Topic(pubsub.topicNames.PAGE_READY).subscribe(function (obj) {
					var parameters = {};
					if (obj.parameters) {
						var param = obj.parameters.split("&");
						for (var i = 0; i < param.length; i++) {
							var tempParam = param[i].split("=");
							parameters[tempParam[0]] = tempParam[1];
						}
					}
					if (parameters.variantName && parameters.variantValue) {
						widget.variantName(decodeURI(parameters.variantName));
						widget.variantValue(decodeURI(parameters.variantValue));
					} else {
						widget.variantName("");
						widget.variantValue("");
					}
				});

				$.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD_SUCCESS).subscribe(function (obj) {
					if (obj.productUpdated) {
						widget.disableAddToSpace(true);
						setTimeout(function () {
							widget.disableAddToSpace(false);
						}, 3000);
					} else {
						widget.isAddToSpaceClicked(true);
						widget.disableAddToSpace(true);
						setTimeout(function () {
							widget.isAddToSpaceClicked(false);
						}, 3000);
						setTimeout(function () {
							widget.disableAddToSpace(false);
						}, 3000);
					}
				});

				$.Topic(pubsub.topicNames.USER_LOGIN_SUCCESSFUL).subscribe(function (obj) {
					widget.getSpaces(function () {});
				});

				$.Topic(pubsub.topicNames.USER_AUTO_LOGIN_SUCCESSFUL).subscribe(function (obj) {
					widget.getSpaces(function () {});
				});

				$.Topic(pubsub.topicNames.SOCIAL_REFRESH_SPACES).subscribe(function (obj) {
					widget.getSpaces(function () {});
				});

				$.Topic('PERSONALIZATION_HASH_PASSED').subscribe(function (obj) {});

				widget.itemQty.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.common:resources.quantityRequireMsg')
					},
					digit: {
						params: true,
						message: CCi18n.t('ns.common:resources.quantityNumericMsg')
					},
					min: {
						params: 1,
						message: CCi18n.t('ns.tr-w-product-widget-latest:resources.quantityGreaterThanMsg', {
							quantity: 0
						})
					}
				});

				widget.stockAvailable.subscribe(function (newValue) {
					var max = parseInt(newValue, 10);
					widget.itemQty.rules.remove(function (item) {
						return item.rule == "max";
					});
					if (max > 0) {
						widget.itemQty.extend({
							max: {
								params: max,
								message: CCi18n.t('ns.tr-w-product-widget-latest:resources.quantityLessThanMsg', {
									quantity: max
								})
							}
						});
					}
				});
				swmRestClient.init(widget.site().tenantId, widget.isPreview(), widget.locale());

				widget.fetchFacebookAppId();

				if (widget.displaySWM) {
					widget.showSWM(true);
				}

				widget.onSelectBOMElement = function (selectedBomItem, event, slotIndex) {
					var data = {},
						skuid = selectedBomItem.skuid,
						slot = selectedBomItem.slot;

					selectedBomItem.slotNumber = slotIndex;
					selectedBomItem.isInStock = ko.observable(true);
					var match = ko.utils.arrayFirst(widget.selectedBomArray(), function (item) {
						return selectedBomItem.slot === item.slot;
					});
					if (!match) {
						widget.selectedBomArray.push(selectedBomItem);
					} else {
						var index = widget.selectedBomArray.indexOf(match);
						widget.selectedBomArray.splice(index, 1, selectedBomItem);
					}
					$(event.target).closest(".bom-content").find(".bom-value").text(selectedBomItem.name);
					$(event.target).closest(".bom-row").find(".selected").removeClass("selected");
					$(event.currentTarget).addClass("selected");

					$(event.target).closest(".bom-row").find(".selected").removeClass("selected");
					$(event.currentTarget).addClass("selected");

					var data = [];
					var productId = widget.product().id();
					data[CCConstants.PRODUCT_IDS] = widget.product().id();
					data[CCConstants.SKU_ID] = selectedBomItem.skuid.trim();
					data[CCConstants.CATREF_ID] = selectedBomItem.skuid.trim();

					var preloader = $("<div id='global-preloader'><div class='content'>Processing Request..</div></div>");
					preloader.appendTo('body');

					ccRestClient.request(CCConstants.ENDPOINT_GET_PRODUCT_AVAILABILITY, data, function (data) {
						if (data.stockStatus === "OUT_OF_STOCK") {
							data.orderableQuantity = 0;
						}

						var stockavailable = data.orderableQuantity;
						var availableflag = 1;
						if (stockavailable < widget.itemQty()) {
							$(event.target).closest(".bom-content").find(".error-msg").text(selectedBomItem.name + " is out of stock").show();
							availableflag = 0;

						}
						if (data.orderableQuantity < widget.itemQty()) {
							availableflag = 0;
						}
						data[CCConstants.PRODUCT_IDS] = selectedBomItem.skuid.trim();
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, data, function (products) {
						    if(products && products.length === 0) {
						        if (preloader) {
        							preloader.remove();
        						}
        						var obj = {};
        						obj.slotnum = slotIndex + 1;
        						obj.prodsku = skuid;
        
        						$(event.target).closest(".bom-content").find(".error-msg").text(selectedBomItem.name + " is out of stock").show();
        						availableflag = 0;
        
        						$.Topic('REMOVE_BOM').publish(obj);
        						widget.personalizeVisibilityFlag(isSelectedBomArrayValid(widget));
						        return;
						    }
							if (preloader) {
								preloader.remove();
							}
							if (availableflag === 0) {
								var price = products[0].salePrice || products[0].listPrice;
								var obj = {};
								obj.slotnum = Number(slotIndex) + 1;
								obj.color = "130,20,30";
								obj.skuid = products[0].repositoryId;
								obj.color = products[0].birthstoneColor || "1302030";
								obj.prodsku = skuid;

								$.Topic('REMOVE_BOM').publish(obj);
							} else {
								$(event.target).closest(".bom-content").find(".error-msg").hide();
								if (products[0].salePrice === null) {
									products[0].salePrice = products[0].listPrice;
								}

								var price = products[0].salePrice || products[0].listPrice;
								var obj = {};
								obj.slotnum = slotIndex + 1;
								obj.skuid = products[0].repositoryId;
								obj.color = products[0].birthstoneColor || "1302030";
								obj.price = price;
								obj.name = selectedBomItem.name;
								obj.slot = "Slot " + obj.slotnum;
								obj.prodsku = skuid;
								$.Topic('BOM_URL').publish(obj);
							}
							widget.personalizeVisibilityFlag(isSelectedBomArrayValid(widget));

						});

					}, function (data) {
						if (preloader) {
							preloader.remove();
						}
						var obj = {};
						obj.slotnum = slotIndex + 1;
						obj.prodsku = skuid;

						$(event.target).closest(".bom-content").find(".error-msg").text(selectedBomItem.name + " is out of stock").show();
						availableflag = 0;

						$.Topic('REMOVE_BOM').publish(obj);
						widget.personalizeVisibilityFlag(isSelectedBomArrayValid(widget));

					}, productId);
				};


				/**
				 * Set up the popover and click handler
				 * @param {Object} widget
				 * @param {Object} event
				 */
				widget.shippingSurchargeMouseOver = function (widget, event) {
					// Popover was not being persisted between
					// different loads of the same 'page', so
					// popoverInitialised flag has been removed

					// remove any previous handlers
					$('.shippingSurchargePopover').off('click');
					$('.shippingSurchargePopover').off('keydown');

					var options = new Object();
					options.trigger = 'manual';
					options.html = true;

					// the button is just a visual aid as clicking anywhere will close popover
					options.title = widget.translate('shippingSurchargePopupTitle') +
						"<button id='shippingSurchargePopupCloseBtn' class='close btn pull-right'>" +
						widget.translate('escapeKeyText') +
						" ×</button>";

					options.content = widget.translate('shippingSurchargePopupText');

					$('.shippingSurchargePopover').popover(options);
					$('.shippingSurchargePopover').on('click', widget.shippingSurchargeShowPopover);
					$('.shippingSurchargePopover').on('keydown', widget.shippingSurchargeShowPopover);
				};

				widget.shippingSurchargeShowPopover = function (e) {

					if (e.type === 'keydown' && e.which !== CCConstants.KEY_CODE_ENTER) {
						return;
					}

					// stop event from bubbling to top, i.e. html
					e.stopPropagation();
					$(this).popover('show');

					// toggle the html click handler
					$('html').on('click', widget.shippingSurchargeHidePopover);
					$('html').on('keydown', widget.shippingSurchargeHidePopover);

					$('.shippingSurchargePopover').off('click');
					$('.shippingSurchargePopover').off('keydown');
				};

				widget.shippingSurchargeHidePopover = function (e) {
					// if keydown, rather than click, check its the escape key
					if (e.type === 'keydown' && e.which !== CCConstants.KEY_CODE_ESCAPE) {
						return;
					}

					$('.shippingSurchargePopover').popover('hide');

					$('.shippingSurchargePopover').on('click', widget.shippingSurchargeShowPopover);
					$('.shippingSurchargePopover').on('keydown', widget.shippingSurchargeShowPopover);

					$('html').off('click');
					$('html').off('keydown');

					$('.shippingSurchargePopover').focus();
				};

				$(window).resize(function () {
					// Optimizing the carousel performance, to not reload when only height changes
					var width = $(window)[0].innerWidth || $(window).width();
					if (widget.product().primaryFullImageURL) {
						if (widget.viewportWidth() == width) {
							// Don't reload as the width is same
						} else {
							// Reload the things
							if (widget.product().videoFlag && typeof widget.product().videoFlag == 'function' && widget.product().videoFlag() === true) {
								if (width > 1024) {
									setTimeout(function () {
										widget.loadImageToMain("", "click", 0);
									}, 1000)

								}
							} else {
								if (width > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
									if (widget.viewportWidth() <= CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
										// Optionally reload the image place in case the view port was different
										widget.activeImgIndex(0);
										widget.mainImgUrl(widget.product().primaryFullImageURL);
										$('#prodDetails-imgCarousel').carousel(0);
										$('#carouselLink0').focus();
									}
								} else if (width >= CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) {
									if ((widget.viewportWidth() < CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) || (widget.viewportWidth() > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH)) {
										// Optionally reload the image place in case the view port was different
										widget.activeImgIndex(0);
										widget.mainImgUrl(widget.product().primaryFullImageURL);
										$('#prodDetails-imgCarousel').carousel({
											interval: 1000000000
										});
										$('#prodDetails-imgCarousel').carousel(0);
										$('#carouselLink0').focus();
									}
								} else {
									if (widget.viewportWidth() > CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) {
										// Optionally reload the carousel in case the view port was different
										$('#prodDetails-mobileCarousel').carousel({
											interval: 1000000000
										});
										$('#prodDetails-mobileCarousel').carousel(0);
									}
								}
							}


						}
					}
					widget.viewportWidth(width);
					widget.checkResponsiveFeatures($(window).width());
					widget.selectOccassionForMobile();
				});

				widget.viewportWidth($(window).width());
				if (widget.product()) {

					var productId = widget.product().id(),
						zeroPaddedProdId = "";


					for (var i = 0; i < (9 - productId.length); i++) {
						zeroPaddedProdId += "0";
					}
					zeroPaddedProdId += productId;
					widget.product().primaryFullImageURL(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					widget.product().primaryMediumImageURL(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					var productImageArr = widget.getProductImages("L");

					productImageArr.unshift(_this.scene7BaseUrl + zeroPaddedProdId + '?wid=' + imageRes);

					widget.product().thumbImageURLs(productImageArr);
					widget.imgGroups(widget.groupImages(productImageArr));
					if (widget.product().defaultImageURL && typeof widget.product().defaultImageURL == 'function') {
						widget.mainImgUrl(widget.product().defaultImageURL());
					}

					widget.product().product.productImagesMetadata = [];
					productImageArr.forEach(function () {

						widget.product().product.productImagesMetadata.push({});

					});
				}

				if (true || widget.skipTheContent()) {
					var focusFirstItem = function () {
						$('#region-12colproductDetailsRegion :focusable').first().focus();
						widget.skipTheContent(false);
					};

					focusFirstItem();
					setTimeout(focusFirstItem, 1); // Daft IE fix.
				}


				/* Quntity String - start */
				$(document).on('keydown blur', '.tr-qty-input', function (event) {
					if ((event.which < 48 || event.which > 57) && event.which != 127 && event.which != 8) {
						event.preventDefault();
					}
				});
				$(document).on('blur', '.tr-qty-input', function () {
					if ($(this).val() === '' || $(this).val() === '0' || $(this).val() === '00' || $(this).val() === '000' || $(this).val() === null) {
						$('.tr-qty-input').val(1);
					}
				});
				/* Qunatity String - end */


				$(document).on('click', '.carousel-inner .thumbnail-container', function () {
					$(".bom-preview-image").remove();

					if ($(this).children(".thumbnail").id != "carouselLink1" && widget.product().videoFlag !== true) {
						$(".cc-image-area").find("#image-viewer").find(".cc-viewer-pane").show();
					}
				});
				$(document).on('click', '.tr-w-button.preview-button', function (event) {

					var url = $(".tr-w-button.preview-button").data("url");
					if (url.length) {
						$(".bom-preview-image").remove();

						var img = "<img onError=\"this.src='/img/no-image.jpg'\" class = 'img-responsive' src = '" + url + "' data-bind='productImageSource: {errorSrc:'/img/no-image.jpg', errorAlt:'No Image Found'}>";
						if ($(".cc-image-area").find("#image-viewer").find(".cc-viewer-pane").length) {
							$(".cc-image-area").find("#image-viewer").find(".cc-viewer-pane").hide();
							$(".cc-image-area").find("#image-viewer").append("<div class = 'bom-preview-image'></div>");
							$(".cc-image-area")
								.find("#image-viewer")
								.find(".bom-preview-image")
								.html(img);
						} else {

							$(".cc-image-area").find("#image-viewer").find(".carousel-inner").hide();
							$(".cc-image-area").find("#image-viewer").find(".row-fluid").hide();
							$(".cc-image-area").find("#image-viewer").find("#prodDetails-mobileCarousel").append("<div class = 'bom-preview-image'></div>");

							$(".cc-image-area")
								.find("#image-viewer")
								.find(".bom-preview-image")
								.html(img);

							$(".cc-image-area")
								.find("#image-viewer")
								.find(".bom-preview-image")
								.prepend("<button type='button' class='close' data-dismiss='modal' aria-label='Close' id='close-button'><img id='close-image' src='/file/general/close_symbol.png' alt='Close'></button>");

						}
					}
				});

				$(document).on('click', '.bom-preview-image .close', function (event) {
					$(".cc-image-area")
						.find("#image-viewer")
						.find(".bom-preview-image")
						.hide();
					$(".cc-image-area").find("#image-viewer").find(".carousel-inner").show();
					$(".cc-image-area").find("#image-viewer").find(".row-fluid").show();
				});


				/* Quantity Increase Decrease Start*/

				$(document).on("click", ".tr-plus", function () {
					var $trQty = $('.tr-qty-input'),
						qtyVal = $trQty.val();
					if (qtyVal < 999) {
						var newQty = Number(qtyVal) + 1;

						$trQty.val(newQty).change();

						var variantOptions = widget.variantOptionsArray(),
							sku_obj = widget.getSelectedSku(variantOptions);

						if (widget.product().childSKUs().length == 1) {
							sku_obj = widget.product().childSKUs()[0];
						}


					}
				});
				$(document).on("click", ".tr-minus", function () {
					var $trQty = $('.tr-qty-input'),
						qtyVal = $trQty.val();
					if (qtyVal > 1) {
						var newQty = Number(qtyVal) - 1;
						$trQty.val(newQty).change();

						var variantOptions = widget.variantOptionsArray(),
							sku_obj = widget.getSelectedSku(variantOptions);
						if (widget.product().childSKUs().length == 1) {
							sku_obj = widget.product().childSKUs()[0];
						}

					}
				});

				$(document).on("change", ".tr-qty-input", function () {
					var $trQty = $('.tr-qty-input'),
						qtyVal = $trQty.val();
					if (qtyVal > 1) {
						var newQty = Number(qtyVal);


						var variantOptions = widget.variantOptionsArray(),
							sku_obj = widget.getSelectedSku(variantOptions);
						if (widget.product().childSKUs().length == 1) {
							sku_obj = widget.product().childSKUs()[0];
						}

						$.Topic('SKU_SELECTED').publish(widget.product(), sku_obj, variantOptions, widget.itemQty());
					}
				});
				/*$(document).on("change", ".quantity-dropdown", function () {
				    var $trQty = $('.quantity-dropdown'),
				        qtyVal = $trQty.val();
				    $("#inventory-error").text("");
				    var newQty = Number(qtyVal);


				    var variantOptions = widget.variantOptionsArray(),
				        sku_obj = widget.getSelectedSku(variantOptions);
				    if (widget.product().childSKUs().length == 1) {
				        sku_obj = widget.product().childSKUs()[0];
				    }

				    $.Topic('SKU_SELECTED').publish(widget.product(), sku_obj, variantOptions, widget.itemQty());
				    //}
				});*/
				widget.quantityChange = function () {
					$(".tr-store-stock-error").hide().text("");
					var $trQty = $('.quantity-dropdown'),
						qtyVal = $trQty.val();
					$("#inventory-error").text("");
					$(".error-msg").text("");
					var newQty = Number(qtyVal);


					var variantOptions = widget.variantOptionsArray(),
						sku_obj = widget.getSelectedSku(variantOptions);
					if (widget.product().childSKUs().length == 1) {
						sku_obj = widget.product().childSKUs()[0];
					}
					$.Topic('SKU_SELECTED_IR').publish(widget.product(), sku_obj, variantOptions, widget.itemQty());

					var shippingType = $("[name=optradio]:checked").val();
					var storeNumber = "920";
					if (shippingType == 1) { //pickup
						$.Topic("PROCESS_INVENTORY_PDP_IR").subscribe(function (data) {
							storeNumber = data.pickup.length > 0 && data.pickup[0].available ? data.pickup[0].store.storeNumber : null;
						});
					}
					//helpers.storeLocatorUtils().getInventoryResponseStoreIR(widget.itemQty(), storeNumber, 4, widget.skuRepoId(),"quantityChange");

					var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();

					quantityChangeDeliveryTriggered = false;

					var skuId = widget.skuRepoId();
					var skuQuantity = widget.itemQty();
					var latitude = $('.my-store-lat').text();
					var longitude = $('.my-store-lon').text();
					var storeNumber = $('.my-store-id').text();
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": skuQuantity,
						"radiusLimit": 400,
						"limit": 4
					};
					var stores = helpers.storeLocatorUtils().getStores(latitude, longitude, storeItems);
					var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);
					var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();


					helpers.storeLocatorUtils().getInventoryResponseIR(stores, skuQuantity, storeNumber, 4, skuId, storeIds, reqData, "pdpLoad", cartInventoryDetailsArrayObj);

				};
				widget.itemQty.subscribe(function () {
					$.Topic('QTY_UPDATE').publish(widget.itemQty());
					var variantOptions = widget.variantOptionsArray(),
						sku_obj = widget.getSelectedSku(variantOptions);

					if (widget.product().childSKUs().length == 1) {
						sku_obj = widget.product().childSKUs()[0];
					}


				});

				$.Topic('AVAILABLE_TODAY').subscribe(function (avl_flag) {
					widget.availableToday(avl_flag);
				});

				$.Topic('AVAILABLE_STORE_NAME').subscribe(function (storeName) {
					widget.availableTodayStore(storeName);

				});
				/* Quantity Increase Decrease End*/

				/* sjoseph - store available today -start */
				function checkProdAvailabilty(qty) {
					var request_obj = {};
					request_obj.skuId = "347943";
					request_obj.storeNumber = 110;
					request_obj.quantity = qty;
					var request_json = JSON.stringify(request_obj);

				}

				$(document).on('click', '.set-Store-link', function (e) {
					var storeNum = $(this).attr("store_id")
					var lat = $(this).attr("latitude");
					var lon = $(this).attr("longitude");
					$.Topic("PICKUP-STORE-CHANGED").publish(storeNum, lat, lon);
					$("#tr-e-available-store").modal("hide");
					quantityChangeDeliveryTriggered = true;
					displayStoreChangeMessage = false;
					$('#available-today').click();
					$("body").removeClass('modal-open');
					$('body').css('overflow', 'unset');
					$('body').css('position', 'unset');
					
				});
				
				$(document).on('click', '.tr-w-occasion li', function () {
					var staticVal = $(this).text();
					$('.tr-w-occasion .btn .text').text(staticVal);
					$('.tr-w-occasion .btn .index').text($(this).attr("index"));
					// $('.tr-w-occasion .occasion-dropdown').toggle();

					if ($('.tr-w-occasion .btn .index').text() != "0") {
						$(".tr-w-occasion").find(".error-msg").hide();
					}
				});


				if (widget.product().virtual && typeof widget.product().virtual == 'function') {
					if (widget.product().virtual()) {
						widget.virtual(true);
					} else {
						widget.virtual(false);
					}
				}

				//Omni Channel
				$.Topic('SKU_OBJ1').subscribe(function (product) { //Pub Sub for sku/qty changed.
					$.Topic("CHECK_PULSE").publish(product.tr_pulse);
					widget.product().attributeFeatures(product.attributeFeatures);
					widget.product().longDescription(product.longDescription);
					widget.prodTitleNew(product.displayName);
					if (!product)
						return;
					var productData = product;
					var latitude = widget.pageStoreLat() || $('.my-store-lat').text();
					var longitude = widget.pageStoreLon() || $('.my-store-lon').text();
					var storeNumber = widget.pageStore() || $('.my-store-id').text();
					var opusProductFlag = productData.OPUS || '';
					// TRV - 2162 - S
					_this.setProductData(productData);
					// TRV - 2162 - E
					widget.skuData(product);
					widget.skuOpus(opusProductFlag);

					if (typeof (productData.STS) == "undefined" || productData.STS == null) {
						stsFlag = false;
					} else {
						stsFlag = productData.STS;
					}
					$("#skuSTSFlag").val(stsFlag);
					widget.skuSts(stsFlag);
					widget.skuRepoId(productData.repositoryId);
					//TRV - 1917 - S
					if (product.photoUploadRequired) {
						widget.isPhotoUpload(true);
					} else {
						widget.isPhotoUpload(false);
					}
					if ($('#available-today').length > 0 && $('#available-today').is(':checked')) {
						$('#available-today').trigger('change', true);
					}
					//TRV - 1917 - E
					// var stsProductFlag = productData.STS;
					var skuId = productData.repositoryId;
					var qty = widget.itemQty();
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": qty,
						"radiusLimit": 400,
						"limit": 4
					};
					setShipping(reqData, opusProductFlag, stsFlag);
				});
				/*$.Topic('STOREID').subscribe(function (storeid, lat, long) {//Pub Sub for store changed.
				    var productData = widget.skuData();
				    var latitude = lat;
				    var longitude = long;
				    var storeNumber = storeid;
				    var opusProductFlag = widget.skuOpus();
				    var stsProductFlag = widget.skuSts();
				    var skuId = widget.skuRepoId();
				    var qty = widget.itemQty();
				    widget.pageStoreLat(latitude);
				    widget.pageStoreLon(longitude);
				    widget.pageStore(storeid);
				    widget.storeCode(storeid);
				    var reqData = {
				        "skuId": skuId,
				        "storeNumber": storeNumber,
				        "location": {"latitude": latitude, "longitude": longitude},
				        "quantity": qty,
				        "radiusLimit": 400,
				        "limit": 4
				    };
				    setShipping(reqData, opusProductFlag, stsProductFlag);

				    $.Topic("displayOpus").subscribe(inventoryCheck);


				    function inventoryCheck() {

				        widget.availableTodayVisibility(widget.displayOpus());
				        $.Topic("displayOpus").unsubscribe(inventoryCheck);

				    }
				});*/
				$.Topic('PICKUP-STORE-CHANGED').subscribe(function (storeid, lat, long) { //Sub for Pickup Store Changed
					var productData = widget.skuData();
					var latitude = lat;
					var longitude = long;
					var storeNumber = storeid;
					var opusProductFlag = widget.skuOpus();
					var stsProductFlag = widget.skuSts();
					var skuId = widget.skuRepoId();
					var qty = widget.itemQty();
					widget.pageStoreLat(lat);
					widget.pageStoreLon(long);
					widget.pageStore(storeid);
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": qty,
						"radiusLimit": 400,
						"limit": 4
					};
					setShipping(reqData, opusProductFlag, stsProductFlag);
				});
				$.Topic('PERSONALIZATION-STORE-MODAL').subscribe(function () { //Sub for store modal in personalization page
					var productData = widget.skuData();
					var latitude = widget.pageStoreLat();
					var longitude = widget.pageStoreLon();
					var storeNumber = widget.pageStore();
					var opusProductFlag = widget.skuOpus();
					var stsProductFlag = widget.skuSts();
					var skuId = widget.skuRepoId();
					var qty = widget.itemQty();
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": qty,
						"radiusLimit": 400,
						"limit": 4
					};
					setShipping(reqData, opusProductFlag, stsProductFlag);
				});


				function func2() {
					return widget.stockAvailable();
				}


				function processInventory(reqData, stores, quantity, storeNumber, locationInventoryInfo) {
					var sthSelected = $('#ship-to-home').prop('checked');

					widget.prodAvlFlag(1);
					var responseData = helpers.storeLocatorUtils().getInventoryResponse(stores, locationInventoryInfo, quantity, storeNumber, 4);
					var data_obj = reqData;
					var data2 = func2();


					if (!data_obj.skuId) {
						return;
					}

					var data = responseData,
						opusProductFlag = data_obj.opusProductFlag,
						stsProductFlag = data_obj.stsProductFlag,
						skuid = data_obj.skuId;

					var selectedStore = data.selectedStore,
						displaySTSflag, store_obj, curr_store_obj,
						my_store_arr = [],
						other_store_arr = [];

					if (data.dcInventory === 0 && data.storePickupEligibleQty === false) {
						if (data_obj.quantity > 1) {
							$(".tr-e-delivery").find(".error-msg").show().text("The requested quantity is not available for purchase.")
						} else {
							$(".tr-e-delivery").find(".error-msg").show().text("This product is no longer available for Purchase.")
						}
						$("#inventory-error").text("");
						widget.displayOpus(false);
						widget.displaySts(false);
						widget.displayShipToHome(false);
						widget.prodAvlFlag(0);
						widget.personalizeVisibilityFlag(false);
						return;
					}

					widget.sfsStoreId(data.sfsStoreId);
					widget.isSFS(data.isSFS);

					// var dc_qty = data2[0].productSkuInventoryStatus[Number(skuid)];
					// var dc_qty = data2;
					var dc_qty = 0;
					widget.DCInventory(data.dcInventory);
					widget.maxQuantityStore(data.maxQuantityStore);
					/*if(data.dcInventory < data.maxQuantityInStore){
					    dc_qty = data.maxQuantityInStore;
					}
					else{

					}*/

					dc_qty = data.dcInventory;
					widget.availableSTHInventory(dc_qty);


					for (var i = 0; i < data.otherStores.length; i++) {
						store_obj = {};
						curr_store_obj = data.otherStores[i];
						store_obj.storeNumber = curr_store_obj.storeNumber || "";
						store_obj.threshold = curr_store_obj.threshold || 0;
						store_obj.storeName = curr_store_obj.storeName || "";
						store_obj.storeHours = curr_store_obj.storeHours || "";
						store_obj.shipToStoreAvailable = curr_store_obj.shipToStoreAvailable || false;
						store_obj.shipToActive = curr_store_obj.shipToActive || "N";
						store_obj.isAvailableToday = curr_store_obj.isAvailableToday || 0;
						store_obj.distanceInMiles = curr_store_obj.distanceInMiles || 0;
						store_obj.deliveryDate = curr_store_obj.deliveryDate || "Today";
						store_obj.closeTime = curr_store_obj.closeTime || 0;
						store_obj.isAvailableForPickup = curr_store_obj.isAvailableForPickup || 0;
						store_obj.onHandQuantity = curr_store_obj.onHandQuantity || 0;
						store_obj.address = curr_store_obj.addressLine1 + " " + curr_store_obj.addressLine2;
						store_obj.address1 = curr_store_obj.addressLine1;
						store_obj.address2 = curr_store_obj.addressLine2;
						store_obj.formattedDeliveryDate = curr_store_obj.formattedDeliveryDate;
						store_obj.phone = curr_store_obj.phone;
						store_obj.openTime = curr_store_obj.openTime ? curr_store_obj.openTime + " - " + store_obj.closeTime : 0;
						store_obj.longitude = curr_store_obj.longitude;
						store_obj.latitude = curr_store_obj.latitude;
						store_obj.zipCode = curr_store_obj.zipCode;
						store_obj.city = curr_store_obj.city;
						store_obj.state = curr_store_obj.state;

						other_store_arr.push(store_obj);
					}

					widget.otherStores(other_store_arr);
					var my_store_obj = {};

					if (selectedStore) {
						my_store_obj.storeNumber = selectedStore.storeNumber || "";
						my_store_obj.threshold = selectedStore.threshold || 0;
						my_store_obj.storeName = selectedStore.storeName || "";
						my_store_obj.storeHours = selectedStore.storeHours || "";
						my_store_obj.shipToStoreAvailable = selectedStore.shipToStoreAvailable || false;
						my_store_obj.shipToActive = selectedStore.shipToActive || "N";
						my_store_obj.isAvailableToday = selectedStore.isAvailableToday || 0;
						my_store_obj.distanceInMiles = selectedStore.distanceInMiles || 0;
						my_store_obj.deliveryDate = selectedStore.deliveryDate || "Today";
						my_store_obj.closeTime = selectedStore.closeTime || 0;
						my_store_obj.isAvailableForPickup = selectedStore.isAvailableForPickup || 0;
						my_store_obj.onHandQuantity = selectedStore.onHandQuantity || 0;
						my_store_obj.address = selectedStore.addressLine1 + " " + selectedStore.addressLine2;
						my_store_obj.address1 = selectedStore.addressLine1;
						my_store_obj.address2 = selectedStore.addressLine2;
						my_store_obj.formattedDeliveryDate = selectedStore.formattedDeliveryDate;
						my_store_obj.phone = selectedStore.phone;
						my_store_obj.openTime = selectedStore.openTime ? selectedStore.openTime + " - " + my_store_obj.closeTime : 0;
						my_store_obj.longitude = selectedStore.longitude;
						my_store_obj.latitude = selectedStore.latitude;
						my_store_obj.zipCode = selectedStore.zipCode;
						my_store_obj.city = selectedStore.city;
						my_store_obj.state = selectedStore.state;


						my_store_arr.push(my_store_obj);
						widget.myStore(my_store_arr);
					}

					widget.displayOpus(false);

					widget.displayShipToHome(false);
					widget.dcQuantity(dc_qty);
					if (widget.availableSTHInventory() >= widget.itemQty()) {
						// display ship to home
						widget.displayShipToHome(true);
						widget.backOrderFlag(false);
						$("#ship-to-home").attr("checked", true);
					}
					$(".tr-pickup-availability").hide();
					/////////// New OPUS-Logic
					//if(opusProductFlag) {
					if (selectedStore && selectedStore.isAvailableForPickup) {
						widget.availableTodayStore(selectedStore.storeName);
						widget.availableState(selectedStore.state);
						widget.availableCity(selectedStore.city);
						widget.availableQuantity(selectedStore.onHandQuantity);
						widget.displayOpus(true);
						widget.displaySts(false);
						widget.availableTodayVisibility(true);
						if (selectedStore.isAvailableToday) {
							// widget.availableDateText("Pick Up " + selectedStore.deliveryDate);
							$(".tr-pickup-availability").show();
						} else {
							widget.availableTodayStore(selectedStore.storeName);
							widget.availableState(selectedStore.state);
							widget.availableCity(selectedStore.city);

							widget.availableQuantity(selectedStore.onHandQuantity);
							// widget.availableDateText("Pick Up " + selectedStore.deliveryDate);
						}
						if (widget.displayShipToHome() === false) {
							$("#available-today").prop("checked", true);
						}
					} else if (selectedStore && selectedStore.shipToStoreAvailable) {

						if (productSts) {
							widget.displayOpus(false);
							widget.displaySts(true);
							widget.availableTodayVisibility(false);
							widget.stsStore(selectedStore.storeName);
							widget.stsAvailableText("Available for store pickup " + selectedStore.storeHours[5].date);
							if (widget.displayShipToHome() === false) {
								$("#available-sts").prop("checked", true);
							}
						}
					} else {
						if (data.storePickupEligibleQty === true && selectedStore) {
							widget.displayOpus(true);
						} else {
							widget.displayOpus(false);
						}
						widget.displaySts(false);
					}

					if (widget.displayOpus() === false &&
						widget.displaySts() === false &&
						widget.displayShipToHome() === false) {

						if (widget.finalBackOrderFlag()) {
							widget.displayShipToHome(true);
							widget.handleBackOrderDate();
						} else {
							$(".tr-e-delivery").find(".error-msg").show().text("This product is no longer available for Purchase.")
							$("#inventory-error").text("");
							widget.prodAvlFlag(0);
							widget.personalizeVisibilityFlag(false);
						}
					}

					$.Topic("displayOpus").publish(widget.displayOpus());
					$.Topic("displaySts").publish(widget.displaySts());
					$.Topic("displayShipToHome").publish(widget.displayShipToHome());

					$("[id^=personalize-button]").prop("disabled", false);
					$("#inventory-error").text("");
					if ($("#available-today").prop("checked")) {
						if (!widget.myStore()[0].isAvailableForPickup && widget.displaySts()) {
							widget.availableTodayVisibility(false);
						}
						if (!widget.myStore()[0].isAvailableForPickup && !widget.displaySts()) {
							$("#inventory-error").text("Sorry, only “" + widget.myStore()[0].onHandQuantity + "” available. Please reselect quantity to proceed")
						} else {
							$("#inventory-error").text("");
						}
					} else if ($("#available-sts").prop("checked")) {
						if (widget.myStore()[0].onHandQuantity < widget.itemQty()) {
							$("#inventory-error").text("Sorry, only “" + widget.myStore()[0].onHandQuantity + "” available. Please reselect quantity to proceed")
						} else {
							$("#inventory-error").text("");

						}

					} else if (sthSelected) {
						if (widget.itemQty() > widget.availableSTHInventory() && widget.availableSTHInventory() > 0) {
							widget.displayShipToHome(true);
							widget.personalizeVisibilityFlag(true);

							$("[id^=personalize-button]").prop("disabled", true);
							$('#ship-to-home').click();
							$("#inventory-error").text("Sorry, the selected quantity is not available .Please reselect the quantity to proceed.")
							$(".tr-e-delivery").find(".error-msg").text('');
						} else {
							$("#inventory-error").text("");
							$("[id^=personalize-button]").prop("disabled", false);

						}

					}
					widget.destroyInventorySpinner();


				}

				function setShipping(reqData, opusProductFlag, stsProductFlag) {
					var validationDeferred = $.Deferred();
					var sthSelected = $('#ship-to-home').prop('checked');
					// widget.personalizeVisibilityFlag(true);
					$(".tr-e-delivery").find(".error-msg").hide();

					//TRV 2162 - S
					var inventoryQuantity = 0;
					if (widget.variantOptionsArray().length < 1) {
						if (widget.getProductData() && widget.getProductData().childSKUs.length == 1) {
							inventoryQuantity = widget.getProductData().childSKUs[0].quantity || 0;
						}
					} else {
						if (widget.getSelectedSKU()) {
							inventoryQuantity = widget.getSelectedSKU().quantity || 0;
						}
					}
					$(".tr-e-delivery").find(".error-msg").hide();
					reqData.inventoryQuantity = func2();
					var productSts = stsProductFlag,
						cartQuantity = 0;

					reqData.quantity = Number(reqData.quantity);
					reqData.stsProductFlag = stsProductFlag;
					reqData.opusProductFlag = opusProductFlag;

					if (widget.skuRepoId()) {
						widget.createInventorySpinner();
						$("#inventory-error").text("");
						$("[id^=personalize-button]").prop("disabled", true);


						var skuId = reqData.skuId;
						var storeNumber = reqData.storeNumber;
						var quantity = reqData.quantity;
						var radiusLimit = reqData.radiusLimit;
						var limit = reqData.limit;
						var latitude = reqData.location.latitude + "";
						var longitude = reqData.location.longitude + "";
						if (latitude.trim() === "" && longitude.trim() === "") {
							if (storeNumber.trim() != "") {
								var store = helpers.storeLocatorUtils().getStore(storeNumber, storeItems);
								if (store) {
									latitude = store.latitude;
									longitude = store.longitude;
								} else {
									latitude = 40.003601; // worst case set the default store lat long
									longitude = -82.931224;
								}
							} else {
								latitude = 40.003601; // worst case set the default store lat long
								longitude = -82.931224;
							}
						}
						var stores = helpers.storeLocatorUtils().getStores(latitude, longitude, storeItems);

						var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);

						/*if (inventoryObject.hasOwnProperty(skuId + "-" + storeIds)) {
						    setTimeout(function () {
						        processInventory(reqData, stores, quantity, storeNumber, inventoryObject[skuId + "-" + storeIds]);
						    }, 500);
						} else {
						    ccRestClient.authenticatedRequest("/ccstoreui/v1/inventories/" + skuId + "?locationIds=" + storeIds + "&includeDefaultLocationInventory=true", {}, function (data) {
						        inventoryObject[skuId + "-" + storeIds] = data.locationInventoryInfo;
						        //storageApi.getInstance().setItem("inventoryData", JSON.stringify(inventoryObject));
						        processInventory(reqData, stores, quantity, storeNumber, data.locationInventoryInfo);
						    }, function (data) {
						    }, "GET");
						}*/
						var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();
						helpers.storeLocatorUtils().getInventoryResponseIR(stores, quantity, storeNumber, 4, skuId, storeIds, reqData, "pdpLoad", cartInventoryDetailsArrayObj);
						

						widget.displayOpus.subscribe(function (data) {
							// This is causing some issues in the inventory check.

							if (widget.displaySts()) {
								widget.availableTodayVisibility(false);
							}

						});
						widget.product.subscribe(function (data) {
							widget.skuData(data);
						});
						$(".tr-other-store.occ-inventory-zero").hide();
						$(".tr-other-store.my-store").show();
						var initData = widget.skuData();
						if (!initData) {
							return;
						}
						if (initData.ItemType !== undefined) {
							var initType = initData.ItemType;
						} else if (initData.itemType === undefined) {
							// var initType = initData.itemType();
						}

						if (initType === 'p' || initType === 'P') {
							var initZone, initPhotoZone, initGift, initGiftId;

							if (typeof initData.ProductZones === 'function') {
								initZone = initData.ProductZones();
							} else {
								initZone = initData.ProductZones
							}

							if (typeof initData.photoUpload === 'function') {
								initPhotoZone = initData.photoUpload();
							} else {
								initPhotoZone = initData.photoUpload
							}

							if (typeof initData.hasGift === 'function') {
								initGift = initData.hasGift();
							} else {
								initGift = initData.hasGift
							}
							if (typeof initData.giftSkuId === 'function') {
								initGiftId = initData.giftSkuId();
							} else {
								initGiftId = initData.giftSkuId
							}
							/*if (!initZone) {
								widget.buttonVisibilityFlag(true); //Display add to cart button
								widget.personalizeVisibilityFlag(false);
							} else {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}
							if (initPhotoZone === true || initPhotoZone === false) {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}
							if (initGift && initGiftId) {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}*/

							//End Code to toggle Add to Cart and Personalize
						} else if (initType === 'v' || initType === 'V') {
							var initZone = initData.ProductZones || initData.productZones;
							if (typeof initZone === "function") {
								initZone = initZone();
							}
							if (typeof initPhotoZone === "function") {
								initPhotoZone = initPhotoZone();
							}
							var initPhotoZone = initData.photoUpload || initData.PhotoUpload;
							/*if ((initZone === null) || (typeof initZone === "undefined")) {
								widget.buttonVisibilityFlag(true); //Display add to cart button
								widget.personalizeVisibilityFlag(false);
							} else {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}
							if (initPhotoZone === true || initPhotoZone === false) {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}*/

							var initGift, initGiftId;

							if (typeof initData.hasGift === 'function') {
								initGift = initData.hasGift();
							} else {
								initGift = initData.hasGift
							}
							if (typeof initData.giftSkuId === 'function') {
								initGiftId = initData.giftSkuId();
							} else {
								initGiftId = initData.giftSkuId
							}

							//if (initData.hasGift() && initData.giftSkuId()) {
							/*if (initGift && initGiftId) {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}*/
						}
						widget.destroyInventorySpinner();

					}

					widget.validationDeferred = validationDeferred;
					return validationDeferred;
				}

				$.Topic('UPDATE_OTHER_STORES_MODAL').subscribe(function (data) {
					var selectedStore = data.selectedStore,
						my_store_arr = [],
						other_store_arr = [];

					for (var i = 0; i < data.otherStores.length; i++) {
						store_obj = {};
						curr_store_obj = data.otherStores[i];
						store_obj.storeNumber = curr_store_obj.storeNumber || "";
						store_obj.threshold = curr_store_obj.threshold || 0;
						store_obj.storeName = curr_store_obj.storeName || "";
						store_obj.storeHours = curr_store_obj.storeHours || "";
						store_obj.shipToStoreAvailable = curr_store_obj.shipToStoreAvailable || false;
						store_obj.shipToActive = curr_store_obj.shipToActive || "N";
						store_obj.isAvailableToday = curr_store_obj.isAvailableToday || 0;
						store_obj.distanceInMiles = curr_store_obj.distanceInMiles || 0;
						store_obj.deliveryDate = curr_store_obj.deliveryDate || "Today";
						store_obj.closeTime = curr_store_obj.closeTime || 0;
						store_obj.isAvailableForPickup = curr_store_obj.isAvailableForPickup || 0;
						store_obj.onHandQuantity = curr_store_obj.onHandQuantity || 0;
						store_obj.address = curr_store_obj.addressLine1 + " " + curr_store_obj.addressLine2
						store_obj.phone = curr_store_obj.phone;
						store_obj.openTime = curr_store_obj.openTime ? curr_store_obj.openTime + " - " + store_obj.closeTime : 0;
						store_obj.longitude = curr_store_obj.longitude;
						store_obj.latitude = curr_store_obj.latitude;
						store_obj.zipCode = curr_store_obj.zipCode;
						store_obj.city = curr_store_obj.city;
						store_obj.state = curr_store_obj.state;

						other_store_arr.push(store_obj);
					}

					widget.otherStores(other_store_arr);
					var my_store_obj = {};

					my_store_obj.storeNumber = selectedStore.storeNumber || "";
					my_store_obj.threshold = selectedStore.threshold || 0;
					my_store_obj.storeName = selectedStore.storeName || "";
					my_store_obj.storeHours = selectedStore.storeHours || "";
					my_store_obj.shipToStoreAvailable = selectedStore.shipToStoreAvailable || false;
					my_store_obj.shipToActive = selectedStore.shipToActive || "N";
					my_store_obj.isAvailableToday = selectedStore.isAvailableToday || 0;
					my_store_obj.distanceInMiles = selectedStore.distanceInMiles || 0;
					my_store_obj.deliveryDate = selectedStore.deliveryDate || "Today";
					my_store_obj.closeTime = selectedStore.closeTime || 0;
					my_store_obj.isAvailableForPickup = selectedStore.isAvailableForPickup || 0;
					my_store_obj.onHandQuantity = selectedStore.onHandQuantity || 0;
					my_store_obj.address = selectedStore.addressLine1 + " " + selectedStore.addressLine2;
					my_store_obj.address1 = selectedStore.addressLine1;
					my_store_obj.address2 = selectedStore.addressLine2;
					my_store_obj.formattedDeliveryDate = selectedStore.formattedDeliveryDate;
					my_store_obj.phone = selectedStore.phone;
					my_store_obj.openTime = selectedStore.openTime ? selectedStore.openTime + " - " + my_store_obj.closeTime : 0;
					my_store_obj.longitude = selectedStore.longitude;
					my_store_obj.latitude = selectedStore.latitude;
					my_store_obj.zipCode = selectedStore.zipCode;
					my_store_obj.city = selectedStore.city;
					my_store_obj.state = selectedStore.state;

					my_store_arr.push(my_store_obj);
					widget.myStore(my_store_arr);
					$('#tr-e-available-store').modal(); //Display Modal to change store
				});


				//End Omni Channel

				$.Topic("tr-personalized-item-to-bag").subscribe(widget.handlePersonalizedPdtAddToCart.bind(widget));
				//TRV - 2162 - S
				$.Topic('SKU_SELECTED').subscribe(function (product, selectedSKUObj, variantOptions, qnty) {
					widget.setSelectedSKU(selectedSKUObj);
					widget.setSelectedSKUQty(qnty);
				});
				//Code to change between addtocart and personalize button
				$.Topic('SKU_OBJ1').subscribe(function (data) {/*
					publishedSkuData = data;
					var publishedZone = publishedSkuData.ProductZones || publishedSkuData.productZones;
					var initPhotoZone = publishedSkuData.photoUpload;
					var productZonesIds = {};
					if (publishedZone) {
						productZonesIds[CCConstants.PRODUCT_IDS] = publishedZone;
						var productZonesData = storageApi.getInstance().getItem('productZones' + publishedSkuData.id);
						if (refreshData || !productZonesData) {
							ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, productZonesIds, function (productZonesArray) {
								storageApi.getInstance().setItem('productZones' + publishedSkuData.id, JSON.stringify(productZonesArray));
								var zoneIds = '';
								for (i = 0; i < productZonesArray.length; i++) {
									if (zoneIds.indexOf(productZonesArray[i].zone) === -1) {
										if (i === 0) {
											zoneIds = productZonesArray[i].zone;
										} else {
											zoneIds = zoneIds + ',' + productZonesArray[i].zone;
										}
									}
								}
								var zone = {};
								if (zoneIds !== '') {
									zone[CCConstants.PRODUCT_IDS] = zoneIds;
									ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, zone, function (zoneData) {
										storageApi.getInstance().setItem('zones' + publishedSkuData.id, JSON.stringify(zoneData));
									});
								}
							});
						}
					}
					//If no product zone data is available, hide personalize and display add to cart.
					if (!publishedZone) {
						widget.buttonVisibilityFlag(true); //Display add to cart button
						widget.personalizeVisibilityFlag(false);
						widget.checkPersonalization(false);
					} else {
						widget.buttonVisibilityFlag(false); //Display personalize button
						widget.personalizeVisibilityFlag(true);
						widget.checkPersonalization(true);
					}
					if (initPhotoZone === true) {
						widget.buttonVisibilityFlag(false); //Display personalize button
						widget.personalizeVisibilityFlag(true);
						widget.checkPersonalization(true);
					}
					if (publishedSkuData.hasGift && publishedSkuData.giftSkuId) {
						widget.buttonVisibilityFlag(false); //Display personalize button
						widget.personalizeVisibilityFlag(true);
						widget.checkPersonalization(true);
					}
				*/});
				//End Code to change between addtocart and personalize button

				$.Topic('SKU_OBJ1').subscribe(function (product_obj) {/*

					publishedSkuData = product_obj;
					widget.skuBackorderFlag(publishedSkuData.backorder);
					//TROCC 3008 Start
					if (widget.skuBackorderFlag() && (widget.stockAvailable() === 0 || widget.itemQty() > widget.stockAvailable())) {
						if ((publishedSkuData.AIRBackorderDeliveryDates ||
								publishedSkuData.BWYBackorderDeliveryDates ||
								publishedSkuData.PRMBackorderDeliveryDates ||
								publishedSkuData.two2NDBackorderDeliveryDates)) {

							var id = product_obj.id;
							ccRestClient.request("getInventory", null, inventoryDetails, inventoryDetailsError, id);

							function inventoryDetails(data) {
								if (data.backorderLevel > 0 && widget.itemQty() <= data.backorderLevel) {
									widget.finalBackOrderFlag(true);
								} else {
									widget.finalBackOrderFlag(false);
								}
							}

							function inventoryDetailsError(data) {

							}

						} else {
							widget.finalBackOrderFlag(false);
						}
					} else {
						widget.finalBackOrderFlag(false);
					}
					//TROCC 3008 End
					// the below code works fine in preview mode but in live mode sees some
					// parallel execution with stock status. so having a fraction of seconds on set time out delay
					setTimeout(function () {
						skuInventory = widget.stockAvailable();
						if ((publishedSkuData.backorder && (widget.stockAvailable() === 0 || widget.itemQty() > widget.stockAvailable()))) {
							// widget.handleBackOrderDate();
						} else {
							widget.backOrderFlag(false);
							widget.deliveryDateHeadingFlag(true);
							var fullDate = product_obj.BWYDeliveryDates || '';
							if (fullDate !== '' && fullDate !== null) {
								var index = fullDate.indexOf('-');
								var newDate = fullDate.substring(index + 1);
								newDate = newDate.replace('-', '/');
								newDate = "Estimated Arrival " + newDate;
								widget.standardDeliveryDate(newDate);
							}
							if ($(".tr-e-product-description #descr-container").height() + 10 < $(".tr-e-product-description #descr-container .tr-prod-descr-string").height()) {
								$(".tr-prod-descr-show-more").show();
							} else {
								$(".tr-prod-descr-show-more").hide();
							}

							if (typeof product_obj.BWYDeliveryDates !== "undefined" && product_obj.BWYDeliveryDates) {
								widget.bwyFlag(true);
								widget.bwyFlagDate(product_obj.BWYDeliveryDates);
							} else {
								widget.bwyFlag(false);
							}
							if (typeof product_obj.two2NDDeliveryDates !== "undefined" && product_obj.two2NDDeliveryDates) {
								widget.two_ndFlag(true);
								widget.two_ndFlagDate(product_obj.two2NDDeliveryDates);
							} else {
								widget.two_ndFlag(false);
							}
							if (typeof product_obj.AIRDeliveryDates !== "undefined" && product_obj.AIRDeliveryDates) {
								widget.airFlag(true);
								widget.airFlagDate((product_obj.AIRDeliveryDates));
							} else {
								widget.airFlag(false);
							}

							if (typeof product_obj.PRMDeliveryDates !== "undefined" && product_obj.PRMDeliveryDates) {
								widget.prmFlag(true);
								widget.prmFlagDate(getPRMDate(product_obj.PRMDeliveryDates));
							} else {
								widget.prmFlag(false);
							}

						}
					}, 300); // set timeout ends
				*/});

				//Update Button
				$.Topic("SKU_OBJ1").subscribe(function (cartData) {/*
					if (widget.cart().items !== undefined) {
						var cartLength = widget.cart().items().length;
						var cartFlag = false;
						if (cartData.id !== undefined) {
							for (var i = 0; i < cartLength; i++) {
								if (widget.cart().items()[i].catRefId == cartData.id) {
									cartFlag = true;
									break;
								}
							}

							if (cartFlag) {
								$("[id^=addToCart-button]").text("Update");
							} else {
								$("[id^=addToCart-button]").text("Add To Bag");
							}
						}
						cartFlag = false;
					}
				*/});
				//End Update Button
				//Product Images
				$.Topic("SKU_OBJ1").subscribe(function (prod) {
					widget.activeImgIndex(0);
					var prodImages = widget.getProductImages("L", prod),
						product = widget.product();
					zeroPaddedProdId = widget.getZeroPaddedSkuId(prod.repositoryId);

					prodImages.unshift(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					product.primaryFullImageURL(_this.scene7BaseUrl + zeroPaddedProdId);
					product.primaryMediumImageURL(_this.scene7BaseUrl + zeroPaddedProdId);
					product.mediumImageURLs(prodImages);
					product.fullImageURLs(prodImages);
					widget.imgGroups(widget.groupImages(prodImages));
					product.thumbImageURLs(prodImages);

					while (product.product.productImagesMetadata.pop()) {}

					prodImages.forEach(function (elem, index) {
					    var alt = "Main Image of " + product.displayName();
					    if(index !== 0){
					        alt = "Alt #" + index + " " + product.displayName();
					    }
						product.product.productImagesMetadata.push({
							altText: alt
						});
						
					});

					widget.imgMetadata = [];
					widget.activeImgIndex.valueHasMutated();
				});
				//End Product Images
				//Stock Validations
				$.Topic('AVAILABLE_TODAY').subscribe(function (avl_flag) {
					widget.availableToday(avl_flag);
					if (!widget.allOptionsSelected()) {
						$(".tr-e-delivery").find(".error-msg").hide();
						return;
					}

					//stockAvailable
					if ($("input[name='optradio']:checked").val() === "2") {
						if (widget.itemQty() > widget.stockAvailable()) {
							if (!(skuBackorderFlag && widget.stockAvailable() === 0)) {
								// $(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
								// $(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);

								/*if(variantOptions.length === 1){
								 $(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
								 }*/
							}
						} else {
							// $(".tr-e-product-variation").find(".error-msg[data-val = 2]").hide();
						}
					}
				});

				//End Stock Validations
				$.Topic('AVAILABLE_STORE_NAME').subscribe(function (storeName) {
					widget.availableTodayStore(storeName);

				});
				// End OnLoad


			}