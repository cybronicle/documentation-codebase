// Share product to FB
shareProductFbClick: function () {
    var widget = this;

    // open fb share dialog
    var protocol = window.location.protocol;
    var host = window.location.host;
    var productUrlEncoded = encodeURIComponent(protocol + "//" + host + "/product/" + widget.product().id());

    var appID = widget.siteFbAppId();
    // NOTE: Once we can support the Facebook Crawler OG meta-tags, then we should try and use the newer Facebook Share Dialog URL
    //       (per https://developers.facebook.com/docs/sharing/reference/share-dialog).  Until then, we will use a legacy
    //       share URL.  Facebook may eventually not support this older URL, so would be good to replace it as soon as possible.
    //var fbShareUrl = "https://www.facebook.com/dialog/share?app_id=" + appID + "&display=popup&href=" + spaceUrlEncoded + "&redirect_uri=https://www.facebook.com";
    var fbShareUrl = "https://www.facebook.com/sharer/sharer.php?app_id=" + appID + "&u=" + productUrlEncoded;
    var facebookWin = window.open(fbShareUrl, 'facebookWin', 'width=720, height=500');
    if (facebookWin) {
        facebookWin.focus();
    }
},

// Share product to Twitter
shareProductTwitterClick: function () {
    var widget = this;
    var productNameEncoded = encodeURIComponent(widget.product().displayName());
    var protocol = window.location.protocol;
    var host = window.location.host;
    var productUrlEncoded = encodeURIComponent(protocol + "//" + host + "/product/" + widget.product().id());
    var twitterWin = window.open('https://twitter.com/share?url=' + productUrlEncoded + '&text=' + productNameEncoded, 'twitterWindow', 'width=720, height=500');
    if (twitterWin) {
        twitterWin.focus();
    }
},

// Share product to Pinterest
shareProductPinterestClick: function () {
    var widget = this;
    var productNameEncoded = encodeURIComponent(widget.product().displayName());
    var protocol = window.location.protocol;
    var host = window.location.host;
    var productUrlEncoded = encodeURIComponent(protocol + "//" + host + "/product/" + widget.product().id());
    var productMediaEncoded = encodeURIComponent(protocol + "//" + host + widget.product().primaryLargeImageURL());

    var pinterestWin = window.open('https://pinterest.com/pin/create/button/?url=' + productUrlEncoded + '&description=' + productNameEncoded + '&media=' + productMediaEncoded, 'pinterestWindow', 'width=720, height=500');
    if (pinterestWin) {
        pinterestWin.focus();
    }
},

// Share product by Email
shareProductEmailClick: function () {
    var widget = this;
    var mailto = [];
    var protocol = window.location.protocol;
    var host = window.location.host;
    var productUrl = protocol + "//" + host + "/product/" + widget.product().id();
    mailto.push("mailto:?");
    mailto.push("subject=");
    mailto.push(encodeURIComponent(widget.translate('shareProductEmailSubject', {
        'productName': widget.product().displayName()
    })));
    mailto.push("&body=");
    var body = [];
    body.push(widget.translate('shareProductEmailBodyIntro', {
        'productName': widget.product().displayName()
    }));
    body.push("\n\n");
    body.push(productUrl);
    mailto.push(encodeURIComponent(body.join("")));
    window.location.href = mailto.join("");
},