/**
 * @fileoverview Product Details Widget.
 *
 * @author
 */
define(
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------
	['knockout', 'pubsub', 'ccConstants', 'koValidate', 'notifier', 'CCi18n', 'storeKoExtensions',
		'swmRestClient', 'spinner', 'pageLayout/product', 'ccRestClient', 'jquery', 'moment', 'storageApi', 'ccResourceLoader!global/helpers', 'pageLayout/order', 'navigation', 'pageLayout/site', 'viewModels/cart-item'
	],

	//----------------------------------personalization-prop-name---------------------------------
	// MODULE DEFINITION
	//-------------------------------------------------------------------
	function (ko, pubsub, CCConstants, koValidate, notifier, CCi18n, storeKoExtensions, swmRestClient,
		spinner, product, ccRestClient, $, moment, storageApi, helpers, order, navigation, site, CartItem) {
		//"use strict";

		var config, cart, getWidget, imageRes = 1500,
			currentProduct;
			
			var h_product={};
            var h_selectedSKUObj={};
            var h_variantOptions={};
            var h_qnty;
            var skuOPUS;
            var request_obj = {};

		var refreshData = false;
		window.inventoryObject = {};
	    window.impressionsOnScroll = "";
		var processingTime = 0;

		var personalizeClicked = false;
		var widgetModel;
		var altText;
		var skuInventory;
		var skuBackorderFlag;
		var LOADED_EVENT = "LOADED";
		var LOADING_EVENT = "LOADING";
		var publishedSkuData = {};
		var productLoadingOptions = {
			parent: '#cc-product-spinner',
			selector: '#cc-product-spinner-area'
		};

		var resourcesAreLoaded = false;
		var resourcesNotLoadedCount = 0;
		var resourcesMaxAttempts = 5;
		var opusPreventModal = false;
		var _this;
		var g_productData = null;
		var g_selectedSKUData = null;
		var g_selectedSKUQty = null;
		var cartInventoryDetailsArray = [];
		var stsFlag = false;
		var productPrice = "";
		var inventoryDataIR = {};
		var newPersonalizationData = "";
		var isPersonalizationPropertyExist = 0;
		var mySpacesComparator = function (opt1, opt2) {
			if (opt1.spaceNameFull() > opt2.spaceNameFull()) {
				return 1;
			} else if (opt1.spaceNameFull() < opt2.spaceNameFull()) {
				return -1;
			} else {
				return 0;
			}
		};
		var joinedSpacesComparator = function (opt1, opt2) {
			if (opt1.spaceNameFull() > opt2.spaceNameFull()) {
				return 1;
			} else if (opt1.spaceNameFull() < opt2.spaceNameFull()) {
				return -1;
			} else {
				return 0;
			}
		};
		var item;
		var urlParams;
		var itemHash;
		var storeItems = window.storeItems;
		var currentPersonalizationData = [];
		var setDetailsToItem = false;
		var quantityChangeDeliveryTriggered = false;
		var displayStoreChangeMessage = true;
        var getDataCall = false;        	

		function ulToArr(ul) {}
	
		return {
		    giftModalData: ko.observableArray([]),
			videoUrl: ko.observable(""),
			availableTodayVisibility: ko.observable(false),
			stockStatus: ko.observable(false),
			showStockStatus: ko.observable(false),
			variantOptionsArray: ko.observableArray([]),
			otherStores: ko.observableArray([]),
			myStore: ko.observableArray([]),
			itemQuantity: ko.observable(1),
			itemQty: ko.observable(1),
			stockAvailable: ko.observable(1),
			selectedSku: ko.observable(),
			isSkuSelected: ko.observable(false),
			disableOptions: ko.observable(false),
			priceRange: ko.observable(false),
			filtered: ko.observable(false),
			WIDGET_ID: 'productDetails',
			isAddToCartClicked: ko.observable(false),
			containerImage: ko.observable(),
			imgGroups: ko.observableArray(),
			mainImgUrl: ko.observable(),
			activeImgIndex: ko.observable(0),
			viewportWidth: ko.observable(),
			skipTheContent: ko.observable(false),
			listPrice: ko.observable(),
			salePrice: ko.observable(),
			personalizationCost: ko.observable(0),
			backLinkActive: ko.observable(true),
			variantName: ko.observable(),
			variantValue: ko.observable(),
			skuBackorderFlag: ko.observable(),
			listingVariant: ko.observable(),
			shippingSurcharge: ko.observable(),
			imgMetadata: [],
			isMobile: ko.observable(false),
			availableTodayXs: ko.observable(false),
			otherStore: ko.observable(),
			availableTodayStore: ko.observable(),
			availableCity: ko.observable(),
			availableState: ko.observable(),
			availableQuantity: ko.observable(),
			buttonVisibilityFlag: ko.observable(false),
			personalizeVisibilityFlag: ko.observable(false),
			personalizeDisabledFlag: ko.observable(true),
			prodAvlFlag: ko.observable(1),
			uiDisplayAttr: ko.observableArray([]),
			itemWisePersonalizationCostArr: [],
			scene7BaseUrl: "https://thingsremembered.scene7.com/is/image/ThingsRemembered/",
			isForUpdating: ko.observable(false),
			occasionCode: ko.observable(),
			checkPersonalization: ko.observable(),
			storeCode: ko.observable(),
			urlObjArr: [],
			skuBOM: {},
			bomArray: ko.observableArray([]),
			bomPrice: {},
			bomTotalPrice: ko.observable(0),
			giftTitle: ko.observable(),
			showSWM: ko.observable(false),
			isAddToSpaceClicked: ko.observable(false),
			disableAddToSpace: ko.observable(false),
			spaceOptionsArray: ko.observableArray([]),
			spaceOptionsGrpMySpacesArr: ko.observableArray([]),
			spaceOptionsGrpJoinedSpacesArr: ko.observableArray([]),
			mySpaces: ko.observableArray([]),
			siteFbAppId: ko.observable(''),
			occasionsArr: ko.observableArray([]),
			deliveryDate: ko.observable(''),
			virtual: ko.observable(false),
			bwyFlag: ko.observable(false),
			airFlag: ko.observable(false),
			two_ndFlag: ko.observable(false),
			prmFlag: ko.observable(false),
			bwyFlagDate: ko.observable(),
			airFlagDate: ko.observable(),
			two_ndFlagDate: ko.observable(),
			prmFlagDate: ko.observable(),
			availableToday: ko.observable(false),
			availableDateText: ko.observable('Pick Up In Store'),
			displayOpus: ko.observable(false),
			displaySts: ko.observable(false),
			isSFS: ko.observable(false),
			sfsStoreId: ko.observable(),
			displayShipToHome: ko.observable(true),
			displayOnlyWarning: ko.observable(false),
			displayWarningMsg: ko.observable(false),
			stsStore: ko.observable(),
			stsAvailableText: ko.observable(),
			stsDisable: ko.observable(false),
			skuRepoId: ko.observable(),
			skuOpus: ko.observable(),
			skuSts: ko.observable(),
			skuData: ko.observable(),
			pageStoreLat: ko.observable(),
			pageStoreLon: ko.observable(),
			pageStore: ko.observable(),
			backOrderFlag: ko.observable(false),
			backOrderMessage: ko.observable(),
			standardDeliveryDate: ko.observable(''),
			aggregateRating: ko.observable(),
			seoReviews: ko.observable(),
			deliveryDateHeadingFlag: ko.observable(true),
			giftText: ko.observable(),
			giftData: ko.observable(),
			giftDisplayName: ko.observable(false),
			hasGift: ko.observable(),
			personaliztionObj: {},
			isPhotoUpload: ko.observable(false),
			finalBackOrderFlag: ko.observable(),
			dcQuantity: ko.observable(),
			selectedBomArray: ko.observableArray([]),
			isConfigReady: ko.observable(false),
			backOrderFlag: ko.observable(),
			DCInventory: ko.observable(),
			maxQuantityStore: ko.observable(),
			availableSTHInventory: ko.observable(0),
			mobileDefaultOccassionId: ko.observable(''),
			mobileDefaultOccassionIndex: ko.observable(''),
			prodTitleNew: ko.observable(''),
			skuidNew: ko.observable(-1),
			skulistPriceNew: ko.observable(0),
			skusalePriceNew: ko.observable(0),
			storeStockErrorMessage: ko.observable(''),
			opusDisabled: ko.observable(false),
			itemsPerRowInLargeDesktopView: ko.observable(4),
			itemsPerRowInDesktopView: ko.observable(4),
			itemsPerRowInTabletView: ko.observable(4),
			itemsPerRowInPhoneView: ko.observable(2),
			itemsPerRow: ko.observable(6),
			viewportWidth: ko.observable(),
			viewportMode: ko.observable(),
			isDeskTopMode: ko.observable(false),
			productGroups: ko.observableArray(),
			products: ko.observableArray(),
			spanClass: ko.observable(),
			relatedProductGroups: ko.observableArray(),
			relatedProducts: ko.observableArray([]),
			activeProdIndex: ko.observable(0),
			numberOfRelatedProductsToShow: ko.observable(),
			numberOfRelatedProducts: ko.observable(20),
			relatedProdsloaded: ko.observable(false),

			resourcesLoaded: function (widget) {
				resourcesAreLoaded = true;
			},
			
			
			loadCarousel: function () {
				$(".owl-carousel").owlCarousel({
					items: 5,
					pagination: false,
					navigation: true,
					navigationText: false,
					rewindNav: false,
					itemsMobile: [479, 5],
					itemsDesktop: [1199, 5],
					itemsDesktopSmall: [980, 5],
					itemsTablet: [768, 5],
					itemsTabletSmall: false,
					animateOut: 'slideOutUp',
					animateIn: 'slideInUp'  
				});
				 
			},
			giftboxModalShown:function(){
			    if ($(window).width() >= 992) {
			        $(".owl-carousels-sticky").owlCarousel({
							items: 4,
							pagination: false,
							navigation: true,
							navigationText: false,
							rewindNav: false,
							lazyLoad: true,
							itemsMobile: [320, 4],
							itemsDesktop: [1199, 4],
							itemsDesktopSmall: [980, 4],
							itemsTablet: [767, 4],
							itemsTabletSmall: false
						});
			    }else{
			        $(".owl-carousels-sticky").owlCarousel({
							items: 1,
							pagination: false,
							navigation: true,
							navigationText: false,
							rewindNav: false,
							lazyLoad: true,
							itemsMobile: [320, 1],
							itemsDesktop: [1199, 1],
							itemsDesktopSmall: [980, 1],
							itemsTablet: [767, 1],
							itemsTabletSmall: false
						});
			    }
			       
			},
			
			
			
			

			

  
			selectOccassionForMobile: function () {
				var widget = this;
				if (!widget.occasionCode()) {  
					if (widget.mobileDefaultOccassionId() !== '') {
						$(".occasion-btn").find("span.index").text(widget.mobileDefaultOccassionIndex());
						widget.occasionCode(widget.mobileDefaultOccassionId());
						$.Topic('OCCASION').publish(widget.occasionCode());
					} else {
						$(".occasion-btn").find("span.index").text(0);
					}
				}
			},

			goBack: function () {
				$(window).scrollTop($(window).height());
				window.history.go(-1);
				return false;
			},


			// Handles loading a default 'no-image' as a fallback
			cancelZoom: function (element) {
				$(element).parent().removeClass('zoomContainer-CC');
			},
			

			

			/*this create view model for variant options this contains
			 name of the option, possible list of option values for the option
			 selected option to store the option selected by the user.
			 ID to map the selected option*/
			productVariantModel: function (optionDisplayName, optionId, optionValues, widget, actualOptionId) {
				var productVariantOption = {};
				var productImages = {};
				productVariantOption.optionDisplayName = optionDisplayName.replace("occ", "");
				productVariantOption.parent = this;
				productVariantOption.optionId = optionId;
				productVariantOption.originalOptionValues = ko.observableArray(optionValues);
				productVariantOption.actualOptionId = actualOptionId;

				var showOptionCation = ko.observable(true);
				if (optionValues.length === 1) {
					showOptionCation(this.checkOptionValueWithSkus(optionId, optionValues[0].value));
				}
				//If there is just one option value in all Skus we dont need any caption
				if (showOptionCation()) {
					productVariantOption.optionCaption = widget.translate('optionCaption', {
						optionName: optionDisplayName
					}, true);
				}
				productVariantOption.selectedOptionValue = ko.observable();
				productVariantOption.countVisibleOptions = ko.computed(function () {
					var count = 0;
					for (var i = 0; i < productVariantOption.originalOptionValues().length; i++) {
						if (optionValues[i].visible() == true) {
							count = count + 1;
						}
					}
					return count;
				}, productVariantOption);
				productVariantOption.disable = ko.computed(function () {
					if (productVariantOption.countVisibleOptions() == 0) {
						return true;
					} else {
						return false;
					}
				}, productVariantOption);
				productVariantOption.setColorExternally = function () {
						$('#CC-prodDetails-sku-TRProduct_color').val('Grey').trigger('change');
					},
					productVariantOption.selectedOption = ko.computed({
						write: function (option) {
							widget.displayOpus(false);
							$(".tr-store-stock-error").hide().text("");
							this.parent.filtered(false);
							productVariantOption.selectedOptionValue(option);

							var variantOptions = this.parent.variantOptionsArray(),
								sku_obj = this.parent.getSelectedSku(variantOptions),
								repoid = this.parent.product().id(),
								prod_descr = this.parent.product().displayName();

							if (this.parent.allOptionsSelectedF()) {
								$(".tr-e-product-variation").find(".error-msg[data-val = 1]").hide();
							}
							if (this.parent.allOptionsSelected()) {
								$(".tr-e-product-variation").find(".error-msg[data-val = 2]").hide();
							}

							if (!this.parent.allOptionsSelected() && this.parent.allOptionsSelectedF()) {
								$(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
								$(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);
								$(".personalizationBtns").addClass('hide');
                                $('.tr-xs-personalize-block').addClass('hide');

								if (variantOptions.length === 1) {
									$(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
									$(".personalizationBtns").addClass('hide');
                                    $('.tr-xs-personalize-block').addClass('hide');
								}
							}


							if (sku_obj) {
								repoid = sku_obj.repositoryId;
								this.parent.selectedSku(sku_obj);
							}

							widget.bomArray([]);
							widget.urlObjArr = [];

							// (/ccstore/v1/products/xprod2119
							$.Topic('SKU_SELECTED').publish(this.parent.product(), sku_obj, variantOptions, this.parent.itemQty());
							$(".tr-e-product-description").find(".id span").text(repoid);
                            $(".tr-desc-review-block").find(".id span").text(repoid);
							if (productVariantOption.actualOptionId === this.parent.listingVariant()) {
								if (option && option.listingConfiguration) {
									this.parent.imgMetadata = option.listingConfiguration.imgMetadata;
									this.parent.assignImagesToProduct(option.listingConfiguration);
								} else {
									this.parent.imgMetadata = this.parent.product().product.productImagesMetadata;
									this.parent.assignImagesToProduct(this.parent.product().product);
								}
							}
							this.parent.filterOptionValues(productVariantOption.optionId);
						},
						read: function () {
							return productVariantOption.selectedOptionValue();
						},
						owner: productVariantOption
					});
				productVariantOption.selectedOption.extend({
					required: {
						params: true,
						message: widget.translate('optionRequiredMsg', {
							optionName: optionDisplayName
						}, true)
					}
				});
				productVariantOption.optionValues = ko.computed({
					write: function (value) {
						productVariantOption.originalOptionValues(value);
					},
					read: function () {
						return ko.utils.arrayFilter(
							productVariantOption.originalOptionValues(),
							function (item) {
								return item.visible() == true;
							}
						);
					},
					owner: productVariantOption
				});


				//The below snippet finds the product display/listing variant (if available)
				//looping through all the product types
				for (var productTypeIdx = 0; productTypeIdx < widget.productTypes().length; productTypeIdx++) {
					//if the product type matched with the current product
					if (widget.product().type() && widget.productTypes()[productTypeIdx].id == widget.product().type()) {
						var variants = widget.productTypes()[productTypeIdx].variants;
						//Below FOR loop is to iterate over the various variant types of that productType
						for (var productTypeVariantIdx = 0; productTypeVariantIdx < variants.length; productTypeVariantIdx++) {
							//if the productType has a listingVariant == true, hence this is the product display variant
							if (variants[productTypeVariantIdx].listingVariant) {
								widget.listingVariant(variants[productTypeVariantIdx].id);
								break;
							}
						}
						break;
					}
				}
				productImages.thumbImageURLs = (widget.product().product.thumbImageURLs.length == 1 && widget.product().product.thumbImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.thumbImageURLs);
				productImages.smallImageURLs = (widget.product().product.smallImageURLs.length == 1 && widget.product().product.smallImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.smallImageURLs);
				productImages.mediumImageURLs = (widget.product().product.mediumImageURLs.length == 1 && widget.product().product.mediumImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.mediumImageURLs);
				productImages.largeImageURLs = (widget.product().product.largeImageURLs.length == 1 && widget.product().product.largeImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.largeImageURLs);
				productImages.fullImageURLs = (widget.product().product.fullImageURLs.length == 1 && widget.product().product.fullImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.fullImageURLs);
				productImages.sourceImageURLs = (widget.product().product.sourceImageURLs.length == 1 && widget.product().product.sourceImageURLs[0].indexOf("/img/no-image.jpg") > 0) ? [] : (widget.product().product.sourceImageURLs);

				var prodImgMetadata = [];
				if (widget.product().thumbImageURLs && widget.product().thumbImageURLs().length > 0) {
					for (var index = 0; index < widget.product().thumbImageURLs().length; index++) {
						prodImgMetadata.push(widget.product().product.productImagesMetadata[index]);
					}
				}

				ko.utils.arrayForEach(productVariantOption.originalOptionValues(), function (option) {

					if (widget.listingVariant() === actualOptionId) {
						for (var childSKUsIdx = 0; childSKUsIdx < widget.product().childSKUs().length; childSKUsIdx++) {
							if (widget.product().childSKUs()[childSKUsIdx].productListingSku()) {
								var listingConfiguration = widget.product().childSKUs()[childSKUsIdx];
								if (listingConfiguration[actualOptionId]() == option.key) {
									var listingConfig = {};
									listingConfig.thumbImageURLs = $.merge($.merge([], listingConfiguration.thumbImageURLs()), productImages.thumbImageURLs);
									listingConfig.smallImageURLs = $.merge($.merge([], listingConfiguration.smallImageURLs()), productImages.smallImageURLs);
									listingConfig.mediumImageURLs = $.merge($.merge([], listingConfiguration.mediumImageURLs()), productImages.mediumImageURLs);
									listingConfig.largeImageURLs = $.merge($.merge([], listingConfiguration.largeImageURLs()), productImages.largeImageURLs);
									listingConfig.fullImageURLs = $.merge($.merge([], listingConfiguration.fullImageURLs()), productImages.fullImageURLs);
									listingConfig.sourceImageURLs = $.merge($.merge([], listingConfiguration.sourceImageURLs()), productImages.sourceImageURLs);
									listingConfig.primaryFullImageURL = listingConfiguration.primaryFullImageURL() ? listingConfiguration.primaryFullImageURL() : widget.product().product.primaryFullImageURL;
									listingConfig.primaryLargeImageURL = listingConfiguration.primaryLargeImageURL() ? listingConfiguration.primaryLargeImageURL() : widget.product().product.primaryLargeImageURL;
									listingConfig.primaryMediumImageURL = listingConfiguration.primaryMediumImageURL() ? listingConfiguration.primaryMediumImageURL() : widget.product().product.primaryMediumImageURL;
									listingConfig.primarySmallImageURL = listingConfiguration.primarySmallImageURL() ? listingConfiguration.primarySmallImageURL() : widget.product().product.primarySmallImageURL;
									listingConfig.primaryThumbImageURL = listingConfiguration.primaryThumbImageURL() ? listingConfiguration.primaryThumbImageURL() : widget.product().product.primaryThumbImageURL;

									//storing the metadata for the images
									var childSKUImgMetadata = [];
									if (listingConfiguration.images && listingConfiguration.images().length > 0) {
										for (var index = 0; index < listingConfiguration.images().length; index++) {
											childSKUImgMetadata.push(widget.product().product.childSKUs[childSKUsIdx].images[index].metadata);
										}
									}
									listingConfig.imgMetadata = $.merge($.merge([], childSKUImgMetadata), prodImgMetadata);
									option.listingConfiguration = listingConfig;
								}
							}
						}
					}
					if (widget.variantName() === actualOptionId && option.key === widget.variantValue()) {
						productVariantOption.selectedOption(option);
					}
				});

				return productVariantOption;
			},

			

			// SC-4166 : ajax success/error callbacks from beforeAppear does not get called in IE9, ensure dropdown options are populated when opening dropdown
			openAddToWishlistDropdownSelector: function () {
				var widget = this;
				if (widget.spaceOptionsArray().length === 0) {
					widget.getSpaces();
				}
			},
			

			isProdDescrOverFlown: function () {

				if ($(".tr-e-product-description #descr-container").height() + 10 < $(".tr-e-product-description #descr-container .tr-prod-descr-string").height()) {
					return true;
				} else {
					return false;
				}
			},
			
			
			
			

			
		};

	}
);
