function checkNearby(product, selectedSKUObj, variantOptions, qnty) {
    $.Topic('TR_CREATE_INVENTORY_SPINNER').publish();
    h_product = product;
    h_selectedSKUObj = selectedSKUObj;
    h_variantOptions = variantOptions;
    h_qnty = qnty;


    if ((typeof selectedSKUObj !== "undefined") && (selectedSKUObj !== null)) {
        var data = {};

        var repoId;

        if (typeof selectedSKUObj.repositoryId === "function") {
            repoId = selectedSKUObj.repositoryId();
        } else {
            repoId = selectedSKUObj.repositoryId;
        }

        data[CCConstants.PRODUCT_IDS] = [repoId];
        ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, data,
            function(products) {
                $(".dotwhackPDP").html("");
                if (products.length > 0) {
                    $(".dotwhackPDP").html(products[0].promotionMessage);
                    $.Topic('SKU_OBJ1').publish(products[0]);
                    $.Topic('SKU_OBJ_BOM').publish(products[0]);
                    skuOPUS = products[0].OPUS;
                    var listPrice, salePrice;
                    if (typeof selectedSKUObj.listPrice === "function") {
                        listPrice = selectedSKUObj.listPrice();
                    } else {
                        listPrice = selectedSKUObj.listPrice;
                    }

                    if (typeof selectedSKUObj.salePrice === "function") {
                        salePrice = selectedSKUObj.salePrice();
                    } else {
                        salePrice = selectedSKUObj.salePrice;
                    }
                    if (product.childSKUs().length === 1) {

                    }

                    $.Topic('TR_PDP_NEW_PRICE.memory').publish(repoId, listPrice,
                        salePrice);
                    if (skuOPUS) {


                        request_obj.skuId = repoId;
                        request_obj.storeNumber = store_id;
                        request_obj.location = {};
                        request_obj.location.latitude = parseFloat(latitude);
                        request_obj.location.longitude = parseFloat(longitude);
                        request_obj.quantity = qnty || 1;
                        request_obj.radiusLimit = 400;
                        request_obj.limit = 400;
                        var request_json = JSON.stringify(request_obj);

                    } else {
                        $.Topic('AVAILABLE_TODAY').publish(false);

                    }
                    setTimeout(function() {
                        $.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
                    }, 5000)
                } else {
                    $.Topic('VARIANT_NO_STOCK_ERROR_MESSAGE').publish();
                    $.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
                }


            });

    } else {
        $.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
        $.Topic('TR_PDP_NEW_PRICE.memory').publish(-1, product.listPrice(), product
            .salePrice());
        $.Topic('AVAILABLE_TODAY').publish(false);
    }
}