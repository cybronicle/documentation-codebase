/**
 * @fileoverview Product Details Widget.
 *
 * @author
 */
define(
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------
	['knockout', 'pubsub', 'ccConstants', 'koValidate', 'notifier', 'CCi18n', 'storeKoExtensions',
		'swmRestClient', 'spinner', 'pageLayout/product', 'ccRestClient', 'jquery', 'moment', 'storageApi', 'ccResourceLoader!global/helpers', 'pageLayout/order', 'navigation', 'pageLayout/site', 'viewModels/cart-item'
	],

	//----------------------------------personalization-prop-name---------------------------------
	// MODULE DEFINITIONapr
	//-------------------------------------------------------------------
	function (ko, pubsub, CCConstants, koValidate, notifier, CCi18n, storeKoExtensions, swmRestClient,
		spinner, product, ccRestClient, $, moment, storageApi, helpers, order, navigation, site, CartItem) {
		//"use strict";

		var config, cart, getWidget, imageRes = 1500,
			currentProduct;
			
			var h_product={};
            var h_selectedSKUObj={};
            var h_variantOptions={};
            var h_qnty;
            var skuOPUS;
            var request_obj = {};

		var refreshData = false;
		window.inventoryObject = {};
	    window.impressionsOnScroll = "";
		var processingTime = 0;

		var personalizeClicked = false;
		var widgetModel;
		var altText;
		var skuInventory;
		var skuBackorderFlag;
		var LOADED_EVENT = "LOADED";
		var LOADING_EVENT = "LOADING";
		var publishedSkuData = {};
		var productLoadingOptions = {
			parent: '#cc-product-spinner',
			selector: '#cc-product-spinner-area'
		};

		var resourcesAreLoaded = false;
		var resourcesNotLoadedCount = 0;
		var resourcesMaxAttempts = 5;
		var opusPreventModal = false;
		var _this;
		var g_productData = null;
		var g_selectedSKUData = null;
		var g_selectedSKUQty = null;
		var cartInventoryDetailsArray = [];
		var stsFlag = false;
		var productPrice = "";
		var inventoryDataIR = {};
		var newPersonalizationData = "";
		var isPersonalizationPropertyExist = 0;
		var mySpacesComparator = function (opt1, opt2) {
			if (opt1.spaceNameFull() > opt2.spaceNameFull()) {
				return 1;
			} else if (opt1.spaceNameFull() < opt2.spaceNameFull()) {
				return -1;
			} else {
				return 0;
			}
		};
		var joinedSpacesComparator = function (opt1, opt2) {
			if (opt1.spaceNameFull() > opt2.spaceNameFull()) {
				return 1;
			} else if (opt1.spaceNameFull() < opt2.spaceNameFull()) {
				return -1;
			} else {
				return 0;
			}
		};
		var item;
		var urlParams;
		var itemHash;
		var storeItems = window.storeItems;
		var currentPersonalizationData = [];
		var setDetailsToItem = false;
		var quantityChangeDeliveryTriggered = false;
		var displayStoreChangeMessage = true;
        var getDataCall = false;        	

		function ulToArr(ul) {}


		function getParametrFromUrl(urlParams, param) {
			for (var i = 0, lth = urlParams.length; i < lth; i++) {
				var oneParametr = urlParams[i];
				if (oneParametr.key == param) {
					return oneParametr.value;
				}
			}
			return "";
		}


		function isSelectedBomArrayValid(widget, parentContext) {
		    
		  //  console.log("parentContext: ", parentContext);
		    var hasZones = true;
		    //console.log("this: ",this);
		    console.log("widget at valid function:", widget);
		  //  console.log("widget.product(): ",widget.product());
		     var initData = widget.skuData();
		     console.log("initData: ",initData);
		     console.log("initData.productzones: ",initData.productZones);
		     //   return widget.skuBOM().length == widget.
		     console.log("widget.bomArray().length",widget.bomArray().length);
		     console.log("widget.selectedBomArray().length",widget.selectedBomArray().length);
		     console.log("widget.product().productZones()",widget.product().productZones());
		     console.log("widget.product().productZones().length",widget.product().productZones().length);
			return widget.bomArray().length == widget.selectedBomArray().length && widget.selectedBomArray().every(function (selectedBomItem) {
					return selectedBomItem.isInStock() == true;
			}) && (widget.product().productZones()!==null && widget.product().productZones().length > 0);
// 			&& (widget.product().productZones()!==null && widget.product().productZones().length > 0);
		}
		
		function getfallBackDatafromSku(widget){
			if(widget.product().childSKUs().length === 1){
				widget.createInventorySpinner();
				var skuId = "personalizationFallback_"+widget.product().childSKUs()[0].repositoryId().split("_")[0];
				ccRestClient.authenticatedRequest('/ccstoreui/v1/products/'+skuId+'?fields=tr_personalizationJson', {},
					function (data) {
						 if(data.hasOwnProperty('tr_personalizationJson')){
							var data = JSON.parse(data.tr_personalizationJson);
							isPersonalizationPropertyExist = data.zones.length;
							if(data.zones.length > 0){
								newPersonalizationData = data;
								widget.generateExpressPhase(newPersonalizationData);
								widget.generateFonts(newPersonalizationData);
								widget.generalFonts(newPersonalizationData);
								widget.generateColorFills(newPersonalizationData);
								widget.generateDesigns(newPersonalizationData);
								widget.generateNographic(newPersonalizationData);
								widget.checkPersonalizationExist();
							}else if($('.error-msg').is(':visible') === false){
								$('.tr-xs-personalize-block').removeClass('hide');
								$(".personalizationBtn").hide();
								$(".addToCartBtn").show();
								$(".personalizationBtns").removeClass('hide');
							}
							}
							widget.destroyInventorySpinner();
					},
							function (data) {

							});
			}else{
				if(widget.selectedSku() !== null){
					skuId = widget.selectedSku().repositoryId;
					ccRestClient.authenticatedRequest('/ccstoreui/v1/products/'+skuId+'?fields=tr_personalizationJson', {},
					function (data) {
						 if(data.hasOwnProperty('tr_personalizationJson')){
							var data = JSON.parse(data.tr_personalizationJson);
						isPersonalizationPropertyExist = data.zones.length;
						if(data.zones.length > 0){
							newPersonalizationData = data;
							widget.generateExpressPhase(newPersonalizationData);
							widget.generateFonts(newPersonalizationData);
							widget.generalFonts(newPersonalizationData);
							widget.generateColorFills(newPersonalizationData);
							widget.generateDesigns(newPersonalizationData);
							widget.generateNographic(newPersonalizationData);
							widget.checkPersonalizationExist();
						}else if($('.error-msg').is(':visible') === false){
							$('.tr-xs-personalize-block').removeClass('hide');
							$(".personalizationBtn").hide();
							$(".addToCartBtn").show();
							$(".personalizationBtns").removeClass('hide');
						}
						}
						widget.destroyInventorySpinner();
					},
					function (data) {

							});
					}
			}
			
			
	}

		return {
		    giftModalData: ko.observableArray([]),
			videoUrl: ko.observable(""),
			availableTodayVisibility: ko.observable(false),
			stockStatus: ko.observable(false),
			showStockStatus: ko.observable(false),
			variantOptionsArray: ko.observableArray([]),
			otherStores: ko.observableArray([]),
			myStore: ko.observableArray([]),
			itemQuantity: ko.observable(1),
			itemQty: ko.observable(1),
			stockAvailable: ko.observable(1),
			selectedSku: ko.observable(),
			isSkuSelected: ko.observable(false),
			disableOptions: ko.observable(false),
			priceRange: ko.observable(false),
			filtered: ko.observable(false),
			WIDGET_ID: 'productDetails',
			isAddToCartClicked: ko.observable(false),
			containerImage: ko.observable(),
			imgGroups: ko.observableArray(),
			mainImgUrl: ko.observable(),
			activeImgIndex: ko.observable(0),
			viewportWidth: ko.observable(),
			skipTheContent: ko.observable(false),
			listPrice: ko.observable(),
			salePrice: ko.observable(),
			personalizationCost: ko.observable(0),
			backLinkActive: ko.observable(true),
			variantName: ko.observable(),
			variantValue: ko.observable(),
			skuBackorderFlag: ko.observable(),
			listingVariant: ko.observable(),
			shippingSurcharge: ko.observable(),
			imgMetadata: [],
			isMobile: ko.observable(false),
			availableTodayXs: ko.observable(false),
			otherStore: ko.observable(),
			availableTodayStore: ko.observable(),
			availableCity: ko.observable(),
			availableState: ko.observable(),
			availableQuantity: ko.observable(),
			buttonVisibilityFlag: ko.observable(false),
			personalizeVisibilityFlag: ko.observable(false),
			personalizeDisabledFlag: ko.observable(true),
			prodAvlFlag: ko.observable(1),
			uiDisplayAttr: ko.observableArray([]),
			itemWisePersonalizationCostArr: [],
			scene7BaseUrl: "https://thingsremembered.scene7.com/is/image/ThingsRemembered/",
			isForUpdating: ko.observable(false),
			occasionCode: ko.observable(),
			checkPersonalization: ko.observable(),
			storeCode: ko.observable(),
			urlObjArr: [],
			skuBOM: {},
			bomArray: ko.observableArray([]),
			bomPrice: {},
			bomTotalPrice: ko.observable(0),
			giftTitle: ko.observable(),
			showSWM: ko.observable(false),
			isAddToSpaceClicked: ko.observable(false),
			disableAddToSpace: ko.observable(false),
			spaceOptionsArray: ko.observableArray([]),
			spaceOptionsGrpMySpacesArr: ko.observableArray([]),
			spaceOptionsGrpJoinedSpacesArr: ko.observableArray([]),
			mySpaces: ko.observableArray([]),
			siteFbAppId: ko.observable(''),
			occasionsArr: ko.observableArray([]),
			deliveryDate: ko.observable(''),
			virtual: ko.observable(false),
			bwyFlag: ko.observable(false),
			airFlag: ko.observable(false),
			two_ndFlag: ko.observable(false),
			prmFlag: ko.observable(false),
			bwyFlagDate: ko.observable(),
			airFlagDate: ko.observable(),
			two_ndFlagDate: ko.observable(),
			prmFlagDate: ko.observable(),
			availableToday: ko.observable(false),
			availableDateText: ko.observable('Pick Up In Store'),
			displayOpus: ko.observable(false),
			displaySts: ko.observable(false),
			isSFS: ko.observable(false),
			sfsStoreId: ko.observable(),
			displayShipToHome: ko.observable(true),
			displayOnlyWarning: ko.observable(false),
			displayWarningMsg: ko.observable(false),
			stsStore: ko.observable(),
			stsAvailableText: ko.observable(),
			stsDisable: ko.observable(false),
			skuRepoId: ko.observable(),
			skuOpus: ko.observable(),
			skuSts: ko.observable(),
			skuData: ko.observable(),
			pageStoreLat: ko.observable(),
			pageStoreLon: ko.observable(),
			pageStore: ko.observable(),
			backOrderFlag: ko.observable(false),
			backOrderMessage: ko.observable(),
			standardDeliveryDate: ko.observable(''),
			aggregateRating: ko.observable(),
			seoReviews: ko.observable(),
			deliveryDateHeadingFlag: ko.observable(true),
			giftText: ko.observable(),
			giftData: ko.observable(),
			giftDisplayName: ko.observable(false),
			hasGift: ko.observable(),
			personaliztionObj: {},
			isPhotoUpload: ko.observable(false),
			finalBackOrderFlag: ko.observable(),
			dcQuantity: ko.observable(),
			selectedBomArray: ko.observableArray([]),
			isConfigReady: ko.observable(false),
			backOrderFlag: ko.observable(),
			DCInventory: ko.observable(),
			maxQuantityStore: ko.observable(),
			availableSTHInventory: ko.observable(0),
			mobileDefaultOccassionId: ko.observable(''),
			mobileDefaultOccassionIndex: ko.observable(''),
			prodTitleNew: ko.observable(''),
			skuidNew: ko.observable(-1),
			skulistPriceNew: ko.observable(0),
			skusalePriceNew: ko.observable(0),
			storeStockErrorMessage: ko.observable(''),
			opusDisabled: ko.observable(false),
			itemsPerRowInLargeDesktopView: ko.observable(4),
			itemsPerRowInDesktopView: ko.observable(4),
			itemsPerRowInTabletView: ko.observable(4),
			itemsPerRowInPhoneView: ko.observable(2),
			itemsPerRow: ko.observable(6),
			viewportWidth: ko.observable(),
			viewportMode: ko.observable(),
			isDeskTopMode: ko.observable(false),
			productGroups: ko.observableArray(),
			products: ko.observableArray(),
			spanClass: ko.observable(),
			relatedProductGroups: ko.observableArray(),
			relatedProducts: ko.observableArray([]),
			activeProdIndex: ko.observable(0),
			numberOfRelatedProductsToShow: ko.observable(),
			numberOfRelatedProducts: ko.observable(20),
			relatedProdsloaded: ko.observable(false),

			resourcesLoaded: function (widget) {
				resourcesAreLoaded = true;
			},
			updateQuantity: function () {
				item.updatableCustomQuantity(this.itemQty());
			},
			checkPersonalizationExist: function () {
				var widget = this;
				if (isPersonalizationPropertyExist > 0) {
					widget.personalizeVisibilityFlag(true);
					widget.buttonVisibilityFlag(false);
					widget.checkPersonalization(true);
				} else {
					widget.personalizeVisibilityFlag(false);
					widget.buttonVisibilityFlag(true);
					widget.checkPersonalization(false);
					widget.personalizeDisabledFlag(true);
				}
			},
			loadCarousel: function () {
				$(".owl-carousel").owlCarousel({
					items: 5,
					pagination: false,
					navigation: true,
					navigationText: false,
					rewindNav: false,
					itemsMobile: [479, 5],
					itemsDesktop: [1199, 5],
					itemsDesktopSmall: [980, 5],
					itemsTablet: [768, 5],
					itemsTabletSmall: false,
					animateOut: 'slideOutUp',
					animateIn: 'slideInUp'  
				});
				 
			},
			giftboxModalShown:function(){
			    if ($(window).width() >= 992) {
			        $(".owl-carousels-sticky").owlCarousel({
							items: 4,
							pagination: false,
							navigation: true,
							navigationText: false,
							rewindNav: false,
							lazyLoad: true,
							itemsMobile: [320, 4],
							itemsDesktop: [1199, 4],
							itemsDesktopSmall: [980, 4],
							itemsTablet: [767, 4],
							itemsTabletSmall: false
						});
			    }else{
			        $(".owl-carousels-sticky").owlCarousel({
							items: 1,
							pagination: false,
							navigation: true,
							navigationText: false,
							rewindNav: false,
							lazyLoad: true,
							itemsMobile: [320, 1],
							itemsDesktop: [1199, 1],
							itemsDesktopSmall: [980, 1],
							itemsTablet: [767, 1],
							itemsTabletSmall: false
						});
			    }
			       
			},
			handleMobileCheckout: function () {
				getWidget.cart().markDirty();
				navigation.goTo('/checkout');
			},
			onClickMoreStores: function (data, event) {
				var widget = this;
				var storeNumber = widget.myStore()[0].storeNumber;
				var lat = widget.myStore()[0].latitude;
				var long = widget.myStore()[0].longitude;
				var skuId = widget.skuRepoId();
				var quantity = widget.itemQty();
				var stores = helpers.storeLocatorUtils().getStores(lat, long, window.storeItems);
				var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);

				var reqData = {
					"skuId": skuId,
					"stsProductFlag": "",
					"storeNumber": storeNumber,
					"location": {
						"latitude": lat,
						"longitude": long
					},
					"quantity": quantity,
					"radiusLimit": 400,
					"limit": 4
				};

				var stores = helpers.storeLocatorUtils().getStores(lat, long, storeItems);
				var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);

				var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();
				helpers.storeLocatorUtils().getInventoryResponseIR(stores, quantity, storeNumber, 4, skuId, storeIds, reqData, "changeStorePDP", cartInventoryDetailsArrayObj);
				
				
			},
			getCartInventoryDetailsArray: function () {
				var widget = this;

				var itemShippingInventoryDetail = {};
				itemShippingInventoryDetail.quantity = 0;
				for (var i = 0; i < widget.cart().items().length; i++) {
					var item = widget.cart().items()[i];
					var trDeliveryDetails = JSON.parse(item.tr_delivery_details());
					if (item.catRefId === widget.skuRepoId()) {
						if (trDeliveryDetails.deliveryType === "2") {
							itemShippingInventoryDetail.storeId = "shipping";
							itemShippingInventoryDetail.quantity = Number(itemShippingInventoryDetail.quantity) + Number(item.quantity());
						} else {
							var found = false;
							for (var j = 0; j < cartInventoryDetailsArray.length; j++) {
								if (cartInventoryDetailsArray[j].storeId === trDeliveryDetails.store_inventory) {
									cartInventoryDetailsArray[j].quantity = cartInventoryDetailsArray[j].quantity + item.quantity();
									found = true;
									break;
								}
							}
							if (found) {
								continue;
							}
							var cartInventoryDetails = {};
							cartInventoryDetails.storeId = trDeliveryDetails.store_inventory;
							cartInventoryDetails.quantity = item.quantity();
							cartInventoryDetailsArray.push(cartInventoryDetails);
						}
					}
				}
				if (itemShippingInventoryDetail.hasOwnProperty('storeId')) {
					cartInventoryDetailsArray.push(itemShippingInventoryDetail);
				}
				return cartInventoryDetailsArray;
			},
			onLoad: function (widget) {
			    $.Topic('SKU_SELECTED').subscribe(function(){
					if(widget.product().childSKUs().length > 1){
						var skuId = "";
						var giftSku = "";
						if(widget.product().hasGift() && widget.product().giftSkuId() && widget.product().giftSkuId() !== "") {
							giftSku = "?giftSku=" + widget.product().giftSkuId();
						}
						widget.personalizeDisabledFlag(true);
						if(widget.selectedSku() !== null){
							skuId = widget.selectedSku().repositoryId;
							$.ajax({
									url : widget.site().extensionSiteSettings.externalSiteSettings.awsUrl + skuId + giftSku,
									method: "GET",
									success : function(data) {
										isPersonalizationPropertyExist = data.zones.length;
										if(data.zones.length > 0){
											newPersonalizationData = data;
											widget.generateExpressPhase(newPersonalizationData);
											widget.generateFonts(newPersonalizationData);
											widget.generalFonts(newPersonalizationData);
											widget.generateColorFills(newPersonalizationData);
											widget.generateDesigns(newPersonalizationData);
											widget.generateNographic(newPersonalizationData);
											var viewModel = ko.mapping.fromJS(newPersonalizationData);
											$.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
										}else if($('.error-msg').is(':visible') === false){
											$('.tr-xs-personalize-block').removeClass('hide');
											$(".personalizationBtn").hide();
											$(".addToCartBtn").show();
											$(".personalizationBtns").removeClass('hide');
										}
										widget.destroyInventorySpinner();
									},
									error: function() {
										getfallBackDatafromSku(widget);
									}
								});
								
						}
					}
			    });
			    
		
				
				$.Topic('PERSONALIZATION_BACK_BUTTON').subscribe(function(){
					var viewModel = ko.mapping.fromJS(newPersonalizationData);
					$.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
				});

				$.Topic('TR_PDP_NEW_PERSONALIZATION_DATA_LOAD_SUCCESS.memory').subscribe(function(){
					widget.checkPersonalizationExist();
					if(isPersonalizationPropertyExist > 0) {
						widget.personalizeDisabledFlag(false);
						widget.personalizeVisibilityFlag(true);
					} else {
						widget.personalizeDisabledFlag(true);
						widget.personalizeVisibilityFlag(false);
					}
				});
			    
			    widget.cart().addItem = function (data) {
			        var self = this;
                    if (data && data.childSKUs) {
                      var productFound = false;
                      var newQuantity;
                      // Create sub-items as CartItems, if they exist.
                      var childItems;
                      if (self.isConfigurableItem(data)) {
                        childItems = [];
                        childItems = self.getChildItemsDataAsCartItems(data);
                      }
                      if(data.selectedAddOnProducts && data.selectedAddOnProducts.length > 0) {
                        var addonChildItems = self.getAddonDataAsCartItems(data);
                        if(childItems && childItems.length > 0) {
                          childItems = childItems.concat(addonChildItems);
                        } else {
                          childItems = [];
                          childItems = addonChildItems;
                        }
                      }
                      // Check for line item only if there are no child items.
                      var cartItem = null;
                      if (!data.commerceItemId && self.isConfigurableItem(data)) {
                        cartItem = self.getConfigurableCartItem(data.id, data.childSKUs[0].repositoryId, data.commerceItemId);
                      } else {
                        if(data.selectedSku) {
                          cartItem = self.getCartItem(data.id, data.selectedSku.repositoryId, data.commerceItemId);
                        } else {
                          cartItem = self.getCartItem(data.id, data.childSKUs[0].repositoryId, data.commerceItemId);
                        }
                        //Search for corresponding cart item only if line items are to be combined, else proceed just like a new item being added to cart.
                        if(!(self.combineLineItems == CCConstants.COMBINE_YES || (cartItem && cartItem.isUpdate()))){
                          cartItem = null;
                        }
                      }
                      if(self.isProductWithAddons(data) && cartItem && !cartItem.isUpdate()) {
                           // In case of Product with add-ons, and if Product is not a CPQ product, always create
                           // a new cart item if its not under update, as shopper may have different shopperInputs and need not decide based
                           // on the actual ShopperInput data
                        cartItem = null;
                      }
                      if (cartItem !== null ) {
                        if(!cartItem.isUpdate()) {
                          newQuantity = cartItem.quantity() + data.orderQuantity;
                          cartItem.quantity(newQuantity);
                          cartItem.updatableQuantity(cartItem.quantity());
            
                          // Add giftWithPurchaseSelections of the cart item
                          if (data.giftProductData) {
                            cartItem.giftWithPurchaseSelections = [
                              {
                                "giftWithPurchaseIdentifier": data.giftProductData.giftWithPurchaseIdentifier,
                                "promotionId": data.giftProductData.promotionId,
                                "giftWithPurchaseQuantity": data.giftProductData.giftWithPurchaseQuantity
                              }];
                          }
                          $.Topic(pubsub.topicNames.CART_UPDATE_QUANTITY_SUCCESS).publishWith(null,
                                  [{message:"success", cartItem: cartItem, prodData:data, createNewSGR: true}]);
                            if(this.storeConfiguration.isLargeCart() === true){
                              self.updateItemPriceForLargeCart(data,cartItem);
                              self.updateItemShippingGroupRelationShipForLargeCart(data,cartItem);
                            }
                        } else {
                            if (cartItem.shippingGroupRelationships().length > 1) {
                                cartItem.productData(data);
                                cartItem.updatableQuantity(data.orderQuantity);
                                $.Topic(pubsub.topicNames.CART_UPDATE_QUANTITY_SUCCESS).publishWith(null,
                                        [{message:"success", createNewSGR: false, data: cartItem, prodData: data}]);
                                cartItem.quantity(data.orderQuantity);
                            }
                            else {
                              var sum =0;
                              cartItem.productData(data);
                              cartItem.shippingGroupRelationships()[0].quantity(data.orderQuantity);
                              cartItem.shippingGroupRelationships()[0].updatableQuantity(data.orderQuantity);
                              cartItem.updatableQuantity(data.orderQuantity);
                              for(var index=0; index< cartItem.shippingGroupRelationships().length; index++) {
                                  sum+= parseFloat(cartItem.shippingGroupRelationships()[index].quantity());
                                  cartItem.shippingGroupRelationships()[index].catRefId = data.childSKUs[0].id;
                              }
                              cartItem.quantity(sum);
                              cartItem.catRefId = data.childSKUs[0].id;
                            }
                            if(data.selectedAddOnProducts && data.selectedAddOnProducts.length > 0) {
                                var addonChildItems = self.getAddonDataAsCartItems(data);
                                //to check if the cartItem.childItems exists or not
                                if(cartItem.childItems && cartItem.childItems.length > 0) {
                                  cartItem.childItems = cartItem.childItems.filter(function(childItem) {
                                    return !childItem.addOnItem;
                                  });
                                }
                               /* duplicate condition checks because above we are filtering the cartItem.childItems
                                 and need to check the length again. This will be the case when the product has a
                                 CPQ childItems and add on products. If CPQ childitems exists, the new addOns will
                                 be concatinated, otherwise the value can be over-written. */
                                if(cartItem.childItems && cartItem.childItems.length > 0) {
                                  cartItem.childItems = cartItem.childItems.concat(addonChildItems);
                                } else {
                                  cartItem.childItems = [];
                                  cartItem.childItems = addonChildItems;
                                }
                            }
                        }
                        productFound = true;
                      }
                      // Adding a condition for reconfiguration flow. In this case the commerceItemId will
                      // be null. However the configuratorId should be set for the item.
                      if ((data.commerceItemId == null) && self.isConfigurableItem(data)) {
                        cartItem = self.getCartItemForReconfiguration(data.id, data.childSKUs[0].repositoryId, data.configuratorId);
                        if (cartItem !== null) {
                          // Update the childItems
                          // Do not overwrite the childItems directly, as there could be add-on product childItems
                          // existing with the CPQ main product item
                          var addOnProductItems = [];
                          for(var index=0; index<cartItem.childItems.length; index++) {
                            if (cartItem.childItems[index].addOnItem) {
                              // If the childItem contains another level of childItems, as in multi level
                              // add-ons then it will be automatically handled as we are persisting the
                              // entire childItem object
                              addOnProductItems.push(cartItem.childItems[index]);
                            }
                          }
                          if (addOnProductItems.length > 0) {
                            childItems = childItems.concat(addOnProductItems);
                          }
                          cartItem.childItems = childItems;
                          cartItem.externalPrice(data.price);
                          cartItem.externalData((data.externalData || []).map(function (data) {
                            return new CartItemExternalData(data);
                          }));
                          cartItem.actionCode(data.actionCode);
                          cartItem.externalRecurringCharge(data.externalRecurringCharge);
                          cartItem.externalRecurringChargeFrequency(data.externalRecurringChargeFrequency);
                          cartItem.externalRecurringChargeDuration(data.externalRecurringChargeDuration);
                          cartItem.assetId(data.assetId);
                          cartItem.serviceId(data.serviceId);
                          cartItem.customerAccountId(data.customerAccountId);
                          cartItem.billingAccountId(data.billingAccountId);
                          cartItem.serviceAccountId(data.serviceAccountId);
                          cartItem.billingProfileId(data.billingProfileId);
                          cartItem.activationDate(data.activationDate);
                          cartItem.deactivationDate(data.deactivationDate);
                          cartItem.transactionDate(data.transactionDate);
                          cartItem.clearUnpricedError();
                          cartItem.updatableQuantity.rules.remove( function(updatableQuantity) {
                            return updatableQuantity.rule == 'max';
                          });
                          self.getCartAvailability();
                          // Add giftWithPurchaseSelections of the cart item
                          if (data.giftProductData) {
                            cartItem.giftWithPurchaseSelections = [
                              {
                                "giftWithPurchaseIdentifier": data.giftProductData.giftWithPurchaseIdentifier,
                                "promotionId": data.giftProductData.promotionId,
                                "giftWithPurchaseQuantity": data.giftProductData.giftWithPurchaseQuantity
                              }
                            ];
                          }
                          productFound = true;
                        }
                      }
                      // If product is not in the cart then add it with the quantity set on the new product.
                      if (!productFound) {
                        newQuantity = data.orderQuantity;
            
                        var cartItemData = {
                          productId: data.id,
                          productData: data,
                          quantity: newQuantity,
                          catRefId: data.childSKUs[0].repositoryId, 
                          selectedOptions: data.selectedOptions,
                          currency: self.currency,
                          externalData: data.externalData,
                          actionCode: data.actionCode,
                          lineAttributes: self.lineAttributes,
                          externalRecurringCharge: data.externalRecurringCharge,
                          externalRecurringChargeFrequency: data.externalRecurringChargeFrequency,
                          externalRecurringChargeDuration: data.externalRecurringChargeDuration,
                          assetId: data.assetId,
                          serviceId: data.serviceId,
                          customerAccountId: data.customerAccountId,
                          billingAccountId: data.billingAccountId,
                          serviceAccountId: data.serviceAccountId,
                          billingProfileId: data.billingProfileId,
                          activationDate: data.activationDate,
                          deactivationDate: data.deactivationDate,
                          transactionDate: data.transactionDate,
                          addOnItem: data.addOnItem,
                          shopperInput: data.shopperInput,
                          selectedStore: typeof data.selectedStore == "function" ? data.selectedStore() : data.selectedStore,
                          availablePickupDateTime : data.availablePickupDateTime
                        };
            
                        if (self.isConfigurableItem(data)) {
                          // Handle configurable items. Expect to get external prices for
                          // the configurable items.
                          $.extend(cartItemData, {
                            configuratorId: data.configuratorId,
                            childItems: childItems,
                            externalPrice: data.price,
                            externalPriceQuantity: -1
                          })
                        } else if (self.isProductWithAddons(data)) {
                          $.extend(cartItemData, {
                            childItems: childItems
                          })
                          // If the product contains add-ons and if the product is externally priced,
                          // then as per the existing restriction on the ExternalPricingCalculator only
                          // -1 can be passed to the externalPriceQuantity
                          var externalQuantity = data.externalPriceQuantity;
                          // Iterate over the childItems and check if any childItem is externally Priced
                          // such that the externalQuantity should be -1 in that case
                          for(var i=0; i<childItems.length; i++) {
                            if(childItems[i].externalPrice) {
                              externalQuantity = -1;
                              break;
                            }
                          }
            
                          if(data.externalPrice) {
                            $.extend(cartItemData, {
                              externalPrice: data.externalPrice,
                              externalPriceQuantity: externalQuantity
                            })
                          }
                        } else if (data.externalPrice && data.externalPriceQuantity) {
                          $.extend(cartItemData, {
                            externalPrice: data.externalPrice,
                            externalPriceQuantity: data.externalPriceQuantity
                          })
                        }
            
                        var productItem = new CartItem(cartItemData);
                        //Update item total for large cart
                        if(this.storeConfiguration.isLargeCart() === true){
                          self.updateItemPriceForLargeCart(data,productItem);
                          self.updateItemShippingGroupRelationShipForLargeCart(data,productItem);
                          /*var price = data.childSKUs[0].salePrice ? data.childSKUs[0].salePrice :data.childSKUs[0].listPrice;
                          productItem.itemTotal(productItem.itemTotal()+price*data.orderQuantity);   */
                        }
                        // SKU properties of the product item.
                        productItem.skuProperties = data.skuProperties;
                        productItem.selectedSkuProperties = data.selectedSkuProperties;
            
                        self.items.push(productItem);
                      }
                      self.isDirty(false);
                      if(this.storeConfiguration.isLargeCart() === true){
                        self.updateCartItemDataForLargeCart(data);
                        self.updateAllItemsArray();
                      }
                      self.markDirty();
                    }
            
                  };

				widget.formatRelatedProducts = function (pProducts) {
					var formattedProducts = [];
					var productsLength = pProducts.length;
					for (var index = 0; index < productsLength; index++) {
						if (pProducts[index]) {
							formattedProducts.push(new product(pProducts[index]));
						}
					}
					return formattedProducts;
				};

				widget.updateSpanClass = function () {
					var classString = "";
					var phoneViewItems = 0,
						tabletViewItems = 0,
						desktopViewItems = 0,
						largeDesktopViewItems = 0;
					if (this.itemsPerRow() == this.itemsPerRowInPhoneView()) {
						phoneViewItems = 12 / this.itemsPerRow();
					}
					if (this.itemsPerRow() == this.itemsPerRowInTabletView()) {
						tabletViewItems = 12 / this.itemsPerRow();
					}
					if (this.itemsPerRow() == this.itemsPerRowInDesktopView()) {
						desktopViewItems = 12 / this.itemsPerRow();
					}
					if (this.itemsPerRow() == this.itemsPerRowInLargeDesktopView()) {
						largeDesktopViewItems = 12 / this.itemsPerRow();
					}

					if (phoneViewItems > 0) {
						classString += "col-xs-" + phoneViewItems;
					}
					if ((tabletViewItems > 0) && (tabletViewItems != phoneViewItems)) {
						classString += " col-sm-" + tabletViewItems;
					}
					if ((desktopViewItems > 0) && (desktopViewItems != tabletViewItems)) {
						classString += " col-md-" + desktopViewItems;
					}
					if ((largeDesktopViewItems > 0) && (largeDesktopViewItems != desktopViewItems)) {
						classString += " col-lg-" + largeDesktopViewItems;
					}

					widget.spanClass(classString);

				};

				widget.checkResponsiveFeatures = function (viewportWidth) {
					widget.isDeskTopMode(false);
					if (viewportWidth > CCConstants.VIEWPORT_LARGE_DESKTOP_LOWER_WIDTH) {
						if (widget.viewportMode() != CCConstants.LARGE_DESKTOP_VIEW) {
							widget.viewportMode(CCConstants.LARGE_DESKTOP_VIEW);
							widget.itemsPerRow(widget.itemsPerRowInLargeDesktopView());
						}
					} else if (viewportWidth > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH &&
						viewportWidth <= CCConstants.VIEWPORT_LARGE_DESKTOP_LOWER_WIDTH) {
						if (widget.viewportMode() != CCConstants.DESKTOP_VIEW) {
							widget.viewportMode(CCConstants.DESKTOP_VIEW);
							widget.itemsPerRow(widget.itemsPerRowInDesktopView());
						}
					} else if (viewportWidth >= CCConstants.VIEWPORT_TABLET_LOWER_WIDTH &&
						viewportWidth <= CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
						if (widget.viewportMode() != CCConstants.TABLET_VIEW) {
							widget.viewportMode(CCConstants.TABLET_VIEW);
							widget.itemsPerRow(widget.itemsPerRowInTabletView());
						}
					} else if (widget.viewportMode() != CCConstants.PHONE_VIEW) {
						widget.viewportMode(CCConstants.PHONE_VIEW);
						widget.itemsPerRow(widget.itemsPerRowInPhoneView());
					}
					widget.updateSpanClass();
				};

				widget.checkResponsiveFeatures($(window)[0].innerWidth || $(window).width());
				$(window).resize(
					function () {
						widget.checkResponsiveFeatures($(window)[0].innerWidth || $(window).width());
						widget.viewportWidth($(window)[0].innerWidth || $(window).width());
					});


				widget.relatedProductGroups = ko.computed(function () {
					var groups = [];
					if (widget.product().relatedProducts != null) {

						widget.numberOfRelatedProductsToShow = widget.numberOfRelatedProducts() < widget.product().relatedProducts.length ?
							widget.numberOfRelatedProducts() : widget.product().relatedProducts.length;
						for (var index = 0; index < widget.numberOfRelatedProductsToShow; index++) {
							if (index % widget.itemsPerRow() == 0) {
								groups.push(ko.observableArray([widget.product().relatedProducts[index]]));
							} else {
								groups[groups.length - 1]().push(widget.product().relatedProducts[index]);
							}
						}
					}

					return groups;

				}, widget);

				$("#color").click(function () {
					$("a").css("color", "white");
				});

				getWidget = widget;
				getWidget.cart().combineLineItems = CCConstants.COMBINE_NO;
				CCConstants.COMBINE_YES = "yes";

				$.Topic(pubsub.topicNames.CART_ADD_SUCCESS).subscribe(function () {
					displayStoreChangeMessage = false;
				});

				$.Topic("PROCESS_INVENTORY_PDP_ZIP_SERACH_IR").subscribe(function (inventoryData, callSource) {

					$("#tr-more-store .tr-row").hide();
					$(".service-error-msg").text("");

					widget.otherStores([]);

					if (inventoryData.pickup[0].available) {

						var otherStoreArr = [];
						var x = inventoryData.pickup.length;
						if (callSource == "changeStorePDP") {
							for (var i = 1; i < x; i++) {
								otherStoreArr.push(inventoryData.pickup[i].store);
							}
						} else if (callSource == "changeStorePDPZip") {
							for (var i = 0; i < x; i++) {
								otherStoreArr.push(inventoryData.pickup[i].store);
							}
						}
						widget.otherStores(otherStoreArr);
					}
					var displayStores = widget.otherStores();

					if (callSource == "changeStorePDP") {
						$("#tr-e-available-store").modal('show');
					}
					if (displayStores.length === 0) {
						$("#tr-more-store .tr-row").hide();
						$(".service-error-msg").text("No nearby stores are found for the selected product and quantity.");
						return;
					} else {
						$("#tr-more-store .tr-row").show();
					}


				});

				$.Topic("STORE_STOCK_ERROR_MESSAGE").subscribe(function (errorType) {
					widget.storeStockErrorMessage(errorType);
				});

				$.Topic("VARIANT_NO_STOCK_ERROR_MESSAGE").subscribe(function (errorType) {
					if (widget.variantOptionsArray().length > 0) {
						$(".tr-store-stock-error").hide().text("");
						$(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
						$(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);
						$(".personalizationBtns").addClass('hide');
                        $('.tr-xs-personalize-block').addClass('hide');

						if (widget.variantOptionsArray().length === 1) {
							$(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
							$(".personalizationBtns").addClass('hide');
                            $('.tr-xs-personalize-block').addClass('hide');
						}
					}
					$("[id^=personalize-button]").prop("disabled", true);
					$("[id^=addToCart-button]").prop("disabled", true);
				});

				$.Topic("PROCESS_INVENTORY_PERSONALIZE_QTY_IR").subscribe(function (requestObj, locationStock) {
					widget.storeStockErrorMessage("");
					var cartQuantity = 0;
					for (var i = 0; i < widget.cart().items().length; i++) {
						var item = widget.cart().items()[i];
						var trDeliveryDetails = JSON.parse(item.tr_delivery_details());
						if (item.catRefId === requestObj.skuNumber && trDeliveryDetails.store_inventory == requestObj.storeNumber) {
							cartQuantity += item.quantity();
						}
					}
					var computedqty = Number(requestObj.skuQuantity) + Number(cartQuantity);
					var computedStock = locationStock.stockLevel - locationStock.stockThreshold - computedqty;

					$(".tr-store-stock-error").hide().text("");
					var shippingType = $("[name=optradio]:checked").val();
					if (shippingType == 2) {
						computedStock = widget.availableSTHInventory() - computedqty;
					}


					var variantOptions = widget.variantOptionsArray(),
						sku_obj = widget.getSelectedSku(variantOptions);
					if (widget.product().childSKUs().length == 1) {
						sku_obj = widget.product().childSKUs()[0];
					}

					if (computedStock < 0) {
						if (shippingType == 1) { //pickup
							$(".tr-store-stock-error").show();
							$.Topic("STORE_STOCK_ERROR_MESSAGE").publish("noSelectedStore");
						} else {
							$(".tr-store-stock-error").show();
							$.Topic("STORE_STOCK_ERROR_MESSAGE").publish("noProduct");
						}

						quantityChangeDeliveryTriggered = false;
						//$.Topic('SKU_SELECTED').publish(widget.product(), sku_obj, variantOptions, requestObj.skuQuantity);
					} else {
						quantityChangeDeliveryTriggered = false;
					}

					var skuId = widget.skuRepoId();
					var skuQuantity = widget.itemQty();
					var latitude = $('.my-store-lat').text();
					var longitude = $('.my-store-lon').text();
					var storeNumber = $('.my-store-id').text();
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": skuQuantity,
						"radiusLimit": 400,
						"limit": 4
					};
					var stores = helpers.storeLocatorUtils().getStores(latitude, longitude, storeItems);

					var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);
					var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();


					helpers.storeLocatorUtils().getInventoryResponseIR(stores, computedqty, storeNumber, 4, skuId, storeIds, reqData, "pdpLoad", cartInventoryDetailsArrayObj);

				});

				$.Topic("GIFT_ITEM").subscribe(updateHeader);

				function updateHeader(giftData) {
					//var giftName = giftData[0].displayName;
					widget.giftTitle(" with Free " + giftData[0].displayName);
				}


				$.Topic("PROCESS_INVENTORY_PDP_IR").subscribe(function (data) {
					var shippingType = $("[name=optradio]:checked").val();
					var storeNumber = "920";
					widget.storeStockErrorMessage("");

					if (!data.pickup[0].available && !data.shipping[0].available) {

						$(".tr-store-stock-error").show();
						widget.storeStockErrorMessage("noProduct");

					} else {
						if (shippingType == 1) { //pickup
						    if (data.pickup.length > 0 && !data.pickup[0].available) {
						        $(".tr-store-stock-error").text('');
						        $(".tr-store-stock-error").show();
								widget.storeStockErrorMessage("noSelectedStore");
						        //$("#ship-to-home").click();
						        setTimeout(function(){
						            $("#ship-to-home").prop('checked', true);
						            $('.personalizationBtns').removeClass('hide'); 
						        },200);
						    } else {
    							storeNumber = widget.myStore()[0] ? widget.myStore()[0].storeNumber : $('.my-store-id').text();
    							var responseStoreId = data.pickup.length > 0 && data.pickup[0].available ? data.pickup[0].store.id : null;
    							if ((!responseStoreId || responseStoreId !== storeNumber) && displayStoreChangeMessage) {
    								if (widget.myStore()[0].onHandQuantity > widget.itemQty()) {
    									$(".tr-store-stock-error").show();
    									$(".tr-store-stock-error").text("The nearest store available for pickup today is " + data.pickup[0].store.name + ".")
    								} else {
    									$(".tr-store-stock-error").show();
    									widget.storeStockErrorMessage("noSelectedStore");
    								}
    							}
						    }
						} else {
							if (!data.shipping[0].available) {
								//$(".tr-store-stock-error").show();
								//widget.storeStockErrorMessage("noProduct");
								widget.displayShipToHome(false);
								$('#available-today').click();
							}
						}
					}

					displayStoreChangeMessage = true;
					inventoryDataIR = data;
					//$(".tr-store-stock-error").text("");
					$.Topic("INVENTORY_PERSONALIZE_STH_QTY.memory").publish(0);
					if (data.shipping[0].available) {
						widget.displayShipToHome(true);
						widget.availableSTHInventory(data.shipping[0].skuQuantity);
						$.Topic("INVENTORY_PERSONALIZE_STH_QTY.memory").publish(data.shipping[0].skuQuantity);
					}
					if (data.pickup[0].available) {
						widget.displayOpus(true);
						widget.availableTodayStore(data.pickup[0].store.name);
						widget.availableState(data.pickup[0].store.state);
						widget.availableCity(data.pickup[0].store.city);

						widget.availableQuantity(data.pickup[0].skuQuantity);
						// widget.availableDateText("Pick Up " + data.pickup[0].store.deliveryDate);

						var myStoreArr = [];
						myStoreArr.push(data.pickup[0].store);
						widget.myStore(myStoreArr);

						var otherStoreArr = [];
						var x = data.pickup.length;
						for (var i = 1; i < x; i++) {
							otherStoreArr.push(data.pickup[i].store);
						}
						widget.otherStores(otherStoreArr);
					}
                    
                    $(".personalizationBtns").removeClass('hide');
				    $('.tr-xs-personalize-block').removeClass('hide');
						        
					if ($(".tr-store-stock-error").is(':visible')) {
						if (widget.storeStockErrorMessage() == "noSelectedStore") {
							if (data.pickup[0].available) {
								$(".tr-store-stock-error").text("Due to low inventory, the nearest store available for pickup today is " + data.pickup[0].store.name + ". Select ADD TO BAG to continue, or choose Change Store or Ship to Home.")
							} else if (data.shipping[0].available) {
								$(".tr-store-stock-error").text("This product is no longer available for Purchase In Store, Please change to Ship to home.")
								$(".personalizationBtns").addClass('hide');
						        $('.tr-xs-personalize-block').addClass('hide');
							}
                            
						} else if (widget.storeStockErrorMessage() == "noProduct") {
							if (!data.shipping[0].available && data.pickup[0].available) {
								$(".tr-store-stock-error").text("This product is no longer available for Ship to Home.Please change to Pickup in Store.");
							} else if (!data.shipping[0].available && !data.pickup[0].available) {
							    widget.buttonVisibilityFlag(false);
							    widget.personalizeVisibilityFlag(false);
								$(".tr-store-stock-error").text("This product is no longer available for Purchase.");
								$(".personalizationBtns").addClass('hide');
						        $('.tr-xs-personalize-block').addClass('hide');
							}

						}
						//$(".personalizationBtns").addClass('hide');
                        //$('.tr-xs-personalize-block').addClass('hide');
					} else {
						widget.storeStockErrorMessage('');
					}

					if (data.pickup[0].available === false && data.shipping[0].available === false && widget.variantOptionsArray().length == 0) {
						$(".tr-button").attr("disabled", "disabled");
						widget.buttonVisibilityFlag(false);
						widget.personalizeVisibilityFlag(false);
						$(".tr-store-stock-error").text("This product is no longer available for Purchase.");
						$(".tr-store-stock-error").show();
						$(".personalizationBtns").addClass('hide');
                        $('.tr-xs-personalize-block').addClass('hide');
					} else if (data.pickup[0].available === false && data.shipping[0].available === false && widget.variantOptionsArray().length > 0) {
						$(".tr-store-stock-error").hide().text("");
						$(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
						$(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);
						$(".personalizationBtns").addClass('hide');
                        $('.tr-xs-personalize-block').addClass('hide');

						if (widget.variantOptionsArray().length === 1) {
							$(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
							$(".personalizationBtns").addClass('hide');
                            $('.tr-xs-personalize-block').addClass('hide');
						}
					}

					var shippingType = $("[name=optradio]:checked").val();
					if ((shippingType == 2 && data.shipping[0].available === true) || (shippingType == 1 && data.pickup[0].available === true)) {
						$("[id^=personalize-button]").prop("disabled", false);
					} else {
						$("[id^=personalize-button]").prop("disabled", true);
					}

					// 

					widget.destroyInventorySpinner();
					$.Topic("INVENTORY_PERSONALIZE_STORE_DATA_IR.memory").publish(widget.myStore(), widget.otherStores());
				});
				$.Topic("CURRENT_PERSONALIZATION_DATA").subscribe(function (data) {
				    console.log("CURRENT_PERSONALIZATION_DATA data", data);
					currentPersonalizationData = data;
					widget.handleCustomAddToCart();
				});
				_this = this;
				widget.loadCarousel();
				var lastDataCacheTimeStamp = storageApi.getInstance().getItem("lastDataCacheTimeStamp");
				if (!lastDataCacheTimeStamp) {
					lastDataCacheTimeStamp = 0;
				}
				refreshData = false;

				/*function checkAndCacheData(refreshData) {
					if (refreshData) {
						var d = new Date();
						var n = d.getTime();
						storageApi.getInstance().setItem("lastDataCacheTimeStamp", n);
						var arr = [];
						for (var i = 0; i < localStorage.length; i++) {
							if ((localStorage.key(i).indexOf('productZones') !== -1) || (localStorage.key(i).indexOf('zones') !== -1)) {
								arr.push(localStorage.key(i));
							}
						}
						for (var j = 0; j < arr.length; j++) {
							storageApi.getInstance().removeItem(arr[j]);
						}
					}
					if (refreshData || !storageApi.getInstance().getItem("siteGraphicCats")) {
						var siteGraphicCatIds = {};
						siteGraphicCatIds['fields'] = "items.displayName,items.designSKUs,items.id";
						siteGraphicCatIds[CCConstants.PRODUCT_IDS] = "catTopHolidayDesigns,catHolidaySeason,catLoveRomance,catGradEdAcademics,catReligious,catWedding,catCelebration,catBabiesChildren,catSports,catMustache,catSymbols,catChineseSymbols,catScrolls,catMonogramDiamondO,catMonogramDiamondS,catMonogramGiraffePrint,catMonogramLeopardPrint,catMonogramZebraPrint,catMonogramTigerPrint,catAnimals,catBugsInsects,catArtMusic,catCustomWeddingLogo,catDragonsSnakesSkulls,catEarthMoonStars,catFlags,catMascots,catOccupations,catTransportationTravel,catUSCitiesStates,catWestern,catZodiac,catHalloween";
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, siteGraphicCatIds, function (graphicCats) {
							storageApi.getInstance().setItem("siteGraphicCats", JSON.stringify(graphicCats));
						});
					}
					if (refreshData || !storageApi.getInstance().getItem("siteColors")) {
						var siteClolorIds = {};
						siteClolorIds['fields'] = "items.id,items.listPrice,items.salePrice,items.upchargeSKU,items.blue,items.red,items.green,items.description";
						siteClolorIds[CCConstants.PRODUCT_IDS] = "Color_WIT_H,Color_VIL_H,Color_TLP_H,Color_SLV_H,Color_NOC_H,Color_MNT_H,Color_IRB_H,Color_HLB_H,Color_GOL_H,Color_EMD_H,Color_CPR_H,Color_BCK_H,Color_RBY_H";
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, siteClolorIds, function (colors) {
							storageApi.getInstance().setItem("siteColors", JSON.stringify(colors));
						});
					}
					if (refreshData || !storageApi.getInstance().getItem("siteFonts")) {
						var siteFontIds = {};
						siteFontIds["fields"] = "items.listPrice,items.salePrice,items.id,items.description,items.subCode,items.upchargeSKU,items.fontLeft,items.fontRight,items.fontCenter,items.tr_pulsePreviewName";
						siteFontIds[CCConstants.PRODUCT_IDS] = "Font_AAC_S,Font_AMZ_S,Font_ARE_S,Font_AS2_H,Font_ASH_H,Font_ASI_H,Font_AVG_H,Font_BAL_H,Font_BBB_S,Font_BBL_S,Font_BDP_S,Font_BLC_H,Font_BLE_S,Font_BLK_H,Font_BMO_H,Font_BOC_H,Font_BOD_H,Font_BOK_H,Font_BOO_H,Font_BOY_H,Font_BRS_S,Font_CAB_H,Font_CAE_S,Font_CCM_H,Font_CDM_H,Font_CEM_H,Font_CES_H,Font_CHE_S,Font_CHF_H,Font_CHS_H,Font_CLB_S,Font_CLK_H,Font_CLM_H,Font_CLN_H,Font_CLS_H,Font_CMA_H,Font_CME_S,Font_CMG_H,Font_CMH_H,Font_CML_H,Font_CMM_H,Font_CMX_H,Font_CNT_S,Font_COM_H,Font_COR_H,Font_COS_H,Font_COU_H,Font_CPB_H,Font_CPM_H,Font_CPT_H,Font_CRL_S,Font_CSE_S,Font_CSL_H,Font_CSM_H,Font_CSX_H,Font_CTE_S,Font_CTM_H,Font_CTN_H,Font_CTX_H,Font_CUE_S,Font_CUR_H,Font_CVM_H,Font_DIA_H,Font_DID_S,Font_DIE_S,Font_DIM_S,Font_DMO_H,Font_DMS_S,Font_EDW_S,Font_EGM_H,Font_ELG_H,Font_ENG_H,Font_FAS_H,Font_FCM_H,Font_FCM_S,Font_FDL_H,Font_FGT_S,Font_FSC_H,Font_FUT_H,Font_FUV_H,Font_FUX_H,Font_GAB_H,Font_GIL_H,Font_GIR_H,Font_GLS_H,Font_GOR_H,Font_GRE_S,Font_GRL_H,Font_GVB_H,Font_H1_S,Font_HDS_H,Font_HHS_H,Font_HMA_S,Font_HMD_S,Font_HMM_S,Font_HPB_H,Font_HPG_H,Font_HS2_H,Font_HSC_H,Font_IMG_H,Font_IMV_H,Font_ISA_H,Font_ISB_H,Font_ISM_H,Font_ISW_H,Font_ISX_H,Font_JAX_S,Font_KBL_H,Font_KDS_S,Font_KLB_H,Font_KMK_H,Font_KSC_H,Font_KZN_H,Font_LAL_H,Font_LAU_H,Font_LBL_H,Font_LCS_H,Font_LEO_H,Font_LNO_H,Font_LSC_H,Font_LSM_H,Font_LVD_H,Font_MLD_S,Font_MME_S,Font_MNS_S,Font_MST_H,Font_MYR_H,Font_NGS_H,Font_NTB_H,Font_NWE_S,Font_OE2_H,Font_OE5_H,Font_OEI_H,Font_OMO_H,Font_ONM_H,Font_ONX_H,Font_OPT_H,Font_PLB_H,Font_PLO_H,Font_PSC_H,Font_PSM_S,Font_QRY_H,Font_RCW_H,Font_RMG_H,Font_RMN_H,Font_ROM_H,Font_RSM_H,Font_RYM_S,Font_SCE_S,Font_SCR_H,Font_SFL_H,Font_SHD_H,Font_SIG_H,Font_SMS_S,Font_SOM_H,Font_SPE_H,Font_SPT_H,Font_STL_H,Font_TBB_H,Font_TCM_H,Font_TDM_H,Font_TDX_H,Font_TGB_H,Font_TGE_H,Font_THA_H,Font_TI2_H,Font_TIG_H,Font_TIM_H,Font_TIS_H,Font_TIX_H,Font_TMC_H,Font_TMM_H,Font_TMX_H,Font_TS2_H,Font_TSG_H,Font_TSM_H,Font_TSS_H,Font_TSX_H,Font_TTC_H,Font_VAN_H,Font_VIM_S,Font_VIN_H,Font_VLC_H,Font_VMM_H,Font_VMX_H,Font_VNA_H,Font_VTS_H,Font_WBK_H,Font_WFT_H,Font_WLG_S,Font_WME_S,Font_WRG_H,Font_WSR_H,Font_ZEB_H";
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, siteFontIds, function (fonts) {
							storageApi.getInstance().setItem("siteFonts", JSON.stringify(fonts));
						});
					}
					if (refreshData || !storageApi.getInstance().getItem("siteExpressPhrases")) {
						var expressPhraseIds = {};
						expressPhraseIds["fields"] = "items.listPrice,items.salePrice,items.expressPhraseDetailsSKUs,items.hardlineSoftline,items.displayName,items.itemType,items.repositoryId,items.discountMultipleInstances,items.firstWordDiscountable,items.fullImageURLs,items.description,items.expressPhraseExample,items.id,items.tr_expressPhraseType";
						expressPhraseIds[CCConstants.PRODUCT_IDS] = "194071,607665,607652,685137,713245,713258,718897,334684,337047,480798,337061,337054,531003,640020,795920,644754,343869,347488,219396,562593,335392,634340,634353,712851,685137,735760,337050,doNotPersonalize";
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, expressPhraseIds, function (expressPhrases) {
							storageApi.getInstance().setItem("siteExpressPhrases", JSON.stringify(expressPhrases));
							var siteExpressPhraseDetailsIds = '';
							for (var k = 0; k < expressPhrases.length; k++) {
								if (siteExpressPhraseDetailsIds.indexOf(expressPhrases[k].expressPhraseDetailsSKUs) === -1) {
									if (k === 0) {
										siteExpressPhraseDetailsIds = expressPhrases[k].expressPhraseDetailsSKUs;
									} else {
										siteExpressPhraseDetailsIds = siteExpressPhraseDetailsIds + ',' + expressPhrases[k].expressPhraseDetailsSKUs;
									}
								}
							}
							var expressPhraseDetailsId = {};
							expressPhraseDetailsId['fields'] = "items.description,items.lineNumber,items.characterCount,items.id,items.inputCapable,items.spaceBefore,items.spaceAfter,items.tr_sequence,items.displayName,items.tr_designList,items.tr_designListType,items.required,items.tr_additionalDesignCost,items.tr_previewOverrideFontId,items.tr_previewOverrideColorId";
							expressPhraseDetailsId[CCConstants.PRODUCT_IDS] = siteExpressPhraseDetailsIds;
							ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, expressPhraseDetailsId, function (expressPhraseDetails) {
								storageApi.getInstance().setItem("siteExpressPhraseDetails", JSON.stringify(expressPhraseDetails));
							});
						});
					} */
					if (refreshData || !storageApi.getInstance().getItem("siteWordCost")) {
						var wordsArrayIds = [];
						wordsArrayIds["fields"] = "items.childSKUs,items.listPrice,items.salePrice,items.id";
						wordsArrayIds[CCConstants.PRODUCT_IDS] = "firstWord,anyWord,personalizationWords";

						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, wordsArrayIds, function (wordSKUs) {
							storageApi.getInstance().setItem("siteWordCost", JSON.stringify(wordSKUs));
						});
					}
					/*if (refreshData || !storageApi.getInstance().getItem("siteDesignSkus")) {
						ccRestClient.authenticatedRequest('/ccstoreui/v1/products?fields=items.description,items.id,items.displayName,items.designClass,items.itemType,items.hardlineSoftline,items.listPrice,items.salePrice&q=type eq "DesignSKU"', {},
							function (data) {
								storageApi.getInstance().setItem("siteDesignSkus", JSON.stringify(data));
							},
							function (data) {

							});
					}
				}*/

				ccRestClient.authenticatedRequest("/ccstoreui/v1/publish", {},
					function (data) {
						var clearCache = false;
						if ((data.lastPublishedTimeStamp * 1) > (lastDataCacheTimeStamp * 1)) {
							clearCache = true;
						}
						//checkAndCacheData(clearCache);
					},
					function (data) {
						//checkAndCacheData(true);
					});
				if (!storeItems || storeItems.length === 0) {
					$.ajax({
						url: "/storelocator/stores.json",
						type: "get",
						async: false,
						success: function (data) {
							storeItems = data;
							window.storeItems = storeItems;
						},
						error: function () {}
					});
				}
				_this.onConfigReady(widget);
				$.Topic(pubsub.topicNames.CART_PRICE_COMPLETE).subscribe(function () {
					setTimeout(function () {

						if (!setDetailsToItem) {
							return;
						}
						setDetailsToItem = false;
						var localCart = getWidget.cart();
						var cartItems = localCart.allItems();
						var itemIndex = 0;
						var cartUpdated = false;


						if (currentPersonalizationData && currentPersonalizationData.length > 0) {
							for (var i = 0; i < cartItems.length; i++) {
								if (cartItems[i].catRefId !== 'personalization1') {
									if (cartItems[i].tr_isPersonalized() && cartItems[i].tr_delivery_details()) {
										var deliveryDetails = JSON.parse(cartItems[i].tr_delivery_details());
										var currentDeliveryDetails = JSON.parse(currentPersonalizationData[itemIndex]['data']['deliveryDetails']);
										if (currentDeliveryDetails && currentDeliveryDetails.skuid === cartItems[i].catRefId && currentDeliveryDetails.deliveryType === "2" && deliveryDetails && deliveryDetails.deliveryType === "2") {
											cartItems[i]['tr_delivery_details'] = ko.observable(currentPersonalizationData[itemIndex]['data']['deliveryDetails']);
											var personalizationData = JSON.parse(cartItems[i].tr_personalization());
											personalizationData.deliveryDetails = currentPersonalizationData[itemIndex]['data']['deliveryDetails'];
											cartItems[i]['tr_personalization'] = ko.observable(JSON.stringify(personalizationData));
										}
									}
									if (!cartItems[i].tr_isPersonalized()) {
										cartItems[i]['tr_personalization_hash'] = ko.observable(currentPersonalizationData[itemIndex]['hash']);
										cartItems[i]['tr_personalization'] = ko.observable(JSON.stringify(currentPersonalizationData[itemIndex]['data']));
										var cost = Number(currentPersonalizationData[itemIndex]['data']['personalizationCost']);
										if (isNaN(cost)) {
											cost = 0;
										}
										cartItems[i]['tr_personalization_cost'] = ko.observable(cost);
										cartItems[i]['tr_image_url'] = ko.observable(helpers.personalizationUtils().generateScene7URL(cartItems[i].catRefId.split("_")[0]));
										cartItems[i]['tr_total_with_personalization'] = ko.observable(cost + Number(cartItems[i].originalPrice()));
										cartItems[i]['tr_delivery_details'] = ko.observable(currentPersonalizationData[itemIndex]['data']['deliveryDetails']);
										cartItems[i]['tr_isPersonalized'] = ko.observable(true);
										cartItems[i].isPersonalized(true);
										cartUpdated = true;
										currentPersonalizationData.shift();
									}
								}
							}
							// Fix for moving cart items at the top //

							getWidget.cart().items().splice(0, 0, widget.cart().items().splice(cartItems.length - 1, 1)[0]);

							// end fix //

						}
						if (cartUpdated && currentPersonalizationData.length === 0) {
							if (helpers.orderUtils.checkIfPersonalizationSkuPresent(localCart)) {
								setDetailsToItem = false;
								if (widget.user().loggedIn()) {
									localCart.updateCurrentProfileOrder();
								} else {
									localCart.cartUpdated();
								}
								
							    widget.destroySpinner();
								$('#addToCartSuccess').modal('show');
								if(newPersonalizationData && newPersonalizationData !== '') {
    								$.Topic('TR_RESET_DATA.memory').publish();
    								var viewModel = ko.mapping.fromJS(newPersonalizationData);
            					    $.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
								}
								//$.Topic("Invoke_Gift_Modal.memory").publish(giftSkuFlag);
								$.Topic("Invoke_External_Pricing").publish(true,false,true);  
								$.Topic(pubsub.topicNames.CART_ADD_SUCCESS).publish(currentProduct);
								$("#select-quantity").val("1").trigger('change');
							} else {
								setDetailsToItem = true;
								helpers.orderUtils.checkAndAddPersonalizationCost(localCart, true);
							}
						} else {
							setDetailsToItem = true;
							if (currentPersonalizationData.length === 0) {
								if (_this.checkIfPersonalizationSkuPresent() === false) {
									helpers.orderUtils.checkAndAddPersonalizationCost(localCart, true);
								} else {
									setDetailsToItem = false;
									if (widget.user().loggedIn()) {
										localCart.updateCurrentProfileOrder();
									} else {
										localCart.cartUpdated();
									}
								    widget.destroySpinner();
									$('#addToCartSuccess').modal('show');
									if(newPersonalizationData && newPersonalizationData !== '') {
        								$.Topic('TR_RESET_DATA.memory').publish();
        								var viewModel = ko.mapping.fromJS(newPersonalizationData);
                					    $.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
    								}
									//$.Topic("Invoke_Gift_Modal.memory").publish(giftSkuFlag);
								    $.Topic("Invoke_External_Pricing").publish(true,false,true);
									$.Topic(pubsub.topicNames.CART_ADD_SUCCESS).publish(currentProduct);
									$("#select-quantity").val("1").trigger('change');
								}
							} else if (currentPersonalizationData.length === 1) {
								/*$.Topic(pubsub.topicNames.CART_ADD).publishWith(currentProduct, [{
								    message: "success"
								}]);*/
								getWidget.cart().addItem(currentProduct);
							} else {
								getWidget.cart().addItem(currentProduct);
							}
						}
					}, processingTime);

				});

				//Enable the radio buttons by default
				widget.availableTodayRadio = ko.computed(function () {
					if (widget.displayOpus() === true && widget.displayShipToHome() === false) {
						return true;
					} else {
						return false;
					}
				});

				widget.totalCost = ko.computed(function () {

					return (widget.salePrice() || widget.listPrice()) + (widget.personalizationCost() || 0);
				});

				$.Topic('TR_PDP_NEW_PRICE.memory').subscribe(function (skuid, skulistprice, skusalePrice) {
					widget.skuidNew(skuid);
					widget.skulistPriceNew(skulistprice);
					widget.skusalePriceNew(skusalePrice);
				});

				$.Topic('TR_CREATE_INVENTORY_SPINNER').subscribe(function () {
					widget.createInventorySpinner();
				});

				$.Topic('TR_DESTROY_INVENTORY_SPINNER').subscribe(function () {
					widget.destroyInventorySpinner();
				});

				$('body').delegate('.tr-color-swatch', 'keydown', function (e) {
					var keyCode = e.keyCode || e.which;
					var that = this;
					if (keyCode == 13) {
						setTimeout(function () {
							if ($(that).parents(".tr-carousel-wrapper").next().next(".tr-carousel-wrapper").length > 0) {
								$(that).parents(".tr-carousel-wrapper").next().next(".tr-carousel-wrapper").find(".owl-item:first a").focus();
							} else if ($(that).parents(".tr-e-product-variation").next(".tr-e-bom").find(".bom-content").length > 0) {
								$(that).parents(".tr-e-product-variation").next(".tr-e-bom").find(".bom-content:first").find(".owl-item:first a").focus();
							} else {
								$(".quantity-dropdown-wrap").focus();
							}
						}, 500)
					}
				});

				$('body').delegate('.tr-bom-item', 'keydown', function (e) {
					var keyCode = e.keyCode || e.which;
					var that = this;
					if (keyCode == 13) {
						setTimeout(function () {
							if ($(that).parents(".bom-content").next(".bom-content").length > 0) {
								$(that).parents(".bom-content").next(".bom-content").find(".owl-item:first a").focus();
							} else {
								$(".quantity-dropdown-wrap").focus();
							}
						}, 500)
					}
				});

				widget.checkIsMobile($(window).width());
				$(window).resize(function () {
					widget.checkIsMobile($(window).width());
				});
				
				
				
				/*$.Topic("Trigger_Gift_Modal").subscribe(function(data){
				    var prodData = data.split(', ');
				    ccRestClient.authenticatedRequest("/ccstoreui/v1/products?productIds="+prodData+ "&fields=items.id,items.displayName,items.route,items.tr_externalImageUrl",{}, function (data) {
                    widget.giftModalData(data);  
                    widget.destroySpinner();
                    //widget.giftboxModalShown();
                    $('#giftModalPopUp').modal('show'); 
                    $("#giftModalPopUp").on("hidden.bs.modal", function () {
                            window.location.reload();
                    });
				}, function (data) {}, "GET");
				})*/
				
				
				$.Topic('SKU_SELECTED').subscribe(checkNearby);
            
            function checkNearby(product, selectedSKUObj, variantOptions, qnty){
                $.Topic('TR_CREATE_INVENTORY_SPINNER').publish();
               h_product=product;
               h_selectedSKUObj= selectedSKUObj;
               h_variantOptions=variantOptions;
               h_qnty=qnty;
                
                
                if((typeof selectedSKUObj !== "undefined") && (selectedSKUObj !== null)){
                     var data = {};
                     
                     var repoId;
                     
                     if (typeof selectedSKUObj.repositoryId === "function"){
                         repoId = selectedSKUObj.repositoryId();
                     } else {
                         repoId = selectedSKUObj.repositoryId;
                     }

                    data[CCConstants.PRODUCT_IDS] = [repoId];
                    ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, data, function(products) {
						$(".dotwhackPDP").html("");
                         if(products.length > 0){
							$(".dotwhackPDP").html(products[0].promotionMessage);
                            $.Topic('SKU_OBJ1').publish(products[0]);
                            $.Topic('SKU_OBJ_BOM').publish(products[0]);
                            skuOPUS=products[0].OPUS;
                            var listPrice, salePrice;
                            if (typeof selectedSKUObj.listPrice === "function"){
                                listPrice = selectedSKUObj.listPrice();
                            } else {
                                listPrice = selectedSKUObj.listPrice;
                            }
                            
                            if (typeof selectedSKUObj.salePrice === "function"){
                                salePrice = selectedSKUObj.salePrice();
                            } else {
                                salePrice = selectedSKUObj.salePrice;
                            }
                            if(product.childSKUs().length === 1){
                                
                            }
                           
                            $.Topic('TR_PDP_NEW_PRICE.memory').publish(repoId,listPrice,salePrice);    
                           if(skuOPUS){
                               

								request_obj.skuId = repoId;
								request_obj.storeNumber=store_id;
								request_obj.location = {};
								request_obj.location.latitude = parseFloat(latitude);
								request_obj.location.longitude = parseFloat(longitude);
								request_obj.quantity = qnty || 1;
								request_obj.radiusLimit =400; 
								request_obj.limit =400; 
								var request_json = JSON.stringify(request_obj);
							
							}
							else{
								$.Topic('AVAILABLE_TODAY').publish(false);
								
							}
							setTimeout(function(){
								$.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
							},5000)   
                    }else {
						$.Topic('VARIANT_NO_STOCK_ERROR_MESSAGE').publish();
                        $.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
                    }
                        
                        
                    }, function(){
                        
                    });
                 
                }
                else{
                    $.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
                    $.Topic('TR_PDP_NEW_PRICE.memory').publish(-1,product.listPrice(),product.salePrice());
                    $.Topic('AVAILABLE_TODAY').publish(false);
                }
            }

			},

			getReviewStar: function () {
				var getStartInterval = setInterval(function () {
					if ($('.bv-rating-stars-container').length > 0) {
					    
					    if(($('.bv-rating-stars-container').attr('title').indexOf('Read 0 Reviews') === 0) == true){
					        $('.bv-rating-stars-container').attr('style','display:none !important');
					    }
					    else{
					       // if ($('.bv-rating-stars-container').find('.bv-off-screen').text().indexOf('No rating value') === 0) {
    						    $('.bv-rating-stars-container').click(function () {
    								$("#review-container").modal('show');
    							});
    							$('.bv-rating-label').click(function () {
    								$("#review-container").modal('show');
    							});
    							window.clearInterval(getStartInterval);
						  //  }
					    }
					    
					}
				}, 100);


			},


			onConfigReady: function (widget) {

				var self = this;


				if (location.search.substr(1).indexOf("basketLineId") != -1) {
					return;
				}

				/* $("a.bv-content-btn-pages").on('click', function () {
				     var collapseButton = $('#reviews > a');
				     if ($('#review-container').hasClass('in')) {
				         collapseButton.removeAttr("data-toggle");
				     }
				 });*/

				var productModel = {
					'event': 'pdp-details-event',
					'ecommerce': {
						'detail': {
							'actionField': {
								'products': [{
									'name': widget.product().displayName(),
									'id': widget.product().id(),
									'price': widget.product().listPrice(),
									'creative': 'detailview'
								}]
							}
						},
					}
				}
				var dataLayer = [];
				dataLayer.push(productModel);

				if ((publishedSkuData.backorder && (widget.stockAvailable() === 0 || widget.itemQty() > widget.stockAvailable()))) {} else {
					widget.deliveryDateHeadingFlag(true);
					widget.backOrderFlag(false);
					var fullDate;
					if (widget.product().BWYDeliveryDates && typeof widget.product().BWYDeliveryDates == 'function') {
						if (typeof widget.product().BWYDeliveryDates() !== "undefined" && widget.product().BWYDeliveryDates() !== null) {
							fullDate = widget.product().BWYDeliveryDates() || '';
						}
						if (fullDate) {
							var index = fullDate.indexOf('-');
							var newDate = fullDate.substring(index + 1);
							newDate = newDate.replace('-', '/');
							newDate = "Estimated Arrival " + newDate;
							widget.standardDeliveryDate(newDate);
						}
					}
				}

				function getPRMDate(data) {
					var deliveryDate = data.split(',');
					var dateStr = _this.getServerTime();
					var dateTime = new Date(dateStr);
					var today = new Date();
					var time;
					Date.prototype.stdTimezoneOffset = function () {
						var jan = new Date(this.getFullYear(), 0, 1);
						var jul = new Date(this.getFullYear(), 6, 1);
						return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
					}

					Date.prototype.dst = function () {
						return this.getTimezoneOffset() < this.stdTimezoneOffset();
					}
					if (today.dst()) {
						time = dateTime.getUTCHours() - 4; //Daylight savings time
					} else {
						time = dateTime.getUTCHours() - 5; //Not daylight savings time
					}
					if (time < 15) { //If current time is before 3 PM EST
						return deliveryDate[0];
					} else {
						if (deliveryDate[1]) {
							return deliveryDate[1];
						} else {
							return deliveryDate[0];
						}
					}
				}

				if (widget.product().BWYDeliveryDates && typeof widget.product().BWYDeliveryDates == 'function') {
					if (typeof widget.product().BWYDeliveryDates() !== "undefined" && widget.product().BWYDeliveryDates() !== null) {
						widget.bwyFlag(true);
						widget.bwyFlagDate(widget.product().BWYDeliveryDates());
					} else {
						widget.bwyFlag(false);
					}
				}

				if (widget.product().AIRDeliveryDates && typeof widget.product().AIRDeliveryDates == 'function') {
					if ((typeof widget.product().AIRDeliveryDates !== "undefined" && widget.product().AIRDeliveryDates())) {
						widget.airFlag(true);
						widget.airFlagDate((widget.product().AIRDeliveryDates()));
					}
				} else if (widget.product().airDeliveryDates && typeof widget.product().airDeliveryDates == 'function') {
					if (typeof widget.product().airDeliveryDates !== "undefined" && widget.product().airDeliveryDates()) {
						widget.airFlag(true);
						widget.airFlagDate((widget.product().airDeliveryDates()));
					}
				} else {
					widget.airFlag(false);
				}
				if (widget.product().two2NDDeliveryDates && typeof widget.product().two2NDDeliveryDates == 'function') {
					if (typeof widget.product().two2NDDeliveryDates() !== "undefined" && widget.product().two2NDDeliveryDates()) {
						widget.two_ndFlag(true);
						widget.two_ndFlagDate(widget.product().two2NDDeliveryDates());
					}
				} else {
					widget.two_ndFlag(false);
				}
				if (widget.product().PRMDeliveryDates && typeof widget.product().PRMDeliveryDates == 'function') {
					if (typeof widget.product().PRMDeliveryDates() !== "undefined" && widget.product().PRMDeliveryDates()) {
						widget.prmFlag(true);
						widget.prmFlagDate(getPRMDate(widget.product().PRMDeliveryDates()));
					}
				} else {
					widget.prmFlag(false);
				}

				widgetModel = widget;
				if (widget.product().BWYBackorderDeliveryDates && typeof widget.product().BWYBackorderDeliveryDates == 'function') {
					widget.deliveryDate(widget.product().BWYBackorderDeliveryDates());
				}

				$.Topic('TR_PERSONALIZATION_DATA_UPDATED').subscribe(function (personalization_obj) {
					widget.personaliztionObj = personalization_obj;

				});
				$.Topic("REMOVE_BOM").subscribe(function (bom_url_obj) {
					var index = -1;
					for (var j = 0; j < widget.urlObjArr.length; j++) {
						if (widget.urlObjArr[j].slotnum == bom_url_obj.slotnum &&
							widget.urlObjArr[j].prodsku == bom_url_obj.prodsku) {
							index = j;
							break;
						}
					}
					if (index > -1) {
						widget.urlObjArr.splice(index, 1);
						$(".tr-w-button.preview-button").data("url", "");

					}

				});

				$.Topic("BOM_URL").subscribe(function (bom_url_obj) {
					var variantOptions = widget.variantOptionsArray(),
						sku_obj = widget.getSelectedSku(variantOptions),
						skuid, flag;

					if (widget.product().childSKUs().length == 1) {
						sku_obj = widget.product().childSKUs()[0];
						skuid = sku_obj.repositoryId();
						flag = 0;
					} else {
						skuid = sku_obj.repositoryId,
							flag = 0;
					}

					for (var j = 0; j < widget.urlObjArr.length; j++) {
						if (widget.urlObjArr[j].slotnum == bom_url_obj.slotnum &&
							widget.urlObjArr[j].prodsku == bom_url_obj.prodsku) {
							widget.urlObjArr[j] = bom_url_obj;
							flag = 1;
							break;
						}
					}

					if (flag === 0) {
						widget.urlObjArr.push(bom_url_obj);
					}
					var url_sku_id = "000" + skuid;
					var url = "https://thingsremembered.scene7.com/ir/render/ThingsRememberedRender/" + url_sku_id + "?"
					for (var i = 0; i < widget.urlObjArr.length; i++) {
						if (skuid == widget.urlObjArr[i].prodsku && widget.urlObjArr[i].color != "1302030") {
							var str = "obj=GEM/" + widget.urlObjArr[i].slotnum;
							str += "&color=" + widget.urlObjArr[i].color + "&";
							url += str;
						}
					}

					url += "cache=off&hei=400";

					if (widget.urlObjArr.length === widget.bomArray().length) {
						$(".tr-w-button.preview-button").data("url", url);
						$(".tr-e-bom").find(".main-error").hide();

						widget.urlObjArr.sort(function (a, b) {
							return Number(a.slotnum) - Number(b.slotnum);
						});

						$.Topic('TR_BOM_UPDATED').publish(widget.urlObjArr);
					}
				});


				$.Topic('SKU_OBJ_BOM').subscribe(function (product_obj) {
					publishedSkuData = product_obj;
					skuInventory = widget.stockAvailable();
					var slot_variant, curr_index,
						slot_vals = [],
						sku_id = product_obj.id;
					widget.selectedBomArray([]);
					console.log("product_obj: ", product_obj);
					
					function getChildSku() {
					    for(i = 0; i < widget.product().childSKUs().length; i++){
                            if (widget.product().childSKUs()[i].repositoryId() == product_obj.id){
                                return i
                            }
                        }
                        return null
					}
                    console.log("billOfMaterial: ", widget.product().childSKUs()[getChildSku()].tr_billOfMaterialJson());
                    console.log("widget: ", widget);
                    if(widget.product().childSKUs()[getChildSku()].tr_billOfMaterialJson()){
						if (widget.skuBOM[sku_id]) {
							widget.bomArray(widget.skuBOM[sku_id]);
							$(".bom-row.owl-carousel").each(function () {
								$(this).owlCarousel({
									items: 5,
									pagination: false,
									navigation: true,
									navigationText: false,
									rewindNav: false,
									itemsMobile: [479, 5],
									itemsDesktop: [1199, 5],
									itemsDesktopSmall: [980, 5],
									itemsTablet: [768, 5],
									itemsTabletSmall: false
								});
							});
							return;
						}

						var bom_variant_arr = [];
						
						//console.log("bom_variant_arr: ", bom_variant_arr);
						
						//console.log("widget product: ", widget.product());

						function billOfMaterialAjax(){
                            // product being rendered in from OCC
                            var productOCC = widget.product();
                        
                            //all the parent product's childSKUs
                            var productChildSkus = productOCC.childSKUs();
                        
                            //used to hold the index of the child product
                            var currentChildProduct = getChildSku();
                        
                            //used to hold the kets for referencedComponentGroup
                            var componentGroupReferences = [];
                        
                        
                            // loop used to find the correct child product selected and set that index
                            
                            
                            //used to grab the specific child skus' BOM. currentChildProduct needs to be set
                            var billOfMaterial = JSON.parse(productChildSkus[currentChildProduct].tr_billOfMaterialJson());
                            
                            //used to grab the child products BOM referenced component group
                            var referencedComponentGroup = billOfMaterial.referencedComponentGroups;
                            //console.log("referencedComponentGroup: ", referencedComponentGroup);
                            //got child sku and its tr_BOM, needs to be fully created 
                            for(i = 0; i < billOfMaterial.slots.length; i++){
                                componentGroupReferences.push(billOfMaterial.slots[i].componentGroupReference);
                            }
                            //fill out the dummy objects
                            //slot column aka slot number
                            for(i=0; i < componentGroupReferences.length; i++){
                                var slot_bom = [];
                                //slot row aka slot gems
                                var t = referencedComponentGroup[componentGroupReferences[i]].length;
                                for(j=0; j < t; j++){
                                    //console.log(referencedComponentGroup[componentGroupReferences[i]][j]);
                                    var obj = {};
                        
                                    obj.skuid = referencedComponentGroup[componentGroupReferences[i]][j].sku;
                        
                                    obj.name = referencedComponentGroup[componentGroupReferences[i]][j].description;
                        
                                    obj.imgurl = "/file/products/" + obj.skuid + ".jpg";
                                    
                                    obj.displayName = billOfMaterial.slots[i].displayName;
                                    
                                    obj.slot = "slot" + (i+1);
                                    
                                    obj.prodsku = sku_id;
                        
                                    slot_bom.push(obj);
                                }
                                
                                //fills bom_variant_arr with slot_bom
                                bom_variant_arr[bom_variant_arr.length] = slot_bom;


                                //console.log("testing bomArray before: ", widget.bomArray());
                                //populating empty bomArray with bom_variant_arr
                                widget.bomArray(bom_variant_arr);
                                console.log("testing bomArray after: ", widget.bomArray());
                        
                                //populates widget skuBOM with parent sku
                                widget.skuBOM[sku_id] = bom_variant_arr;
                            }
                            //console.log("slot_bom: ", slot_bom);
                        
                            
                        
							var owl = $(".bom-row.owl-carousel")[$(".bom-row.owl-carousel").length - 1]

                            //sets up bom carousel
                            $(".bom-row.owl-carousel").each(function() {
                                $(this).owlCarousel({
                                    items: 5,
                                    pagination: false,
                                    navigation: true,
                                    navigationText: false,
                                    rewindNav: false,
                                    itemsMobile: [479, 5],
                                    itemsDesktop: [1199, 5],
                                    itemsDesktopSmall: [980, 5],
                                    itemsTablet: [768, 5],
                                    itemsTabletSmall: false
                                });
                            });
                        }
					    billOfMaterialAjax()
                    }
					
					if ((publishedSkuData.backorder && (widget.stockAvailable() === 0 || widget.itemQty() > widget.stockAvailable()))) {

					} else {
						widget.deliveryDateHeadingFlag(true);
						widget.backOrderFlag(false);
						var fullDate = product_obj.BWYDeliveryDates || '';

						if (fullDate !== '' && fullDate !== null) {
							var index = fullDate.indexOf('-');
							var newDate = fullDate.substring(index + 1);
							newDate = newDate.replace('-', '/');
							newDate = "Estimated Arrival " + newDate;
							widget.standardDeliveryDate(newDate);
						}

						if ($(".tr-e-product-description #descr-container").height() + 10 < $(".tr-e-product-description #descr-container .tr-prod-descr-string").height()) {
							$(".tr-prod-descr-show-more").show();
						} else {
							$(".tr-prod-descr-show-more").hide();
						}

						if (typeof product_obj.BWYDeliveryDates !== "undefined" && product_obj.BWYDeliveryDates) {
							widget.bwyFlag(true);
							widget.bwyFlagDate(product_obj.BWYDeliveryDates);
						} else {
							widget.bwyFlag(false);
						}

						if (typeof product_obj.two2NDDeliveryDates !== "undefined" && product_obj.two2NDDeliveryDates) {
							widget.two_ndFlag(true);
							widget.two_ndFlagDate(product_obj.two2NDDeliveryDates);
						} else {
							widget.two_ndFlag(false);
						}
						if (typeof product_obj.AIRDeliveryDates !== "undefined" && product_obj.AIRDeliveryDates) {
							widget.airFlag(true);
							widget.airFlagDate((product_obj.AIRDeliveryDates));

						} else {
							widget.airFlag(false);
						}

						if (typeof product_obj.PRMDeliveryDates !== "undefined" && product_obj.PRMDeliveryDates) {
							widget.prmFlag(true);
							widget.prmFlagDate(getPRMDate(product_obj.PRMDeliveryDates));

						} else {
							widget.prmFlag(false);
						}
					}
					$.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
				});


				$.Topic(pubsub.topicNames.UPDATE_LISTING_FOCUS).subscribe(function (obj) {
					widget.skipTheContent(true);
				});


				$.Topic(pubsub.topicNames.PAGE_READY).subscribe(function (obj) {
					var parameters = {};
					if (obj.parameters) {
						var param = obj.parameters.split("&");
						for (var i = 0; i < param.length; i++) {
							var tempParam = param[i].split("=");
							parameters[tempParam[0]] = tempParam[1];
						}
					}
					if (parameters.variantName && parameters.variantValue) {
						widget.variantName(decodeURI(parameters.variantName));
						widget.variantValue(decodeURI(parameters.variantValue));
					} else {
						widget.variantName("");
						widget.variantValue("");
					}
				});

				$.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD_SUCCESS).subscribe(function (obj) {
					if (obj.productUpdated) {
						widget.disableAddToSpace(true);
						setTimeout(function () {
							widget.disableAddToSpace(false);
						}, 3000);
					} else {
						widget.isAddToSpaceClicked(true);
						widget.disableAddToSpace(true);
						setTimeout(function () {
							widget.isAddToSpaceClicked(false);
						}, 3000);
						setTimeout(function () {
							widget.disableAddToSpace(false);
						}, 3000);
					}
				});

				$.Topic(pubsub.topicNames.USER_LOGIN_SUCCESSFUL).subscribe(function (obj) {
					widget.getSpaces(function () {});
				});

				$.Topic(pubsub.topicNames.USER_AUTO_LOGIN_SUCCESSFUL).subscribe(function (obj) {
					widget.getSpaces(function () {});
				});

				$.Topic(pubsub.topicNames.SOCIAL_REFRESH_SPACES).subscribe(function (obj) {
					widget.getSpaces(function () {});
				});

				$.Topic('PERSONALIZATION_HASH_PASSED').subscribe(function (obj) {});

				widget.itemQty.extend({
					required: {
						params: true,
						message: CCi18n.t('ns.common:resources.quantityRequireMsg')
					},
					digit: {
						params: true,
						message: CCi18n.t('ns.common:resources.quantityNumericMsg')
					},
					min: {
						params: 1,
						message: CCi18n.t('ns.tr-w-product-widget-latest:resources.quantityGreaterThanMsg', {
							quantity: 0
						})
					}
				});

				widget.stockAvailable.subscribe(function (newValue) {
					var max = parseInt(newValue, 10);
					widget.itemQty.rules.remove(function (item) {
						return item.rule == "max";
					});
					if (max > 0) {
						widget.itemQty.extend({
							max: {
								params: max,
								message: CCi18n.t('ns.tr-w-product-widget-latest:resources.quantityLessThanMsg', {
									quantity: max
								})
							}
						});
					}
				});
				swmRestClient.init(widget.site().tenantId, widget.isPreview(), widget.locale());

				widget.fetchFacebookAppId();

				if (widget.displaySWM) {
					widget.showSWM(true);
				}

				widget.onSelectBOMElement = function (selectedBomItem, event, slotIndex, parentContext, parent) {
				    // console.log("onSelectBOMElement");
				    // console.log(JSON.stringify(parentContext));
				    // console.log(JSON.stringify(parent));
				    console.log("selectedBomItem: ", selectedBomItem);
				    console.log("parent",parent);
				    // console.log(JSON.stringify(widget));
				    console.log("widget",widget);
					var data = {},
						skuid = selectedBomItem.skuid,
						slot = selectedBomItem.slot;

					selectedBomItem.slotNumber = slotIndex;
					selectedBomItem.isInStock = ko.observable(true);
					var match = ko.utils.arrayFirst(widget.selectedBomArray(), function (item) {
						return selectedBomItem.slot === item.slot;
					});
					if (!match) {
						widget.selectedBomArray.push(selectedBomItem);
					} else {
						var index = widget.selectedBomArray.indexOf(match);
						widget.selectedBomArray.splice(index, 1, selectedBomItem);
					}
					console.log("widget.selectedBomArray",widget.selectedBomArray());
					$(event.target).closest(".bom-content").find(".bom-value").text(selectedBomItem.name);
					$(event.target).closest(".bom-row").find(".selected").removeClass("selected");
					$(event.currentTarget).addClass("selected");

					$(event.target).closest(".bom-row").find(".selected").removeClass("selected");
					$(event.currentTarget).addClass("selected");

					var data = [];
					var productId = widget.product().id();
					data[CCConstants.PRODUCT_IDS] = widget.product().id();
					data[CCConstants.SKU_ID] = selectedBomItem.skuid.trim();
					data[CCConstants.CATREF_ID] = selectedBomItem.skuid.trim();

					var preloader = $("<div id='global-preloader'><div class='content'>Processing Request..</div></div>");
					preloader.appendTo('body');

					ccRestClient.request(CCConstants.ENDPOINT_GET_PRODUCT_AVAILABILITY, data, function (data) {
					    //console.log("ENDPOINT_GET_PRODUCT_AVAILABILITY: ", data);
					    //console.log("widget before personalizevisibility flag: ", widget);
						if (data.stockStatus === "OUT_OF_STOCK") {
							data.orderableQuantity = 0;
						}

						var stockavailable = data.orderableQuantity;
						var availableflag = 1;
						if (stockavailable < widget.itemQty()) {
							$(event.target).closest(".bom-content").find(".error-msg").text(selectedBomItem.name + " is out of stock").show();
							availableflag = 0;

						}
						if (data.orderableQuantity < widget.itemQty()) {
							availableflag = 0;
						}
						data[CCConstants.PRODUCT_IDS] = selectedBomItem.skuid.trim();
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, data, function (products) {
						    if(products && products.length === 0) {
						        if (preloader) {
        							preloader.remove();
        						}
        						var obj = {};
        						obj.slotnum = slotIndex + 1;
        						obj.prodsku = skuid;
        
        						$(event.target).closest(".bom-content").find(".error-msg").text(selectedBomItem.name + " is out of stock").show();
        						availableflag = 0;
        
        						$.Topic('REMOVE_BOM').publish(obj);
        						widget.personalizeVisibilityFlag(isSelectedBomArrayValid(widget));
						        return;
						    }
							if (preloader) {
								preloader.remove();
							}
							if (availableflag === 0) {
								var price = products[0].salePrice || products[0].listPrice;
								var obj = {};
								obj.slotnum = Number(slotIndex) + 1;
								obj.color = "130,20,30";
								obj.skuid = products[0].repositoryId;
								obj.color = products[0].birthstoneColor || "1302030";
								obj.prodsku = skuid;

								$.Topic('REMOVE_BOM').publish(obj);
							} else {
								$(event.target).closest(".bom-content").find(".error-msg").hide();
								if (products[0].salePrice === null) {
									products[0].salePrice = products[0].listPrice;
								}

								var price = products[0].salePrice || products[0].listPrice;
								var obj = {};
								obj.slotnum = slotIndex + 1;
								obj.skuid = products[0].repositoryId;
								obj.color = products[0].birthstoneColor || "1302030";
								obj.price = price;
								obj.name = selectedBomItem.name;
								obj.slot = "Slot " + obj.slotnum;
								obj.prodsku = skuid;
								$.Topic('BOM_URL').publish(obj);
							}
							widget.personalizeVisibilityFlag(isSelectedBomArrayValid(widget, parentContext));

						});

					}, function (data) {
						if (preloader) {
							preloader.remove();
						}
						var obj = {};
						obj.slotnum = slotIndex + 1;
						obj.prodsku = skuid;

						$(event.target).closest(".bom-content").find(".error-msg").text(selectedBomItem.name + " is out of stock").show();
						availableflag = 0;

						$.Topic('REMOVE_BOM').publish(obj);
						widget.personalizeVisibilityFlag(isSelectedBomArrayValid(widget));

					}, productId);
				};


				/**
				 * Set up the popover and click handler
				 * @param {Object} widget
				 * @param {Object} event
				 */
				widget.shippingSurchargeMouseOver = function (widget, event) {
					// Popover was not being persisted between
					// different loads of the same 'page', so
					// popoverInitialised flag has been removed

					// remove any previous handlers
					$('.shippingSurchargePopover').off('click');
					$('.shippingSurchargePopover').off('keydown');

					var options = new Object();
					options.trigger = 'manual';
					options.html = true;

					// the button is just a visual aid as clicking anywhere will close popover
					options.title = widget.translate('shippingSurchargePopupTitle') +
						"<button id='shippingSurchargePopupCloseBtn' class='close btn pull-right'>" +
						widget.translate('escapeKeyText') +
						" ×</button>";

					options.content = widget.translate('shippingSurchargePopupText');

					$('.shippingSurchargePopover').popover(options);
					$('.shippingSurchargePopover').on('click', widget.shippingSurchargeShowPopover);
					$('.shippingSurchargePopover').on('keydown', widget.shippingSurchargeShowPopover);
				};

				widget.shippingSurchargeShowPopover = function (e) {

					if (e.type === 'keydown' && e.which !== CCConstants.KEY_CODE_ENTER) {
						return;
					}

					// stop event from bubbling to top, i.e. html
					e.stopPropagation();
					$(this).popover('show');

					// toggle the html click handler
					$('html').on('click', widget.shippingSurchargeHidePopover);
					$('html').on('keydown', widget.shippingSurchargeHidePopover);

					$('.shippingSurchargePopover').off('click');
					$('.shippingSurchargePopover').off('keydown');
				};

				widget.shippingSurchargeHidePopover = function (e) {
					// if keydown, rather than click, check its the escape key
					if (e.type === 'keydown' && e.which !== CCConstants.KEY_CODE_ESCAPE) {
						return;
					}

					$('.shippingSurchargePopover').popover('hide');

					$('.shippingSurchargePopover').on('click', widget.shippingSurchargeShowPopover);
					$('.shippingSurchargePopover').on('keydown', widget.shippingSurchargeShowPopover);

					$('html').off('click');
					$('html').off('keydown');

					$('.shippingSurchargePopover').focus();
				};

				$(window).resize(function () {
					// Optimizing the carousel performance, to not reload when only height changes
					var width = $(window)[0].innerWidth || $(window).width();
					if (widget.product().primaryFullImageURL) {
						if (widget.viewportWidth() == width) {
							// Don't reload as the width is same
						} else {
							// Reload the things
							if (widget.product().videoFlag && typeof widget.product().videoFlag == 'function' && widget.product().videoFlag() === true) {
								if (width > 1024) {
									setTimeout(function () {
										widget.loadImageToMain("", "click", 0);
									}, 1000)

								}
							} else {
								if (width > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
									if (widget.viewportWidth() <= CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
										// Optionally reload the image place in case the view port was different
										widget.activeImgIndex(0);
										widget.mainImgUrl(widget.product().primaryFullImageURL);
										$('#prodDetails-imgCarousel').carousel(0);
										$('#carouselLink0').focus();
									}
								} else if (width >= CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) {
									if ((widget.viewportWidth() < CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) || (widget.viewportWidth() > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH)) {
										// Optionally reload the image place in case the view port was different
										widget.activeImgIndex(0);
										widget.mainImgUrl(widget.product().primaryFullImageURL);
										$('#prodDetails-imgCarousel').carousel({
											interval: 1000000000
										});
										$('#prodDetails-imgCarousel').carousel(0);
										$('#carouselLink0').focus();
									}
								} else {
									if (widget.viewportWidth() > CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) {
										// Optionally reload the carousel in case the view port was different
										$('#prodDetails-mobileCarousel').carousel({
											interval: 1000000000
										});
										$('#prodDetails-mobileCarousel').carousel(0);
									}
								}
							}


						}
					}
					widget.viewportWidth(width);
					widget.checkResponsiveFeatures($(window).width());
					widget.selectOccassionForMobile();
				});

				widget.viewportWidth($(window).width());
				if (widget.product()) {

					var productId = widget.product().id(),
						zeroPaddedProdId = "";


					for (var i = 0; i < (9 - productId.length); i++) {
						zeroPaddedProdId += "0";
					}
					zeroPaddedProdId += productId;
					widget.product().primaryFullImageURL(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					widget.product().primaryMediumImageURL(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					var productImageArr = widget.getProductImages("L");

					productImageArr.unshift(_this.scene7BaseUrl + zeroPaddedProdId + '?wid=' + imageRes);

					widget.product().thumbImageURLs(productImageArr);
					widget.imgGroups(widget.groupImages(productImageArr));
					if (widget.product().defaultImageURL && typeof widget.product().defaultImageURL == 'function') {
						widget.mainImgUrl(widget.product().defaultImageURL());
					}

					widget.product().product.productImagesMetadata = [];
					productImageArr.forEach(function () {

						widget.product().product.productImagesMetadata.push({});

					});
				}

				if (true || widget.skipTheContent()) {
					var focusFirstItem = function () {
						$('#region-12colproductDetailsRegion :focusable').first().focus();
						widget.skipTheContent(false);
					};

					focusFirstItem();
					setTimeout(focusFirstItem, 1); // Daft IE fix.
				}


				/* Quntity String - start */
				$(document).on('keydown blur', '.tr-qty-input', function (event) {
					if ((event.which < 48 || event.which > 57) && event.which != 127 && event.which != 8) {
						event.preventDefault();
					}
				});
				$(document).on('blur', '.tr-qty-input', function () {
					if ($(this).val() === '' || $(this).val() === '0' || $(this).val() === '00' || $(this).val() === '000' || $(this).val() === null) {
						$('.tr-qty-input').val(1);
					}
				});
				/* Qunatity String - end */


				$(document).on('click', '.carousel-inner .thumbnail-container', function () {
					$(".bom-preview-image").remove();

					if ($(this).children(".thumbnail").id != "carouselLink1" && widget.product().videoFlag !== true) {
						$(".cc-image-area").find("#image-viewer").find(".cc-viewer-pane").show();
					}
				});
				$(document).on('click', '.tr-w-button.preview-button', function (event) {

					var url = $(".tr-w-button.preview-button").data("url");
					if (url.length) {
						$(".bom-preview-image").remove();

						var img = "<img onError=\"this.src='/img/no-image.jpg'\" class = 'img-responsive' src = '" + url + "' data-bind='productImageSource: {errorSrc:'/img/no-image.jpg', errorAlt:'No Image Found'}>";
						if ($(".cc-image-area").find("#image-viewer").find(".cc-viewer-pane").length) {
							$(".cc-image-area").find("#image-viewer").find(".cc-viewer-pane").hide();
							$(".cc-image-area").find("#image-viewer").append("<div class = 'bom-preview-image'></div>");
							$(".cc-image-area")
								.find("#image-viewer")
								.find(".bom-preview-image")
								.html(img);
						} else {

							$(".cc-image-area").find("#image-viewer").find(".carousel-inner").hide();
							$(".cc-image-area").find("#image-viewer").find(".row-fluid").hide();
							$(".cc-image-area").find("#image-viewer").find("#prodDetails-mobileCarousel").append("<div class = 'bom-preview-image'></div>");

							$(".cc-image-area")
								.find("#image-viewer")
								.find(".bom-preview-image")
								.html(img);

							$(".cc-image-area")
								.find("#image-viewer")
								.find(".bom-preview-image")
								.prepend("<button type='button' class='close' data-dismiss='modal' aria-label='Close' id='close-button'><img id='close-image' src='/file/general/close_symbol.png' alt='Close'></button>");

						}
					}
				});

				$(document).on('click', '.bom-preview-image .close', function (event) {
					$(".cc-image-area")
						.find("#image-viewer")
						.find(".bom-preview-image")
						.hide();
					$(".cc-image-area").find("#image-viewer").find(".carousel-inner").show();
					$(".cc-image-area").find("#image-viewer").find(".row-fluid").show();
				});


				/* Quantity Increase Decrease Start*/

				$(document).on("click", ".tr-plus", function () {
					var $trQty = $('.tr-qty-input'),
						qtyVal = $trQty.val();
					if (qtyVal < 999) {
						var newQty = Number(qtyVal) + 1;

						$trQty.val(newQty).change();

						var variantOptions = widget.variantOptionsArray(),
							sku_obj = widget.getSelectedSku(variantOptions);

						if (widget.product().childSKUs().length == 1) {
							sku_obj = widget.product().childSKUs()[0];
						}


					}
				});
				$(document).on("click", ".tr-minus", function () {
					var $trQty = $('.tr-qty-input'),
						qtyVal = $trQty.val();
					if (qtyVal > 1) {
						var newQty = Number(qtyVal) - 1;
						$trQty.val(newQty).change();

						var variantOptions = widget.variantOptionsArray(),
							sku_obj = widget.getSelectedSku(variantOptions);
						if (widget.product().childSKUs().length == 1) {
							sku_obj = widget.product().childSKUs()[0];
						}

					}
				});

				$(document).on("change", ".tr-qty-input", function () {
					var $trQty = $('.tr-qty-input'),
						qtyVal = $trQty.val();
					if (qtyVal > 1) {
						var newQty = Number(qtyVal);


						var variantOptions = widget.variantOptionsArray(),
							sku_obj = widget.getSelectedSku(variantOptions);
						if (widget.product().childSKUs().length == 1) {
							sku_obj = widget.product().childSKUs()[0];
						}

						$.Topic('SKU_SELECTED').publish(widget.product(), sku_obj, variantOptions, widget.itemQty());
					}
				});
				/*$(document).on("change", ".quantity-dropdown", function () {
				    var $trQty = $('.quantity-dropdown'),
				        qtyVal = $trQty.val();
				    $("#inventory-error").text("");
				    var newQty = Number(qtyVal);


				    var variantOptions = widget.variantOptionsArray(),
				        sku_obj = widget.getSelectedSku(variantOptions);
				    if (widget.product().childSKUs().length == 1) {
				        sku_obj = widget.product().childSKUs()[0];
				    }

				    $.Topic('SKU_SELECTED').publish(widget.product(), sku_obj, variantOptions, widget.itemQty());
				    //}
				});*/
				widget.quantityChange = function () {
					$(".tr-store-stock-error").hide().text("");
					var $trQty = $('.quantity-dropdown'),
						qtyVal = $trQty.val();
					$("#inventory-error").text("");
					$(".error-msg").text("");
					var newQty = Number(qtyVal);


					var variantOptions = widget.variantOptionsArray(),
						sku_obj = widget.getSelectedSku(variantOptions);
					if (widget.product().childSKUs().length == 1) {
						sku_obj = widget.product().childSKUs()[0];
					}
					$.Topic('SKU_SELECTED_IR').publish(widget.product(), sku_obj, variantOptions, widget.itemQty());

					var shippingType = $("[name=optradio]:checked").val();
					var storeNumber = "920";
					if (shippingType == 1) { //pickup
						$.Topic("PROCESS_INVENTORY_PDP_IR").subscribe(function (data) {
							storeNumber = data.pickup.length > 0 && data.pickup[0].available ? data.pickup[0].store.storeNumber : null;
						});
					}
					//helpers.storeLocatorUtils().getInventoryResponseStoreIR(widget.itemQty(), storeNumber, 4, widget.skuRepoId(),"quantityChange");

					var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();

					quantityChangeDeliveryTriggered = false;

					var skuId = widget.skuRepoId();
					var skuQuantity = widget.itemQty();
					var latitude = $('.my-store-lat').text();
					var longitude = $('.my-store-lon').text();
					var storeNumber = $('.my-store-id').text();
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": skuQuantity,
						"radiusLimit": 400,
						"limit": 4
					};
					var stores = helpers.storeLocatorUtils().getStores(latitude, longitude, storeItems);
					var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);
					var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();


					helpers.storeLocatorUtils().getInventoryResponseIR(stores, skuQuantity, storeNumber, 4, skuId, storeIds, reqData, "pdpLoad", cartInventoryDetailsArrayObj);

				};
				widget.itemQty.subscribe(function () {
					$.Topic('QTY_UPDATE').publish(widget.itemQty());
					var variantOptions = widget.variantOptionsArray(),
						sku_obj = widget.getSelectedSku(variantOptions);

					if (widget.product().childSKUs().length == 1) {
						sku_obj = widget.product().childSKUs()[0];
					}


				});

				$.Topic('AVAILABLE_TODAY').subscribe(function (avl_flag) {
					widget.availableToday(avl_flag);
				});

				$.Topic('AVAILABLE_STORE_NAME').subscribe(function (storeName) {
					widget.availableTodayStore(storeName);

				});
				/* Quantity Increase Decrease End*/

				/* sjoseph - store available today -start */
				function checkProdAvailabilty(qty) {
					var request_obj = {};
					request_obj.skuId = "347943";
					request_obj.storeNumber = 110;
					request_obj.quantity = qty;
					var request_json = JSON.stringify(request_obj);

				}

				$(document).on('click', '.set-Store-link', function (e) {
					var storeNum = $(this).attr("store_id")
					var lat = $(this).attr("latitude");
					var lon = $(this).attr("longitude");
					$.Topic("PICKUP-STORE-CHANGED").publish(storeNum, lat, lon);
					$("#tr-e-available-store").modal("hide");
					quantityChangeDeliveryTriggered = true;
					displayStoreChangeMessage = false;
					$('#available-today').click();
					$("body").removeClass('modal-open');
					$('body').css('overflow', 'unset');
					$('body').css('position', 'unset');
					
				});
				
				$(document).on('click', '.tr-w-occasion li', function () {
					var staticVal = $(this).text();
					$('.tr-w-occasion .btn .text').text(staticVal);
					$('.tr-w-occasion .btn .index').text($(this).attr("index"));
					// $('.tr-w-occasion .occasion-dropdown').toggle();

					if ($('.tr-w-occasion .btn .index').text() != "0") {
						$(".tr-w-occasion").find(".error-msg").hide();
					}
				});


				if (widget.product().virtual && typeof widget.product().virtual == 'function') {
					if (widget.product().virtual()) {
						widget.virtual(true);
					} else {
						widget.virtual(false);
					}
				}

				//Omni Channel
				$.Topic('SKU_OBJ1').subscribe(function (product) { //Pub Sub for sku/qty changed.
					$.Topic("CHECK_PULSE").publish(product.tr_pulse);
					widget.product().attributeFeatures(product.attributeFeatures);
					widget.product().longDescription(product.longDescription);
					widget.prodTitleNew(product.displayName);
					if (!product)
						return;
					var productData = product;
					var latitude = widget.pageStoreLat() || $('.my-store-lat').text();
					var longitude = widget.pageStoreLon() || $('.my-store-lon').text();
					var storeNumber = widget.pageStore() || $('.my-store-id').text();
					var opusProductFlag = productData.OPUS || '';
					// TRV - 2162 - S
					_this.setProductData(productData);
					// TRV - 2162 - E
					widget.skuData(product);
					widget.skuOpus(opusProductFlag);

					if (typeof (productData.STS) == "undefined" || productData.STS == null) {
						stsFlag = false;
					} else {
						stsFlag = productData.STS;
					}
					$("#skuSTSFlag").val(stsFlag);
					widget.skuSts(stsFlag);
					widget.skuRepoId(productData.repositoryId);
					//TRV - 1917 - S
					if (product.photoUploadRequired) {
						widget.isPhotoUpload(true);
					} else {
						widget.isPhotoUpload(false);
					}
					if ($('#available-today').length > 0 && $('#available-today').is(':checked')) {
						$('#available-today').trigger('change', true);
					}
					//TRV - 1917 - E
					// var stsProductFlag = productData.STS;
					var skuId = productData.repositoryId;
					var qty = widget.itemQty();
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": qty,
						"radiusLimit": 400,
						"limit": 4
					};
					setShipping(reqData, opusProductFlag, stsFlag);
				});
				/*$.Topic('STOREID').subscribe(function (storeid, lat, long) {//Pub Sub for store changed.
				    var productData = widget.skuData();
				    var latitude = lat;
				    var longitude = long;
				    var storeNumber = storeid;
				    var opusProductFlag = widget.skuOpus();
				    var stsProductFlag = widget.skuSts();
				    var skuId = widget.skuRepoId();
				    var qty = widget.itemQty();
				    widget.pageStoreLat(latitude);
				    widget.pageStoreLon(longitude);
				    widget.pageStore(storeid);
				    widget.storeCode(storeid);
				    var reqData = {
				        "skuId": skuId,
				        "storeNumber": storeNumber,
				        "location": {"latitude": latitude, "longitude": longitude},
				        "quantity": qty,
				        "radiusLimit": 400,
				        "limit": 4
				    };
				    setShipping(reqData, opusProductFlag, stsProductFlag);

				    $.Topic("displayOpus").subscribe(inventoryCheck);


				    function inventoryCheck() {

				        widget.availableTodayVisibility(widget.displayOpus());
				        $.Topic("displayOpus").unsubscribe(inventoryCheck);

				    }
				});*/
				$.Topic('PICKUP-STORE-CHANGED').subscribe(function (storeid, lat, long) { //Sub for Pickup Store Changed
					var productData = widget.skuData();
					var latitude = lat;
					var longitude = long;
					var storeNumber = storeid;
					var opusProductFlag = widget.skuOpus();
					var stsProductFlag = widget.skuSts();
					var skuId = widget.skuRepoId();
					var qty = widget.itemQty();
					widget.pageStoreLat(lat);
					widget.pageStoreLon(long);
					widget.pageStore(storeid);
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": qty,
						"radiusLimit": 400,
						"limit": 4
					};
					setShipping(reqData, opusProductFlag, stsProductFlag);
				});
				$.Topic('PERSONALIZATION-STORE-MODAL').subscribe(function () { //Sub for store modal in personalization page
					var productData = widget.skuData();
					var latitude = widget.pageStoreLat();
					var longitude = widget.pageStoreLon();
					var storeNumber = widget.pageStore();
					var opusProductFlag = widget.skuOpus();
					var stsProductFlag = widget.skuSts();
					var skuId = widget.skuRepoId();
					var qty = widget.itemQty();
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": qty,
						"radiusLimit": 400,
						"limit": 4
					};
					setShipping(reqData, opusProductFlag, stsProductFlag);
				});


				function func2() {
					return widget.stockAvailable();
				}


				function processInventory(reqData, stores, quantity, storeNumber, locationInventoryInfo) {
					var sthSelected = $('#ship-to-home').prop('checked');

					widget.prodAvlFlag(1);
					var responseData = helpers.storeLocatorUtils().getInventoryResponse(stores, locationInventoryInfo, quantity, storeNumber, 4);
					var data_obj = reqData;
					var data2 = func2();


					if (!data_obj.skuId) {
						return;
					}

					var data = responseData,
						opusProductFlag = data_obj.opusProductFlag,
						stsProductFlag = data_obj.stsProductFlag,
						skuid = data_obj.skuId;

					var selectedStore = data.selectedStore,
						displaySTSflag, store_obj, curr_store_obj,
						my_store_arr = [],
						other_store_arr = [];

					if (data.dcInventory === 0 && data.storePickupEligibleQty === false) {
						if (data_obj.quantity > 1) {
							$(".tr-e-delivery").find(".error-msg").show().text("The requested quantity is not available for purchase.")
						} else {
						    widget.buttonVisibilityFlag(false);
						    widget.personalizeVisibilityFlag(false);
							$(".tr-e-delivery").find(".error-msg").show().text("This product is no longer available for Purchase.")
						}
						$("#inventory-error").text("");
						widget.displayOpus(false);
						widget.displaySts(false);
						widget.displayShipToHome(false);
						widget.prodAvlFlag(0);
						widget.personalizeVisibilityFlag(false);
						return;
					}

					widget.sfsStoreId(data.sfsStoreId);
					widget.isSFS(data.isSFS);

					// var dc_qty = data2[0].productSkuInventoryStatus[Number(skuid)];
					// var dc_qty = data2;
					var dc_qty = 0;
					widget.DCInventory(data.dcInventory);
					widget.maxQuantityStore(data.maxQuantityStore);
					/*if(data.dcInventory < data.maxQuantityInStore){
					    dc_qty = data.maxQuantityInStore;
					}
					else{

					}*/

					dc_qty = data.dcInventory;
					widget.availableSTHInventory(dc_qty);


					for (var i = 0; i < data.otherStores.length; i++) {
						store_obj = {};
						curr_store_obj = data.otherStores[i];
						store_obj.storeNumber = curr_store_obj.storeNumber || "";
						store_obj.threshold = curr_store_obj.threshold || 0;
						store_obj.storeName = curr_store_obj.storeName || "";
						store_obj.storeHours = curr_store_obj.storeHours || "";
						store_obj.shipToStoreAvailable = curr_store_obj.shipToStoreAvailable || false;
						store_obj.shipToActive = curr_store_obj.shipToActive || "N";
						store_obj.isAvailableToday = curr_store_obj.isAvailableToday || 0;
						store_obj.distanceInMiles = curr_store_obj.distanceInMiles || 0;
						store_obj.deliveryDate = curr_store_obj.deliveryDate || "Today";
						store_obj.closeTime = curr_store_obj.closeTime || 0;
						store_obj.isAvailableForPickup = curr_store_obj.isAvailableForPickup || 0;
						store_obj.onHandQuantity = curr_store_obj.onHandQuantity || 0;
						store_obj.address = curr_store_obj.addressLine1 + " " + curr_store_obj.addressLine2;
						store_obj.address1 = curr_store_obj.addressLine1;
						store_obj.address2 = curr_store_obj.addressLine2;
						store_obj.formattedDeliveryDate = curr_store_obj.formattedDeliveryDate;
						store_obj.phone = curr_store_obj.phone;
						store_obj.openTime = curr_store_obj.openTime ? curr_store_obj.openTime + " - " + store_obj.closeTime : 0;
						store_obj.longitude = curr_store_obj.longitude;
						store_obj.latitude = curr_store_obj.latitude;
						store_obj.zipCode = curr_store_obj.zipCode;
						store_obj.city = curr_store_obj.city;
						store_obj.state = curr_store_obj.state;

						other_store_arr.push(store_obj);
					}

					widget.otherStores(other_store_arr);
					var my_store_obj = {};

					if (selectedStore) {
						my_store_obj.storeNumber = selectedStore.storeNumber || "";
						my_store_obj.threshold = selectedStore.threshold || 0;
						my_store_obj.storeName = selectedStore.storeName || "";
						my_store_obj.storeHours = selectedStore.storeHours || "";
						my_store_obj.shipToStoreAvailable = selectedStore.shipToStoreAvailable || false;
						my_store_obj.shipToActive = selectedStore.shipToActive || "N";
						my_store_obj.isAvailableToday = selectedStore.isAvailableToday || 0;
						my_store_obj.distanceInMiles = selectedStore.distanceInMiles || 0;
						my_store_obj.deliveryDate = selectedStore.deliveryDate || "Today";
						my_store_obj.closeTime = selectedStore.closeTime || 0;
						my_store_obj.isAvailableForPickup = selectedStore.isAvailableForPickup || 0;
						my_store_obj.onHandQuantity = selectedStore.onHandQuantity || 0;
						my_store_obj.address = selectedStore.addressLine1 + " " + selectedStore.addressLine2;
						my_store_obj.address1 = selectedStore.addressLine1;
						my_store_obj.address2 = selectedStore.addressLine2;
						my_store_obj.formattedDeliveryDate = selectedStore.formattedDeliveryDate;
						my_store_obj.phone = selectedStore.phone;
						my_store_obj.openTime = selectedStore.openTime ? selectedStore.openTime + " - " + my_store_obj.closeTime : 0;
						my_store_obj.longitude = selectedStore.longitude;
						my_store_obj.latitude = selectedStore.latitude;
						my_store_obj.zipCode = selectedStore.zipCode;
						my_store_obj.city = selectedStore.city;
						my_store_obj.state = selectedStore.state;


						my_store_arr.push(my_store_obj);
						widget.myStore(my_store_arr);
					}

					widget.displayOpus(false);

					widget.displayShipToHome(false);
					widget.dcQuantity(dc_qty);
					if (widget.availableSTHInventory() >= widget.itemQty()) {
						// display ship to home
						widget.displayShipToHome(true);
						widget.backOrderFlag(false);
						$("#ship-to-home").attr("checked", true);
					}
					$(".tr-pickup-availability").hide();
					/////////// New OPUS-Logic
					//if(opusProductFlag) {
					if (selectedStore && selectedStore.isAvailableForPickup) {
						widget.availableTodayStore(selectedStore.storeName);
						widget.availableState(selectedStore.state);
						widget.availableCity(selectedStore.city);
						widget.availableQuantity(selectedStore.onHandQuantity);
						widget.displayOpus(true);
						widget.displaySts(false);
						widget.availableTodayVisibility(true);
						if (selectedStore.isAvailableToday) {
							// widget.availableDateText("Pick Up " + selectedStore.deliveryDate);
							$(".tr-pickup-availability").show();
						} else {
							widget.availableTodayStore(selectedStore.storeName);
							widget.availableState(selectedStore.state);
							widget.availableCity(selectedStore.city);

							widget.availableQuantity(selectedStore.onHandQuantity);
							// widget.availableDateText("Pick Up " + selectedStore.deliveryDate);
						}
						if (widget.displayShipToHome() === false) {
							$("#available-today").prop("checked", true);
						}
					} else if (selectedStore && selectedStore.shipToStoreAvailable) {

						if (productSts) {
							widget.displayOpus(false);
							widget.displaySts(true);
							widget.availableTodayVisibility(false);
							widget.stsStore(selectedStore.storeName);
							widget.stsAvailableText("Available for store pickup " + selectedStore.storeHours[5].date);
							if (widget.displayShipToHome() === false) {
								$("#available-sts").prop("checked", true);
							}
						}
					} else {
						if (data.storePickupEligibleQty === true && selectedStore) {
							widget.displayOpus(true);
						} else {
							widget.displayOpus(false);
						}
						widget.displaySts(false);
					}

					if (widget.displayOpus() === false &&
						widget.displaySts() === false &&
						widget.displayShipToHome() === false) {

						if (widget.finalBackOrderFlag()) {
							widget.displayShipToHome(true);
							widget.handleBackOrderDate();
						} else {
						    widget.buttonVisibilityFlag(false);
						    widget.personalizeVisibilityFlag(false);
							$(".tr-e-delivery").find(".error-msg").show().text("This product is no longer available for Purchase.")
							$("#inventory-error").text("");
							widget.prodAvlFlag(0);
							widget.personalizeVisibilityFlag(false);
						}
					}

					$.Topic("displayOpus").publish(widget.displayOpus());
					$.Topic("displaySts").publish(widget.displaySts());
					$.Topic("displayShipToHome").publish(widget.displayShipToHome());

					$("[id^=personalize-button]").prop("disabled", false);
					$("#inventory-error").text("");
					if ($("#available-today").prop("checked")) {
						if (!widget.myStore()[0].isAvailableForPickup && widget.displaySts()) {
							widget.availableTodayVisibility(false);
						}
						if (!widget.myStore()[0].isAvailableForPickup && !widget.displaySts()) {
							$("#inventory-error").text("Sorry, only “" + widget.myStore()[0].onHandQuantity + "” available. Please reselect quantity to proceed")
						} else {
							$("#inventory-error").text("");
						}
					} else if ($("#available-sts").prop("checked")) {
						if (widget.myStore()[0].onHandQuantity < widget.itemQty()) {
							$("#inventory-error").text("Sorry, only “" + widget.myStore()[0].onHandQuantity + "” available. Please reselect quantity to proceed")
						} else {
							$("#inventory-error").text("");

						}

					} else if (sthSelected) {
						if (widget.itemQty() > widget.availableSTHInventory() && widget.availableSTHInventory() > 0) {
							widget.displayShipToHome(true);
							widget.personalizeVisibilityFlag(true);

							$("[id^=personalize-button]").prop("disabled", true);
							$('#ship-to-home').click();
							$("#inventory-error").text("Sorry, the selected quantity is not available .Please reselect the quantity to proceed.")
							$(".tr-e-delivery").find(".error-msg").text('');
						} else {
							$("#inventory-error").text("");
							$("[id^=personalize-button]").prop("disabled", false);

						}

					}
					widget.destroyInventorySpinner();


				}

				function setShipping(reqData, opusProductFlag, stsProductFlag) {
					var validationDeferred = $.Deferred();
					var sthSelected = $('#ship-to-home').prop('checked');
					// widget.personalizeVisibilityFlag(true);
					$(".tr-e-delivery").find(".error-msg").hide();

					//TRV 2162 - S
					var inventoryQuantity = 0;
					if (widget.variantOptionsArray().length < 1) {
						if (widget.getProductData() && widget.getProductData().childSKUs.length == 1) {
							inventoryQuantity = widget.getProductData().childSKUs[0].quantity || 0;
						}
					} else {
						if (widget.getSelectedSKU()) {
							inventoryQuantity = widget.getSelectedSKU().quantity || 0;
						}
					}
					$(".tr-e-delivery").find(".error-msg").hide();
					reqData.inventoryQuantity = func2();
					var productSts = stsProductFlag,
						cartQuantity = 0;

					reqData.quantity = Number(reqData.quantity);
					reqData.stsProductFlag = stsProductFlag;
					reqData.opusProductFlag = opusProductFlag;

					if (widget.skuRepoId()) {
						widget.createInventorySpinner();
						$("#inventory-error").text("");
						$("[id^=personalize-button]").prop("disabled", true);


						var skuId = reqData.skuId;
						var storeNumber = reqData.storeNumber;
						var quantity = reqData.quantity;
						var radiusLimit = reqData.radiusLimit;
						var limit = reqData.limit;
						var latitude = reqData.location.latitude + "";
						var longitude = reqData.location.longitude + "";
						if (latitude.trim() === "" && longitude.trim() === "") {
							if (storeNumber.trim() != "") {
								var store = helpers.storeLocatorUtils().getStore(storeNumber, storeItems);
								if (store) {
									latitude = store.latitude;
									longitude = store.longitude;
								} else {
									latitude = 40.003601; // worst case set the default store lat long
									longitude = -82.931224;
								}
							} else {
								latitude = 40.003601; // worst case set the default store lat long
								longitude = -82.931224;
							}
						}
						var stores = helpers.storeLocatorUtils().getStores(latitude, longitude, storeItems);

						var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);

						/*if (inventoryObject.hasOwnProperty(skuId + "-" + storeIds)) {
						    setTimeout(function () {
						        processInventory(reqData, stores, quantity, storeNumber, inventoryObject[skuId + "-" + storeIds]);
						    }, 500);
						} else {
						    ccRestClient.authenticatedRequest("/ccstoreui/v1/inventories/" + skuId + "?locationIds=" + storeIds + "&includeDefaultLocationInventory=true", {}, function (data) {
						        inventoryObject[skuId + "-" + storeIds] = data.locationInventoryInfo;
						        //storageApi.getInstance().setItem("inventoryData", JSON.stringify(inventoryObject));
						        processInventory(reqData, stores, quantity, storeNumber, data.locationInventoryInfo);
						    }, function (data) {
						    }, "GET");
						}*/
						var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();
						helpers.storeLocatorUtils().getInventoryResponseIR(stores, quantity, storeNumber, 4, skuId, storeIds, reqData, "pdpLoad", cartInventoryDetailsArrayObj);
						

						widget.displayOpus.subscribe(function (data) {
							// This is causing some issues in the inventory check.

							if (widget.displaySts()) {
								widget.availableTodayVisibility(false);
							}

						});
						widget.product.subscribe(function (data) {
							widget.skuData(data);
						});
						$(".tr-other-store.occ-inventory-zero").hide();
						$(".tr-other-store.my-store").show();
						var initData = widget.skuData();
						if (!initData) {
							return;
						}
						if (initData.ItemType !== undefined) {
							var initType = initData.ItemType;
						} else if (initData.itemType === undefined) {
							// var initType = initData.itemType();
						}

						if (initType === 'p' || initType === 'P') {
							var initZone, initPhotoZone, initGift, initGiftId;

							if (typeof initData.ProductZones === 'function') {
								initZone = initData.ProductZones();
							} else {
								initZone = initData.ProductZones
							}

							if (typeof initData.photoUpload === 'function') {
								initPhotoZone = initData.photoUpload();
							} else {
								initPhotoZone = initData.photoUpload
							}

							if (typeof initData.hasGift === 'function') {
								initGift = initData.hasGift();
							} else {
								initGift = initData.hasGift
							}
							if (typeof initData.giftSkuId === 'function') {
								initGiftId = initData.giftSkuId();
							} else {
								initGiftId = initData.giftSkuId
							}
							/*if (!initZone) {
								widget.buttonVisibilityFlag(true); //Display add to cart button
								widget.personalizeVisibilityFlag(false);
							} else {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}
							if (initPhotoZone === true || initPhotoZone === false) {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}
							if (initGift && initGiftId) {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}*/

							//End Code to toggle Add to Cart and Personalize
						} else if (initType === 'v' || initType === 'V') {
							var initZone = initData.ProductZones || initData.productZones;
							if (typeof initZone === "function") {
								initZone = initZone();
							}
							if (typeof initPhotoZone === "function") {
								initPhotoZone = initPhotoZone();
							}
							var initPhotoZone = initData.photoUpload || initData.PhotoUpload;
							/*if ((initZone === null) || (typeof initZone === "undefined")) {
								widget.buttonVisibilityFlag(true); //Display add to cart button
								widget.personalizeVisibilityFlag(false);
							} else {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}
							if (initPhotoZone === true || initPhotoZone === false) {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}*/

							var initGift, initGiftId;

							if (typeof initData.hasGift === 'function') {
								initGift = initData.hasGift();
							} else {
								initGift = initData.hasGift
							}
							if (typeof initData.giftSkuId === 'function') {
								initGiftId = initData.giftSkuId();
							} else {
								initGiftId = initData.giftSkuId
							}

							//if (initData.hasGift() && initData.giftSkuId()) {
							/*if (initGift && initGiftId) {
								widget.buttonVisibilityFlag(false); //Display personalize button
								widget.personalizeVisibilityFlag(true);
							}*/
						}
						widget.destroyInventorySpinner();

					}

					widget.validationDeferred = validationDeferred;
					return validationDeferred;
				}

				$.Topic('UPDATE_OTHER_STORES_MODAL').subscribe(function (data) {
					var selectedStore = data.selectedStore,
						my_store_arr = [],
						other_store_arr = [];

					for (var i = 0; i < data.otherStores.length; i++) {
						store_obj = {};
						curr_store_obj = data.otherStores[i];
						store_obj.storeNumber = curr_store_obj.storeNumber || "";
						store_obj.threshold = curr_store_obj.threshold || 0;
						store_obj.storeName = curr_store_obj.storeName || "";
						store_obj.storeHours = curr_store_obj.storeHours || "";
						store_obj.shipToStoreAvailable = curr_store_obj.shipToStoreAvailable || false;
						store_obj.shipToActive = curr_store_obj.shipToActive || "N";
						store_obj.isAvailableToday = curr_store_obj.isAvailableToday || 0;
						store_obj.distanceInMiles = curr_store_obj.distanceInMiles || 0;
						store_obj.deliveryDate = curr_store_obj.deliveryDate || "Today";
						store_obj.closeTime = curr_store_obj.closeTime || 0;
						store_obj.isAvailableForPickup = curr_store_obj.isAvailableForPickup || 0;
						store_obj.onHandQuantity = curr_store_obj.onHandQuantity || 0;
						store_obj.address = curr_store_obj.addressLine1 + " " + curr_store_obj.addressLine2
						store_obj.phone = curr_store_obj.phone;
						store_obj.openTime = curr_store_obj.openTime ? curr_store_obj.openTime + " - " + store_obj.closeTime : 0;
						store_obj.longitude = curr_store_obj.longitude;
						store_obj.latitude = curr_store_obj.latitude;
						store_obj.zipCode = curr_store_obj.zipCode;
						store_obj.city = curr_store_obj.city;
						store_obj.state = curr_store_obj.state;

						other_store_arr.push(store_obj);
					}

					widget.otherStores(other_store_arr);
					var my_store_obj = {};

					my_store_obj.storeNumber = selectedStore.storeNumber || "";
					my_store_obj.threshold = selectedStore.threshold || 0;
					my_store_obj.storeName = selectedStore.storeName || "";
					my_store_obj.storeHours = selectedStore.storeHours || "";
					my_store_obj.shipToStoreAvailable = selectedStore.shipToStoreAvailable || false;
					my_store_obj.shipToActive = selectedStore.shipToActive || "N";
					my_store_obj.isAvailableToday = selectedStore.isAvailableToday || 0;
					my_store_obj.distanceInMiles = selectedStore.distanceInMiles || 0;
					my_store_obj.deliveryDate = selectedStore.deliveryDate || "Today";
					my_store_obj.closeTime = selectedStore.closeTime || 0;
					my_store_obj.isAvailableForPickup = selectedStore.isAvailableForPickup || 0;
					my_store_obj.onHandQuantity = selectedStore.onHandQuantity || 0;
					my_store_obj.address = selectedStore.addressLine1 + " " + selectedStore.addressLine2;
					my_store_obj.address1 = selectedStore.addressLine1;
					my_store_obj.address2 = selectedStore.addressLine2;
					my_store_obj.formattedDeliveryDate = selectedStore.formattedDeliveryDate;
					my_store_obj.phone = selectedStore.phone;
					my_store_obj.openTime = selectedStore.openTime ? selectedStore.openTime + " - " + my_store_obj.closeTime : 0;
					my_store_obj.longitude = selectedStore.longitude;
					my_store_obj.latitude = selectedStore.latitude;
					my_store_obj.zipCode = selectedStore.zipCode;
					my_store_obj.city = selectedStore.city;
					my_store_obj.state = selectedStore.state;

					my_store_arr.push(my_store_obj);
					widget.myStore(my_store_arr);
					$('#tr-e-available-store').modal(); //Display Modal to change store
				});


				//End Omni Channel

				$.Topic("tr-personalized-item-to-bag").subscribe(widget.handlePersonalizedPdtAddToCart.bind(widget));
				//TRV - 2162 - S
				$.Topic('SKU_SELECTED').subscribe(function (product, selectedSKUObj, variantOptions, qnty) {
					widget.setSelectedSKU(selectedSKUObj);
					widget.setSelectedSKUQty(qnty);
				});
				//Code to change between addtocart and personalize button
				$.Topic('SKU_OBJ1').subscribe(function (data) {/*
					publishedSkuData = data;
					var publishedZone = publishedSkuData.ProductZones || publishedSkuData.productZones;
					var initPhotoZone = publishedSkuData.photoUpload;
					var productZonesIds = {};
					if (publishedZone) {
						productZonesIds[CCConstants.PRODUCT_IDS] = publishedZone;
						var productZonesData = storageApi.getInstance().getItem('productZones' + publishedSkuData.id);
						if (refreshData || !productZonesData) {
							ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, productZonesIds, function (productZonesArray) {
								storageApi.getInstance().setItem('productZones' + publishedSkuData.id, JSON.stringify(productZonesArray));
								var zoneIds = '';
								for (i = 0; i < productZonesArray.length; i++) {
									if (zoneIds.indexOf(productZonesArray[i].zone) === -1) {
										if (i === 0) {
											zoneIds = productZonesArray[i].zone;
										} else {
											zoneIds = zoneIds + ',' + productZonesArray[i].zone;
										}
									}
								}
								var zone = {};
								if (zoneIds !== '') {
									zone[CCConstants.PRODUCT_IDS] = zoneIds;
									ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, zone, function (zoneData) {
										storageApi.getInstance().setItem('zones' + publishedSkuData.id, JSON.stringify(zoneData));
									});
								}
							});
						}
					}
					//If no product zone data is available, hide personalize and display add to cart.
					if (!publishedZone) {
						widget.buttonVisibilityFlag(true); //Display add to cart button
						widget.personalizeVisibilityFlag(false);
						widget.checkPersonalization(false);
					} else {
						widget.buttonVisibilityFlag(false); //Display personalize button
						widget.personalizeVisibilityFlag(true);
						widget.checkPersonalization(true);
					}
					if (initPhotoZone === true) {
						widget.buttonVisibilityFlag(false); //Display personalize button
						widget.personalizeVisibilityFlag(true);
						widget.checkPersonalization(true);
					}
					if (publishedSkuData.hasGift && publishedSkuData.giftSkuId) {
						widget.buttonVisibilityFlag(false); //Display personalize button
						widget.personalizeVisibilityFlag(true);
						widget.checkPersonalization(true);
					}
				*/});
				//End Code to change between addtocart and personalize button

				$.Topic('SKU_OBJ1').subscribe(function (product_obj) {/*

					publishedSkuData = product_obj;
					widget.skuBackorderFlag(publishedSkuData.backorder);
					//TROCC 3008 Start
					if (widget.skuBackorderFlag() && (widget.stockAvailable() === 0 || widget.itemQty() > widget.stockAvailable())) {
						if ((publishedSkuData.AIRBackorderDeliveryDates ||
								publishedSkuData.BWYBackorderDeliveryDates ||
								publishedSkuData.PRMBackorderDeliveryDates ||
								publishedSkuData.two2NDBackorderDeliveryDates)) {

							var id = product_obj.id;
							ccRestClient.request("getInventory", null, inventoryDetails, inventoryDetailsError, id);

							function inventoryDetails(data) {
								if (data.backorderLevel > 0 && widget.itemQty() <= data.backorderLevel) {
									widget.finalBackOrderFlag(true);
								} else {
									widget.finalBackOrderFlag(false);
								}
							}

							function inventoryDetailsError(data) {

							}

						} else {
							widget.finalBackOrderFlag(false);
						}
					} else {
						widget.finalBackOrderFlag(false);
					}
					//TROCC 3008 End
					// the below code works fine in preview mode but in live mode sees some
					// parallel execution with stock status. so having a fraction of seconds on set time out delay
					setTimeout(function () {
						skuInventory = widget.stockAvailable();
						if ((publishedSkuData.backorder && (widget.stockAvailable() === 0 || widget.itemQty() > widget.stockAvailable()))) {
							// widget.handleBackOrderDate();
						} else {
							widget.backOrderFlag(false);
							widget.deliveryDateHeadingFlag(true);
							var fullDate = product_obj.BWYDeliveryDates || '';
							if (fullDate !== '' && fullDate !== null) {
								var index = fullDate.indexOf('-');
								var newDate = fullDate.substring(index + 1);
								newDate = newDate.replace('-', '/');
								newDate = "Estimated Arrival " + newDate;
								widget.standardDeliveryDate(newDate);
							}
							if ($(".tr-e-product-description #descr-container").height() + 10 < $(".tr-e-product-description #descr-container .tr-prod-descr-string").height()) {
								$(".tr-prod-descr-show-more").show();
							} else {
								$(".tr-prod-descr-show-more").hide();
							}

							if (typeof product_obj.BWYDeliveryDates !== "undefined" && product_obj.BWYDeliveryDates) {
								widget.bwyFlag(true);
								widget.bwyFlagDate(product_obj.BWYDeliveryDates);
							} else {
								widget.bwyFlag(false);
							}
							if (typeof product_obj.two2NDDeliveryDates !== "undefined" && product_obj.two2NDDeliveryDates) {
								widget.two_ndFlag(true);
								widget.two_ndFlagDate(product_obj.two2NDDeliveryDates);
							} else {
								widget.two_ndFlag(false);
							}
							if (typeof product_obj.AIRDeliveryDates !== "undefined" && product_obj.AIRDeliveryDates) {
								widget.airFlag(true);
								widget.airFlagDate((product_obj.AIRDeliveryDates));
							} else {
								widget.airFlag(false);
							}

							if (typeof product_obj.PRMDeliveryDates !== "undefined" && product_obj.PRMDeliveryDates) {
								widget.prmFlag(true);
								widget.prmFlagDate(getPRMDate(product_obj.PRMDeliveryDates));
							} else {
								widget.prmFlag(false);
							}

						}
					}, 300); // set timeout ends
				*/});

				//Update Button
				$.Topic("SKU_OBJ1").subscribe(function (cartData) {/*
					if (widget.cart().items !== undefined) {
						var cartLength = widget.cart().items().length;
						var cartFlag = false;
						if (cartData.id !== undefined) {
							for (var i = 0; i < cartLength; i++) {
								if (widget.cart().items()[i].catRefId == cartData.id) {
									cartFlag = true;
									break;
								}
							}

							if (cartFlag) {
								$("[id^=addToCart-button]").text("Update");
							} else {
								$("[id^=addToCart-button]").text("Add To Bag");
							}
						}
						cartFlag = false;
					}
				*/});
				//End Update Button
				//Product Images
				$.Topic("SKU_OBJ1").subscribe(function (prod) {
					widget.activeImgIndex(0);
					var prodImages = widget.getProductImages("L", prod),
						product = widget.product();
					zeroPaddedProdId = widget.getZeroPaddedSkuId(prod.repositoryId);

					prodImages.unshift(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					product.primaryFullImageURL(_this.scene7BaseUrl + zeroPaddedProdId);
					product.primaryMediumImageURL(_this.scene7BaseUrl + zeroPaddedProdId);
					product.mediumImageURLs(prodImages);
					product.fullImageURLs(prodImages);
					widget.imgGroups(widget.groupImages(prodImages));
					product.thumbImageURLs(prodImages);

					while (product.product.productImagesMetadata.pop()) {}

					prodImages.forEach(function (elem, index) {
					    var alt = "Main Image of " + product.displayName();
					    if(index !== 0){
					        alt = "Alt #" + index + " " + product.displayName();
					    }
						product.product.productImagesMetadata.push({
							altText: alt
						});
						
					});

					widget.imgMetadata = [];
					widget.activeImgIndex.valueHasMutated();
				});
				//End Product Images
				//Stock Validations
				$.Topic('AVAILABLE_TODAY').subscribe(function (avl_flag) {
					widget.availableToday(avl_flag);
					if (!widget.allOptionsSelected()) {
						$(".tr-e-delivery").find(".error-msg").hide();
						return;
					}

					//stockAvailable
					if ($("input[name='optradio']:checked").val() === "2") {
						if (widget.itemQty() > widget.stockAvailable()) {
							if (!(skuBackorderFlag && widget.stockAvailable() === 0)) {
								// $(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
								// $(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);

								/*if(variantOptions.length === 1){
								 $(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
								 }*/
							}
						} else {
							// $(".tr-e-product-variation").find(".error-msg[data-val = 2]").hide();
						}
					}
				});

				//End Stock Validations
				$.Topic('AVAILABLE_STORE_NAME').subscribe(function (storeName) {
					widget.availableTodayStore(storeName);

				});
				
				
				
				
				
				
				// End OnLoad
			},
		
			setCookie: function (cname, cvalue) {
				document.cookie = cname + "=" + cvalue;
			},
			beforeAppear: function (page) {
				var widget = this;
				widget.giftTitle('');
				var opusGlobalOffFlag = site.getInstance().extensionSiteSettings['externalSiteSettings']['opusGlobalOffFlag'];
				if (opusGlobalOffFlag && opusGlobalOffFlag.toUpperCase() === "TRUE") {
					widget.opusDisabled(true);
				}
				widget.imgMetadata = [];
				widget.selectedBomArray([]);
				widget.bomArray([]);
				
				cartInventoryDetailsArray = [];
				processingTime = 0;
				currentPersonalizationData = [];
				var storeItemObj = storageApi.getInstance().getItem("storeItems");
				if (storeItemObj && storeItemObj !== '') {
					storeItems = JSON.parse(storeItemObj);
					window.storeItems = storeItems;
				}
				if (location.search.substr(1).indexOf("basketLineId") != -1) {
					$("#tr-w-product-details").addClass("hide");
				}

				$(window).on("hashchange", function () {
					if (window.location.hash === '') {
						$('body').removeClass("modal-open");
					}
				});

				if (location.search.substr(1).indexOf("basketLineId") == -1) {
					var e = document.getElementById("refreshBack");
					if (!e || e.value === "yes") {
						//location.reload();
					} else {
						e.value = "yes";
					}
				}

				inventoryObject = {};

				function detectIE() {
					var ua = window.navigator.userAgent;

					var msie = ua.indexOf('MSIE ');
					if (msie > 0) {
						// IE 10 or older => return version number
						return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
					}

					var trident = ua.indexOf('Trident/');
					if (trident > 0) {
						// IE 11 => return version number
						var rv = ua.indexOf('rv:');
						return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
					}

					var edge = ua.indexOf('Edge/');
					if (edge > 0) {
						// Edge (IE 12+) => return version number
						return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
					}

					// other browser
					return false;
				}

				var isIEbrowser = detectIE();
				if (isIEbrowser) {
					setTimeout(function () {
						if (location.search.substr(1).indexOf("basketLineId") != -1) {
							return;
						}
					}, 1500)
				} else {
					if (location.search.substr(1).indexOf("basketLineId") != -1) {
						return;
					}
				}


				document.onclick = function (event) {
					//$('#at15s').css('visibility','hidden');
				};

				widget.prodTitleNew(widget.product().displayName());
				widget.skuidNew(-1);
				widget.skulistPriceNew(0);
				widget.skusalePriceNew(0);
				widget.occasionCode('');
				if ($(window).width() < 767) {
					widget.createInventorySpinner();
				}
				$('#main').wrap('<div itemscope itemtype="http://schema.org/Product">');

				var reviewsClicked = sessionStorage.getItem('ReviewsClicked');

				var extraPixels;
				if ((navigator.userAgent.match(/iPhone/i))) {
					extraPixels = 1200;
				} else {
					extraPixels = 500;
				}

				if (reviewsClicked == 'True') {

					var checkExist = setInterval(function () {

						if ($('.bv-content-container').length) {

							$('#tr-w-product-details .tr-e-review>a').click();
							$('html, body').animate({
								scrollTop: $('.bv-content-container').position().top + extraPixels
							}, 'slow');
							sessionStorage.removeItem('ReviewsClicked');
							clearInterval(checkExist);

						} else {

							$('#tr-w-product-details .tr-e-review>a').click();
							$('html, body').animate({
								scrollTop: $('#reviews').position().top + extraPixels
							}, 'slow');
							sessionStorage.removeItem('ReviewsClicked');
							clearInterval(checkExist);

						}

					}, 100);

				}

				var widget = this;
				if ($(window).width() < 767) {
					widget.createInventorySpinner();
				}
				this.setCookie("personalizationProducts", "");
				this.setCookie("zoneProduct", "");
				$("html, body").animate({
					scrollTop: 0
				});
				var addThisSrc = "//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58206e7d9dbcbcd1";
				$('script[src="' + addThisSrc + '"]').remove();
				$('<script>').attr('src', addThisSrc).appendTo('body');

				setTimeout(function () { // IE fix for accessibility to avoid focus on SVG
					$("svg").attr("focusable", false);
				}, 2000)

				widget.skuRepoId(null);
				//widget.personalizeVisibilityFlag(true);
				$(".tr-e-delivery").find(".error-msg").hide();

				widget.giftText('');
				widget.giftData('');
				widget.hasGift('');
				widget.giftDisplayName(false);
				//Free Gift Code
				if (widget.product().hasGift && typeof widget.product().hasGift == 'function') {
					widget.hasGift(widget.product().hasGift());
				}
				if (widget.product().giftSkuId && typeof widget.product().giftSkuId == 'function') {
					if (widget.hasGift() && widget.product().giftSkuId()) {
						var data = [];
						data[CCConstants.PRODUCT_IDS] = widget.product().giftSkuId();
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, data, function handleGift(data) {
							var giftItemName = data[0].displayName;
							var giftText = "Minimum personalization charge required on promotional item:" + "<i>" + giftItemName + "</i>";
							widget.giftText(giftText);
							widget.giftData(data[0]);
							widget.giftDisplayName(widget.product().displayName() + ' with Free ' + giftItemName);
							$.Topic("GIFT_ITEM").publish(data);
							widget.prodTitleNew(widget.product().displayName());
							widget.giftTitle(' with Free ' + giftItemName);
							var publishedZone = data[0].ProductZones || data[0].productZones;
							var productZonesIds = {};
							if (publishedZone) {
								productZonesIds[CCConstants.PRODUCT_IDS] = publishedZone;
								var productZonesData = storageApi.getInstance().getItem(data[0].id + '-productZones');
								if (refreshData || !productZonesData) {
									ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, productZonesIds, function (productZonesArray) {
										storageApi.getInstance().setItem(data[0].id + '-productZones', JSON.stringify(productZonesArray));
										var zoneIds = '';
										for (i = 0; i < productZonesArray.length; i++) {
											if (zoneIds.indexOf(productZonesArray[i].zone) === -1) {
												if (i === 0) {
													zoneIds = productZonesArray[i].zone;
												} else {
													zoneIds = zoneIds + ',' + productZonesArray[i].zone;
												}
											}
										}
										var zone = {};
										if (zoneIds !== '') {
											zone[CCConstants.PRODUCT_IDS] = zoneIds;
											ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, zone, function (zoneData) {
												storageApi.getInstance().setItem('zones' + data[0].id, JSON.stringify(zoneData));
											});
										}
									});
								}
							}
						}, function (data) {});
					}
				}
				//End Free Gift Code
				//Hide the Available for today and OPUS/STS Data from previous product.
				$(".tr-pickup-availability").hide();
				widget.displayOpus(false);
				widget.displaySts(false);
				widget.buttonVisibilityFlag(false);
				//Display personalization by default
				//widget.personalizeVisibilityFlag(true);
				widget.checkResponsiveFeatures($(window).width());
				this.backLinkActive(true);
				if (!widget.isPreview() && !widget.historyStack.length) {
					this.backLinkActive(false);
				}

				/* reset active img index to 0 */
				widget.shippingSurcharge(null);
				widget.activeImgIndex(0);
				widget.bomArray([]);
				widget.firstTimeRender = true;
				this.populateVariantOptions(widget);
				this.populateUIDisplayAttr(widget);
				this.populateOccasions(widget);
				this.loadCarousel();
				if (widget.product()) {

					var productId = widget.product().id().split('_')[0],
						zeroPaddedProdId = "";


					for (var i = 0; i < (9 - productId.length); i++) {
						zeroPaddedProdId += "0";
					}
					zeroPaddedProdId += productId;


					var productImageArr = widget.getProductImages("S");

					productImageArr.unshift(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);


					//widget.mainImgUrl(widget.product().primaryFullImageURL());
					widget.product().primaryFullImageURL(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					widget.product().primaryMediumImageURL(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					widget.product().mediumImageURLs(productImageArr);
					widget.product().fullImageURLs(productImageArr);

					widget.imgGroups(widget.groupImages(productImageArr));
					widget.product().thumbImageURLs(productImageArr);

					productImageArr.forEach(function () {

						widget.product().product.productImagesMetadata.push({
							altText: "Alt # " + widget.product().displayName()
						});

					});
				}
				widget.loaded(true);

				// the dropdown values should be pre-selected if there is only one sku
				if ((widget.product() && widget.product().childSKUs().length == 1) || storageApi.getInstance().getItem("test")) { // TODO
					this.filtered(false);
					this.filterOptionValues(null);
					var sku_id = widget.product().childSKUs()[0].repositoryId(),
						publishedSkuData = widget.product().childSKUs()[0];

					var skuToSelect;
					if (storageApi.getInstance().getItem("test")) {
						skuToSelect = widget.product().childSKUs().filter(function (item) {
							return item.repositoryId() === "793252"; // TODO
						})[0];
						publishedSkuData = skuToSelect;
						sku_id = skuToSelect.repositoryId();
					}

					if (widget.product().backorder && widget.product().backorder()) {
						widget.skuBackorderFlag(widget.product().backorder());
					}
					widget.skuRepoId(sku_id);
					// widget.skuSts(widget.product().STS());
					widget.skuSts(true);
					if (widget.product().OPUS && typeof widget.product().OPUS == 'function') {
						widget.skuOpus(widget.product().OPUS());
					}
					$(".tr-e-product-description").find('a').find('div.id span').text(sku_id);

					$.Topic('SKU_SELECTED').publish(widget.product(), publishedSkuData, [], widget.itemQty());


					//Checks if product has zones and then displays the appropriate button
					var initData = widget.product();
					if (widget.product().ItemType !== undefined) {
						var initType = widget.product().ItemType();
					} else if (widget.product().itemType !== undefined) {
						var initType = widget.product().itemType();
					}

					if (initType === 'p' || initType === 'P') {
						if (typeof initData.ProductZones === 'function') {
							var initZone = initData.ProductZones();
						} else {
							var initZone = initData.ProductZones
						}

						var initPhotoZone = initData.photoUpload();
						var initGift = initData.hasGift();
						var initGiftId = initData.giftSkuId();
						if (!initZone) {
							widget.buttonVisibilityFlag(true); //Display add to cart button
							widget.personalizeVisibilityFlag(false);
						} else {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
						if (initPhotoZone === true || initPhotoZone === false) {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
						if (initGift && initGiftId) {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
					}
					//End Code to toggle Add to Cart and Personalize
				} else {

					//Checks if product has zones and then displays the appropriate button

					var initData = widget.product();
					if (widget.product().ItemType !== undefined) {
						var initType = widget.product().ItemType();
					} else if (widget.product().itemType !== undefined) {
						var initType = widget.product().itemType();
					}
					if (initType === 'v' || initType === 'V') {
						var initZone = initData.ProductZones || initData.productZones;
						if (typeof initZone === "function") {
							initZone = initZone();
						}
						if (typeof initPhotoZone === "function") {
							initPhotoZone = initPhotoZone();
						}
						var initPhotoZone = initData.photoUpload || initData.PhotoUpload;
						if ((initZone === null) || (typeof initZone === "undefined")) {
							widget.buttonVisibilityFlag(true); //Display add to cart button
							widget.personalizeVisibilityFlag(false);
						} else {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
						if (initPhotoZone === true || initPhotoZone === false) {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
						if (initData.hasGift() && initData.giftSkuId()) {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
					}


				}


				notifier.clearSuccess(this.WIDGET_ID);
				var catalogId = null;
				if (widget.user().catalog) {
					catalogId = widget.user().catalog.repositoryId;
				}
				widget.listPrice(widget.product().listPrice());
				widget.salePrice(widget.product().salePrice());

				if (widget.product()) {
					widget.product().stockStatus.subscribe(function (newValue) {
						if (widget.product().stockStatus().stockStatus === CCConstants.IN_STOCK) {
							if (widget.product().stockStatus().orderableQuantity) {
								widget.stockAvailable(widget.product().stockStatus().orderableQuantity);
							} else {
								widget.stockAvailable(1);
							}
							widget.disableOptions(false);
							widget.stockStatus(true);
						} else {
							widget.stockAvailable(0);
							widget.disableOptions(true);
							widget.stockStatus(false);
						}
						widget.showStockStatus(true);
					});
					var firstchildSKU = widget.product().childSKUs()[0];
					if (firstchildSKU) {
						var skuId = firstchildSKU.repositoryId();
						if (this.variantOptionsArray().length > 0) {
							skuId = '';
						}
						this.showStockStatus(false);
						widget.product().getAvailability(widget.product().id(), skuId, catalogId);
						widget.product().getPrices(widget.product().id(), skuId);
					} else {
						widget.stockStatus(false);
						widget.disableOptions(true);
						widget.showStockStatus(true);
					}
					this.priceRange(this.product().hasPriceRange);
					widget.mainImgUrl(widget.product().primaryFullImageURL());

					$.Topic(pubsub.topicNames.PRODUCT_VIEWED).publish(widget.product());
					$.Topic(pubsub.topicNames.PRODUCT_PRICE_CHANGED).subscribe(function () {
						widget.listPrice(widget.product().listPrice());
						widget.salePrice(widget.product().salePrice());
						widget.shippingSurcharge(widget.product().shippingSurcharge());
					});
				}

				// Load spaces
				if (widget.user().loggedIn()) {
					widget.getSpaces(function () {});
				}

				// Delivery Date
				if (widget.product().BWYBackorderDeliveryDates && typeof widget.product().BWYBackorderDeliveryDates == 'function') {
					var fullDate = widget.product().BWYBackorderDeliveryDates();
					if (fullDate !== '' && fullDate !== null) {
						var index = fullDate.indexOf('-');
						var newDate = fullDate.substring(index + 1);
						newDate = newDate.replace('-', '/');
						newDate = "Estimated Arrival " + newDate;
						widget.standardDeliveryDate(newDate);
					}
				}

				//Flash Video

				if (widget.product().videoFlag && typeof widget.product().videoFlag == 'function' && widget.product().videoFlag() === true) {
					var params = {
						wmode: "transparent"
					};
					var skuID = widget.product().id();

					widget.videoUrl(window.location.protocol + "//s7d1.scene7.com/s7viewers/html5/VideoViewer.html?videoserverurl=https://s7d1.scene7.com/is/content&asset=ThingsRemembered/000" + skuID + "_MP4")

				}


				$(document).on("click", ".occasion-btn", function (e) {
					if ($(this).parent(".dropdown").hasClass("open")) {} else {
						$(this).parent(".dropdown").addClass("open");
					}

				});
				$(document).on("click", ".occasion-dropdown", function () {
					$(this).parent(".dropdown").removeClass("open");
				});

				$(document).on("click", ".occasion-dropdown li", function () {

					widget.occasionCode($(this).attr('occasion_index'));
					$.Topic('OCCASION').publish(widget.occasionCode());

				});


				if (storageApi.getInstance().getItem("test")) { // TODO
					location.hash = "personalize";
					$(".tr-qty-input").change();
					$('html,body').animate({
						scrollTop: 0
					}, 600);
					// $('#personalization-zone-container').empty();
					var variantOptions = this.variantOptionsArray();
					notifier.clearSuccess(this.WIDGET_ID);
					//get the selected options, if all the options are selected.
					var selectedOptions = this.getSelectedSkuOptions(variantOptions);
					//Check if gift item is present. And send it to personalization
					if (this.hasGift() && this.giftData()) {
						$.Topic('PERSONALIZE_GIFT_ITEM').publish(this.giftData());
					} else {
						$.Topic('PERSONALIZE_GIFT_ITEM').publish(false);
					}
					var selectedOptionsObj = {
						'selectedOptions': selectedOptions
					};

					var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
					if (this.variantOptionsArray().length > 0) {
						//assign only the selected sku as child skus
						newProduct.childSKUs = [this.selectedSku()];
					}
					newProduct.orderQuantity = parseInt(this.itemQty(), 10);

					var l_windowWidth = $(window).width();
					if (l_windowWidth <= 767) {
						var pdpTitleInfo = $('.tr-personlization-pdp-header-info');
						if (selectedOptions[0] && selectedOptions[0].optionName === "Color") {
							var colorVal = selectedOptions[0].optionValue.split('|')[0];
							pdpTitleInfo.find('#tr-persona-color-xs').text(colorVal);
						}
						if (selectedOptions[1] && selectedOptions[1].optionName === "Size") {
							var sizeVal = selectedOptions[1].optionValue.split('|')[0];
							pdpTitleInfo.find('#tr-persona-size-xs').text(sizeVal);
						}
						pdpTitleInfo.css({
							'display': 'block'
						});
						$('.tr-personlization-pdp-header-section').append(pdpTitleInfo);
					}

					$.Topic("SENT_TO_PERSONALIZE").publish({
						qty: parseInt(this.itemQty(), 10),
						opusPhotoUpload: this.isPhotoUpload
					});

					// To disable Add to cart button for three seconds when it is clicked and enabling again
					this.isAddToCartClicked(true);
					var self = this;
					setTimeout(enableAddToCartButton, 3000);

					function enableAddToCartButton() {
						self.isAddToCartClicked(false);
					};
				}

				// added for to handle TROCC-3448 starts
				
				
				
				$(window).click(function(e) {
                    $('#tr-e-available-store').on('shown.bs.modal', function () {
        					$('body').css('overflow', 'hidden');
        					$('body').css('position', 'fixed');
				    });
				    $('#tr-e-available-store').on('hidden.bs.modal', function () {
        					$('body').css('overflow', 'unset');
        					$('body').css('position', 'unset');
				    });
                });
				
				
				
				widget.selectOccassionForMobile();

				var productID = widget.product().id();
				var productName = widget.product().displayName();
				var orderQuantity = 1;
				var productBrand = widget.product().brand();
				var productListPrice = widget.product().productListPrice;
				var saleprice = widget.product().salePrice();
				var productVariant = widget.product().variantName();
				var productCategory;

				if (widget.product().parentCategories().length > 1) {
					productCategory = ko.toJS(widget.product().parentCategories()["1"].id);
				}

				productPrice = productListPrice;

				if (saleprice !== null && saleprice < productListPrice)
					productPrice = saleprice;

				dataLayer.push({
					'event': 'productView',
					'ecommerce': {
						'detail': {
							'products': [{
								'name': productName,
								'id': productID,
								'quantity': orderQuantity, // TODO: Needed?
								'price': productPrice,
								'category': productCategory, // TODO: Unknown for now
								'brand': productBrand, // TODO: always null
								'variant': productVariant // TODO: always undefined
							}]
						}
					}
				});

				$.getScript("https://display.ugc.bazaarvoice.com/static/Thingsremembered/en_US/bvapi.js", function () {
					$.Topic(pubsub.topicNames.PRODUCT_VIEWED).subscribe(function (v) {
						$BV.ui('rr', 'show_reviews', {
							productId: widget.product().id(),
							doShowContent: function () {
								$('#tr-w-product-details .tr-e-review>a').click();
							}
						});
					});

				});
				var skuId = "";
				var giftSku = "";
				if(widget.product().hasGift() && widget.product().giftSkuId() && widget.product().giftSkuId() !== "") {
				    giftSku = "?giftSku=" + widget.product().giftSkuId();
				}
				if(widget.product().childSKUs().length === 0 || widget.product().childSKUs().length === 1){
				    widget.personalizeDisabledFlag(true);
				    widget.createInventorySpinner();
				    if(widget.product().childSKUs().length === 0){
				        skuId = widget.product().id();
				    }else{
				        skuId = widget.product().childSKUs()[0].repositoryId().split("_")[0];
				    }
				    $.ajax({
                        url : widget.site().extensionSiteSettings.externalSiteSettings.awsUrl + skuId + giftSku,
                        method: "GET",
                        success : function(data) {
							isPersonalizationPropertyExist = data.zones.length;
                            if(data.zones.length > 0){
                                newPersonalizationData = data;
                                widget.generateExpressPhase(newPersonalizationData);
            					widget.generateFonts(newPersonalizationData);
            					widget.generalFonts(newPersonalizationData);
            					widget.generateColorFills(newPersonalizationData);
            					widget.generateDesigns(newPersonalizationData);
            					widget.generateNographic(newPersonalizationData);
            					var viewModel = ko.mapping.fromJS(newPersonalizationData);
            					$.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
                            }else if($('.error-msg').is(':visible') === false){
                                $('.tr-xs-personalize-block').removeClass('hide');
                                $(".personalizationBtn").hide();
								$(".addToCartBtn").show();
								$(".personalizationBtns").removeClass('hide');
                            }
                            
        					widget.destroyInventorySpinner();
                        },
                        error: function() {
                            getfallBackDatafromSku(widget);
                        }
				    });
				}
				
				$("body").delegate(".minicart-button", "click", function(){
				    $("body").removeClass('modal-open');
				});
			},  

  
			selectOccassionForMobile: function () {
				var widget = this;
				if (!widget.occasionCode()) {  
					if (widget.mobileDefaultOccassionId() !== '') {
						$(".occasion-btn").find("span.index").text(widget.mobileDefaultOccassionIndex());
						widget.occasionCode(widget.mobileDefaultOccassionId());
						$.Topic('OCCASION').publish(widget.occasionCode());
					} else {
						$(".occasion-btn").find("span.index").text(0);
					}
				}
			},

			goBack: function () {
				$(window).scrollTop($(window).height());
				window.history.go(-1);
				return false;
			},


			// Handles loading a default 'no-image' as a fallback
			cancelZoom: function (element) {
				$(element).parent().removeClass('zoomContainer-CC');
			},
			populateOccasions: function (widget) {
				if (!widget.product().occasions) {
					return;
				}
				var occasions = widget.product().occasions().replace(/<p>/g, ""),
					occasions_arr = occasions.split("</p>");
				occasions_arr.unshift("00000|Select Occasion");
				widget.occasionsArr.removeAll();
				for (var i = 0; i < occasions_arr.length; i++) {
					if (occasions_arr[i].indexOf('No Thanks') != -1) {
						widget.mobileDefaultOccassionId(occasions_arr[i].split('|')[0]);
						widget.mobileDefaultOccassionIndex(i);
					}
					widget.occasionsArr().push(occasions_arr[i] + '~' + i)
				}
				if (widget.mobileDefaultOccassionId() == '' && occasions_arr.length > 0) {
					widget.mobileDefaultOccassionId(occasions_arr[1].split('|')[0]);
					widget.mobileDefaultOccassionIndex(1);
				}
			},
			populateUIDisplayAttr: function (widget) {
				if (!widget.product().UIDisplayAttribute || !widget.product().UIDisplayAttribute()) {
					widget.uiDisplayAttr("true");
					return;
				}
                // console.log("uiDisplayAttr");
                var dispAttr = "";
			 //   console.log(dispAttr);
				widget.uiDisplayAttr(widget.product().UIDisplayAttribute().split(","));

			},

			//this method populates productVariantOption model to display the variant options of the product
			populateVariantOptions: function (widget) {
				var options = widget.productVariantOptions();
				if (options && options !== null && options.length > 0) {
					var optionsArray = [],
						productLevelOrder, productTypeLevelVariantOrder = {},
						optionValues,
						productVariantOption, variants;
					for (var typeIdx = 0, typeLen = widget.productTypes().length; typeIdx < typeLen; typeIdx++) {
						if (widget.productTypes()[typeIdx].id == widget.product().type()) {
							variants = widget.productTypes()[typeIdx].variants;
							for (var variantIdx = 0, variantLen = variants.length; variantIdx < variantLen; variantIdx++) {
								productTypeLevelVariantOrder[variants[variantIdx].id] = variants[variantIdx].values;
							}
						}
					}

					for (var i = 0; i < options.length; i++) {
					    if(Object.keys(options[i].optionValueMap).length > 0) {
					        if (widget.product().variantValuesOrder[options[i].optionId]) {
    							productLevelOrder = widget.product().variantValuesOrder[options[i].optionId]();
    						}
    						optionValues = this.mapOptionsToArray(options[i].optionValueMap, productLevelOrder ? productLevelOrder : productTypeLevelVariantOrder[options[i].optionId]);
    
    						productVariantOption = this.productVariantModel(options[i].optionName, options[i].mapKeyPropertyAttribute, optionValues, widget, options[i].optionId);

    						optionsArray.push(productVariantOption);
					    }
					}
				// 	console.log(optionsArray);
					widget.variantOptionsArray(optionsArray);

					setTimeout(function () {
						widget.loadCarousel();
					}, 100)

				} else {
					widget.imgMetadata = widget.product().product.productImagesMetadata;
					widget.variantOptionsArray([]);
				}
			},

			/*this create view model for variant options this contains
			 name of the option, possible list of option values for the option
			 selected option to store the option selected by the user.
			 ID to map the selected option*/
			productVariantModel: function (optionDisplayName, optionId, optionValues, widget, actualOptionId) {
				var productVariantOption = {};
				var productImages = {};
				productVariantOption.optionDisplayName = optionDisplayName.replace("occ", "");
				productVariantOption.parent = this;
				productVariantOption.optionId = optionId;
				productVariantOption.originalOptionValues = ko.observableArray(optionValues);
				productVariantOption.actualOptionId = actualOptionId;

				var showOptionCation = ko.observable(true);
				if (optionValues.length === 1) {
					showOptionCation(this.checkOptionValueWithSkus(optionId, optionValues[0].value));
				}
				//If there is just one option value in all Skus we dont need any caption
				if (showOptionCation()) {
					productVariantOption.optionCaption = widget.translate('optionCaption', {
						optionName: optionDisplayName
					}, true);
				}
				productVariantOption.selectedOptionValue = ko.observable();
				productVariantOption.countVisibleOptions = ko.computed(function () {
					var count = 0;
					for (var i = 0; i < productVariantOption.originalOptionValues().length; i++) {
						if (optionValues[i].visible() == true) {
							count = count + 1;
						}
					}
					return count;
				}, productVariantOption);
				productVariantOption.disable = ko.computed(function () {
					if (productVariantOption.countVisibleOptions() == 0) {
						return true;
					} else {
						return false;
					}
				}, productVariantOption);
				productVariantOption.setColorExternally = function () {
						$('#CC-prodDetails-sku-TRProduct_color').val('Grey').trigger('change');
					},
					productVariantOption.selectedOption = ko.computed({
						write: function (option) {
							widget.displayOpus(false);
							$(".tr-store-stock-error").hide().text("");
							this.parent.filtered(false);
							productVariantOption.selectedOptionValue(option);

							var variantOptions = this.parent.variantOptionsArray(),
								sku_obj = this.parent.getSelectedSku(variantOptions),
								repoid = this.parent.product().id(),
								prod_descr = this.parent.product().displayName();

							if (this.parent.allOptionsSelectedF()) {
								$(".tr-e-product-variation").find(".error-msg[data-val = 1]").hide();
							}
							if (this.parent.allOptionsSelected()) {
								$(".tr-e-product-variation").find(".error-msg[data-val = 2]").hide();
							}

							if (!this.parent.allOptionsSelected() && this.parent.allOptionsSelectedF()) {
								$(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
								$(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);
								$(".personalizationBtns").addClass('hide');
                                $('.tr-xs-personalize-block').addClass('hide');

								if (variantOptions.length === 1) {
									$(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
									$(".personalizationBtns").addClass('hide');
                                    $('.tr-xs-personalize-block').addClass('hide');
								}
							}


							if (sku_obj) {
								repoid = sku_obj.repositoryId;
								this.parent.selectedSku(sku_obj);
							}

							widget.bomArray([]);
							widget.urlObjArr = [];

							// (/ccstore/v1/products/xprod2119
							$.Topic('SKU_SELECTED').publish(this.parent.product(), sku_obj, variantOptions, this.parent.itemQty());
							$(".tr-e-product-description").find(".id span").text(repoid);
                            $(".tr-desc-review-block").find(".id span").text(repoid);
							if (productVariantOption.actualOptionId === this.parent.listingVariant()) {
								if (option && option.listingConfiguration) {
									this.parent.imgMetadata = option.listingConfiguration.imgMetadata;
									this.parent.assignImagesToProduct(option.listingConfiguration);
								} else {
									this.parent.imgMetadata = this.parent.product().product.productImagesMetadata;
									this.parent.assignImagesToProduct(this.parent.product().product);
								}
							}
							this.parent.filterOptionValues(productVariantOption.optionId);
						},
						read: function () {
							return productVariantOption.selectedOptionValue();
						},
						owner: productVariantOption
					});
				productVariantOption.selectedOption.extend({
					required: {
						params: true,
						message: widget.translate('optionRequiredMsg', {
							optionName: optionDisplayName
						}, true)
					}
				});
				productVariantOption.optionValues = ko.computed({
					write: function (value) {
						productVariantOption.originalOptionValues(value);
					},
					read: function () {
						return ko.utils.arrayFilter(
							productVariantOption.originalOptionValues(),
							function (item) {
								return item.visible() == true;
							}
						);
					},
					owner: productVariantOption
				});


				//The below snippet finds the product display/listing variant (if available)
				//looping through all the product types
				for (var productTypeIdx = 0; productTypeIdx < widget.productTypes().length; productTypeIdx++) {
					//if the product type matched with the current product
					if (widget.product().type() && widget.productTypes()[productTypeIdx].id == widget.product().type()) {
						var variants = widget.productTypes()[productTypeIdx].variants;
						//Below FOR loop is to iterate over the various variant types of that productType
						for (var productTypeVariantIdx = 0; productTypeVariantIdx < variants.length; productTypeVariantIdx++) {
							//if the productType has a listingVariant == true, hence this is the product display variant
							if (variants[productTypeVariantIdx].listingVariant) {
								widget.listingVariant(variants[productTypeVariantIdx].id);
								break;
							}
						}
						break;
					}
				}
				productImages.thumbImageURLs = (widget.product().product.thumbImageURLs.length == 1 && widget.product().product.thumbImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.thumbImageURLs);
				productImages.smallImageURLs = (widget.product().product.smallImageURLs.length == 1 && widget.product().product.smallImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.smallImageURLs);
				productImages.mediumImageURLs = (widget.product().product.mediumImageURLs.length == 1 && widget.product().product.mediumImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.mediumImageURLs);
				productImages.largeImageURLs = (widget.product().product.largeImageURLs.length == 1 && widget.product().product.largeImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.largeImageURLs);
				productImages.fullImageURLs = (widget.product().product.fullImageURLs.length == 1 && widget.product().product.fullImageURLs[0].indexOf("/img/no-image.jpg&") > 0) ? [] : (widget.product().product.fullImageURLs);
				productImages.sourceImageURLs = (widget.product().product.sourceImageURLs.length == 1 && widget.product().product.sourceImageURLs[0].indexOf("/img/no-image.jpg") > 0) ? [] : (widget.product().product.sourceImageURLs);

				var prodImgMetadata = [];
				if (widget.product().thumbImageURLs && widget.product().thumbImageURLs().length > 0) {
					for (var index = 0; index < widget.product().thumbImageURLs().length; index++) {
						prodImgMetadata.push(widget.product().product.productImagesMetadata[index]);
					}
				}

				ko.utils.arrayForEach(productVariantOption.originalOptionValues(), function (option) {

					if (widget.listingVariant() === actualOptionId) {
						for (var childSKUsIdx = 0; childSKUsIdx < widget.product().childSKUs().length; childSKUsIdx++) {
							if (widget.product().childSKUs()[childSKUsIdx].productListingSku()) {
								var listingConfiguration = widget.product().childSKUs()[childSKUsIdx];
								if (listingConfiguration[actualOptionId]() == option.key) {
									var listingConfig = {};
									listingConfig.thumbImageURLs = $.merge($.merge([], listingConfiguration.thumbImageURLs()), productImages.thumbImageURLs);
									listingConfig.smallImageURLs = $.merge($.merge([], listingConfiguration.smallImageURLs()), productImages.smallImageURLs);
									listingConfig.mediumImageURLs = $.merge($.merge([], listingConfiguration.mediumImageURLs()), productImages.mediumImageURLs);
									listingConfig.largeImageURLs = $.merge($.merge([], listingConfiguration.largeImageURLs()), productImages.largeImageURLs);
									listingConfig.fullImageURLs = $.merge($.merge([], listingConfiguration.fullImageURLs()), productImages.fullImageURLs);
									listingConfig.sourceImageURLs = $.merge($.merge([], listingConfiguration.sourceImageURLs()), productImages.sourceImageURLs);
									listingConfig.primaryFullImageURL = listingConfiguration.primaryFullImageURL() ? listingConfiguration.primaryFullImageURL() : widget.product().product.primaryFullImageURL;
									listingConfig.primaryLargeImageURL = listingConfiguration.primaryLargeImageURL() ? listingConfiguration.primaryLargeImageURL() : widget.product().product.primaryLargeImageURL;
									listingConfig.primaryMediumImageURL = listingConfiguration.primaryMediumImageURL() ? listingConfiguration.primaryMediumImageURL() : widget.product().product.primaryMediumImageURL;
									listingConfig.primarySmallImageURL = listingConfiguration.primarySmallImageURL() ? listingConfiguration.primarySmallImageURL() : widget.product().product.primarySmallImageURL;
									listingConfig.primaryThumbImageURL = listingConfiguration.primaryThumbImageURL() ? listingConfiguration.primaryThumbImageURL() : widget.product().product.primaryThumbImageURL;

									//storing the metadata for the images
									var childSKUImgMetadata = [];
									if (listingConfiguration.images && listingConfiguration.images().length > 0) {
										for (var index = 0; index < listingConfiguration.images().length; index++) {
											childSKUImgMetadata.push(widget.product().product.childSKUs[childSKUsIdx].images[index].metadata);
										}
									}
									listingConfig.imgMetadata = $.merge($.merge([], childSKUImgMetadata), prodImgMetadata);
									option.listingConfiguration = listingConfig;
								}
							}
						}
					}
					if (widget.variantName() === actualOptionId && option.key === widget.variantValue()) {
						productVariantOption.selectedOption(option);
					}
				});

				return productVariantOption;
			},

			//this method is triggered to check if the option value is present in all the child Skus.
			checkOptionValueWithSkus: function (optionId, value) {
				var childSkus = this.product().childSKUs();
				var childSkusLength = childSkus.length;
				for (var i = 0; i < childSkusLength; i++) {
					if (!childSkus[i].dynamicPropertyMapLong[optionId] || childSkus[i].dynamicPropertyMapLong[optionId]() === undefined) {
						return true;
					}
				}
				return false;
			},

			//this method is triggered whenever there is a change to the selected option.
			filterOptionValues: function (selectedOptionId) {
				if (this.filtered()) {
					return;
				}
				var variantOptions = this.variantOptionsArray();
				for (var i = 0; i < variantOptions.length; i++) {
					var currentOption = variantOptions[i];
					var matchingSkus = this.getMatchingSKUs(variantOptions[i].optionId);
					// var optionValues = this.updateOptionValuesFromSku(matchingSkus, selectedOptionId, currentOption);
					// variantOptions[i].optionValues(optionValues);
					this.filtered(true);
				}
				this.updateSingleSelection(selectedOptionId);
			},

			// get all the matching SKUs
			getMatchingSKUs: function (optionId) {
				var childSkus = this.product().childSKUs();
				var matchingSkus = [];
				var variantOptions = this.variantOptionsArray();
				var selectedOptionMap = {};
				for (var j = 0; j < variantOptions.length; j++) {
					if (variantOptions[j].optionId != optionId && variantOptions[j].selectedOption() != undefined) {
						selectedOptionMap[variantOptions[j].optionId] = variantOptions[j].selectedOption().value;
					}
				}
				for (var i = 0; i < childSkus.length; i++) {
					var skuMatched = true;
					for (var key in selectedOptionMap) {
						if (selectedOptionMap.hasOwnProperty(key)) {
							if (!childSkus[i].dynamicPropertyMapLong[key] ||
								childSkus[i].dynamicPropertyMapLong[key]() != selectedOptionMap[key]) {
								skuMatched = false;
								break;
							}
						}
					}
					if (skuMatched) {
						matchingSkus.push(childSkus[i]);
					}
				}
				return matchingSkus;
			},

			//this method constructs option values for all the options other than selected option
			//from the matching skus.
			updateOptionValuesFromSku: function (skus, selectedOptionID, currentOption) {
				var optionId = currentOption.optionId;
				var options = [];
				var optionValues = currentOption.originalOptionValues();
				for (var k = 0; k < skus.length; k++) {
					var optionValue = skus[k].dynamicPropertyMapLong[optionId];
					if (optionValue != undefined) {
						options.push(optionValue());
					}
				}
				for (var j = 0; j < optionValues.length; j++) {
					var value = optionValues[j].value;
					var visible = false;
					var index = options.indexOf(value);
					if (index != -1) {
						visible = true;
					}
					optionValues[j].visible(visible);
				}
				return optionValues;
			},

			//This method returns true if the option passed is the only one not selected
			//and all other options are either selected or disabled.
			validForSingleSelection: function (optionId) {
				var variantOptions = this.variantOptionsArray();
				for (var j = 0; j < variantOptions.length; j++) {
					if (variantOptions[j].disable() || (variantOptions[j].optionId != optionId && variantOptions[j].selectedOption() != undefined)) {
						return true;
					}
					if (variantOptions[j].optionId != optionId && variantOptions[j].selectedOption() == undefined && variantOptions[j].countVisibleOptions() == 1) {
						return true;
					}
				}
				return false;
			},

			//This method updates the selection value for the options wiht single option values.
			updateSingleSelection: function (selectedOptionID) {
				var variantOptions = this.variantOptionsArray();
				for (var i = 0; i < variantOptions.length; i++) {
					var optionId = variantOptions[i].optionId;
					if (variantOptions[i].countVisibleOptions() == 1 && variantOptions[i].selectedOption() == undefined && optionId != selectedOptionID) {
						var isValidForSingleSelection = this.validForSingleSelection(optionId);
						var optionValues = variantOptions[i].originalOptionValues();
						for (var j = 0; j < optionValues.length; j++) {
							if (optionValues[j].visible() == true) {
								variantOptions[i].selectedOption(optionValues[j]);
								break;
							}
						}
					}
				}
			},

			//this method convert the map to array of key value object and sort them based on the enum value
			//to use it in the select binding of knockout
			mapOptionsToArray: function (variantOptions, order) {
				var optionArray = [];

				for (var idx = 0, len = order.length; idx < len; idx++) {
					if (variantOptions.hasOwnProperty(order[idx])) {
						optionArray.push({
							key: order[idx],
							value: variantOptions[order[idx]],
							visible: ko.observable(true)
						});
					}
				}
				return optionArray;
			},

			//this method returns the selected sku in the product, Based on the options selected
			getSelectedSku: function (variantOptions) {
				var childSkus = this.product().product.childSKUs;
				var selectedSKUObj = {};
				if (variantOptions.length === 0) {
					return null;
				}
				try {
					for (var i = 0; i < childSkus.length; i++) {
						selectedSKUObj = childSkus[i];
						for (var j = 0; j < variantOptions.length; j++) {
							if (!variantOptions[j].disable() && childSkus[i].dynamicPropertyMapLong[variantOptions[j].optionId] != variantOptions[j].selectedOption().value) {
								selectedSKUObj = null;
								break;
							}
						}
						if (selectedSKUObj !== null) {
							// $.Topic('SKU_SELECTED').publish(this.product(), selectedSKUObj, variantOptions);
							return selectedSKUObj;
						}
					}
				} catch (e) {};
				return null;
			},

			//refreshes the prices based on the variant options selected
			refreshSkuPrice: function (selectedSKUObj) {
				if (selectedSKUObj === null) {
					if (this.product().hasPriceRange) {
						this.priceRange(true);
					} else {
						this.listPrice(this.product().listPrice());
						this.salePrice(this.product().salePrice());
						this.priceRange(false);
					}
				} else {
					this.priceRange(false);
					var skuPriceData = this.product().getSkuPrice(selectedSKUObj);
					this.listPrice(skuPriceData.listPrice);
					this.salePrice(skuPriceData.salePrice);
				}
			},

			//refreshes the stockstatus based on the variant options selected
			refreshSkuStockStatus: function (selectedSKUObj) {
				var key;
				if (selectedSKUObj === null) {
					key = 'stockStatus';
				} else {
					key = selectedSKUObj.repositoryId;
				}
				var stockStatusMap = this.product().stockStatus();
				for (var i in stockStatusMap) {
					if (i == key) {
						if (stockStatusMap[key] == 'IN_STOCK') {
							this.stockStatus(true);
							if (selectedSKUObj === null) {
								this.stockAvailable(1);
							} else {
								this.stockAvailable(selectedSKUObj.quantity);
							}
						} else {
							this.stockStatus(false);
							this.stockAvailable(0);
						}
						return;
					}
				}
			},

			refreshSkuData: function (selectedSKUObj) {
				this.refreshSkuPrice(selectedSKUObj);
				this.refreshSkuStockStatus(selectedSKUObj);
			},

			// this method  returns a map of all the options selected by the user for the product
			getSelectedSkuOptions: function (variantOptions) {
				var selectedOptions = [];

				for (var i = 0; i < variantOptions.length; i++) {
					if (!variantOptions[i].disable()) {
						selectedOptions.push({
							'optionName': variantOptions[i].optionDisplayName,
							'optionValue': variantOptions[i].selectedOption().key,
							'optionId': variantOptions[i].actualOptionId,
							'optionValueId': variantOptions[i].selectedOption().value
						});

					}
				}
				return selectedOptions;
			},
			getSelectedProductVariant: function (variantOptions) {
				var selectedOptions = [];
				for (var i = 0; i < variantOptions.length; i++) {
					if (!variantOptions[i].disable() && variantOptions[i].selectedOption()) {
						var tempOptionValue = variantOptions[i].selectedOption().key;
						if (tempOptionValue.indexOf("|") != -1) {
							tempOptionValue = tempOptionValue.split("|");
							tempOptionValue = tempOptionValue[0];
						}
						selectedOptions.push({
							'optionName': variantOptions[i].optionDisplayName,
							'optionValue': variantOptions[i].selectedOption().key,
							'optionId': variantOptions[i].actualOptionId,
							'optionValueId': variantOptions[i].selectedOption().value,
							'optionText': tempOptionValue
						});

					}
				}
				return selectedOptions;
			},
			allOptionsSelected: function () {
				var allOptionsSelected = true;
				if (this.variantOptionsArray().length > 0) {
					var variantOptions = this.variantOptionsArray();
					for (var i = 0; i < variantOptions.length; i++) {
						if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
							allOptionsSelected = false;
							this.selectedSku(null);
							break;
						}
					}

					if (allOptionsSelected) {
						// get the selected sku based on the options selected by the user
						var selectedSKUObj = this.getSelectedSku(variantOptions);
						if (selectedSKUObj === null) {
							return false;
						}
						this.selectedSku(selectedSKUObj);
					}
					this.refreshSkuData(this.selectedSku());
				}

				return allOptionsSelected;
			},
			allOptionsSelectedF: function () {
				var allOptionsSelected = true;
				if (this.variantOptionsArray().length > 0) {
					var variantOptions = this.variantOptionsArray();
					for (var i = 0; i < variantOptions.length; i++) {
						if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
							allOptionsSelected = false;
							this.selectedSku(null);
							break;
						}
					}
				}

				return allOptionsSelected;
			},

			quantityIsValid: function () {
				var cartQty = 0;
				cartQty = Number($('#CC-shoppingCart-productQuantity-' + this.product().id() + this.product().repositoryId()).text());
				return this.itemQty() > 0 && parseInt(this.itemQty()) + cartQty <= this.stockAvailable();
			},

			// this method validated if all the options of the product are selected
			validateAddToCart: function () {

				var AddToCartButtonFlag = this.allOptionsSelected() && this.stockStatus() && this.quantityIsValid();
				if (!AddToCartButtonFlag) {
					$('#cc-prodDetailsAddToCart').attr("aria-disabled", "true");
				}

				return AddToCartButtonFlag;

			},

			handleChangeQuantity: function (data, event) {
				var quantity = this.itemQty();

				if (quantity < 1) {} else if (quantity > this.stockAvailable()) {}

				return true;
			},
			handleBackOrderDate: function () {
				var fullDate;


				if (this.product().childSKUs().length == 1) {
					publishedSkuData = this.product();;
				}
				if (publishedSkuData.backorder) {
					this.deliveryDateHeadingFlag(false);
					fullDate = typeof publishedSkuData.backorderDeliveryDate === "function" ?
						publishedSkuData.backorderDeliveryDate() : publishedSkuData.backorderDeliveryDate;
					this.backOrderFlag(typeof publishedSkuData.backorder == "function" ? publishedSkuData.backorder() : publishedSkuData.backorder);
					this.backOrderMessage('Back order item');
					if (fullDate !== '' && fullDate !== null) {
						var index = fullDate.indexOf('-');
						var newDate = fullDate.substring(index + 1);
						newDate = newDate.replace('-', '/');
						newDate = "Estimated Arrival " + newDate;
						this.standardDeliveryDate(newDate);
					}

					if (publishedSkuData.BWYBackorderDeliveryDate) {
						this.bwyFlag(true);

						if (typeof publishedSkuData.BWYBackorderDeliveryDates == "function") {
							if (publishedSkuData.BWYBackorderDeliveryDates()) {
								this.bwyFlag(true);
							} else {
								this.bwyFlag(false);
							}
						}
						if (this.bwyFlag() == true) {
							this.bwyFlagDate(typeof publishedSkuData.BWYBackorderDeliveryDates == "function" ? publishedSkuData.BWYBackorderDeliveryDates() : publishedSkuData.BWYBackorderDeliveryDates);

						}
					} else {
						this.bwyFlag(false);
					}

					if (publishedSkuData.TwoNDBackorderDeliveryDates) {
						this.two_ndFlag(true);

						if (typeof publishedSkuData.TwoNDBackorderDeliveryDates == "function") {
							if (publishedSkuData.TwoNDBackorderDeliveryDates() !== null) {
								this.two_ndFlag(true);
							} else {
								this.two_ndFlag(false);
							}
						}
						if (this.two_ndFlag() == true) {
							this.two_ndFlagDate(typeof publishedSkuData.TwoNDBackorderDeliveryDates == "function" ? publishedSkuData.TwoNDBackorderDeliveryDates() : publishedSkuData.TwoNDBackorderDeliveryDates);
						}
					} else {
						this.two_ndFlag(false);
					}

					if (typeof publishedSkuData.AIRBackorderDeliveryDates !== "undefined" && publishedSkuData.AIRBackorderDeliveryDates !== null) {
						this.airFlag(true);

						if (typeof publishedSkuData.AIRBackorderDeliveryDates == "function") {
							if (publishedSkuData.AIRBackorderDeliveryDates() !== null) {
								this.airFlag(true);
							} else {
								this.airFlag(false);
							}
						}
						if (this.airFlag() == true) {
							this.airFlagDate(typeof publishedSkuData.AIRBackorderDeliveryDates == "function" ? publishedSkuData.AIRBackorderDeliveryDates() : publishedSkuData.AIRBackorderDeliveryDates);

						}
					} else {
						this.airFlag(false);
					}

					if (typeof publishedSkuData.PRMBackorderDeliveryDates !== "undefined" && publishedSkuData.PRMBackorderDeliveryDates !== null) {
						this.prmFlag(true);

						this.prmFlag(true);

						if (typeof publishedSkuData.PRMBackorderDeliveryDates == "function") {
							if (publishedSkuData.PRMBackorderDeliveryDates() !== null) {
								this.prmFlag(true);
							} else {
								this.prmFlag(false);
							}
						}
						if (this.prmFlag() == true) {
							this.prmFlagDate(typeof publishedSkuData.PRMBackorderDeliveryDates == "function" ? publishedSkuData.PRMBackorderDeliveryDates() : publishedSkuData.PRMBackorderDeliveryDates);
						}

					} else {
						this.prmFlag(false);
					}


				} else {
					this.backOrderFlag(false);
				}


			},

			checkIfPersonalizationSkuPresent: function () {
				var cartItems = getWidget.cart().items();
				for (var i in cartItems) {
					if (cartItems[i].catRefId === "personalization1") {
						return true;
					}
				}
				return false;
			},
            handleCustomAddToCart: function(){
                var self = _this;
                function enableAddToCartButton() {
					self.isAddToCartClicked(false);
				};
				_this.isAddToCartClicked(true);
				setTimeout(enableAddToCartButton, 3000);
				var k = currentPersonalizationData.length;
                
                var variantOptions = this.variantOptionsArray();
				notifier.clearSuccess(this.WIDGET_ID);
				//get the selected options, if all the options are selected.
				var selectedOptions = this.getSelectedSkuOptions(variantOptions);
				var selectedOptionsObj = {
					'selectedOptions': selectedOptions
				};
				var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
				if (this.variantOptionsArray().length > 0) {
					//assign only the selected sku as child skus
					newProduct.childSKUs = [this.selectedSku()];
				}
				currentProduct = newProduct;
				
				setDetailsToItem = true;
				if (k === 1) {
					processingTime = 0;
					newProduct.orderQuantity = parseInt(this.itemQty(), 10);
					console.log("handleCustomAddToCart -> $.Topic(pubsub.topicNames.CART_ADD)");
					console.log(pubsub.topicNames.CART_ADD);
					$.Topic(pubsub.topicNames.CART_ADD).publishWith(
						newProduct, [{
							message: "success"
						}]);
				} else {
					processingTime = 250;
					newProduct.orderQuantity = parseInt(1, 10);
					getWidget.cart().addItem(newProduct);
				}  
            },
			// Sends a message to the cart to add this product
			handleAddToCart: function () {
				var self = this,
					personalizationUtils = helpers.personalizationUtils();

				if (!this.handlePersonalizeButtonValidation()) {
					return;
				}
				var all_delivery_details = JSON.parse(storageApi.getInstance().getItem("alldeliveryDetails")) || {},
					curr_flag = 0;

				var delivery_details = {};
				/*delivery_details["skuid"] = this.skuRepoId()
				delivery_details["deliveryType"] = $("input[name='optradio']:checked").val();
				var deliveryDate = '';
				if ((this.myStore().length > 0)) {
				    deliveryDate = this.myStore()[0].deliveryDate;
				}
				if (this.myStore().length > 0 && this.displaySts() && this.myStore()[0].storeHours && this.myStore()[0].storeHours != "") {
				    deliveryDate = this.myStore()[0].storeHours[5].date;
				}
				if (this.myStore().length > 0) {
				    if (this.isPhotoUpload()) {
				        delivery_details['pickupAvailable'] = false;
				    } else {
				        delivery_details['pickupAvailable'] = true;
				    }
				    delivery_details["store_details"] = this.myStore()[0]["storeNumber"] + "~~" + this.myStore()[0]["storeName"] + "~~" + this.myStore()[0].address1 + "~~" + deliveryDate + "~~" + this.myStore()[0].formattedDeliveryDate + "~~" + this.myStore()[0].address2;
				    var address = {};
				    address["storeName"] = this.myStore()[0].storeName;
				    address["storeNumber"] = this.myStore()[0].storeNumber;
				    address["address1"] = this.myStore()[0].address1 || '';
				    address["address"] = this.myStore()[0].address || '';
				    address["address2"] = this.myStore()[0].address2 || '';
				    address["city"] = this.myStore()[0].city;
				    address["state"] = this.myStore()[0].state;
				    address["zipCode"] = this.myStore()[0].zipCode;
				    address["phone"] = "";
				    address["deliveryDate"] = this.myStore()[0].deliveryDate;
				    address["isSTS"] = this.displaySts();
				    address["closeTime"] = this.myStore()[0].closeTime;
				    address["onHandQuantity"] = this.myStore()[0].onHandQuantity;
				    address["addToCartDate"] = new Date();
				    if (this.myStore()[0].storeHours && this.myStore()[0].storeHours != "") {
				        address["stsDate"] = this.myStore()[0].storeHours[5].date;
				    } else {
				        address["stsDate"] = "";
				    }
				    if ($("input[name='optradio']:checked").val() == 1) {
				        if (this.displaySts()) {
				            delivery_details["store_inventory"] = 920;
				        }
				        delivery_details["userSelectedStore"] = true;
				    } else {
				        delivery_details["userSelectedStore"] = false;
				        if (this.isSFS()) {
				            delivery_details["store_inventory"] = this.sfsStoreId();
				            ;
				        } else {
				            delivery_details["store_inventory"] = 920;
				        }
				    }
				    delivery_details["store_address"] = address;
				} else {
				    delivery_details["userSelectedStore"] = false;
				    delivery_details['pickupAvailable'] = false;
				    delivery_details["store_details"] = "";
				    if (this.isSFS()) {
				        delivery_details["store_inventory"] = this.sfsStoreId();
				        ;
				    } else {
				        delivery_details["store_inventory"] = 920;
				    }
				}*/
				
				var tempStoreDetails = {};
				var tempaddress = {};
				tempStoreDetails.skuid = this.skuRepoId()
				tempStoreDetails.deliveryType = $("input[name='optradio']:checked").val();
				if ($("input[name='optradio']:checked").val() == 2) { /**** Ship to Home */
					if (inventoryDataIR.shipping[0].store.id !== "920") { /****Ship from Store */
						tempStoreDetails.store_details = inventoryDataIR.shipping[0].store.id + "~~" + inventoryDataIR.shipping[0].store.name + "~~" + inventoryDataIR.shipping[0].store.street + "~~" + inventoryDataIR.shipping[0].store.deliveryDate + "~~" + inventoryDataIR.shipping[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.shipping[0].store.streetExt;

						tempaddress.storeName = inventoryDataIR.shipping[0].store.name;
						tempaddress.storeNumber = inventoryDataIR.shipping[0].store.id;
						tempaddress.address = inventoryDataIR.shipping[0].store.street;
						tempaddress.address1 = inventoryDataIR.shipping[0].store.street;
						tempaddress.address2 = inventoryDataIR.shipping[0].store.streetExt;
						tempaddress.city = inventoryDataIR.shipping[0].store.city;
						tempaddress.state = inventoryDataIR.shipping[0].store.state;
						tempaddress.zipCode = inventoryDataIR.shipping[0].store.postalCode;
						tempaddress.phone = inventoryDataIR.shipping[0].store.phone;
						tempaddress.deliveryDate = inventoryDataIR.shipping[0].store.deliveryDate;
						tempaddress.isSTS = this.displaySts();
						tempaddress.closeTime = inventoryDataIR.shipping[0].store.closeTime;
						tempaddress.onHandQuantity = inventoryDataIR.shipping[0].store.onHandQuantity;
						tempaddress.addToCartDate = new Date();
						tempaddress.stsDate = "";
						if (inventoryDataIR.shipping[0].store.hours.daysOfWeek && inventoryDataIR.shipping[0].store.hours.daysOfWeek != "") {
							tempaddress.stsDate = inventoryDataIR.shipping[0].store.hours.daysOfWeek[5].date;
						} else {
							tempaddress.stsDate = "";
						}

						tempStoreDetails.store_address = tempaddress;
						tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
						tempStoreDetails.userSelectedStore = true;
						if (inventoryDataIR.pickup[0].store) {
							tempStoreDetails.pickupAvailable = inventoryDataIR.pickup[0].available;
						} else {
							tempStoreDetails.pickupAvailable = false;
						}
					} else {
						if (inventoryDataIR.pickup[0].store) {
							tempStoreDetails.store_details = inventoryDataIR.pickup[0].store.id + "~~" + inventoryDataIR.pickup[0].store.name + "~~" + inventoryDataIR.pickup[0].store.street + "~~" + inventoryDataIR.pickup[0].store.deliveryDate + "~~" + inventoryDataIR.pickup[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.pickup[0].store.streetExt;

							tempaddress.storeName = inventoryDataIR.pickup[0].store.name;
							tempaddress.storeNumber = inventoryDataIR.pickup[0].store.id;
							tempaddress.address = inventoryDataIR.pickup[0].store.street;
							tempaddress.address1 = inventoryDataIR.pickup[0].store.street;
							tempaddress.address2 = inventoryDataIR.pickup[0].store.streetExt;
							tempaddress.city = inventoryDataIR.pickup[0].store.city;
							tempaddress.state = inventoryDataIR.pickup[0].store.state;
							tempaddress.zipCode = inventoryDataIR.pickup[0].store.postalCode;
							tempaddress.phone = inventoryDataIR.pickup[0].store.phone;
							tempaddress.deliveryDate = inventoryDataIR.pickup[0].store.deliveryDate;
							tempaddress.isSTS = this.displaySts();
							tempaddress.closeTime = inventoryDataIR.pickup[0].store.closeTime;
							tempaddress.onHandQuantity = inventoryDataIR.pickup[0].store.onHandQuantity;
							tempaddress.addToCartDate = new Date();
							tempaddress.stsDate = "";
							if (inventoryDataIR.pickup[0].store.hours.daysOfWeek && inventoryDataIR.pickup[0].store.hours.daysOfWeek != "") {
								tempaddress.stsDate = inventoryDataIR.pickup[0].store.hours.daysOfWeek[5].date;
							} else {
								tempaddress.stsDate = "";
							}

							tempStoreDetails.store_address = tempaddress;
							tempStoreDetails.store_inventory = inventoryDataIR.pickup[0].store.id;
							tempStoreDetails.userSelectedStore = true;
							tempStoreDetails.pickupAvailable = inventoryDataIR.pickup[0].available;
						} else {
							tempStoreDetails.store_details = "";
							tempStoreDetails.store_address = {};
							tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
							tempStoreDetails.userSelectedStore = true;
							tempStoreDetails.pickupAvailable = false;
						}
					}
				} else { /**** Pick up */
					if (inventoryDataIR.pickup[0].store) {
						tempStoreDetails.store_details = inventoryDataIR.pickup[0].store.id + "~~" + inventoryDataIR.pickup[0].store.name + "~~" + inventoryDataIR.pickup[0].store.street + "~~" + inventoryDataIR.pickup[0].store.deliveryDate + "~~" + inventoryDataIR.pickup[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.pickup[0].store.streetExt;

						tempaddress.storeName = inventoryDataIR.pickup[0].store.name;
						tempaddress.storeNumber = inventoryDataIR.pickup[0].store.id;
						tempaddress.address = inventoryDataIR.pickup[0].store.street;
						tempaddress.address1 = inventoryDataIR.pickup[0].store.street;
						tempaddress.address2 = inventoryDataIR.pickup[0].store.streetExt;
						tempaddress.city = inventoryDataIR.pickup[0].store.city;
						tempaddress.state = inventoryDataIR.pickup[0].store.state;
						tempaddress.zipCode = inventoryDataIR.pickup[0].store.postalCode;
						tempaddress.phone = inventoryDataIR.pickup[0].store.phone;
						tempaddress.deliveryDate = inventoryDataIR.pickup[0].store.deliveryDate;
						tempaddress.isSTS = this.displaySts();
						tempaddress.closeTime = inventoryDataIR.pickup[0].store.closeTime;
						tempaddress.onHandQuantity = inventoryDataIR.pickup[0].store.onHandQuantity;
						tempaddress.addToCartDate = new Date();
						tempaddress.stsDate = "";
						if (inventoryDataIR.pickup[0].store.hours.daysOfWeek && inventoryDataIR.pickup[0].store.hours.daysOfWeek != "") {
							tempaddress.stsDate = inventoryDataIR.pickup[0].store.hours.daysOfWeek[5].date;
						} else {
							tempaddress.stsDate = "";
						}

						tempStoreDetails.store_address = tempaddress;
						tempStoreDetails.store_inventory = inventoryDataIR.pickup[0].store.id;
						tempStoreDetails.userSelectedStore = true;
						if (this.isPhotoUpload()) {
							tempStoreDetails.pickupAvailable = false;
						} else {
							tempStoreDetails.pickupAvailable = true;
						}
					}
				}
				delivery_details = tempStoreDetails;
				for (var key in all_delivery_details) {
					if (all_delivery_details.hasOwnProperty(key)) {
						if (key == this.skuRepoId()) {
							curr_flag = 1;
							all_delivery_details[key] = delivery_details;
						}
					}
				}
				if (curr_flag === 0) {
					all_delivery_details[this.skuRepoId()] = delivery_details;
				}
				var occasion_obj = JSON.parse(storageApi.getInstance().getItem("occasion") || "{}"),
					longDescr_obj = JSON.parse(storageApi.getInstance().getItem("prodDescr") || "{}");
				occasion_obj[this.skuRepoId()] = this.occasionCode();
				longDescr_obj[this.skuRepoId()] = $(".js-product-header-descr").text();
				//start
				var store_obj = JSON.parse(storageApi.getInstance().getItem("storeId") || "{}");
				store_obj[this.skuRepoId()] = this.storeCode();
				//localStorage.setItem('skuID',this.skuRepoId());
				storageApi.getInstance().setItem('deliveryDetails', JSON.stringify(delivery_details));
				storageApi.getInstance().setItem('alldeliveryDetails', JSON.stringify(all_delivery_details));
				storageApi.getInstance().setItem('occasion', JSON.stringify(occasion_obj));
				storageApi.getInstance().setItem('prodDescr', JSON.stringify(longDescr_obj));
				//start
				storageApi.getInstance().setItem('storeId', JSON.stringify(store_obj));
				var variantOptions = this.variantOptionsArray();
				notifier.clearSuccess(this.WIDGET_ID);
				//get the selected options, if all the options are selected.
				var selectedOptions = this.getSelectedSkuOptions(variantOptions);

				var selectedOptionsObj = {
					'selectedOptions': selectedOptions
				};

				var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);

				if (this.variantOptionsArray().length > 0) {
					//assign only the selected sku as child skus
					newProduct.childSKUs = [this.selectedSku()];
				}
				newProduct.orderQuantity = parseInt(this.itemQty(), 10);
				currentProduct = newProduct;

				var productId = self.product().id(),
					bomDataToSave = self.selectedBomArray().map(function (item) {
						delete item["imgurl"];
						return item;
					}),
					deliveryData = storageApi.getInstance().getItem("deliveryDetails") || {};


				var hash = personalizationUtils.genHash(productId, null, bomDataToSave, null, deliveryData, new Date().getTime()),
					skuSTSFlag = $("#skuSTSFlag").val() === "true" ? true : false;

				var selectedProductVariants = self.getSelectedProductVariant(variantOptions);

				var pData = {
					id: productId,
					sku: self.skuRepoId(),
					quantity: parseInt(self.itemQty()),
					bom: bomDataToSave,
					productVariant: selectedProductVariants,
					deliveryDetails: deliveryData,
					skuSTS: skuSTSFlag
				};

				var currentPersonalizationDataObj = {};
				currentPersonalizationDataObj['hash'] = hash
				currentPersonalizationDataObj['data'] = pData;
				currentPersonalizationData.push(currentPersonalizationDataObj);

				setDetailsToItem = true;

				if (self.checkIfPersonalizationSkuPresent() === false) {
					self.cart().addItem(newProduct);
				} else {
					$.Topic(pubsub.topicNames.CART_ADD).publishWith(newProduct, [{
						message: "success"
					}]);


				}

				// To disable Add to cart button for three seconds when it is clicked and enabling again
				this.isAddToCartClicked(true);
				var self = this;
				setTimeout(enableAddToCartButton, 3000);

				function enableAddToCartButton() {
					self.isAddToCartClicked(false);
				};

			},
			processPersonalizationCost: function (personalizationCostObj) {
				var item_wise_cost_arr = [];
				for (var prop in personalizationCostObj) {
					var obj = {};
					if (personalizationCostObj.hasOwnProperty()) {
						continue;
					}
					var zone_obj = personalizationCostObj[prop];
					for (var zone_prop in zone_obj) {


						var inernal_obj = zone_obj[zone_prop];
						for (var per_props in inernal_obj) {
							if (obj[inernal_obj[per_props]] && obj[inernal_obj[per_props]["name"]]) {
								obj[inernal_obj[per_props]["name"]] = Number(obj[inernal_obj[per_props]["name"]]) + Number(inernal_obj[per_props]["price"])
							} else {
								if (obj[inernal_obj[per_props]])
									obj[inernal_obj[per_props]["name"]] = inernal_obj[per_props]["price"];
							}
						}
					}
					item_wise_cost_arr.push(obj);
				}
				return item_wise_cost_arr;
			},

			// Sends a message to the cart to add this product
			handlePersonalizedPdtAddToCart: function (selectedSkuPersonalizationData, personalizationCostSummary, shippingOptions) {
				if (!_this.handlePersonalizeButtonValidation()) {
					return;
				}


				var variantOptions = this.variantOptionsArray();
				notifier.clearSuccess(this.WIDGET_ID);
				//get the selected options, if all the options are selected.
				var selectedOptions = _this.getSelectedSkuOptions(variantOptions);


				if ($.isArray(_this.getSelectedSkuOptions(variantOptions)) && _this.getSelectedSkuOptions(variantOptions).length > 0) {
					selectedOptions.push({
						optionId: "product_variants",
						optionName: "Product Variants",
						optionValue: JSON.stringify(_this.getSelectedSkuOptions(variantOptions)),
						// optionValue:"test_this",
						optionValueId: Math.floor((Math.random() * 10000) + 1)
					});

				}


				var personalizationSkus = selectedSkuPersonalizationData();

				var selectedSku;
				var selectedSkuPrice;
				if (_this.getSelectedSKU()) {
					if (typeof _this.getSelectedSKU()['listPrice'] === "function") {
						selectedSkuPrice = (_this.getSelectedSKU()['salePrice']() != null) ? _this.getSelectedSKU()['salePrice']() : _this.getSelectedSKU()['listPrice']();
					} else {
						selectedSkuPrice = (_this.getSelectedSKU()['salePrice'] != null) ? _this.getSelectedSKU()['salePrice'] : _this.getSelectedSKU()['listPrice'];
					}

					if (typeof _this.getSelectedSKU()['repositoryId'] === "function") {
						selectedSku = _this.getSelectedSKU()['repositoryId']();
					} else {
						selectedSku = _this.getSelectedSKU()['repositoryId'];
					}
				}

				if (!selectedSkuPrice) {
					selectedSkuPrice = (this.product().product.salePrice != null) ? this.product().product.salePrice : this.product().product.listPrice;
				}

				personalizationSkus.originalSku = {
					sku: (selectedSku || this.product().id()),
					price: selectedSkuPrice
				};

				personalizationSkus.photoImageUrls = ["https://thingsremembered.scene7.com/is/image/ThingsRemembered/000751016?wid=" + imageRes];
				if (this.occasionCode()) {
					personalizationSkus.occasionCode = this.occasionCode().trim();
				} else {
					personalizationSkus.occasionCode = '';
				}


				selectedOptions.push({

					optionId: "personalization_skus",
					optionName: "Personalization SKUS",
					optionValue: JSON.stringify(personalizationSkus),
					// optionValue:"test_this",
					optionValueId: Math.floor((Math.random() * 10000) + 1)

				});
				// var item_wise_data = _this.processPersonalizationCost(selectedSkuPersonalizationData());
				var item_wise_data = this.personaliztionObj;
				selectedOptions.push({

					optionId: "personalization",
					optionName: "Personalization",
					optionValue: JSON.stringify(item_wise_data),
					// optionValue:"test_this",
					optionValueId: Math.floor((Math.random() * 10000) + 1)

				});
				selectedOptions.push({

					optionId: "personalization_summary",
					optionName: "Personalization Summary",
					optionValue: JSON.stringify(personalizationCostSummary()),
					optionValueId: Math.floor((Math.random() * 10000) + 1)

				});
				selectedOptions.push({
					optionId: "shipping_options",
					optionName: "Shipping Options",
					optionValue: JSON.stringify(shippingOptions),
					optionValueId: Math.floor((Math.random() * 10000) + 1)

				});
				//Adding Delivery Dates to selected Options
				var dateObject = {};
				dateObject.AIR = getDate(this.airFlagDate());
				dateObject.BWY = getDate(this.bwyFlagDate());
				dateObject.TND = getDate(this.two_ndFlagDate());

				function getDate(date) {
					var tempMonth = date.substring(3, 5);
					var tempDate = date.substring(6, 8);
					var tempDay = date.substring(13);
					var tempYear = date.split('|')[1];
					if (!tempYear) {
						var date = new Date();
						tempYear = date.getFullYear();
					}
					return tempYear + tempMonth + tempDate;
				}

				selectedOptions.push({
					optionId: "product_delivery_dates",
					optionName: "Product Delivery Dates",
					optionValue: JSON.stringify(dateObject),
					optionValueId: Math.floor((Math.random() * 10000) + 1)
				});
				selectedOptions.push({
					optionId: "ItemProductDescription",
					optionName: "Product Description",
					optionValue: this.product().displayName(),
					optionValueId: Math.floor((Math.random() * 10000) + 1)
				});
				if (this.giftDisplayName()) {
					selectedOptions.push({
						optionId: "giftItemName",
						optionName: "Gift Item Name",
						optionValue: this.giftDisplayName(),
						optionValueId: Math.floor((Math.random() * 10000) + 1)
					});
				}
				var selectedOptionsObj = {
					'selectedOptions': selectedOptions
				};

				function enableAddToCartButton() {
					self.isAddToCartClicked(false);
				};

				_this.isAddToCartClicked(true);
				var self = _this;
				setTimeout(enableAddToCartButton, 3000);
				var k = currentPersonalizationData.length;

				var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
				if (this.variantOptionsArray().length > 0) {
					//assign only the selected sku as child skus
					newProduct.childSKUs = [this.selectedSku()];
				}
				setDetailsToItem = true;
				//getWidget.cart().addItem(newProduct);
				currentProduct = newProduct;

				if (k === 1) {
					processingTime = 0;
					newProduct.orderQuantity = parseInt(this.itemQty(), 10);
					$.Topic(pubsub.topicNames.CART_ADD).publishWith(
						newProduct, [{
							message: "success"
						}]);
				} else {
					processingTime = 250;
					newProduct.orderQuantity = parseInt(1, 10);
					getWidget.cart().addItem(newProduct);
				}
				
				

			},
			onClickZipSubmit: function (data, event) {
				if (event.type == "click" || (event.type == "keyup" && event.keyCode == 13)) {
					var searchZipCode = $("#zip-code-text-available").val().trim();
					if (searchZipCode.length === 0) {
						return;
					}
					$('body').css('overflow', 'hidden');
					$('body').css('position', 'fixed');

					function validateZipCode(elementValue) {
						var zipCodePattern = /^\d{5}$|^\d{5}-\d{4}$/;
						return zipCodePattern.test(elementValue);
					}
					$('.new-zip-section .error-msg').hide();
					/*if (validateZipCode(searchZipCode)) {
					    $('.new-zip-section .error-msg').show();
					} else {
					    $('.new-zip-section .error-msg').hide();
					}*/

					var widget = arguments[0];
					$.ajax({
						url: "https://api.tomtom.com/search/2/search/"+searchZipCode+".json?key=iDGYNjVRhRq1hFJFf3bJXm886ieFlYKR&countrySet=US&limit=1",
						method: "GET",
						success: function (data) {
							$(".new-zip-section").find(".error-msg").hide();
							if (data.status === "ZERO_RESULTS") {
								$(".new-zip-section").find(".error-msg").show().text("Entered Zip code is not valid.");
								return;

							}
							var storeNumber;

							var latitude = data.results["0"].position.lat,
								longitude = data.results["0"].position.lon;
								productData = widget.skuData();
							if (!productData) {
								productData = widget.product().product;
							}
							var opusProductFlag = productData.OPUS;


							//TRV - 1917 - E
							var stsProductFlag = widget.skuSts();

							var skuId = productData.repositoryId;
							var qty = widget.itemQty();
							var cartQuantity = 0;

							if (!reqData) reqData = {
								"skuId": skuId,
								"stsProductFlag": "",
								"storeNumber": storeNumber,
								"location": {
									"latitude": latitude,
									"longitude": longitude
								},
								"quantity": qty,
								"radiusLimit": 400,
								"limit": 4
							}
							qty = Number(qty);
							//widget.itemQty(qty);


							//var qty = widget.itemQty();
							var reqData = {
								"skuId": skuId,
								"stsProductFlag": "",
								"storeNumber": storeNumber,
								"location": {
									"latitude": latitude,
									"longitude": longitude
								},
								"quantity": qty,
								"radiusLimit": 400,
								"limit": 4
							};
							//_this.getSkuInventoryFromRepository(reqData, 400, widget);
							var stores = helpers.storeLocatorUtils().getStores(latitude, longitude, storeItems);
							var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);

							var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();
							helpers.storeLocatorUtils().getInventoryResponseIR(stores, qty, storeNumber, 4, skuId, storeIds, reqData, "changeStorePDPZip", cartInventoryDetailsArrayObj);
						}
					});
				}
			},
			onClickDelivery: function (e, event) {
				var widget = this;
				$(".tr-e-delivery").find(".error-msg").hide();
				if (!quantityChangeDeliveryTriggered) {
					$(".tr-store-stock-error").hide().text("");
					widget.quantityChange();
				} else {
					quantityChangeDeliveryTriggered = false;

				}
			},
			opusOption: function (data, event) {

				var sWarnOptid = $(event.currentTarget).parents().find('#storePickupWarngModal .store-warning input[type=radio]:checked').attr('id');
				if ("s-decline-upload" === sWarnOptid) {
					data.isPhotoUpload(false);
				} else if ("s-accept-upload" === sWarnOptid) {
					$('#available-today').prop('checked', false);
					$('#ship-to-home').prop('checked', true);
					data.isPhotoUpload(true);

				}
				$.Topic("opusIsPhotoUpload").publish(data.isPhotoUpload());

			},
			//TRV - TROCC-1917 - E
			handlePersonalizeButtonValidation: function () {
				personalizeClicked = false;
				var flag = 1,
					avl_flag = 0,
					variantOptions = this.variantOptionsArray();

				if ($(".occasion-btn:visible").length > 0) {
					var occasion_index = $(".occasion-btn").find("span.index").text();

					if (occasion_index == "0") {
						$(".tr-w-occasion").find(".error-msg").show();
						flag = 0;
					}
				}
				if (!$("input[name='optradio']:checked").val()) {
					$(".tr-e-delivery").find(".error-msg").show();
					flag = 0;
				}

				if (this.bomArray().length > this.urlObjArr.length) {
					$(".tr-e-bom").find(".main-error").text("Please select all applicable options").show();
					flag = 0;
				}

				if (!this.allOptionsSelectedF()) {
					$(".tr-e-product-variation").find(".error-msg").text("Please select the applicable variants").show();
					$(".tr-e-product-variation").find(".error-msg").attr("data-val", 1)
					flag = 0;
				} else if (!this.allOptionsSelected()) {
					$(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
					$(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);
					$(".personalizationBtns").addClass('hide');
                    $('.tr-xs-personalize-block').addClass('hide');

					flag = 0;

					if (variantOptions.length === 1) {
						$(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
						$(".personalizationBtns").addClass('hide');
                        $('.tr-xs-personalize-block').addClass('hide');
					}
				}
				if (this.allOptionsSelected()) {
					avl_flag = 1;
				}

				if (flag && this.skuRepoId()) {
					$('.personalization-main-container').show();
				}
				if (!flag) {
					if ($('.error-msg:visible:first').offset()) {
						var position = $('.error-msg:visible:first').offset().top;
						window.scrollTo(0, position);
					}
				}
				return flag;

			},
			handlePersonalize: function () {
			    var widget = this;
				var productID = this.product().id() || "unknown";
				dataLayer.push({
					'event': 'personalizeStart',
					'productId': productID
				});

				$("#inventory-error").text("");
				if (!this.handlePersonalizeButtonValidation() || this.prodAvlFlag() === 0) {
					return;
				}

				var widget = this,
					productData = widget.skuData();


				var variantOptions = this.variantOptionsArray(),
					sku_obj = this.getSelectedSku(variantOptions);

				if (this.product().childSKUs().length == 1) {
					sku_obj = this.product().childSKUs()[0];
				}
			
				$("#inventory-error").text("");
				goToPersonalize.call(widget);
				
			//   for when there's only a DYNAMIC tab on PDP and nothing else and not opened up
			    var initData = widget.skuData();
		        var firstProductZone = "#item-1-Zone-ProductZone_"+initData.id+"_1";
		        $(firstProductZone).find(".expressPhase-class.selectedExpress").click();
		        
		        
				function goToPersonalize() {
					var all_delivery_details = JSON.parse(storageApi.getInstance().getItem("alldeliveryDetails")) || {},
						curr_flag = 0;

					var delivery_details = {};
				
					
					var tempStoreDetails = {};
					var tempaddress = {};
					tempStoreDetails.skuid = this.skuRepoId();
					tempStoreDetails.deliveryType = $("input[name='optradio']:checked").val();
					if ($("input[name='optradio']:checked").val() == 2) { /**** Ship to Home */
						if (inventoryDataIR.hasOwnProperty('shipping') &&  inventoryDataIR.shipping[0].store.id !== "920") { /****Ship from Store */
							tempStoreDetails.store_details = inventoryDataIR.shipping[0].store.id + "~~" + inventoryDataIR.shipping[0].store.name + "~~" + inventoryDataIR.shipping[0].store.street + "~~" + inventoryDataIR.shipping[0].store.deliveryDate + "~~" + inventoryDataIR.shipping[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.shipping[0].store.streetExt;

							tempaddress.storeName = inventoryDataIR.shipping[0].store.name;
							tempaddress.storeNumber = inventoryDataIR.shipping[0].store.id;
							tempaddress.address = inventoryDataIR.shipping[0].store.street;
							tempaddress.address1 = inventoryDataIR.shipping[0].store.street;
							tempaddress.address2 = inventoryDataIR.shipping[0].store.streetExt;
							tempaddress.city = inventoryDataIR.shipping[0].store.city;
							tempaddress.state = inventoryDataIR.shipping[0].store.state;
							tempaddress.zipCode = inventoryDataIR.shipping[0].store.postalCode;
							tempaddress.phone = inventoryDataIR.shipping[0].store.phone;
							tempaddress.deliveryDate = inventoryDataIR.shipping[0].store.deliveryDate;
							tempaddress.isSTS = this.displaySts();
							tempaddress.closeTime = inventoryDataIR.shipping[0].store.closeTime;
							tempaddress.onHandQuantity = inventoryDataIR.shipping[0].store.onHandQuantity;
							tempaddress.addToCartDate = new Date();
							tempaddress.stsDate = "";
							if (inventoryDataIR.shipping[0].store.hours.daysOfWeek && inventoryDataIR.shipping[0].store.hours.daysOfWeek != "") {
								tempaddress.stsDate = inventoryDataIR.shipping[0].store.hours.daysOfWeek[5].date;
							} else {
								tempaddress.stsDate = "";
							}

							tempStoreDetails.store_address = tempaddress;
							tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
							tempStoreDetails.userSelectedStore = true;
							if (inventoryDataIR.pickup[0].store) {
								tempStoreDetails.pickupAvailable = inventoryDataIR.pickup[0].available;
							} else {
								tempStoreDetails.pickupAvailable = false;
							}
						} else {
							if (inventoryDataIR.hasOwnProperty('pickup') && inventoryDataIR.pickup[0].store) {
								tempStoreDetails.store_details = inventoryDataIR.pickup[0].store.id + "~~" + inventoryDataIR.pickup[0].store.name + "~~" + inventoryDataIR.pickup[0].store.street + "~~" + inventoryDataIR.pickup[0].store.deliveryDate + "~~" + inventoryDataIR.pickup[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.pickup[0].store.streetExt;

								tempaddress.storeName = inventoryDataIR.pickup[0].store.name;
								tempaddress.storeNumber = inventoryDataIR.pickup[0].store.id;
								tempaddress.address = inventoryDataIR.pickup[0].store.street;
								tempaddress.address1 = inventoryDataIR.pickup[0].store.street;
								tempaddress.address2 = inventoryDataIR.pickup[0].store.streetExt;
								tempaddress.city = inventoryDataIR.pickup[0].store.city;
								tempaddress.state = inventoryDataIR.pickup[0].store.state;
								tempaddress.zipCode = inventoryDataIR.pickup[0].store.postalCode;
								tempaddress.phone = inventoryDataIR.pickup[0].store.phone;
								tempaddress.deliveryDate = inventoryDataIR.pickup[0].store.deliveryDate;
								tempaddress.isSTS = this.displaySts();
								tempaddress.closeTime = inventoryDataIR.pickup[0].store.closeTime;
								tempaddress.onHandQuantity = inventoryDataIR.pickup[0].store.onHandQuantity;
								tempaddress.addToCartDate = new Date();
								tempaddress.stsDate = "";
								if (inventoryDataIR.pickup[0].store.hours.daysOfWeek && inventoryDataIR.pickup[0].store.hours.daysOfWeek != "") {
									tempaddress.stsDate = inventoryDataIR.pickup[0].store.hours.daysOfWeek[5].date;
								} else {
									tempaddress.stsDate = "";
								}

								tempStoreDetails.store_address = tempaddress;
								tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
								tempStoreDetails.userSelectedStore = true;
								tempStoreDetails.pickupAvailable = inventoryDataIR.pickup[0].available;
							} else {
								tempStoreDetails.store_details = "";
								tempStoreDetails.store_address = {};
								if(inventoryDataIR.hasOwnProperty('shipping')){
								    tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
								}
								tempStoreDetails.userSelectedStore = true;
								tempStoreDetails.pickupAvailable = false;
							}
						}
					} else { /**** Pick up */
						if (inventoryDataIR.hasOwnProperty('pickup') && inventoryDataIR.pickup[0].store) {
							tempStoreDetails.store_details = inventoryDataIR.pickup[0].store.id + "~~" + inventoryDataIR.pickup[0].store.name + "~~" + inventoryDataIR.pickup[0].store.street + "~~" + inventoryDataIR.pickup[0].store.deliveryDate + "~~" + inventoryDataIR.pickup[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.pickup[0].store.streetExt;

							tempaddress.storeName = inventoryDataIR.pickup[0].store.name;
							tempaddress.storeNumber = inventoryDataIR.pickup[0].store.id;
							tempaddress.address = inventoryDataIR.pickup[0].store.street;
							tempaddress.address1 = inventoryDataIR.pickup[0].store.street;
							tempaddress.address2 = inventoryDataIR.pickup[0].store.streetExt;
							tempaddress.city = inventoryDataIR.pickup[0].store.city;
							tempaddress.state = inventoryDataIR.pickup[0].store.state;
							tempaddress.zipCode = inventoryDataIR.pickup[0].store.postalCode;
							tempaddress.phone = inventoryDataIR.pickup[0].store.phone;
							tempaddress.deliveryDate = inventoryDataIR.pickup[0].store.deliveryDate;
							tempaddress.isSTS = this.displaySts();
							tempaddress.closeTime = inventoryDataIR.pickup[0].store.closeTime;
							tempaddress.onHandQuantity = inventoryDataIR.pickup[0].store.onHandQuantity;
							tempaddress.addToCartDate = new Date();
							tempaddress.stsDate = "";
							if (inventoryDataIR.pickup[0].store.hours.daysOfWeek && inventoryDataIR.pickup[0].store.hours.daysOfWeek != "") {
								tempaddress.stsDate = inventoryDataIR.pickup[0].store.hours.daysOfWeek[5].date;
							} else {
								tempaddress.stsDate = "";
							}

							tempStoreDetails.store_address = tempaddress;
							tempStoreDetails.store_inventory = inventoryDataIR.pickup[0].store.id;
							tempStoreDetails.userSelectedStore = true;
							if (this.isPhotoUpload()) {
								tempStoreDetails.pickupAvailable = false;
							} else {
								tempStoreDetails.pickupAvailable = true;
							}
						}
					}
					delivery_details = tempStoreDetails;
					for (var key in all_delivery_details) {
						if (all_delivery_details.hasOwnProperty(key)) {
							if (key == this.skuRepoId()) {
								curr_flag = 1;
								all_delivery_details[key] = delivery_details;
							}
						}
					}

					if (curr_flag === 0) {
						all_delivery_details[this.skuRepoId()] = delivery_details;
					}

					var occasion_obj = JSON.parse(storageApi.getInstance().getItem("occasion") || "{}"),
						longDescr_obj = JSON.parse(storageApi.getInstance().getItem("prodDescr") || "{}");

					occasion_obj[this.skuRepoId()] = this.occasionCode();
					longDescr_obj[this.skuRepoId()] = $(".js-product-header-descr").text();


					//start
					var store_obj = JSON.parse(storageApi.getInstance().getItem("storeId") || "{}");
					store_obj[this.skuRepoId()] = this.storeCode();
					//localStorage.setItem('skuID',this.skuRepoId());


					storageApi.getInstance().setItem('deliveryDetails', JSON.stringify(delivery_details));
					storageApi.getInstance().setItem('alldeliveryDetails', JSON.stringify(all_delivery_details));
					storageApi.getInstance().setItem('occasion', JSON.stringify(occasion_obj));
					storageApi.getInstance().setItem('prodDescr', JSON.stringify(longDescr_obj));

					//start
					storageApi.getInstance().setItem('storeId', JSON.stringify(store_obj));


					//$('html,body').animate({scrollTop: 0}, 600);
					// $('#personalization-zone-container').empty();
					var variantOptions = this.variantOptionsArray();
					notifier.clearSuccess(this.WIDGET_ID);
					//get the selected options, if all the options are selected.
					var selectedOptions = this.getSelectedSkuOptions(variantOptions);

					var selectedOptionsObj = {
						'selectedOptions': selectedOptions
					};
					if (this.hasGift() && this.giftData()) {
						$.Topic('PERSONALIZE_GIFT_ITEM').publish(this.giftData());
					} else {
						$.Topic('PERSONALIZE_GIFT_ITEM').publish(false);
					}
					location.hash = "personalize";
					personalizeClicked = true;
					$.Topic("URL_HASH_CHANGE.memory").publish();
					var requestQuantity = parseInt($('#select-quantity').val());
					
					if(widget.selectedBomArray().length > 0 ){
					    $.each(widget.selectedBomArray(), function(k,v){
					        delete v['imgurl'];
					        delete v['isInStock'];
					        widget.selectedBomArray()[k] = v;
					    });
					}
					
					var viewModel = ko.mapping.fromJS(newPersonalizationData);
					$.Topic('TR_PDP_NEW_PERSONALIZATION_DATA').publish(viewModel,requestQuantity,widget.selectedBomArray());
					
					var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
					if (this.variantOptionsArray().length > 0) {
						//assign only the selected sku as child skus
						newProduct.childSKUs = [this.selectedSku()];
					}
					newProduct.orderQuantity = parseInt(this.itemQty(), 10);
					var l_windowWidth = $(window).width();
					if (l_windowWidth <= 767) {
						var pdpTitleInfo = $('.tr-personlization-pdp-header-info');
						if (selectedOptions[0] && selectedOptions[0].optionName === "Color") {
							var colorVal = selectedOptions[0].optionValue.split('|')[0];
							pdpTitleInfo.find('#tr-persona-color-xs').text(colorVal);
						}
						if (selectedOptions[1] && selectedOptions[1].optionName === "Size") {
							var sizeVal = selectedOptions[1].optionValue.split('|')[0];
							pdpTitleInfo.find('#tr-persona-size-xs').text(sizeVal);
						}
						pdpTitleInfo.css({
							'display': 'block'
						});
						$('.tr-personlization-pdp-header-section').append(pdpTitleInfo);
					}

					$.Topic("SENT_TO_PERSONALIZE").publish({
						qty: parseInt(this.itemQty(), 10),
						opusPhotoUpload: this.isPhotoUpload
					});
					$.Topic('backOrderFlag').publish(this.backOrderFlag());
					// To disable Add to cart button for three seconds when it is clicked and enabling again
					this.isAddToCartClicked(true);
					var self = this;
					setTimeout(enableAddToCartButton, 3000);

					function enableAddToCartButton() {
						self.isAddToCartClicked(false);
					};

				}
			},
			
			generateExpressPhase:function(expressData){
			    //console.log("expressData: ", expressData);
			    var widget = this;
		        var getProductData = expressData;
		         var skuDetails = [];
		          var getDesignSku = [];
		          var specialDesignAssociations = [];
		          var specialTempDesignAvailable = false;
		        if(getProductData.zones.length > 0){
		            $.each(getProductData.zones, function(k,v){
		                $.each(v.expressPhraseReferences, function(m,n){
		                    if(getProductData.referencedExpressPhrases[n].id === n){
		                        v.expressPhraseReferences[m] = JSON.parse(JSON.stringify(getProductData.referencedExpressPhrases[n]));
		                    }
		                    if(v.expressPhraseReferences[m].priceModel.price === null){
		                        v.expressPhraseReferences[m].priceModel.price = {"amount": 0,
                                                                                "currency": "USD"}
		                    }
		                    if(v.expressPhraseReferences[m].type === "CYO"){
		                        if(typeof(v.vertical) !== "undefined" && v.vertical){
		                            v.expressPhraseReferences[m].details[0].characterCount = v.height;
		                            v.expressPhraseReferences[m].details[0].lineNumber = v.width;
		                        }else{
		                            v.expressPhraseReferences[m].details[0].characterCount = v.width;
		                            v.expressPhraseReferences[m].details[0].lineNumber = v.height;
		                        }
		                        
		                    }
		                    if(v.expressPhraseReferences[m].type === "DYNAMIC"){
		                        var inputWidth = v.width;
		                        $.each(v.expressPhraseReferences[m].details, function(b,x){
		                            if(x.type === "TEXT"){
		                                v.width = inputWidth;
		                                if(x.characterCount == 0){
		                                    x.characterCount = v.width;
		                                }else{
		                                    v.width = x.characterCount;
		                                }
		                            }
		                            if(x.type === "DESIGN"){
		                                if(x.availableDesignClassReferences.length > 0){
		                                    specialTempDesignAvailable = true;
		                                    $.each(x.availableDesignClassReferences, function(a,q){
    		                                    x.availableDesignClassReferences[a] = JSON.parse(JSON.stringify(getProductData.referencedDesignClasses[q]));
    		                                    $.each(x.availableDesignClassReferences[a].classMemberReferences, function(e,r){
    		                                        var id = parseInt(r);
    		                                        skuDetails.push(getProductData.referencedDesigns[r]);
    		                                    });
    		                                    x.availableDesignClassReferences[a]['skuDetails'] = skuDetails;
                		                        skuDetails = [];
                		                        getDesignSku = [];
		                                    });
		                                    x.availableDesignClassReferences.unshift({
                    		                        "classMemberReferences":"",
                    		                        "displayName":"No Graphics",
                    		                        "id":"catNo",
                    		                });
                    		                specialDesignAssociations = x.availableDesignClassReferences;
		                                }
		                            }
				
		                            
		                        });
		                    }
		                });
		                if(getProductData.photoUpload.required === true){
		                    v['minimumPhotoResolution'] = '576,720';
		                    v['photoUrl'] = "/file/general/no-image.jpg";
		                }
		                v['specialDesignAssociations'] = specialDesignAssociations;
		                v['specialTemplate'] = false;
		                v['zoneCost'] = 0;
		                v['personalizeCompleted'] = false;
		                v['isPhotoUploaded'] = false;
		                v['personalizeImage'] = 'https://thingsremembered.scene7.com/ir/render/ThingsRememberedRender/'+v.zoneAttributes.vignette;
		            });
		        }
		        if($('.error-msg').is(':visible') === false){
		            $('.tr-xs-personalize-block').removeClass('hide');
            		$(".addToCartBtn").hide();
					$(".personalizationBtn").show();
					$(".personalizationBtns").removeClass('hide');
		        }
		        
		        
		    },
		    
		    generateFonts:function(expressData){
		        var getProductData = expressData;
		        if(getProductData.zones.length > 0){
		            $.each(getProductData.zones, function(k,v){
						if(v.fontOptions && v.fontOptions.defaultSelectionReference) {
		                	v.fontOptions.defaultSelectionReference = getProductData.referencedFonts[v.fontOptions.defaultSelectionReference];
							$.each(v.fontOptions.groupMemberReferences, function(m,n){
								if(getProductData.referencedFonts[n].id === n){
									v.fontOptions.groupMemberReferences[m] = getProductData.referencedFonts[n];
								}
							});
						}
		            });
		        }
		        
		    },
		    generalFonts: function(expressData){
		       var getProductData = expressData;
		       var CYOFonts = [];
		       var monogram1 = [];
		       var monogram2 = [];
		       var monogram3 = []; 
		       if(getProductData.zones.length > 0){
		            $.each(getProductData.zones, function(k,v){
		                CYOFonts = [];
		                monogram1 = [];
		                monogram2 = [];
						monogram3 = [];
						if(v.fontOptions && v.fontOptions.groupMemberReferences) {
							$.each(v.fontOptions.groupMemberReferences, function(m,n){
								n.groupId=v.fontOptions.groupId;
								n.optionType = v.fontOptions.optionType;
								if(n.type === "STANDARD"){
									CYOFonts.push(n);
								}
								if(n.type === "MONOGRAM1" || n.type === "INITIAL1"){
									monogram1.push(n);
								}
								if(n.type === "MONOGRAM2" || n.type === "INITIAL2"){
									monogram2.push(n);
								}
								if(n.type === "MONOGRAM3" || n.type === "INITIAL3"){
									monogram3.push(n);
								}
							});
							v.fontOptions["cyoFonts"] = CYOFonts;
							v.fontOptions["monogram1Fonts"] = monogram1;
							v.fontOptions["monogram2Fonts"] = monogram2;
							v.fontOptions["monogram3Fonts"] = monogram3;
						}
		            });
		        }
		    },
		    generateColorFills:function(expressData){
		        var getProductData = expressData;
		        if(getProductData.zones.length > 0){
		            $.each(getProductData.zones, function(k,v){
		                if(v.colorOptions !== null){
		                    v.colorOptions.defaultSelectionReference = getProductData.referencedColors[v.colorOptions.defaultSelectionReference];
		                    $.each(v.colorOptions.groupMemberReferences, function(m,n){
		                    if(getProductData.referencedColors[n].id === n){
		                        v.colorOptions.groupMemberReferences[m] = getProductData.referencedColors[n];
		                    }
		                    if(v.colorOptions.groupMemberReferences[m].upcharge === null){
		                        v.colorOptions.groupMemberReferences[m].upcharge = {
		                            "price": {
		                                "amount":0,
		                                "currency":"USD"
		                            },
		                            "sku":"0000"
		                        }
		                    }
		                });
		                }
		                
		            });
		        }
		    },
		    generateDesigns:function(expressData){
		            var getProductData = expressData;
		            var skuDetails = [];
		            var getDesignSku = [];
		            if(getProductData.zones.length > 0){
		                $.each(getProductData.zones, function(k,v){
		                    $.each(v.designClassAssociations, function(m,n){
		                        var getData = getProductData.referencedDesignClasses[n.designClassReference];
		                        v.designClassAssociations[m] = getData;
		                        $.each(getData.classMemberReferences, function(l,s){
		                            var id = parseInt(s);
		                            skuDetails.push(getProductData.referencedDesigns[id]);
		                        });
		                        v.designClassAssociations[m]['skuDetails'] = skuDetails;
		                        skuDetails = [];
		                        getDesignSku = [];
		                    });
		                });
		            }
		        },
		        
		        generateNographic:function(expressData){
		            if(expressData.zones.length > 0){
		                
		                $.each(expressData.zones, function(k,v){
		                    if(v.designClassAssociations.length > 0){
		                        if(v.designClassAssociations[0].id !== "catNo"){
		                             v.designClassAssociations.unshift({
        		                        "classMemberReferences":"",
        		                        "displayName":"No Graphics",
        		                        "id":"catNo"
        		                    });
		                        }
		                    }
		                   
		                   // return false;
		                });
		            }
		        },
		    

			/**
			 * Retrieve list of spaces for a user
			 */
			getSpaces: function (callback) {
				var widget = this;
				var successCB = function (result) {
					var mySpaceOptions = [];
					var joinedSpaceOptions = [];
					if (result.response.code.indexOf("200") === 0) {

						//spaces
						var spaces = result.items;
						spaces.forEach(function (space, index) {
							var spaceOption = {
								spaceid: space.spaceId,
								spaceNameFull: ko.observable(space.spaceName),
								spaceNameFormatted: ko.computed(function () {
									return space.spaceName + " (" + space.creatorFirstName + " " + space.creatorLastName + ")";
								}, widget),
								creatorid: space.creatorId,
								accessLevel: space.accessLevel,
								spaceOwnerFirstName: space.creatorFirstName,
								spaceOwnerLastName: space.creatorLastName
							};

							// if user created the space, add it to My Spaces, otherwise add it to Joined Spaces
							if (space.creatorId == swmRestClient.apiuserid) {
								mySpaceOptions.push(spaceOption);
							} else {
								joinedSpaceOptions.push(spaceOption);
							}
						});

						// sort each group alphabetically
						mySpaceOptions.sort(mySpacesComparator);
						joinedSpaceOptions.sort(joinedSpacesComparator);

						widget.spaceOptionsGrpMySpacesArr(mySpaceOptions);
						widget.spaceOptionsGrpJoinedSpacesArr(joinedSpaceOptions);

						var groups = [];
						var mySpacesGroup = {
							label: widget.translate('mySpacesGroupText'),
							children: ko.observableArray(widget.spaceOptionsGrpMySpacesArr())
						};
						var joinedSpacesGroup = {
							label: widget.translate('joinedSpacesGroupText'),
							children: ko.observableArray(widget.spaceOptionsGrpJoinedSpacesArr())
						};

						var createOptions = [];
						var createNewOption = {
							spaceid: "createnewspace",
							spaceNameFull: ko.observable(widget.translate('createNewSpaceOptText'))
						};
						createOptions.push(createNewOption);
						var createNewSpaceGroup = {
							label: "",
							children: ko.observableArray(createOptions)
						};

						groups.push(mySpacesGroup);
						groups.push(joinedSpacesGroup);
						groups.push(createNewSpaceGroup);
						widget.spaceOptionsArray(groups);
						widget.mySpaces(mySpaceOptions);

						if (callback) {
							callback();
						}
					}
				};
				var errorCB = function (resultStr, status, errorThrown) {};

				swmRestClient.request('GET', '/swm/rs/v1/sites/{siteid}/spaces', '', successCB, errorCB, {});
			},

			// SC-4166 : ajax success/error callbacks from beforeAppear does not get called in IE9, ensure dropdown options are populated when opening dropdown
			openAddToWishlistDropdownSelector: function () {
				var widget = this;
				if (widget.spaceOptionsArray().length === 0) {
					widget.getSpaces();
				}
			},
			// this method validates if all the options of the product are selected before allowing
			// add to space. Unlike validateAddToCart, however, it does not take into account inventory.
			validateAddToSpace: function () {
				var allOptionsSelected = true;
				if (this.variantOptionsArray().length > 0) {
					var variantOptions = this.variantOptionsArray();
					for (var i = 0; i < variantOptions.length; i++) {
						if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
							allOptionsSelected = false;
							break;
						}
					}
					if (allOptionsSelected) {
						// get the selected sku based on the options selected by the user
						var selectedSKUObj = this.getSelectedSku(variantOptions);


						if (selectedSKUObj === null) {
							return false;
						}
					}
				}

				// get quantity input value
				var quantityInput = this.itemQty();
				if (quantityInput.toString() != "") {
					if (!quantityInput.toString().match(/^\d+$/) || Number(quantityInput) < 0) {
						return false;
					}
				}

				var addToSpaceButtonFlag = allOptionsSelected;
				if (!addToSpaceButtonFlag) {
					$('#cc-prodDetailsAddToSpace').attr("aria-disabled", "true");
				}

				return addToSpaceButtonFlag;
			},

			//check whether all the variant options are selected and if so, populate selectedSku with the correct sku of the product.
			//this is generic method, can be reused in validateAddToSpace and validateAddToCart in future
			validateAndSetSelectedSku: function (refreshRequired) {
				var allOptionsSelected = true;
				if (this.variantOptionsArray().length > 0) {
					var variantOptions = this.variantOptionsArray();
					for (var i = 0; i < variantOptions.length; i++) {
						if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
							allOptionsSelected = false;
							this.selectedSku(null);
							break;
						}
					}
					if (allOptionsSelected) {
						// get the selected sku based on the options selected by the user
						var selectedSKUObj = this.getSelectedSku(variantOptions);
						if (selectedSKUObj === null) {
							return false;
						}
						this.selectedSku(selectedSKUObj);
					}
					if (refreshRequired) {
						this.refreshSkuData(this.selectedSku());
					}
				}
				return allOptionsSelected;
			},

			// displays Add to Space modal
			addToSpaceClick: function (widget) {
				var variantOptions = this.variantOptionsArray();
				notifier.clearSuccess(this.WIDGET_ID);

				//get the selected options, if all the options are selected.
				var selectedOptions = this.getSelectedSkuOptions(variantOptions);
				var selectedOptionsObj = {
					'selectedOptions': selectedOptions
				};
				var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
				newProduct.desiredQuantity = this.itemQty();

				if (this.variantOptionsArray().length > 0) {
					//assign only the selected sku as child skus
					newProduct.childSKUs = [this.selectedSku()];
				}
				newProduct.productPrice = (newProduct.salePrice != null) ? newProduct.salePrice : newProduct.listPrice;
				$.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD).publishWith(newProduct, [{
					message: "success"
				}]);
			},

			// displays Add to Space modal, triggered from selector button
			addToSpaceSelectorClick: function (widget) {
				var variantOptions = this.variantOptionsArray();
				notifier.clearSuccess(this.WIDGET_ID);

				//get the selected options, if all the options are selected.
				var selectedOptions = this.getSelectedSkuOptions(variantOptions);
				var selectedOptionsObj = {
					'selectedOptions': selectedOptions
				};
				var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
				newProduct.desiredQuantity = this.itemQty();

				if (this.variantOptionsArray().length > 0) {
					//assign only the selected sku as child skus
					newProduct.childSKUs = [this.selectedSku()];
				}
				newProduct.productPrice = (newProduct.salePrice != null) ? newProduct.salePrice : newProduct.listPrice;
				$.Topic(pubsub.topicNames.SOCIAL_SPACE_SELECTOR_ADD).publishWith(newProduct, [{
					message: "success"
				}]);
			},

			// automatically add product to selected space
			addToSpaceSelect: function (widget, spaceId) {
				var variantOptions = this.variantOptionsArray();
				notifier.clearSuccess(this.WIDGET_ID);

				//get the selected options, if all the options are selected.
				var selectedOptions = this.getSelectedSkuOptions(variantOptions);
				var selectedOptionsObj = {
					'selectedOptions': selectedOptions
				};
				var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
				newProduct.desiredQuantity = this.itemQty();

				if (this.variantOptionsArray().length > 0) {
					//assign only the selected sku as child skus
					newProduct.childSKUs = [this.selectedSku()];
				}
				newProduct.productPrice = (newProduct.salePrice != null) ? newProduct.salePrice : newProduct.listPrice;
				$.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD_TO_SELECTED_SPACE).publishWith(newProduct, [spaceId]);
			},

			/**
			 * Fetch Facebook app id
			 */
			fetchFacebookAppId: function () {
				var widget = this;
				var serverType = CCConstants.EXTERNALDATA_PRODUCTION_FACEBOOK;
				if (widget.isPreview()) {
					serverType = CCConstants.EXTERNALDATA_PREVIEW_FACEBOOK;
				}
				ccRestClient.request(CCConstants.ENDPOINT_MERCHANT_GET_EXTERNALDATA,
					null, widget.fetchFacebookAppIdSuccessHandler.bind(widget),
					widget.fetchFacebookAppIdErrorHandler.bind(widget),
					serverType);
			},

			/**
			 * Fetch Facebook app id successHandler, update local and global scope data
			 */
			fetchFacebookAppIdSuccessHandler: function (pResult) {
				var widget = this;
				widget.siteFbAppId(pResult.serviceData.applicationId);

				//if (widget.siteFbAppId()) {
				//  facebookSDK.init(widget.siteFbAppId());
				//}
			},

			/**
			 * Fetch Facebook app id error handler
			 */
			fetchFacebookAppIdErrorHandler: function (pResult) {
				//logger.debug("Failed to get Facebook appId.", result);
			},

			// Share product to FB
			shareProductFbClick: function () {
				var widget = this;

				// open fb share dialog
				var protocol = window.location.protocol;
				var host = window.location.host;
				var productUrlEncoded = encodeURIComponent(protocol + "//" + host + "/product/" + widget.product().id());

				var appID = widget.siteFbAppId();
				// NOTE: Once we can support the Facebook Crawler OG meta-tags, then we should try and use the newer Facebook Share Dialog URL
				//       (per https://developers.facebook.com/docs/sharing/reference/share-dialog).  Until then, we will use a legacy
				//       share URL.  Facebook may eventually not support this older URL, so would be good to replace it as soon as possible.
				//var fbShareUrl = "https://www.facebook.com/dialog/share?app_id=" + appID + "&display=popup&href=" + spaceUrlEncoded + "&redirect_uri=https://www.facebook.com";
				var fbShareUrl = "https://www.facebook.com/sharer/sharer.php?app_id=" + appID + "&u=" + productUrlEncoded;
				var facebookWin = window.open(fbShareUrl, 'facebookWin', 'width=720, height=500');
				if (facebookWin) {
					facebookWin.focus();
				}
			},

			// Share product to Twitter
			shareProductTwitterClick: function () {
				var widget = this;
				var productNameEncoded = encodeURIComponent(widget.product().displayName());
				var protocol = window.location.protocol;
				var host = window.location.host;
				var productUrlEncoded = encodeURIComponent(protocol + "//" + host + "/product/" + widget.product().id());
				var twitterWin = window.open('https://twitter.com/share?url=' + productUrlEncoded + '&text=' + productNameEncoded, 'twitterWindow', 'width=720, height=500');
				if (twitterWin) {
					twitterWin.focus();
				}
			},

			// Share product to Pinterest
			shareProductPinterestClick: function () {
				var widget = this;
				var productNameEncoded = encodeURIComponent(widget.product().displayName());
				var protocol = window.location.protocol;
				var host = window.location.host;
				var productUrlEncoded = encodeURIComponent(protocol + "//" + host + "/product/" + widget.product().id());
				var productMediaEncoded = encodeURIComponent(protocol + "//" + host + widget.product().primaryLargeImageURL());

				var pinterestWin = window.open('https://pinterest.com/pin/create/button/?url=' + productUrlEncoded + '&description=' + productNameEncoded + '&media=' + productMediaEncoded, 'pinterestWindow', 'width=720, height=500');
				if (pinterestWin) {
					pinterestWin.focus();
				}
			},

			// Share product by Email
			shareProductEmailClick: function () {
				var widget = this;
				var mailto = [];
				var protocol = window.location.protocol;
				var host = window.location.host;
				var productUrl = protocol + "//" + host + "/product/" + widget.product().id();
				mailto.push("mailto:?");
				mailto.push("subject=");
				mailto.push(encodeURIComponent(widget.translate('shareProductEmailSubject', {
					'productName': widget.product().displayName()
				})));
				mailto.push("&body=");
				var body = [];
				body.push(widget.translate('shareProductEmailBodyIntro', {
					'productName': widget.product().displayName()
				}));
				body.push("\n\n");
				body.push(productUrl);
				mailto.push(encodeURIComponent(body.join("")));
				window.location.href = mailto.join("");
			},

			handleLoadEvents: function (eventName) {
				if (eventName.toUpperCase() === LOADING_EVENT) {
					spinner.create(productLoadingOptions);
					$('#cc-product-spinner').css('z-index', 1);
				} else if (eventName.toUpperCase() === LOADED_EVENT) {
					this.removeSpinner();
				}
			},
			// Loads the Magnifier and/or Viewer, when required
			loadImage: function () {
				if (resourcesAreLoaded) {
					var contents = $('#cc-image-viewer').html();
					if (!contents) {
						if (this.viewportWidth() > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
							this.loadMagnifier();
						} else if (this.viewportWidth() >= CCConstants.VIEWPORT_TABLET_LOWER_WIDTH) {
							this.loadZoom();
						} else {
							//Load zoom on carousel
							this.loadCarouselZoom();
						}
					} else {
						this.loadViewer(this.handleLoadEvents.bind(this));
					}
				    
				} else if (resourcesNotLoadedCount++ < resourcesMaxAttempts) {
					setTimeout(this.loadImage, 500);
				}
			},

			groupImages: function (imageSrc) {
				var self = this;
				var images = [];
				if (imageSrc) {
					for (var i = 0; i < imageSrc.length; i++) {
						if (i % 4 == 0) {
							images.push(ko.observableArray([imageSrc[i]]));
						} else {
							images[images.length - 1].push(imageSrc[i]);
						}
					}
				}

				return images;
			},

			handleCarouselArrows: function (data, event) {
				// Handle left key
				if (event.keyCode == 37) {
					$('#prodDetails-imgCarousel').carousel('prev');
				}
				// Handle right key
				if (event.keyCode == 39) {
					$('#prodDetails-imgCarousel').carousel('next');
				}
			},

			handleCycleImages: function (data, event, index, parentIndex) {
				var absoluteIndex = index + parentIndex * 4;
				// Handle left key
				if (event.keyCode == 37) {
					if (absoluteIndex == 0) {
						$('#prodDetails-imgCarousel').carousel('prev');
						$('#carouselLink' + (this.product().thumbImageURLs.length - 1)).focus();
					} else if (index == 0) {
						// Go to prev slide
						$('#prodDetails-imgCarousel').carousel('prev');
						$('#carouselLink' + (absoluteIndex - 1)).focus();
					} else {
						$('#carouselLink' + (absoluteIndex - 1)).focus();
					}
				}
				// Handle right key
				if (event.keyCode == 39) {
					if (index == 3) {
						$('#prodDetails-imgCarousel').carousel('next');
						$('#carouselLink' + (absoluteIndex + 1)).focus();
					} else if (absoluteIndex == (this.product().thumbImageURLs.length - 1)) {
						// Extra check when the item is the last item of the carousel
						$('#prodDetails-imgCarousel').carousel('next');
						$('#carouselLink0').focus();
					} else {
						$('#carouselLink' + (absoluteIndex + 1)).focus();
					}
				}
			},

			loadImageToMain: function (data, event, index) {
				this.activeImgIndex(index);
				$(document).find(".cc-viewer-pane").removeClass("hide");
				$(document).find("#display-video").addClass("hide");

				//this.mainImgUrl(this.getProductImages("L")[index]);
				return false;
			},

			assignImagesToProduct: function (pInput) {
				if (this.firstTimeRender == true) {
					this.product().primaryFullImageURL(pInput.primaryFullImageURL);
					this.product().primaryLargeImageURL(pInput.primaryLargeImageURL);
					this.product().primaryMediumImageURL(pInput.primaryMediumImageURL);
					this.product().primarySmallImageURL(pInput.primarySmallImageURL);
					this.product().primaryThumbImageURL(pInput.primaryThumbImageURL);
					this.firstTimeRender = false;
				}

				this.product().thumbImageURLs(pInput.thumbImageURLs);
				this.product().smallImageURLs(pInput.smallImageURLs);
				this.product().mediumImageURLs(pInput.mediumImageURLs);
				this.product().largeImageURLs(pInput.largeImageURLs);
				this.product().fullImageURLs([]);
				this.product().fullImageURLs(pInput.fullImageURLs);
				this.product().sourceImageURLs(pInput.sourceImageURLs);

				this.mainImgUrl(pInput.primaryFullImageURL);
				this.imgGroups(this.groupImages(pInput.thumbImageURLs));
				this.activeImgIndex(0);
				this.activeImgIndex.valueHasMutated();
			},

			onClickSetStore: function () {},

			onClickShowMore: function (e) {
				e.cancelBubble = true;
				if (e.stopPropagation) e.stopPropagation();

				if ($(".tr-prod-descr-show-more a").hasClass("js-hidden")) {
					$("#descr-container").addClass("overflown");
					$(".tr-prod-descr-show-more a").text("less...");
					$(".tr-prod-descr-show-more a").removeClass("js-hidden").addClass("js-shown");
				} else {
					$("#descr-container").removeClass("overflown");
					$(".tr-prod-descr-show-more a").text("more...");
					$(".tr-prod-descr-show-more a").addClass("js-hidden").removeClass("js-shown");
				}
			},
			onChangeBOMTypes: function () {},

			isProdDescrOverFlown: function () {

				if ($(".tr-e-product-description #descr-container").height() + 10 < $(".tr-e-product-description #descr-container .tr-prod-descr-string").height()) {
					return true;
				} else {
					return false;
				}
			},
			getProductImages: function (size, ProductObj) {

				var imgArr = [];

				var productObj = ProductObj || this.product(),
					productId, alternateImgCount = 0;

				if (typeof productObj.AlternateImageCount !== "undefined") {
					alternateImgCount = parseInt(this.getActualValue(productObj.AlternateImageCount));
				} else if (typeof productObj.alternateImageCount !== "undefined") {
					alternateImgCount = parseInt(this.getActualValue(productObj.alternateImageCount));
				}

				productId = this.getActualValue(productObj.id);

				var zeroPaddedProdId = this.getZeroPaddedSkuId(productId);

				switch (size) {
					case "L":
						for (var j = 0; j < alternateImgCount; j++) {
							imgArr.push(_this.scene7BaseUrl + zeroPaddedProdId + "_" + (j + 1) + "_lg?wid=" + imageRes);
						}
						break;

					case "S":
						for (var k = 0; k < alternateImgCount; k++) {

							imgArr.push(_this.scene7BaseUrl + zeroPaddedProdId + "_" + (k + 1) + "_lg?wid=" + imageRes);

						}
						break;
				}

				return imgArr;
			},
			getZeroPaddedSkuId: function (productId) {


				var zeroPaddedProdId = "";


				for (var i = 0; i < (9 - productId.length); i++) {
					zeroPaddedProdId += "0";
				}
				zeroPaddedProdId += productId;


				return zeroPaddedProdId;
			},
			getActualValue: function (obj) {

				if (typeof obj === "function") {


					return obj();
				} else {
					return obj;
				}
			},


			checkIsMobile: function (viewportWidth) {
				var widget = this;

				if (viewportWidth > 991) {
					widget.isMobile(false);
				} else if (viewportWidth <= 991) {
					widget.isMobile(true);
				}
			},
			mobileImageZoomEvents: function () {
				$("#prodDetails-mobileCarousel").on("click", ".item.active", function () {
					var imgUrl = $(this).find("img").attr("src");
					$("#mobileImageZoomedImg").attr("src", imgUrl.replace(/512/g, "712"));
					$("#imageMobileZoomModal").modal("show");
				});
			},
			//TRV - 2162 - S
			setProductData: function (value) {
				_this.g_productData = value;
			},
			getProductData: function () {
				return _this.g_productData;
			},
			setSelectedSKU: function (value) {
				_this.g_selectedSKUData = value;
			},
			getSelectedSKU: function () {
				return _this.g_selectedSKUData;
			},
			setSelectedSKUQty: function (value) {
				_this.g_selectedSKUQty = value;
			},
			getSelectedSKUQty: function () {
				return _this.g_selectedSKUQty;
			},
			timeStringToFloat: function (p_time) {
				var hoursMinutes = p_time.split(/[.:]/);
				var hours = parseInt(hoursMinutes[0], 10);
				var minutes = hoursMinutes[1] ? parseInt(hoursMinutes[1], 10) : 0;
				return hours + minutes / 60;
			},
			getServerTime: function () {
				var xmlHttp;
				try {
					//FF, Opera, Safari, Chrome
					xmlHttp = new XMLHttpRequest();
				} catch (err1) {
					//IE
					try {
						xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
					} catch (err2) {
						try {
							xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
						} catch (eerr3) {
							//AJAX not supported, use CPU time.
						}
					}
				}
				var url = "https://"+window.location.hostname+"/storelocator/stores.json";
				xmlHttp.open('HEAD', window.location.href.toString(), false);
				xmlHttp.setRequestHeader("Content-Type", "text/html");
				xmlHttp.send('');
				return xmlHttp.getResponseHeader("Date");

			},
			/**
			 * Destroy the 'loading' spinner.
			 * @function  OrderViewModel.destroySpinner
			 */
			destroySpinner: function () {
				$('#loadingModal').hide();
				spinner.destroy();
			},

			/**
			 * Create the 'loading' spinner.
			 * @function  OrderViewModel.createSpinner
			 */
			createSpinner: function (loadingText) {
				var indicatorOptions = {
					parent: '#loadingModal',
					posTop: '0',
					posLeft: '50%'
				};
				var loadingText = CCi18n.t('ns.common:resources.loadingText');
				$('#loadingModal').removeClass('hide');
				$('#loadingModal').show();
				indicatorOptions.loadingText = loadingText;
				spinner.create(indicatorOptions);
			},

			createInventorySpinner: function (loadingText) {
				if ($("#loadingInventoryModal").length == 0) {
					var createInventoryLoader = '<div id="loadingInventoryModal" class="loadingInventoryIndicator"><div id="cc-spinner" class="cc-spinner"><div class="cc-spinner-css" style="top:0;left:38%"></div></div></div>';
					$("#tr-w-product-details .tr-w-product-left-column").prepend(createInventoryLoader);
					$(".cc-spinner-css").html("");
					$(".cc-spinner-css").append('<span class="ie-show">Loading...</span>');
					for (var i = 1; i <= 12; i++) {
						$(".cc-spinner-css").append('<div class="cc-spinner-css-' + i + '"></div>')
					}
				}

			},

			destroyInventorySpinner: function () {
				$('#loadingInventoryModal').remove();

			},

			addPersonalizationToCart: function () {
				var productIds = [];
				var productIdsList = {};
				var zoneCookie = _this.getCookie("zoneProduct");
				var zoneData = {};
				if (zoneCookie != '') {
					zoneData = $.parseJSON(zoneCookie);
					_this.setCookie("zoneProduct", "");
				} else {
					productIds = ['194071', '574015', '336476', '607652', '574015', '587031'];
				}
				for (var key in zoneData) {
					var data = $.parseJSON(zoneData[key]);

					for (var key1 in data) {
						var data1 = data[key1];
						if ($.isArray(data1)) {
							productIds.concat(data1);
						} else {
							productIds.push(data1);
						}
					}
				}

				//productIdsList[CCConstants.PRODUCT_IDS] = productIds;

				var delay = 0;
				for (var i in productIds) {
					var productId = productIds[i];
					setTimeout(_this.addPersonalizationItemToCart.bind(null, productId, i), delay);
					delay = delay + 1000;
				}
			},

			setCookie: function (cname, cvalue) {
				document.cookie = cname + "=" + cvalue;
			},

			addPersonalizationItemToCart: function (productId, index) {
				var a = {};
				ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_GET_PRODUCT, a, function (product) {
					//var product = products[i];

					var quantity = 1;
					var t = [];
					var result = {};
					var skuItem = product.childSKUs[0];
					for (var variant in product.productVariantOptions) {
						t.push({
							optionName: product.productVariantOptions[variant].optionName,
							optionValue: skuItem[product.productVariantOptions[variant].optionName],
							optionId: product.productVariantOptions[variant].optionId,
							optionValueId: skuItem.dynamicPropertyMapLong[product.productVariantOptions[variant].mapKeyPropertyAttribute]
						});
					}

					result = {
						selectedOptions: t
					};


					var s = $.extend(!0, {}, product, result);
					s.childSKUs = [skuItem], s.orderQuantity = parseInt(quantity, 10);
					$.Topic(pubsub.topicNames.CART_ADD).publishWith(s, [{
						message: "success"
					}]);
				}, function (output) {}, productId);
			},
			getCookie: function (cname) {
				var name = cname + "=";
				var ca = document.cookie.split(';');
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == ' ') {
						c = c.substring(1);
					}
					if (c.indexOf(name) == 0) {
						return c.substring(name.length, c.length);
					}
				}
				return "";
			},


		};

	}
);
