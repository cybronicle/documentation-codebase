/**
			 * Fetch Facebook app id
			 */
			fetchFacebookAppId: function () {
				var widget = this;
				var serverType = CCConstants.EXTERNALDATA_PRODUCTION_FACEBOOK;
				if (widget.isPreview()) {
					serverType = CCConstants.EXTERNALDATA_PREVIEW_FACEBOOK;
				}
				ccRestClient.request(CCConstants.ENDPOINT_MERCHANT_GET_EXTERNALDATA,
					null, widget.fetchFacebookAppIdSuccessHandler.bind(widget),
					widget.fetchFacebookAppIdErrorHandler.bind(widget),
					serverType);
			},

			/**
			 * Fetch Facebook app id successHandler, update local and global scope data
			 */
			fetchFacebookAppIdSuccessHandler: function (pResult) {
				var widget = this;
				widget.siteFbAppId(pResult.serviceData.applicationId);

				//if (widget.siteFbAppId()) {
				//  facebookSDK.init(widget.siteFbAppId());
				//}
			},

			/**
			 * Fetch Facebook app id error handler
			 */
			fetchFacebookAppIdErrorHandler: function (pResult) {
				//logger.debug("Failed to get Facebook appId.", result);
			},