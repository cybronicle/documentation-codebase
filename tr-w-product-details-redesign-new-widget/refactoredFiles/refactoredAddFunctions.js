function fillVariantOptions(widget){
    var variantOptions = this.variantOptionsArray();
    notifier.clearSuccess(this.WIDGET_ID);

    //get the selected options, if all the options are selected.
    var selectedOptions = this.getSelectedSkuOptions(variantOptions);
    var selectedOptionsObj = {
        'selectedOptions': selectedOptions
    };
    var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
    newProduct.desiredQuantity = this.itemQty();

    if (this.variantOptionsArray().length > 0) {
        //assign only the selected sku as child skus
        newProduct.childSKUs = [this.selectedSku()];
    }
    newProduct.productPrice = (newProduct.salePrice != null) ? newProduct.salePrice : newProduct.listPrice;
}

function publishSocialSpaceAdd(widget){
    $.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD).publishWith(newProduct, [{
        message: "success"
    }]);
}

function publishSocialSpaceSelectorAdd(widget){
    $.Topic(pubsub.topicNames.SOCIAL_SPACE_SELECTOR_ADD).publishWith(newProduct, [{
        message: "success"
    }]);
}

function publishSocialSpaceAddToSelectedSpace(widget, spaceId){
    $.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD_TO_SELECTED_SPACE).publishWith(newProduct, [spaceId]);
}