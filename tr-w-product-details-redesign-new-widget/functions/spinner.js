/**
			 * Destroy the 'loading' spinner.
			 * @function  OrderViewModel.destroySpinner
			 */
			destroySpinner: function () {
				$('#loadingModal').hide();
				spinner.destroy();
			},

			/**
			 * Create the 'loading' spinner.
			 * @function  OrderViewModel.createSpinner
			 */
			createSpinner: function (loadingText) {
				var indicatorOptions = {
					parent: '#loadingModal',
					posTop: '0',
					posLeft: '50%'
				};
				var loadingText = CCi18n.t('ns.common:resources.loadingText');
				$('#loadingModal').removeClass('hide');
				$('#loadingModal').show();
				indicatorOptions.loadingText = loadingText;
				spinner.create(indicatorOptions);
			},

			createInventorySpinner: function (loadingText) {
				if ($("#loadingInventoryModal").length == 0) {
					var createInventoryLoader = '<div id="loadingInventoryModal" class="loadingInventoryIndicator"><div id="cc-spinner" class="cc-spinner"><div class="cc-spinner-css" style="top:0;left:38%"></div></div></div>';
					$("#tr-w-product-details .tr-w-product-left-column").prepend(createInventoryLoader);
					$(".cc-spinner-css").html("");
					$(".cc-spinner-css").append('<span class="ie-show">Loading...</span>');
					for (var i = 1; i <= 12; i++) {
						$(".cc-spinner-css").append('<div class="cc-spinner-css-' + i + '"></div>')
					}
				}

			},

			destroyInventorySpinner: function () {
				$('#loadingInventoryModal').remove();

			},