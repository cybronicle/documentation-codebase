handleLoadEvents: function (eventName) {
				if (eventName.toUpperCase() === LOADING_EVENT) {
					spinner.create(productLoadingOptions);
					$('#cc-product-spinner').css('z-index', 1);
				} else if (eventName.toUpperCase() === LOADED_EVENT) {
					this.removeSpinner();
				}
			},
			

			handleCarouselArrows: function (data, event) {
				// Handle left key
				if (event.keyCode == 37) {
					$('#prodDetails-imgCarousel').carousel('prev');
				}
				// Handle right key
				if (event.keyCode == 39) {
					$('#prodDetails-imgCarousel').carousel('next');
				}
            },
            

            handlePersonalize: function () {
			    var widget = this;
				var productID = this.product().id() || "unknown";
				dataLayer.push({
					'event': 'personalizeStart',
					'productId': productID
				});

				$("#inventory-error").text("");
				if (!this.handlePersonalizeButtonValidation() || this.prodAvlFlag() === 0) {
					return;
				}

				var widget = this,
					productData = widget.skuData();


				var variantOptions = this.variantOptionsArray(),
					sku_obj = this.getSelectedSku(variantOptions);

				if (this.product().childSKUs().length == 1) {
					sku_obj = this.product().childSKUs()[0];
				}
			
				$("#inventory-error").text("");
				goToPersonalize.call(widget);

				function goToPersonalize() {

					var all_delivery_details = JSON.parse(storageApi.getInstance().getItem("alldeliveryDetails")) || {},
						curr_flag = 0;

					var delivery_details = {};
				
					
					var tempStoreDetails = {};
					var tempaddress = {};
					tempStoreDetails.skuid = this.skuRepoId();
					tempStoreDetails.deliveryType = $("input[name='optradio']:checked").val();
					if ($("input[name='optradio']:checked").val() == 2) { /**** Ship to Home */
						if (inventoryDataIR.hasOwnProperty('shipping') &&  inventoryDataIR.shipping[0].store.id !== "920") { /****Ship from Store */
							tempStoreDetails.store_details = inventoryDataIR.shipping[0].store.id + "~~" + inventoryDataIR.shipping[0].store.name + "~~" + inventoryDataIR.shipping[0].store.street + "~~" + inventoryDataIR.shipping[0].store.deliveryDate + "~~" + inventoryDataIR.shipping[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.shipping[0].store.streetExt;

							tempaddress.storeName = inventoryDataIR.shipping[0].store.name;
							tempaddress.storeNumber = inventoryDataIR.shipping[0].store.id;
							tempaddress.address = inventoryDataIR.shipping[0].store.street;
							tempaddress.address1 = inventoryDataIR.shipping[0].store.street;
							tempaddress.address2 = inventoryDataIR.shipping[0].store.streetExt;
							tempaddress.city = inventoryDataIR.shipping[0].store.city;
							tempaddress.state = inventoryDataIR.shipping[0].store.state;
							tempaddress.zipCode = inventoryDataIR.shipping[0].store.postalCode;
							tempaddress.phone = inventoryDataIR.shipping[0].store.phone;
							tempaddress.deliveryDate = inventoryDataIR.shipping[0].store.deliveryDate;
							tempaddress.isSTS = this.displaySts();
							tempaddress.closeTime = inventoryDataIR.shipping[0].store.closeTime;
							tempaddress.onHandQuantity = inventoryDataIR.shipping[0].store.onHandQuantity;
							tempaddress.addToCartDate = new Date();
							tempaddress.stsDate = "";
							if (inventoryDataIR.shipping[0].store.hours.daysOfWeek && inventoryDataIR.shipping[0].store.hours.daysOfWeek != "") {
								tempaddress.stsDate = inventoryDataIR.shipping[0].store.hours.daysOfWeek[5].date;
							} else {
								tempaddress.stsDate = "";
							}

							tempStoreDetails.store_address = tempaddress;
							tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
							tempStoreDetails.userSelectedStore = true;
							if (inventoryDataIR.pickup[0].store) {
								tempStoreDetails.pickupAvailable = inventoryDataIR.pickup[0].available;
							} else {
								tempStoreDetails.pickupAvailable = false;
							}
						} else {
							if (inventoryDataIR.hasOwnProperty('pickup') && inventoryDataIR.pickup[0].store) {
								tempStoreDetails.store_details = inventoryDataIR.pickup[0].store.id + "~~" + inventoryDataIR.pickup[0].store.name + "~~" + inventoryDataIR.pickup[0].store.street + "~~" + inventoryDataIR.pickup[0].store.deliveryDate + "~~" + inventoryDataIR.pickup[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.pickup[0].store.streetExt;

								tempaddress.storeName = inventoryDataIR.pickup[0].store.name;
								tempaddress.storeNumber = inventoryDataIR.pickup[0].store.id;
								tempaddress.address = inventoryDataIR.pickup[0].store.street;
								tempaddress.address1 = inventoryDataIR.pickup[0].store.street;
								tempaddress.address2 = inventoryDataIR.pickup[0].store.streetExt;
								tempaddress.city = inventoryDataIR.pickup[0].store.city;
								tempaddress.state = inventoryDataIR.pickup[0].store.state;
								tempaddress.zipCode = inventoryDataIR.pickup[0].store.postalCode;
								tempaddress.phone = inventoryDataIR.pickup[0].store.phone;
								tempaddress.deliveryDate = inventoryDataIR.pickup[0].store.deliveryDate;
								tempaddress.isSTS = this.displaySts();
								tempaddress.closeTime = inventoryDataIR.pickup[0].store.closeTime;
								tempaddress.onHandQuantity = inventoryDataIR.pickup[0].store.onHandQuantity;
								tempaddress.addToCartDate = new Date();
								tempaddress.stsDate = "";
								if (inventoryDataIR.pickup[0].store.hours.daysOfWeek && inventoryDataIR.pickup[0].store.hours.daysOfWeek != "") {
									tempaddress.stsDate = inventoryDataIR.pickup[0].store.hours.daysOfWeek[5].date;
								} else {
									tempaddress.stsDate = "";
								}

								tempStoreDetails.store_address = tempaddress;
								tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
								tempStoreDetails.userSelectedStore = true;
								tempStoreDetails.pickupAvailable = inventoryDataIR.pickup[0].available;
							} else {
								tempStoreDetails.store_details = "";
								tempStoreDetails.store_address = {};
								if(inventoryDataIR.hasOwnProperty('shipping')){
								    tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
								}
								tempStoreDetails.userSelectedStore = true;
								tempStoreDetails.pickupAvailable = false;
							}
						}
					} else { /**** Pick up */
						if (inventoryDataIR.hasOwnProperty('pickup') && inventoryDataIR.pickup[0].store) {
							tempStoreDetails.store_details = inventoryDataIR.pickup[0].store.id + "~~" + inventoryDataIR.pickup[0].store.name + "~~" + inventoryDataIR.pickup[0].store.street + "~~" + inventoryDataIR.pickup[0].store.deliveryDate + "~~" + inventoryDataIR.pickup[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.pickup[0].store.streetExt;

							tempaddress.storeName = inventoryDataIR.pickup[0].store.name;
							tempaddress.storeNumber = inventoryDataIR.pickup[0].store.id;
							tempaddress.address = inventoryDataIR.pickup[0].store.street;
							tempaddress.address1 = inventoryDataIR.pickup[0].store.street;
							tempaddress.address2 = inventoryDataIR.pickup[0].store.streetExt;
							tempaddress.city = inventoryDataIR.pickup[0].store.city;
							tempaddress.state = inventoryDataIR.pickup[0].store.state;
							tempaddress.zipCode = inventoryDataIR.pickup[0].store.postalCode;
							tempaddress.phone = inventoryDataIR.pickup[0].store.phone;
							tempaddress.deliveryDate = inventoryDataIR.pickup[0].store.deliveryDate;
							tempaddress.isSTS = this.displaySts();
							tempaddress.closeTime = inventoryDataIR.pickup[0].store.closeTime;
							tempaddress.onHandQuantity = inventoryDataIR.pickup[0].store.onHandQuantity;
							tempaddress.addToCartDate = new Date();
							tempaddress.stsDate = "";
							if (inventoryDataIR.pickup[0].store.hours.daysOfWeek && inventoryDataIR.pickup[0].store.hours.daysOfWeek != "") {
								tempaddress.stsDate = inventoryDataIR.pickup[0].store.hours.daysOfWeek[5].date;
							} else {
								tempaddress.stsDate = "";
							}

							tempStoreDetails.store_address = tempaddress;
							tempStoreDetails.store_inventory = inventoryDataIR.pickup[0].store.id;
							tempStoreDetails.userSelectedStore = true;
							if (this.isPhotoUpload()) {
								tempStoreDetails.pickupAvailable = false;
							} else {
								tempStoreDetails.pickupAvailable = true;
							}
						}
					}
					delivery_details = tempStoreDetails;
					for (var key in all_delivery_details) {
						if (all_delivery_details.hasOwnProperty(key)) {
							if (key == this.skuRepoId()) {
								curr_flag = 1;
								all_delivery_details[key] = delivery_details;
							}
						}
					}

					if (curr_flag === 0) {
						all_delivery_details[this.skuRepoId()] = delivery_details;
					}

					var occasion_obj = JSON.parse(storageApi.getInstance().getItem("occasion") || "{}"),
						longDescr_obj = JSON.parse(storageApi.getInstance().getItem("prodDescr") || "{}");

					occasion_obj[this.skuRepoId()] = this.occasionCode();
					longDescr_obj[this.skuRepoId()] = $(".js-product-header-descr").text();


					//start
					var store_obj = JSON.parse(storageApi.getInstance().getItem("storeId") || "{}");
					store_obj[this.skuRepoId()] = this.storeCode();
					//localStorage.setItem('skuID',this.skuRepoId());


					storageApi.getInstance().setItem('deliveryDetails', JSON.stringify(delivery_details));
					storageApi.getInstance().setItem('alldeliveryDetails', JSON.stringify(all_delivery_details));
					storageApi.getInstance().setItem('occasion', JSON.stringify(occasion_obj));
					storageApi.getInstance().setItem('prodDescr', JSON.stringify(longDescr_obj));

					//start
					storageApi.getInstance().setItem('storeId', JSON.stringify(store_obj));


					//$('html,body').animate({scrollTop: 0}, 600);
					// $('#personalization-zone-container').empty();
					var variantOptions = this.variantOptionsArray();
					notifier.clearSuccess(this.WIDGET_ID);
					//get the selected options, if all the options are selected.
					var selectedOptions = this.getSelectedSkuOptions(variantOptions);

					var selectedOptionsObj = {
						'selectedOptions': selectedOptions
					};
					if (this.hasGift() && this.giftData()) {
						$.Topic('PERSONALIZE_GIFT_ITEM').publish(this.giftData());
					} else {
						$.Topic('PERSONALIZE_GIFT_ITEM').publish(false);
					}
					location.hash = "personalize";
					personalizeClicked = true;
					$.Topic("URL_HASH_CHANGE.memory").publish();
					var requestQuantity = parseInt($('#select-quantity').val());
					
					if(widget.selectedBomArray().length > 0 ){
					    $.each(widget.selectedBomArray(), function(k,v){
					        delete v['imgurl'];
					        delete v['isInStock'];
					        widget.selectedBomArray()[k] = v;
					    });
					}
					
					var viewModel = ko.mapping.fromJS(newPersonalizationData);
					$.Topic('TR_PDP_NEW_PERSONALIZATION_DATA').publish(viewModel,requestQuantity,widget.selectedBomArray());
					
					var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
					if (this.variantOptionsArray().length > 0) {
						//assign only the selected sku as child skus
						newProduct.childSKUs = [this.selectedSku()];
					}
					newProduct.orderQuantity = parseInt(this.itemQty(), 10);
					var l_windowWidth = $(window).width();
					if (l_windowWidth <= 767) {
						var pdpTitleInfo = $('.tr-personlization-pdp-header-info');
						if (selectedOptions[0] && selectedOptions[0].optionName === "Color") {
							var colorVal = selectedOptions[0].optionValue.split('|')[0];
							pdpTitleInfo.find('#tr-persona-color-xs').text(colorVal);
						}
						if (selectedOptions[1] && selectedOptions[1].optionName === "Size") {
							var sizeVal = selectedOptions[1].optionValue.split('|')[0];
							pdpTitleInfo.find('#tr-persona-size-xs').text(sizeVal);
						}
						pdpTitleInfo.css({
							'display': 'block'
						});
						$('.tr-personlization-pdp-header-section').append(pdpTitleInfo);
					}

					$.Topic("SENT_TO_PERSONALIZE").publish({
						qty: parseInt(this.itemQty(), 10),
						opusPhotoUpload: this.isPhotoUpload
					});
					$.Topic('backOrderFlag').publish(this.backOrderFlag());
					// To disable Add to cart button for three seconds when it is clicked and enabling again
					this.isAddToCartClicked(true);
					var self = this;
					setTimeout(enableAddToCartButton, 3000);

					function enableAddToCartButton() {
						self.isAddToCartClicked(false);
					};

				}
            },
            //TRV - TROCC-1917 - E
			handlePersonalizeButtonValidation: function () {
				personalizeClicked = false;
				var flag = 1,
					avl_flag = 0,
					variantOptions = this.variantOptionsArray();

				if ($(".occasion-btn:visible").length > 0) {
					var occasion_index = $(".occasion-btn").find("span.index").text();

					if (occasion_index == "0") {
						$(".tr-w-occasion").find(".error-msg").show();
						flag = 0;
					}
				}
				if (!$("input[name='optradio']:checked").val()) {
					$(".tr-e-delivery").find(".error-msg").show();
					flag = 0;
				}

				if (this.bomArray().length > this.urlObjArr.length) {
					$(".tr-e-bom").find(".main-error").text("Please select all applicable options").show();
					flag = 0;
				}

				if (!this.allOptionsSelectedF()) {
					$(".tr-e-product-variation").find(".error-msg").text("Please select the applicable variants").show();
					$(".tr-e-product-variation").find(".error-msg").attr("data-val", 1)
					flag = 0;
				} else if (!this.allOptionsSelected()) {
					$(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
					$(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);
					$(".personalizationBtns").addClass('hide');
                    $('.tr-xs-personalize-block').addClass('hide');

					flag = 0;

					if (variantOptions.length === 1) {
						$(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
						$(".personalizationBtns").addClass('hide');
                        $('.tr-xs-personalize-block').addClass('hide');
					}
				}
				if (this.allOptionsSelected()) {
					avl_flag = 1;
				}

				if (flag && this.skuRepoId()) {
					$('.personalization-main-container').show();
				}
				if (!flag) {
					if ($('.error-msg:visible:first').offset()) {
						var position = $('.error-msg:visible:first').offset().top;
						window.scrollTo(0, position);
					}
				}
				return flag;

            },
            
            // Sends a message to the cart to add this product
			handlePersonalizedPdtAddToCart: function (selectedSkuPersonalizationData, personalizationCostSummary, shippingOptions) {
				if (!_this.handlePersonalizeButtonValidation()) {
					return;
				}


				var variantOptions = this.variantOptionsArray();
				notifier.clearSuccess(this.WIDGET_ID);
				//get the selected options, if all the options are selected.
				var selectedOptions = _this.getSelectedSkuOptions(variantOptions);


				if ($.isArray(_this.getSelectedSkuOptions(variantOptions)) && _this.getSelectedSkuOptions(variantOptions).length > 0) {
					selectedOptions.push({
						optionId: "product_variants",
						optionName: "Product Variants",
						optionValue: JSON.stringify(_this.getSelectedSkuOptions(variantOptions)),
						// optionValue:"test_this",
						optionValueId: Math.floor((Math.random() * 10000) + 1)
					});

				}


				var personalizationSkus = selectedSkuPersonalizationData();

				var selectedSku;
				var selectedSkuPrice;
				if (_this.getSelectedSKU()) {
					if (typeof _this.getSelectedSKU()['listPrice'] === "function") {
						selectedSkuPrice = (_this.getSelectedSKU()['salePrice']() != null) ? _this.getSelectedSKU()['salePrice']() : _this.getSelectedSKU()['listPrice']();
					} else {
						selectedSkuPrice = (_this.getSelectedSKU()['salePrice'] != null) ? _this.getSelectedSKU()['salePrice'] : _this.getSelectedSKU()['listPrice'];
					}

					if (typeof _this.getSelectedSKU()['repositoryId'] === "function") {
						selectedSku = _this.getSelectedSKU()['repositoryId']();
					} else {
						selectedSku = _this.getSelectedSKU()['repositoryId'];
					}
				}

				if (!selectedSkuPrice) {
					selectedSkuPrice = (this.product().product.salePrice != null) ? this.product().product.salePrice : this.product().product.listPrice;
				}

				personalizationSkus.originalSku = {
					sku: (selectedSku || this.product().id()),
					price: selectedSkuPrice
				};

				personalizationSkus.photoImageUrls = ["https://thingsremembered.scene7.com/is/image/ThingsRemembered/000751016?wid=" + imageRes];
				if (this.occasionCode()) {
					personalizationSkus.occasionCode = this.occasionCode().trim();
				} else {
					personalizationSkus.occasionCode = '';
				}


				selectedOptions.push({

					optionId: "personalization_skus",
					optionName: "Personalization SKUS",
					optionValue: JSON.stringify(personalizationSkus),
					// optionValue:"test_this",
					optionValueId: Math.floor((Math.random() * 10000) + 1)

				});
				// var item_wise_data = _this.processPersonalizationCost(selectedSkuPersonalizationData());
				var item_wise_data = this.personaliztionObj;
				selectedOptions.push({

					optionId: "personalization",
					optionName: "Personalization",
					optionValue: JSON.stringify(item_wise_data),
					// optionValue:"test_this",
					optionValueId: Math.floor((Math.random() * 10000) + 1)

				});
				selectedOptions.push({

					optionId: "personalization_summary",
					optionName: "Personalization Summary",
					optionValue: JSON.stringify(personalizationCostSummary()),
					optionValueId: Math.floor((Math.random() * 10000) + 1)

				});
				selectedOptions.push({
					optionId: "shipping_options",
					optionName: "Shipping Options",
					optionValue: JSON.stringify(shippingOptions),
					optionValueId: Math.floor((Math.random() * 10000) + 1)

				});
				//Adding Delivery Dates to selected Options
				var dateObject = {};
				dateObject.AIR = getDate(this.airFlagDate());
				dateObject.BWY = getDate(this.bwyFlagDate());
				dateObject.TND = getDate(this.two_ndFlagDate());

				function getDate(date) {
					var tempMonth = date.substring(3, 5);
					var tempDate = date.substring(6, 8);
					var tempDay = date.substring(13);
					var tempYear = date.split('|')[1];
					if (!tempYear) {
						var date = new Date();
						tempYear = date.getFullYear();
					}
					return tempYear + tempMonth + tempDate;
				}

				selectedOptions.push({
					optionId: "product_delivery_dates",
					optionName: "Product Delivery Dates",
					optionValue: JSON.stringify(dateObject),
					optionValueId: Math.floor((Math.random() * 10000) + 1)
				});
				selectedOptions.push({
					optionId: "ItemProductDescription",
					optionName: "Product Description",
					optionValue: this.product().displayName(),
					optionValueId: Math.floor((Math.random() * 10000) + 1)
				});
				if (this.giftDisplayName()) {
					selectedOptions.push({
						optionId: "giftItemName",
						optionName: "Gift Item Name",
						optionValue: this.giftDisplayName(),
						optionValueId: Math.floor((Math.random() * 10000) + 1)
					});
				}
				var selectedOptionsObj = {
					'selectedOptions': selectedOptions
				};

				function enableAddToCartButton() {
					self.isAddToCartClicked(false);
				};

				_this.isAddToCartClicked(true);
				var self = _this;
				setTimeout(enableAddToCartButton, 3000);
				var k = currentPersonalizationData.length;

				var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
				if (this.variantOptionsArray().length > 0) {
					//assign only the selected sku as child skus
					newProduct.childSKUs = [this.selectedSku()];
				}
				setDetailsToItem = true;
				//getWidget.cart().addItem(newProduct);
				currentProduct = newProduct;

				if (k === 1) {
					processingTime = 0;
					newProduct.orderQuantity = parseInt(this.itemQty(), 10);
					$.Topic(pubsub.topicNames.CART_ADD).publishWith(
						newProduct, [{
							message: "success"
						}]);
				} else {
					processingTime = 250;
					newProduct.orderQuantity = parseInt(1, 10);
					getWidget.cart().addItem(newProduct);
				}

            },
            
            // Sends a message to the cart to add this product
			handleAddToCart: function () {
				var self = this,
					personalizationUtils = helpers.personalizationUtils();

				if (!this.handlePersonalizeButtonValidation()) {
					return;
				}
				var all_delivery_details = JSON.parse(storageApi.getInstance().getItem("alldeliveryDetails")) || {},
					curr_flag = 0;

				var delivery_details = {};
				/*delivery_details["skuid"] = this.skuRepoId()
				delivery_details["deliveryType"] = $("input[name='optradio']:checked").val();
				var deliveryDate = '';
				if ((this.myStore().length > 0)) {
				    deliveryDate = this.myStore()[0].deliveryDate;
				}
				if (this.myStore().length > 0 && this.displaySts() && this.myStore()[0].storeHours && this.myStore()[0].storeHours != "") {
				    deliveryDate = this.myStore()[0].storeHours[5].date;
				}
				if (this.myStore().length > 0) {
				    if (this.isPhotoUpload()) {
				        delivery_details['pickupAvailable'] = false;
				    } else {
				        delivery_details['pickupAvailable'] = true;
				    }
				    delivery_details["store_details"] = this.myStore()[0]["storeNumber"] + "~~" + this.myStore()[0]["storeName"] + "~~" + this.myStore()[0].address1 + "~~" + deliveryDate + "~~" + this.myStore()[0].formattedDeliveryDate + "~~" + this.myStore()[0].address2;
				    var address = {};
				    address["storeName"] = this.myStore()[0].storeName;
				    address["storeNumber"] = this.myStore()[0].storeNumber;
				    address["address1"] = this.myStore()[0].address1 || '';
				    address["address"] = this.myStore()[0].address || '';
				    address["address2"] = this.myStore()[0].address2 || '';
				    address["city"] = this.myStore()[0].city;
				    address["state"] = this.myStore()[0].state;
				    address["zipCode"] = this.myStore()[0].zipCode;
				    address["phone"] = "";
				    address["deliveryDate"] = this.myStore()[0].deliveryDate;
				    address["isSTS"] = this.displaySts();
				    address["closeTime"] = this.myStore()[0].closeTime;
				    address["onHandQuantity"] = this.myStore()[0].onHandQuantity;
				    address["addToCartDate"] = new Date();
				    if (this.myStore()[0].storeHours && this.myStore()[0].storeHours != "") {
				        address["stsDate"] = this.myStore()[0].storeHours[5].date;
				    } else {
				        address["stsDate"] = "";
				    }
				    if ($("input[name='optradio']:checked").val() == 1) {
				        if (this.displaySts()) {
				            delivery_details["store_inventory"] = 920;
				        }
				        delivery_details["userSelectedStore"] = true;
				    } else {
				        delivery_details["userSelectedStore"] = false;
				        if (this.isSFS()) {
				            delivery_details["store_inventory"] = this.sfsStoreId();
				            ;
				        } else {
				            delivery_details["store_inventory"] = 920;
				        }
				    }
				    delivery_details["store_address"] = address;
				} else {
				    delivery_details["userSelectedStore"] = false;
				    delivery_details['pickupAvailable'] = false;
				    delivery_details["store_details"] = "";
				    if (this.isSFS()) {
				        delivery_details["store_inventory"] = this.sfsStoreId();
				        ;
				    } else {
				        delivery_details["store_inventory"] = 920;
				    }
				}*/
				
				var tempStoreDetails = {};
				var tempaddress = {};
				tempStoreDetails.skuid = this.skuRepoId()
				tempStoreDetails.deliveryType = $("input[name='optradio']:checked").val();
				if ($("input[name='optradio']:checked").val() == 2) { /**** Ship to Home */
					if (inventoryDataIR.shipping[0].store.id !== "920") { /****Ship from Store */
						tempStoreDetails.store_details = inventoryDataIR.shipping[0].store.id + "~~" + inventoryDataIR.shipping[0].store.name + "~~" + inventoryDataIR.shipping[0].store.street + "~~" + inventoryDataIR.shipping[0].store.deliveryDate + "~~" + inventoryDataIR.shipping[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.shipping[0].store.streetExt;

						tempaddress.storeName = inventoryDataIR.shipping[0].store.name;
						tempaddress.storeNumber = inventoryDataIR.shipping[0].store.id;
						tempaddress.address = inventoryDataIR.shipping[0].store.street;
						tempaddress.address1 = inventoryDataIR.shipping[0].store.street;
						tempaddress.address2 = inventoryDataIR.shipping[0].store.streetExt;
						tempaddress.city = inventoryDataIR.shipping[0].store.city;
						tempaddress.state = inventoryDataIR.shipping[0].store.state;
						tempaddress.zipCode = inventoryDataIR.shipping[0].store.postalCode;
						tempaddress.phone = inventoryDataIR.shipping[0].store.phone;
						tempaddress.deliveryDate = inventoryDataIR.shipping[0].store.deliveryDate;
						tempaddress.isSTS = this.displaySts();
						tempaddress.closeTime = inventoryDataIR.shipping[0].store.closeTime;
						tempaddress.onHandQuantity = inventoryDataIR.shipping[0].store.onHandQuantity;
						tempaddress.addToCartDate = new Date();
						tempaddress.stsDate = "";
						if (inventoryDataIR.shipping[0].store.hours.daysOfWeek && inventoryDataIR.shipping[0].store.hours.daysOfWeek != "") {
							tempaddress.stsDate = inventoryDataIR.shipping[0].store.hours.daysOfWeek[5].date;
						} else {
							tempaddress.stsDate = "";
						}

						tempStoreDetails.store_address = tempaddress;
						tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
						tempStoreDetails.userSelectedStore = true;
						if (inventoryDataIR.pickup[0].store) {
							tempStoreDetails.pickupAvailable = inventoryDataIR.pickup[0].available;
						} else {
							tempStoreDetails.pickupAvailable = false;
						}
					} else {
						if (inventoryDataIR.pickup[0].store) {
							tempStoreDetails.store_details = inventoryDataIR.pickup[0].store.id + "~~" + inventoryDataIR.pickup[0].store.name + "~~" + inventoryDataIR.pickup[0].store.street + "~~" + inventoryDataIR.pickup[0].store.deliveryDate + "~~" + inventoryDataIR.pickup[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.pickup[0].store.streetExt;

							tempaddress.storeName = inventoryDataIR.pickup[0].store.name;
							tempaddress.storeNumber = inventoryDataIR.pickup[0].store.id;
							tempaddress.address = inventoryDataIR.pickup[0].store.street;
							tempaddress.address1 = inventoryDataIR.pickup[0].store.street;
							tempaddress.address2 = inventoryDataIR.pickup[0].store.streetExt;
							tempaddress.city = inventoryDataIR.pickup[0].store.city;
							tempaddress.state = inventoryDataIR.pickup[0].store.state;
							tempaddress.zipCode = inventoryDataIR.pickup[0].store.postalCode;
							tempaddress.phone = inventoryDataIR.pickup[0].store.phone;
							tempaddress.deliveryDate = inventoryDataIR.pickup[0].store.deliveryDate;
							tempaddress.isSTS = this.displaySts();
							tempaddress.closeTime = inventoryDataIR.pickup[0].store.closeTime;
							tempaddress.onHandQuantity = inventoryDataIR.pickup[0].store.onHandQuantity;
							tempaddress.addToCartDate = new Date();
							tempaddress.stsDate = "";
							if (inventoryDataIR.pickup[0].store.hours.daysOfWeek && inventoryDataIR.pickup[0].store.hours.daysOfWeek != "") {
								tempaddress.stsDate = inventoryDataIR.pickup[0].store.hours.daysOfWeek[5].date;
							} else {
								tempaddress.stsDate = "";
							}

							tempStoreDetails.store_address = tempaddress;
							tempStoreDetails.store_inventory = inventoryDataIR.pickup[0].store.id;
							tempStoreDetails.userSelectedStore = true;
							tempStoreDetails.pickupAvailable = inventoryDataIR.pickup[0].available;
						} else {
							tempStoreDetails.store_details = "";
							tempStoreDetails.store_address = {};
							tempStoreDetails.store_inventory = inventoryDataIR.shipping[0].store.id;
							tempStoreDetails.userSelectedStore = true;
							tempStoreDetails.pickupAvailable = false;
						}
					}
				} else { /**** Pick up */
					if (inventoryDataIR.pickup[0].store) {
						tempStoreDetails.store_details = inventoryDataIR.pickup[0].store.id + "~~" + inventoryDataIR.pickup[0].store.name + "~~" + inventoryDataIR.pickup[0].store.street + "~~" + inventoryDataIR.pickup[0].store.deliveryDate + "~~" + inventoryDataIR.pickup[0].store.formattedDeliveryDate + "~~" + inventoryDataIR.pickup[0].store.streetExt;

						tempaddress.storeName = inventoryDataIR.pickup[0].store.name;
						tempaddress.storeNumber = inventoryDataIR.pickup[0].store.id;
						tempaddress.address = inventoryDataIR.pickup[0].store.street;
						tempaddress.address1 = inventoryDataIR.pickup[0].store.street;
						tempaddress.address2 = inventoryDataIR.pickup[0].store.streetExt;
						tempaddress.city = inventoryDataIR.pickup[0].store.city;
						tempaddress.state = inventoryDataIR.pickup[0].store.state;
						tempaddress.zipCode = inventoryDataIR.pickup[0].store.postalCode;
						tempaddress.phone = inventoryDataIR.pickup[0].store.phone;
						tempaddress.deliveryDate = inventoryDataIR.pickup[0].store.deliveryDate;
						tempaddress.isSTS = this.displaySts();
						tempaddress.closeTime = inventoryDataIR.pickup[0].store.closeTime;
						tempaddress.onHandQuantity = inventoryDataIR.pickup[0].store.onHandQuantity;
						tempaddress.addToCartDate = new Date();
						tempaddress.stsDate = "";
						if (inventoryDataIR.pickup[0].store.hours.daysOfWeek && inventoryDataIR.pickup[0].store.hours.daysOfWeek != "") {
							tempaddress.stsDate = inventoryDataIR.pickup[0].store.hours.daysOfWeek[5].date;
						} else {
							tempaddress.stsDate = "";
						}

						tempStoreDetails.store_address = tempaddress;
						tempStoreDetails.store_inventory = inventoryDataIR.pickup[0].store.id;
						tempStoreDetails.userSelectedStore = true;
						if (this.isPhotoUpload()) {
							tempStoreDetails.pickupAvailable = false;
						} else {
							tempStoreDetails.pickupAvailable = true;
						}
					}
				}
				delivery_details = tempStoreDetails;
				for (var key in all_delivery_details) {
					if (all_delivery_details.hasOwnProperty(key)) {
						if (key == this.skuRepoId()) {
							curr_flag = 1;
							all_delivery_details[key] = delivery_details;
						}
					}
				}
				if (curr_flag === 0) {
					all_delivery_details[this.skuRepoId()] = delivery_details;
				}
				var occasion_obj = JSON.parse(storageApi.getInstance().getItem("occasion") || "{}"),
					longDescr_obj = JSON.parse(storageApi.getInstance().getItem("prodDescr") || "{}");
				occasion_obj[this.skuRepoId()] = this.occasionCode();
				longDescr_obj[this.skuRepoId()] = $(".js-product-header-descr").text();
				//start
				var store_obj = JSON.parse(storageApi.getInstance().getItem("storeId") || "{}");
				store_obj[this.skuRepoId()] = this.storeCode();
				//localStorage.setItem('skuID',this.skuRepoId());
				storageApi.getInstance().setItem('deliveryDetails', JSON.stringify(delivery_details));
				storageApi.getInstance().setItem('alldeliveryDetails', JSON.stringify(all_delivery_details));
				storageApi.getInstance().setItem('occasion', JSON.stringify(occasion_obj));
				storageApi.getInstance().setItem('prodDescr', JSON.stringify(longDescr_obj));
				//start
				storageApi.getInstance().setItem('storeId', JSON.stringify(store_obj));
				var variantOptions = this.variantOptionsArray();
				notifier.clearSuccess(this.WIDGET_ID);
				//get the selected options, if all the options are selected.
				var selectedOptions = this.getSelectedSkuOptions(variantOptions);

				var selectedOptionsObj = {
					'selectedOptions': selectedOptions
				};

				var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);

				if (this.variantOptionsArray().length > 0) {
					//assign only the selected sku as child skus
					newProduct.childSKUs = [this.selectedSku()];
				}
				newProduct.orderQuantity = parseInt(this.itemQty(), 10);
				currentProduct = newProduct;

				var productId = self.product().id(),
					bomDataToSave = self.selectedBomArray().map(function (item) {
						delete item["imgurl"];
						return item;
					}),
					deliveryData = storageApi.getInstance().getItem("deliveryDetails") || {};


				var hash = personalizationUtils.genHash(productId, null, bomDataToSave, null, deliveryData, new Date().getTime()),
					skuSTSFlag = $("#skuSTSFlag").val() === "true" ? true : false;

				var selectedProductVariants = self.getSelectedProductVariant(variantOptions);

				var pData = {
					id: productId,
					sku: self.skuRepoId(),
					quantity: parseInt(self.itemQty()),
					bom: bomDataToSave,
					productVariant: selectedProductVariants,
					deliveryDetails: deliveryData,
					skuSTS: skuSTSFlag
				};

				var currentPersonalizationDataObj = {};
				currentPersonalizationDataObj['hash'] = hash
				currentPersonalizationDataObj['data'] = pData;
				currentPersonalizationData.push(currentPersonalizationDataObj);

				setDetailsToItem = true;

				if (self.checkIfPersonalizationSkuPresent() === false) {
					self.cart().addItem(newProduct);
				} else {
					$.Topic(pubsub.topicNames.CART_ADD).publishWith(newProduct, [{
						message: "success"
					}]);


				}

				// To disable Add to cart button for three seconds when it is clicked and enabling again
				this.isAddToCartClicked(true);
				var self = this;
				setTimeout(enableAddToCartButton, 3000);

				function enableAddToCartButton() {
					self.isAddToCartClicked(false);
				};

            },
            

            handleCustomAddToCart: function(){
                var self = _this;
                function enableAddToCartButton() {
					self.isAddToCartClicked(false);
				};

				_this.isAddToCartClicked(true);
				setTimeout(enableAddToCartButton, 3000);
				var k = currentPersonalizationData.length;
                
                var variantOptions = this.variantOptionsArray();
				notifier.clearSuccess(this.WIDGET_ID);
				//get the selected options, if all the options are selected.
				var selectedOptions = this.getSelectedSkuOptions(variantOptions);

				var selectedOptionsObj = {
					'selectedOptions': selectedOptions
				};
				
				var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
				if (this.variantOptionsArray().length > 0) {
					//assign only the selected sku as child skus
					newProduct.childSKUs = [this.selectedSku()];
				}
				currentProduct = newProduct;
				
				setDetailsToItem = true;

				if (k === 1) {
					processingTime = 0;
					newProduct.orderQuantity = parseInt(this.itemQty(), 10);
					$.Topic(pubsub.topicNames.CART_ADD).publishWith(
						newProduct, [{
							message: "success"
						}]);
				} else {
					processingTime = 250;
					newProduct.orderQuantity = parseInt(1, 10);
					getWidget.cart().addItem(newProduct);
				}  
            },
            handleChangeQuantity: function (data, event) {
				var quantity = this.itemQty();

				if (quantity < 1) {} else if (quantity > this.stockAvailable()) {}

				return true;
			},
			handleBackOrderDate: function () {
				var fullDate;


				if (this.product().childSKUs().length == 1) {
					publishedSkuData = this.product();;
				}
				if (publishedSkuData.backorder) {
					this.deliveryDateHeadingFlag(false);
					fullDate = typeof publishedSkuData.backorderDeliveryDate === "function" ?
						publishedSkuData.backorderDeliveryDate() : publishedSkuData.backorderDeliveryDate;
					this.backOrderFlag(typeof publishedSkuData.backorder == "function" ? publishedSkuData.backorder() : publishedSkuData.backorder);
					this.backOrderMessage('Back order item');
					if (fullDate !== '' && fullDate !== null) {
						var index = fullDate.indexOf('-');
						var newDate = fullDate.substring(index + 1);
						newDate = newDate.replace('-', '/');
						newDate = "Estimated Arrival " + newDate;
						this.standardDeliveryDate(newDate);
					}

					if (publishedSkuData.BWYBackorderDeliveryDate) {
						this.bwyFlag(true);

						if (typeof publishedSkuData.BWYBackorderDeliveryDates == "function") {
							if (publishedSkuData.BWYBackorderDeliveryDates()) {
								this.bwyFlag(true);
							} else {
								this.bwyFlag(false);
							}
						}
						if (this.bwyFlag() == true) {
							this.bwyFlagDate(typeof publishedSkuData.BWYBackorderDeliveryDates == "function" ? publishedSkuData.BWYBackorderDeliveryDates() : publishedSkuData.BWYBackorderDeliveryDates);

						}
					} else {
						this.bwyFlag(false);
					}

					if (publishedSkuData.TwoNDBackorderDeliveryDates) {
						this.two_ndFlag(true);

						if (typeof publishedSkuData.TwoNDBackorderDeliveryDates == "function") {
							if (publishedSkuData.TwoNDBackorderDeliveryDates() !== null) {
								this.two_ndFlag(true);
							} else {
								this.two_ndFlag(false);
							}
						}
						if (this.two_ndFlag() == true) {
							this.two_ndFlagDate(typeof publishedSkuData.TwoNDBackorderDeliveryDates == "function" ? publishedSkuData.TwoNDBackorderDeliveryDates() : publishedSkuData.TwoNDBackorderDeliveryDates);
						}
					} else {
						this.two_ndFlag(false);
					}

					if (typeof publishedSkuData.AIRBackorderDeliveryDates !== "undefined" && publishedSkuData.AIRBackorderDeliveryDates !== null) {
						this.airFlag(true);

						if (typeof publishedSkuData.AIRBackorderDeliveryDates == "function") {
							if (publishedSkuData.AIRBackorderDeliveryDates() !== null) {
								this.airFlag(true);
							} else {
								this.airFlag(false);
							}
						}
						if (this.airFlag() == true) {
							this.airFlagDate(typeof publishedSkuData.AIRBackorderDeliveryDates == "function" ? publishedSkuData.AIRBackorderDeliveryDates() : publishedSkuData.AIRBackorderDeliveryDates);

						}
					} else {
						this.airFlag(false);
					}

					if (typeof publishedSkuData.PRMBackorderDeliveryDates !== "undefined" && publishedSkuData.PRMBackorderDeliveryDates !== null) {
						this.prmFlag(true);

						this.prmFlag(true);

						if (typeof publishedSkuData.PRMBackorderDeliveryDates == "function") {
							if (publishedSkuData.PRMBackorderDeliveryDates() !== null) {
								this.prmFlag(true);
							} else {
								this.prmFlag(false);
							}
						}
						if (this.prmFlag() == true) {
							this.prmFlagDate(typeof publishedSkuData.PRMBackorderDeliveryDates == "function" ? publishedSkuData.PRMBackorderDeliveryDates() : publishedSkuData.PRMBackorderDeliveryDates);
						}

					} else {
						this.prmFlag(false);
					}


				} else {
					this.backOrderFlag(false);
				}


            },
            
            handleMobileCheckout: function () {
				getWidget.cart().markDirty();
				navigation.goTo('/checkout');
			},