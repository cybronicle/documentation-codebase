/* 
    * Flags used to deny setup changes if resizing doesnt change 
    * to a different viewport
*/ 
var timeout = false; 
var isMobile = false;
var wasMobile = false;

/*
    variables used for desktopSetup()
*/
var primaryMenuDataPlace;
var columnDataPlace = 0; 
var rowDataPlace = 0;
var allMenus = $(".primaryMenuItem + div");

/*
    * Document listener ot check the width and get page setup
*/
$(document).ready(function() {
    if(window.innerWidth > 1024){
        desktopSetup();
    }else{
        isMobile = true;
        wasMobile = true;
        mobileSetup()
    }
})

/*
    * A window listener that watches for a resizing function
    * Uses flags to limit the number of viewport setups to 1
*/
$(window).on('resize', function(){
    clearTimeout(timeout);
    wasMobile = isMobile;
    if (window.innerWidth < 1024){
        isMobile = true;
    }else{
        isMobile = false;
    }
    timeout = setTimeout(resizeSetup(), 500);
});

/*
    * tears down listeners if window is resized to mobile
*/
function desktopTearDown(){
    $('.secondaryMenuItem').unbind();
    $('.primaryMenuItem').unbind();
    $('.tertiaryMenuItem').unbind();
}

/*
    * removes buttons if window is resized to desktop
*/
function mobileTearDown(){
    closeAllMenus();
    $('.primaryMenuItem + button').remove();
    $('.secondaryMenuItem + button').remove()
}

/*
     * Creates the correct meganav layout based the resulting resize width
     * Checks checks all use cases of resized width
*/
function resizeSetup(){
    /*resized from mobile to mobile or desktop to desktop */
    if((isMobile === false && wasMobile ===false) || (isMobile == true && wasMobile == true) ){
        return
    }
    /*resized from mobile to desktop */
    if(isMobile === false && wasMobile === true){
        mobileTearDown();
        desktopSetup();
    }
    /* resized from desktop to mobile */
    if(isMobile === true && wasMobile === false){
        desktopTearDown();
        mobileSetup();
    }
}

function toggleTertiaryMenu(status){
    var tertiaryMenus = $('.tertiaryMenu.selected');
    if(status==="open"){
        tertiaryMenus.fadeIn();
    }else{
        tertiaryMenus.fadeOut();
    }
}

function hideSecondaryMenuItems(menu){
    Array.prototype.forEach.call(menu, function(el){
        Array.prototype.forEach.call(el.children, function(li){
            if(!$(li).context.className.includes('dontHide')){
                $(li).fadeOut();
            }
        })
    })
}

function showSecondaryMenuItems(menu){
    Array.prototype.forEach.call(menu, function(el){
        Array.prototype.forEach.call(el.children, function(li){
            if($(li).context.style.cssText === "display: none;"){
                $(li).fadeIn();
            }
        })
    })
}

function showPrimaryMenuItems(){
    var primaryMenuLIElements = $(".tr-nav-menu > ul > li.node");
    Array.prototype.forEach.call(primaryMenuLIElements, function(el){
        if(el.className.includes("hide")){
            $(el).removeClass('hide');
        }
        if(el.style.display != ""){
            el.style.display = "";
        }
    });
}

function addButtons(){
    var primaryMenuItems = $('.primaryMenuItem');
    var secondaryMenuItems = $('.secondaryMenuItem.has-submenu');

    Array.prototype.forEach.call(primaryMenuItems, function(el){
        if($(el).context.nextElementSibling.nodeName !== "BUTTON"){
            var btn = '<button class="menuDownButton" aria-expanded="false"><img src="caret-down.svg"/></button>';  
            el.insertAdjacentHTML('afterend',btn);
        }
    });

    Array.prototype.forEach.call(secondaryMenuItems, function(el){
        if($(el).context.className.includes('has-submenu') && $($(el).context.parentNode).children.length === 2) {
            var btn = '<button class="menuDownButton dontHide" aria-expanded="false"><img src="caret-down.svg"/></button>';  
            el.insertAdjacentHTML('afterend',btn);
        }
    });
}


function changeCaretSource(context, level){
    if(level === "primary"){
        if(context.context.firstElementChild.attributes[0].value === "caret-down.svg" ){
            context.context.firstElementChild.attributes[0].value = "TR-Caret-Icon-HOVER.svg";
        }else{
            context.context.firstElementChild.attributes[0].value = "caret-down.svg";
        }
    }else{
        if(context.firstElementChild.attributes[0].value === "caret-down.svg" ){
            context.firstElementChild.attributes[0].value = "TR-Caret-Icon-HOVER.svg";
        }else{
            context.firstElementChild.attributes[0].value = "caret-down.svg";
        }
    }
}

function primaryDownMenuButton(context){
    $(context.context.parentNode).removeClass("blueSelected")
    changeCaretSource(context, "primary");
}

function primaryUpMenuButton(context){
    $(context.context.parentNode).addClass("blueSelected")
    changeCaretSource(context, "primary");
}

function secondaryDownMenuButton(context){
    $(context.parentNode).removeClass("blueSelected dontHide")
    changeCaretSource(context, "secondary");
}

function secondaryUpMenuButton(context){
    $(context.parentNode).addClass("blueSelected dontHide")                        
    changeCaretSource(context, "secondary");
}

function closePrimaryMenus(){
    Array.prototype.forEach.call($('.node > button img'), function(el){
        if(el.attributes.src.value == "TR-Caret-Icon-HOVER.svg"){
            el.parentNode.click();
        }
    })
}

function closeSecondaryMenus(){
    Array.prototype.forEach.call($('.secondaryMenu > ul > li > button img'), function(el){
        if(el.attributes.src.value == "TR-Caret-Icon-HOVER.svg"){
            el.parentNode.click();
        }
    })
}

function closeAllMenus(){
    toggleTertiaryMenu("close");
    toggleSecondaryMenu("close");
    showPrimaryMenuItems();
    closeSecondaryMenus();
    closePrimaryMenus();
}

/* not used in this js file but is here button attached to a different widget */
function menuToggle(){
    var menu = $('.tr-nav-menu')
    if(menu[0].className.includes("open")){
        menu.fadeOut();
        $(menu[0]).removeClass("open");
        closeAllMenus();
        setTimeout(initailTertiaryHide("open"),100);
    }else{
        $(menu[0]).addClass("open");
        menu.animate({width: 'toggle'},200);
        setTimeout(initailTertiaryHide("close"),100);
    }
}

function togglePrimaryMenu(status){
    if(status === "open"){
        var primaryElements = $('.node');
        Array.prototype.forEach.call(primaryElements, function(el){
            if (!el.className.includes("selected")){
                $(el).addClass("notSelected");
            }
        });
        $('.node.notSelected').fadeOut();
    }else{
        var primaryElements = $('.node');
        $('.node.notSelected').fadeIn();
        Array.prototype.forEach.call(primaryElements, function(el){
            if (el.className.includes("notSelected")){
                $(el).removeClass("notSelected");
            }
        });
    }
}

function toggleSecondaryMenu(status){
    var second = $(".menu.has-submenu.selected");
    if(status === "open"){
        $(".tr-nav-menu")[0].style.boxShadow = "none";
        second.fadeIn();
    }else{
        $(".tr-nav-menu")[0].style.boxShadow = "";
        second.fadeOut();
    }
    toggleSecondaryElements(status);
}

function toggleSecondaryElements(status){
    var secondaryElements = $(".menu.has-submenu.selected > .secondaryMenu");
    if(status === "open"){
        secondaryElements.fadeIn();
    }else{
        secondaryElements.fadeOut();
    }
}

function initailTertiaryHide(status){
    var tertiaryMenus = $('.tertiaryMenu');
    if(status === "open"){
        tertiaryMenus.fadeIn()
    }else{
        tertiaryMenus.fadeOut()
    }
}

function focusPrimaryMenuElement(){
    $('a[data_place="' + primaryMenuDataPlace + '"]').focus();
}

function focusLowerMenuElement(){
    if (!$('span[data_place="'+ primaryMenuDataPlace+ ',' + columnDataPlace + ',' + rowDataPlace +'"]').length == 0 &&
    document.activeElement.className.includes("primaryMenuItem")){
        rowDataPlace = rowDataPlace + 1;
        $('a[data_place="'+ primaryMenuDataPlace+ ',' + columnDataPlace + ',' + rowDataPlace +'"]').focus();
        return;
    }
    $('a[data_place="'+ primaryMenuDataPlace+ ',' + columnDataPlace + ',' + rowDataPlace +'"]').focus();
}

function getNumberOfColumnsInSubmenu(){
    return Number($('a[data_place="'+ primaryMenuDataPlace + '"] + div ul[data_place]').length) - 1;
}

function getNumberOfRowsInColumnInSubmenu(){
    return Number($('ul[data_place="'+ primaryMenuDataPlace + ',' + columnDataPlace + '"] > li  a').length) - 1;
}

function addSubmenuKeyDownEventHandler(){
    /* used for inside menu arrow navigation (up, down, left, right) */
    $('.secondaryMenuItem, .tertiaryMenuItem').on("keydown", function(event){
        rowDataPlace = this.attributes.data_place.value.split(',')[2];
        
        /* Listen for the up, down, left and right arrow keys, otherwise, end here */
        if ([9,27,37,38,39,40].indexOf(event.keyCode) == -1) {
            return; 
        }

        switch(event.keyCode) {
            case 9: /* tab */
                event.preventDefault();
                $('a[data_place="'+ primaryMenuDataPlace+'"]')[0].setAttribute('aria-expanded', 'false');
                $($('a[data_place="'+primaryMenuDataPlace+'"]')[0].nextElementSibling).removeClass("isFocused");
                if(event.shiftKey){
                    $(".tr-nav-menu").prev().focus();
                }else{
                    $(".tr-nav-menu").next().focus();
                }
                return
            
            case 27: /* escape */
                $('a[data_place="'+ primaryMenuDataPlace+'"]')[0].setAttribute('aria-expanded', 'false');
                $($('a[data_place="'+primaryMenuDataPlace+'"]')[0].nextElementSibling).removeClass("isFocused");
                focusPrimaryMenuElement();
                return
            
            case 37: /* left arrow */
                $('a[data_place="'+ primaryMenuDataPlace+'"]')[0].setAttribute('aria-expanded', 'false');
                $($('a[data_place="'+primaryMenuDataPlace+'"]')[0].nextElementSibling).removeClass("isFocused");
                if($('a[data_place="'+primaryMenuDataPlace+'"]')[0].parentNode.previousElementSibling.className.includes("mobileMenuTitle") ){
                    focusPrimaryMenuElement();
                    return true
                }else{
                    primaryMenuDataPlace = primaryMenuDataPlace - 1;
                    $('a[data_place="'+ primaryMenuDataPlace+'"]')[0].setAttribute('aria-expanded', 'true');
                    $('a[data_place="'+ primaryMenuDataPlace +'"] + div').addClass("isFocused");
                    $('a[data_place="'+ primaryMenuDataPlace+',0,0"]').focus();
                }
                return true
            
            case 38: /* up arrow */
                /* checks to see if the focus is the first element in the secondaryMenuColumn */
                if(rowDataPlace == 0){
                    if(columnDataPlace == 0){
                        /* sends user to end of the submenu */
                        columnDataPlace = getNumberOfColumnsInSubmenu();
                        rowDataPlace = getNumberOfRowsInColumnInSubmenu();
                    }else{
                        /* sends user to the end of the previous column */
                        columnDataPlace = columnDataPlace - 1;
                        rowDataPlace = getNumberOfRowsInColumnInSubmenu();
                    }
                }else{
                    /*sends the user to the element above the current */
                    rowDataPlace = rowDataPlace - 1;
                }
                break
            
            case 39: /* right arrow */
                $('a[data_place="'+ primaryMenuDataPlace+'"]')[0].setAttribute('aria-expanded', 'false');
                $($('a[data_place="'+primaryMenuDataPlace+'"]')[0].nextElementSibling).removeClass("isFocused");
                /* checks to see if there is a menu to the right of the current */
                if(!$('a[data_place="'+primaryMenuDataPlace+'"]')[0].parentNode.nextElementSibling){
                    focusPrimaryMenuElement();
                }else{
                    primaryMenuDataPlace = primaryMenuDataPlace + 1;
                    $('a[data_place="'+ primaryMenuDataPlace+'"]')[0].setAttribute('aria-expanded', 'true');
                    $('a[data_place="'+ primaryMenuDataPlace +'"] + div').addClass("isFocused");
                    $('a[data_place="'+ primaryMenuDataPlace +',0,0"]').focus();
                }
                return
            
            case 40: /* down arrow */
                /*checks to see if the focus is on the last element in the column */
                if(rowDataPlace == getNumberOfRowsInColumnInSubmenu()){
                    rowDataPlace = 0;
                    /*checks to see if the focus is on the last element in the last row */ 
                    if(columnDataPlace === getNumberOfColumnsInSubmenu()){
                        columnDataPlace = 0;
                    }else{
                        columnDataPlace = columnDataPlace + 1;
                    }
                }else{
                    /*sends focus to the element directly below the current focus */
                    rowDataPlace = Number(rowDataPlace) + 1;
                }
        }
        focusLowerMenuElement();
    });
}

function addPrimaryKeyDownEventHandler(){
    /* puts keydown listener on keydown */ 
    $('.primaryMenuItem').on("keydown", function(event){
        /* Listen for certain keys, otherwise, end here */
        if ([9, 13, 27, 32, 37, 38, 39, 40].indexOf(event.keyCode) == -1) {
            return;
        }
        switch(event.keyCode) {
            case 9: /* tab */
                event.preventDefault();
                if(event.shiftKey){
                    $(".tr-nav-menu").prev().focus();
                }else{
                    $(".tr-nav-menu").next().focus();
                }
                return
                
            case 13: case 32: case 40: /* enter, space and down arrow */
                event.preventDefault();
                $('a[data_place="'+ primaryMenuDataPlace+'"]')[0].setAttribute('aria-expanded', 'true');
                $('a[data_place="'+ primaryMenuDataPlace+'"] + div').addClass("isFocused");
                columnDataPlace = 0;
                rowDataPlace = 0;
                break
            
            case 27: /* escape */
                $(".tr-nav-menu").next().focus();
                return

            case 37: /* left arrow */
                if(!event.target.parentNode.previousElementSibling.className.includes("mobileMenuTitle")){
                    primaryMenuDataPlace = primaryMenuDataPlace - 1;
                    focusPrimaryMenuElement();
                }
                return

            case 38: /* up arrow */
                event.preventDefault();
                $('a[data_place="'+ primaryMenuDataPlace+'"]')[0].setAttribute('aria-expanded', 'true');
                $('a[data_place="'+ primaryMenuDataPlace+'"] + div').addClass("isFocused");
                columnDataPlace = getNumberOfColumnsInSubmenu();
                rowDataPlace = getNumberOfRowsInColumnInSubmenu();
                break

            case 39: /* right arrow */
                if(event.target.parentNode.nextElementSibling != null){
                    primaryMenuDataPlace = primaryMenuDataPlace + 1
                    focusPrimaryMenuElement();
                }
                return
        }
        focusLowerMenuElement();

    });
}

function checkIfSubmenusAreOpen(){
    Array.prototype.forEach.call(allMenus, function(el){
        if(el.className.includes("isFocused")){
            $(el).removeClass("isFocused")
        }
    })
}

/* 
    * function used to set the data_place for column elements
*/
function setDataPlaceOnSubmenuColumns(){
    var test = $(".secondaryMenu > ul")
    var data_place;
    var columnPlace;
    Array.prototype.forEach.call(test, function(el){
        if (data_place != el.parentNode.parentNode.previousElementSibling.attributes.data_place.value){
            data_place = el.parentNode.parentNode.previousElementSibling.attributes.data_place.value;
            columnPlace = 0;
        }else{
            columnPlace = columnPlace + 1;
        }
        el.setAttribute("data_place", data_place+","+columnPlace);
    })
}   

/* 
    * function used to set the data_place for row elements
    * Note: there are 3 levels in the HTML that these elements can be found
    * hence the number of if statements. Hopefully this will change with 
    * creative changes for menu designs.
*/
function setDataPlaceOnSecondaryAndTertiaryMenuItems(){
    var test = $("a.secondaryMenuItem, a.tertiaryMenuItem")
    var data_place;
    var row_place;

    Array.prototype.forEach.call(test, function(el){
        if(el.className.includes("secondaryMenuItem")){
            if(data_place != el.parentNode.parentNode.attributes.data_place.value){
                data_place = el.parentNode.parentNode.attributes.data_place.value;
                row_place = 0;
            }else{
                row_place = row_place + 1;
            }
            el.setAttribute("data_place", data_place+','+row_place)
        }else{
            if(el.parentNode.className.includes("breakColumn")){
                if(data_place != el.parentNode.parentNode.parentNode.parentNode.attributes.data_place.value){
                    data_place = el.parentNode.parentNode.parentNode.parentNode.attributes.data_place.value;
                    row_place = 0;
                }else{
                    row_place = row_place + 1;
                }
                el.setAttribute("data_place", data_place+','+row_place)
            }else{
                if(data_place != el.parentNode.parentNode.parentNode.attributes.data_place.value){
                    data_place = el.parentNode.parentNode.parentNode.attributes.data_place.value;
                    row_place = 0;
                }else{
                    row_place = row_place + 1;
                }
                el.setAttribute("data_place", data_place+','+row_place)
            }
        }
    });
}

/*
    * function used for desktop setup for the mega menu
*/
function desktopSetup(){
    addPrimaryKeyDownEventHandler();
    addSubmenuKeyDownEventHandler();

    $('.tertiaryMenuItem').on("click", function(event){
        event.preventDefault();
        console.log("clicktest", $(event.currentTarget).attr("data-sec"));
    })

    /* used for primaryMenu focus */
    $('.primaryMenuItem').on("focusin", function()  {
        primaryMenuDataPlace = Number($(this).context.attributes.data_place.value);
        columnDataPlace = 0;
        rowDataPlace = 0;
        checkIfSubmenusAreOpen();
    });

    $('.secondaryMenuItem, .tertiaryMenuItem').on("focusin", function(event){
        columnDataPlace = Number(event.target.attributes.data_place.value.split(',')[1]);
        rowDataPlace = Number(event.target.attributes.data_place.value.split(',')[2]);
    });


    /* helps close the menu if focus is lost on menu */
    $('.secondaryMenuItem, .tertiaryMenuItem').on("focusout",function(event){
        if(!event.relatedTarget || !event.relatedTarget.className){
            checkIfSubmenusAreOpen();
        }else{
            if (!event.relatedTarget.className.includes('secondaryMenuItem') && !event.relatedTarget.className.includes('tertiaryMenuItem')){
                checkIfSubmenusAreOpen(); 
            } 
        }
    });
    setDataPlaceOnSubmenuColumns();
    setDataPlaceOnSecondaryAndTertiaryMenuItems();
}

/*
    * Mobile setup for the mega menu
*/
function mobileSetup(){

    /* inserts caret svg button if there is no button currently */
    addButtons();    

    /* click listener on primary list items buttons */
    $('.primaryMenuItem + button').on("click", function(event){
        event.preventDefault()
        /* checks to see if menu is open */
        if($(this).context.nextElementSibling.className.includes("selected")){
            togglePrimaryMenu("close");
            $($(this).context.parentNode).removeClass("selected");

            /* changes direction of caret */
            primaryDownMenuButton($(this));
            toggleSecondaryMenu("close");
            $($(this).context.nextElementSibling).removeClass("selected");
            /* showing other primary menu items */
            showPrimaryMenuItems();
            closeSecondaryMenus();
        }else{
            $($(this).context.parentNode).addClass("selected");
            togglePrimaryMenu("open");
            /*changes direction of caret*/
            primaryUpMenuButton($(this));

            /*adds class to open secondary menu*/
            $($(this).context.nextElementSibling).addClass("selected");
            toggleSecondaryMenu("open");
        }
    });

    /* click listener on secondary list item buttons */
    $('.secondaryMenuItem + button').on("click", function(event){
        event.preventDefault()
        /* check the status of the menu button and make changes accordingly */
        if ($(this).context.firstElementChild.attributes[0].value === "caret-down.svg"){
            $($(this).context.nextElementSibling).addClass("selected")
            secondaryUpMenuButton($(this).context);
            hideSecondaryMenuItems($(this).context.parentNode.parentNode.parentNode.children);
            toggleTertiaryMenu("open");
        }else{
            toggleTertiaryMenu("close");
            $($(this).context.nextElementSibling).removeClass("selected")
            secondaryDownMenuButton($(this).context);
            showSecondaryMenuItems($(this).context.parentNode.parentNode.parentNode.children)
        }

    });
}

/*
    * This is the object that contains the contents for the desktop and mobile menus
    * in one spot. Edit this object but reference the documentation for order when changing
*/
var menuObject = [
    {
        "element": "li",
        "class": "mobileMenuTitle",
        "contents":[
            {
                "element": "span", 
                "text" : "MENU"
            }
        ]
    }, 

    {
        "element": "li",
        "contents" : [
            {
                "element": "a",
                "class": "primaryMenuItem",
                "href": "/personalized-gifts-for-any-occasion",
                "tabindex": "1",
                "title": "Personalized Gifts For Any Occasion",
                "text": "OCCASIONS"
            },
            {
                "element": "div",
                "class": "menu has-submenu",
                "contents":[
                        {
                        "element": "div",
                        "class": "secondaryMenu",
                        "contents": [
                            {
                                "element": "ul",
                                "contents": [
                                    {
                                        "element": "li",
                                        "contents":[
                                            {
                                                "element": "span",
                                                "class": "secondaryMenuItem header has-submenu",
                                                "text": "UPCOMING HOLIDAYS"
                                            },
                                            {
                                                "element": "div",
                                                "class": "tertiaryMenu",
                                                "contents": [
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-easter",
                                                        "text": "Easter"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-mothers-day",
                                                        "text": "Mother's Day"
                                                    }, 
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-graduation-gifts",
                                                        "text": "Graduation"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "element": "ul",
                                "class": "multipleColumns3",
                                "contents": [
                                    {
                                        "element": "li",
                                        "contents":[
                                            {
                                                "element": "span",
                                                "class": "secondaryMenuItem header has-submenu",
                                                "text": "SPECIAL OCCASIONS"
                                            },
                                            {
                                                "element": "div",
                                                "class": "tertiaryMenu",
                                                "contents": [
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-birthday",
                                                        "text": "Birthday"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-baby-gifts/category/new-baby",
                                                        "text": "New Baby"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-wedding-gifts",
                                                        "text": "Wedding"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/engagement-gifts/category/engagement",
                                                        "text": "Engagement"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-anniversary-gifts",
                                                        "text": "Anniversary"
                                                    },
                                                    {
                                                        "element": "div",
                                                        "class": "breakColumn",
                                                        "contents":[
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/personalized-housewarming-gifts",
                                                                "text": "Housewarming"
                                                            },
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/religious-occasions/category/religious-occasions",
                                                                "text": "Religious"
                                                            },
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/personalized-baptism-gifts/category/baptism",
                                                                "text": "Baptism"
                                                            },
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/personalized-first-communion-gifts/category/first-communion",
                                                                "text": "First Communion"
                                                            },
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/personalized-confirmation-gifts/category/confirmation",
                                                                "text": "Confirmation"
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        "element":"div",
                                                        "class": "breakColumn",
                                                        "contents":[
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/personalized-retirement-gifts/category/retirement",
                                                                "text": "Retirement"
                                                            },
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/thank-you-occasions/category/thank-you-occasions",
                                                                "text": "Thank You"
                                                            },
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/personalized-graduation-gifts",
                                                                "text": "Graduation"
                                                            },
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/personalized-memorial-sympathy-gifts/category/sympathy",
                                                                "text": "Sympathy"
                                                            },
                                                            {
                                                                "element": "a",
                                                                "class": "tertiaryMenuItem",
                                                                "href": "/greeting-cards/category/greeting-cards",
                                                                "text": "Greetings Card"
                                                            }
                                                        ]
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "element": "ul",
                                "contents": [
                                    {
                                        "element": "li",
                                        "contents":[
                                            {
                                                "element": "a",
                                                "class": "tertiaryMenuItem",
                                                "href": "/personalized-gifts-for-easter",
                                                "text": "Easter"
                                            },
                                            {
                                                "element": "a",
                                                "class": "tertiaryMenuItem",
                                                "href": "/personalized-gifts-for-mothers-day",
                                                "text": "Mother's Day"
                                            },
                                            {
                                                "element": "a",
                                                "class": "tertiaryMenuItem",
                                                "href": "/personalized-gifts-for-fathers-day",
                                                "text": "Father's Day"
                                            },
                                            {
                                                "element": "a",
                                                "class": "tertiaryMenuItem",
                                                "href": "/thanksgiving-gifts/category/thanksgiving",
                                                "text": "Thanksgiving"
                                            },
                                            {
                                                "element": "a",
                                                "class": "tertiaryMenuItem",
                                                "href": "/personalized-gifts-for-christmas",
                                                "text": "Christmas"
                                            },
                                            {
                                                "element": "a",
                                                "class": "tertiaryMenuItem",
                                                "href": "/personalized-gifts-for-valentines-day",
                                                "text": "Valentine's Day"
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    },

    {
        "element": "li",
        "contents" : [
            {
                "element": "a",
                "class": "primaryMenuItem",
                "href": "/personalized-gifts-for-recipients",
                "tabindex": "1",
                "title": "Personalized Gifts For Any Recipient",
                "text": "RECIPIENTS"
            },
            {
                "element": "div",
                "class": "menu has-submenu",
                "contents": [
                    {
                        "element": "div",
                        "class": "secondaryMenu",
                        "contents": [
                            {
                                "element": "ul",
                                "contents": [
                                    {
                                        "element": "li",
                                        "contents":[
                                            {
                                                "element": "a",
                                                "class": "secondaryMenuItem header has-submenu",
                                                "href": "/gifts-for-him/category/gifts-for-him",
                                                "text": "HIM"
                                            },
                                            {
                                                "element": "div",
                                                "class": "tertiaryMenu",
                                                "contents": [
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-dad/category/for-dad",
                                                        "text": "Dad"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-grandpa/category/for-grandpa",
                                                        "text": "Grandpa"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-husband/category/for-husband",
                                                        "text": "Husband"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/for-brother/category/for-brother",
                                                        "text": "Brother"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/for-uncle/category/for-uncle",
                                                        "text": "Uncle"
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        "element": "li",
                                        "contents":[
                                            {
                                                "element": "a",
                                                "class": "secondaryMenuItem header has-submenu",
                                                "href": "/gifts-for-her/category/gifts-for-her",
                                                "text": "HER"
                                            },
                                            {
                                                "element": "div",
                                                "class": "tertiaryMenu",
                                                "contents": [
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-mom/category/for-mom",
                                                        "text": "Mom"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-grandma/category/for-grandma",
                                                        "text": "Grandma"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-wife/category/for-wife",
                                                        "text": "Wife"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/for-sister/category/for-sister",
                                                        "text": "Sister"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/for-aunt/category/for-aunt",
                                                        "text": "Aunt"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "element": "ul",
                                "contents": [
                                    {
                                        "element": "li",
                                        "contents":[
                                            {
                                                "element": "a",
                                                "class": "secondaryMenuItem header has-submenu",
                                                "href": "/recipients-babykids/category/recipients-babykids",
                                                "text": "BABY + KIDS"
                                            },
                                            {
                                                "element": "div",
                                                "class": "tertiaryMenu",
                                                "contents": [
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-baby-gifts/category/new-baby",
                                                        "text": "Baby"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-daughters/category/for-daughters",
                                                        "text": "Daughter"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-sons/category/for-sons",
                                                        "text": "Son"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/teen-girls/category/teen-girls",
                                                        "text": "Teen Girl"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/teen-boys/category/teen-boys",
                                                        "text": "Teen Boy"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "element": "ul",
                                "contents": [
                                    {
                                        "element": "li",
                                        "contents":[
                                            {
                                                "element": "a",
                                                "class": "secondaryMenuItem header has-submenu",
                                                "href": "/family-friends-recipients/category/family-friends-recipients",
                                                "text": "FAMILY + FRIENDS"
                                            },
                                            {
                                                "element": "div",
                                                "class": "tertiaryMenu",
                                                "contents": [
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/grandparents-day/category/grandparents-day",
                                                        "text": "Grandparents"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-host-gifts/category/host",
                                                        "text": "Host"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-hostess-gifts/category/hostess",
                                                        "text": "Hostess"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-wedding-gifts-for-parents/category/for-the-parents",
                                                        "text": "Parents"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-sports-gifts/category/sports-fan",
                                                        "text": "Sports Fan"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "element": "ul",
                                "contents": [
                                    {
                                        "element": "li",
                                        "contents":[
                                            {
                                                "element": "a",
                                                "class": "secondaryMenuItem header has-submenu",
                                                "href": "/participants-wedding/category/participants-wedding",
                                                "text": "Wedding Party"
                                            },
                                            {
                                                "element": "div",
                                                "class": "tertiaryMenu",
                                                "contents": [
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-bride-gifts/category/for-the-bride",
                                                        "text": "Bride"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-groom/category/for-the-groom",
                                                        "text": "Groom"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/gifts-for-bridesmaids-wedding/category/gifts-for-bridesmaids-wedding",
                                                        "text": "Bridesmaids"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/gifts-for-groomsmen-wedding/category/gifts-for-groomsmen-wedding",
                                                        "text": "Groomsmen"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/gifts-for-the-bridal-party/category/for-the-bridal-party",
                                                        "text": "Bridal Party"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-wedding-gifts-for-parents/category/for-the-parents",
                                                        "text": "Parents"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-flower-girl-gifts/category/for-the-flower-girl",
                                                        "text": "Flower Girl"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-ring-bearer-gifts/category/for-the-ring-bearer",
                                                        "text": "Ring Bearer"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-wedding-officiant-helpers/category/for-the-wedding-helpers-officiants",
                                                        "text": "Wedding Helpers + Officiants"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "element": "ul",
                                "contents": [
                                    {
                                        "element": "li",
                                        "contents":[
                                            {
                                                "element": "a",
                                                "class": "secondaryMenuItem header has-submenu",
                                                "href": "/professionals-recipients/category/professionals-recipients",
                                                "text": "PROFESSSIONALS"
                                            },
                                            {
                                                "element": "div",
                                                "class": "tertiaryMenu",
                                                "contents": [
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-administrative-professional-gifts/category/administrative-professional-gifts",
                                                        "text": "Administrative Professionals"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem", 
                                                        "href": "/personalized-gifts-for-boss/category/for-the-boss",
                                                        "text": "Boss"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-client-gifts/category/gifts-for-clients",
                                                        "text": "Clients"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-coaches/category/coach",
                                                        "text": "Coach"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-coworkers/category/gifts-for-coworkers",
                                                        "text": "Coworkers"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/for-lawyers/category/for-lawyers",
                                                        "text": "Lawyer"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-military-gifts/category/military-recognition",
                                                        "text": "Military Recognition"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-gifts-for-nurses/category/nurse-or-caregiver",
                                                        "text": "Nurse & Caregiver"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-firefighter-gifts-police-gifts/category/public-service",
                                                        "text": "Public Servant"
                                                    },
                                                    {
                                                        "element": "a",
                                                        "class": "tertiaryMenuItem",
                                                        "href": "/personalized-teacher-gifts/category/teacher",
                                                        "text": "Teacher"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    },

    {
        "element": "li",
        "contents" : [
            {
                "element": "a",
                "class": "primaryMenuItem",
                "href": "/personalized-gifts-for-him",
                "tabindex": "1",
                "title": "Personalized Gifts for Him",
                "text": "HIM"
            },
            {
                "element": "div",
                "class": "menu has-submenu",
                "contents": [
                    {
                    "element": "div",
                    "class": "secondaryMenu",
                    "contents": [
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header",
                                            "href": "/new-arrivals-for-him/category/new-arrivals-for-him",
                                            "text": "New Arrivals"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header",
                                            "href": "/birthday-gifts-for-him/category/birthday-gifts-for-him",
                                            "text": "Birthday Gifts"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/gifts-for-him/category/gifts-for-him",
                                            "text": "By Recipient"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-dad/category/for-dad",
                                                    "text": "Dad"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-grandpa/category/for-grandpa",
                                                    "text": "Grandpa"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-husband/category/for-husband",
                                                    "text": "Husband"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/for-brother/category/for-brother",
                                                    "text": "Brother"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/for-uncle/category/for-uncle",
                                                    "text": "Uncle"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/jewelry-for-him/category/jewelry-for-him",
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-cuff-links-tie-clips/category/cuff-links-tie-bars",
                                                    "text": "Cuff Links + Tie Bars"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-dog-tags-pendants/category/dog-tags-pendants",
                                                    "text": "Dog Tags + Pendants"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-id-bracelets/category/id-bracelets",
                                                    "text": "ID Bracelets"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-pocket-watches/category/pocket-watches",
                                                    "text": "Pocket Watches"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-rings-for-him/category/mens-rings",
                                                    "text": "Rings"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-sterling-silver-mens-jewelry/category/mens-sterling-silver-jewelry",
                                                    "text": "Sterling Silver"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-watch-boxes-for-men/category/watch-boxes-valets",
                                                    "text": "Watch Boxes + Valets"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-watches-for-him/category/mens-watches",
                                                    "text": "Watches"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/accessories-for-him/category/accessories-for-him",
                                            "text": "Accessories"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-business-card-holders/category/card-cases-holders",
                                                    "text": "Card Cases + Holders"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-key-chains/category/key-chains-for-him",
                                                    "text": "Key Chains"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-lighters/category/lighters-for-him",
                                                    "text": "Lighters"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-apparel-for-him/category/apparel-for-him",
                                                    "text": "Men's Apparel"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-money-clips-wallets/category/money-clips-wallets",
                                                    "text": "Money Clips + Wallets"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-pocket-knives-tools/category/pocket-knives-tools",
                                                    "text": "Pocket Knives + Tools"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/smoking-accessories/category/smoking-accessories",
                                                    "text": "Smoking Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-mens-bags/category/bags-for-him",
                                                    "text": "Travel Bags + Dopp Kits"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/barware-for-him/category/barware-for-him",
                                            "text": "Barware"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/barware-for-him/category/barware-for-him",
                                                    "text": "Bar Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-bar-whisky-glasses/category/bar-glasses",
                                                    "text": "Bar Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-beer-gifts/category/beer-glasses",
                                                    "text": "Beer Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-decanters-carafes/category/decanters",
                                                    "text": "Decanters"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-flasks/category/flasks",
                                                    "text": "Flasks"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/bottle-openers-wine-stoppers/category/bottle-openers-wine-stoppers",
                                                    "text": "Openers + Wine Stoppers"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-shot-glasses/category/shot-glasses",
                                                    "text": "Shot Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-whiskey-glasses/category/whiskey-glasses",
                                                    "text": "Whiskey Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-wine-glasses/category/wine-glasses",
                                                    "text": "Wine Glasses"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/sports-for-him/category/sports-for-him",
                                            "text": "Sports"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-mlb-gifts/category/mlb",
                                                    "text": "Baseball + MLB"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-nba-gifts/category/nba",
                                                            "text": "Basketball + NBA"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-ncaa-gifts/category/ncaa",
                                                    "text": "College Teams + NCAA"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-nfl-gifts/category/nfl",
                                                    "text": "Football + NFL"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-golf-gifts/category/golf",
                                                    "text": "Golf"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
                ]
            }
        ]
    },

    {
        "element": "li",
        "contents" : [
            {
                "element": "a",
                "class": "primaryMenuItem",
                "href": "/personalized-gifts-for-her",
                "tabindex": "1",
                "title": "Personalized Gift for Her",
                "text": "HER"
            },
            {
                "element": "div",
                "class": "menu has-submenu",
                "contents": [
                    {
                    "element": "div",
                    "class": "secondaryMenu",
                    "contents": [
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header",
                                            "href": "/new-arrivals-for-her/category/new-arrivals-for-her",
                                            "text": "New Arrivals"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header",
                                            "href": "/birthday-gifts-for-her/category/birthday-gifts-for-her",
                                            "text": "Birthday Gifts"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/gifts-for-her/category/gifts-for-her",
                                            "text": "By Recipient"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-mom/category/for-mom",
                                                    "text": "Mom"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-grandma/category/for-grandma",
                                                    "text": "Grandma"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-wife/category/for-wife",
                                                    "text": "Wife"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/for-sister/category/for-sister",
                                                    "text": "Sister"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/for-aunt/category/for-aunt",
                                                    "text": "Aunt"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/jewelry-for-her/category/jewelry-for-her",
                                            "text": "Jewelry"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-birthstone-jewelry",
                                                    "text": "Birthstone"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/bracelets-for-her/category/bracelets-for-her",
                                                    "text": "Bracelets"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/diamond-14k-gold-jewelry/category/diamond-14k-gold-jewelry",
                                                    "text": "Diamond + 14k Gold"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-monogram-jewelry/category/monogram-jewelry",
                                                    "text": "Monogram"      
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/necklaces-for-her/category/necklaces-for-her",
                                                    "text": "Necklaces"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-rings-for-her/category/womens-rings",
                                                    "text": "Rings"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/womens-sterling-silver-jewelry/category/sterling-silver-jewelry",
                                                    "text": "Sterling Silver"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-watches-for-her/category/womens-watches",
                                                    "text": "Watches"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-wedding-jewelry/category/wedding-jewelry",
                                                    "text": "Wedding"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/accessories-for-her/category/accessories-for-her",
                                            "text": "Accessories"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-tote-bags/category/totes-bags-wallets",
                                                    "text": "Bags + Wallets"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-compact-mirrors/category/compacts",
                                                    "text": "Compacts + Card Cases"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/keychains-for-her/category/key-chains-for-her",
                                                    "text": "Key Chains"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-robes/category/womens-robes",
                                                    "text": "Robes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-scarves/category/scarves",
                                                    "text": "Scarves + Wraps"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-apparel-for-her/category/apparel-for-her",
                                                    "text": "Women's Apparel"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/storage-for-her/category/storage-for-her",
                                            "text": "Storage"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-jewelry-boxes/category/jewelry-boxes",
                                                    "text": "Jewlery Boxes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-keepsake-boxes/category/keepsake-boxes",
                                                    "text": "Keepsake Boxes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-music-boxes/category/music-boxes",
                                                    "text": "Music Boxes"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/drinkware-for-her/category/drinkware-for-her",
                                            "text": "Drinkware"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-bar-accessories-tools/category/bar-accessories",
                                                    "text": "Bar Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-beer-gifts/category/beer-glasses",
                                                    "text": "Beer Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-champagne-flutes/category/champagne-flutes",
                                                    "text": "Champange Flutes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-coffee-mugs-tea-cups/category/coffee-mugs-tea-cups",
                                                    "text": "Coffee Mugs + Tea Cups"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-travel-mugs/category/travel-mugs-water-bottles",
                                                    "text": "Travel Mugs + Water Bottles"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-wine-glasses/category/wine-glasses",
                                                    "text": "Wine Glasses"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
                ]
            }
        ]
    },

    {
        "element": "li",
        "contents" : [
            {
                "element": "a",
                "class": "primaryMenuItem",
                "href": "/personalized-gifts-baby-kids",
                "tabindex": "1",
                "title": "Personalized Gifts for Babies + Kids",
                "text": "BABY + KIDS"
            },
            {
                "element": "div",
                "class": "menu has-submenu",
                "contents": [
                    {
                    "element": "div",
                    "class": "secondaryMenu",
                    "contents": [
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header",
                                            "href": "/new-arrivals-for-baby-kids/category/new-arrivals-for-baby-kids",
                                            "text": "New Arrivals Children"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header",
                                            "href": "/personalized-gifts-for-teens/category/new-arrivals-for-teens",
                                            "text": "New Arrivals Teen"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/occasions-babykids/category/occasions-babykids",
                                            "text": "Occasions"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-baby-gifts/category/new-baby",
                                                    "text": "New Baby"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-baby-gifts/category/baptism",
                                                    "text": "Baptism"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-first-communion-gifts/category/first-communion",
                                                    "text": "First Communion"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-confirmation-gifts/category/confirmation",
                                                    "text": "Confirmation"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/religious-gifts-for-children/category/religious-childrens-gifts",
                                                    "text": "Religous Children's Gifts"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-babys-first-christmas/category/babys-first-christmas",
                                                    "text": "Baby's First Christmas"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/gifts-for-babykids/category/gifts-for-babykids",
                                            "text": "Baby"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-baby-blankets/category/baby-blankets-quilts",
                                                    "text": "Baby Blankets"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-baby-keepsakes/category/baby-keepsakes",
                                                    "text": "Baby Keepsakes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-piggy-banks/category/piggy-banks",
                                                    "text": "Piggy Banks"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-stuffed-animals/category/stuffed-animals-plush",
                                                    "text": "Stuffed Animals"
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/recipients-babykids/category/recipients-babykids",
                                            "text": "By Recipients"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-daughters/category/for-daughters",
                                                    "text": "Daughter"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-sons/category/for-sons",
                                                    "text": "Son"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/gifts-for-kids-babykids/category/gifts-for-kids-babykids",
                                            "text": "Kids"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-picture-frames-for-kids/category/childrens-frames-albums",
                                                    "text": "Frames + Albums"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-kids-furniture/category/furniture-room-decor",
                                                    "text": "Furniture + Home Decor"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-kids-baby-jewelry/category/kids-jewelry",
                                                    "text": "Jewelry"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/keepsakes-figurines-for-kids/category/keepsakes-figurines-for-kids",
                                                    "text": "Keepsakes + Figurines"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/snow-globes-for-kids/category/kids-snow-globes",
                                                    "text": "Snow Globes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-kids-toys/category/toys",
                                                    "text": "Toys"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/gifts-for-teens-babykids/category/gifts-for-teens-babykids",
                                            "text": "Teens"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-tote-bags/category/totes-bags-wallets",
                                                        "text": "Bags + Wallets"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-kids-baby-jewelry/category/kids-jewelry",
                                                    "text": "Jewelry"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-kids-baby-jewelry/category/kids-jewelry",
                                                    "text": "Jewelry Boxes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/kids-desk-accessories/category/journals-desk-accessories",
                                                    "text": "Journals + Desk Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-travel-mugs/category/travel-mugs-water-bottles",
                                                    "text": "Water Bottles"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/back-to-school-babykids/category/back-to-school-babykids",
                                            "text": "Back To School"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-kids-backpacks-school-supplies/category/backpacks-bags-lunch-bags",
                                                    "text": "Backpacks + Bags"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-lunch-boxes-gifts/category/lunch-boxes",
                                                    "text": "Lunch Boxes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-school-desk-supplies/category/school-desk-supplies",
                                                    "text": "School + Desk Supplies"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-tumblers-canteens-more/category/tumblers-canteens-more",
                                                    "text": "Tumblers + Canteens"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-college-students/category/college-101",
                                                    "text": "College 101"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
                ]
            }
        ]
    },

    {
        "element": "li",
        "contents" : [
            {
                "element": "a",
                "class": "primaryMenuItem",
                "href": "/personalized-gifts-home-bar",
                "tabindex": "1",
                "title": "Personalized Gifts for Home + Bar",
                "text": "HOME + BAR"
            },
            {
                "element": "div",
                "class": "menu has-submenu",
                "contents": [ 
                    {
                    "element": "div",
                    "class": "secondaryMenu",
                    "contents": [
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header",
                                            "href": "/new-arrivals-for-home-bar/category/new-arrivals-for-home-bar",
                                            "text": "New Arrivals"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/outdoor-travel-homebar/category/outdoor-travel-homebar",
                                            "text": "Outdoor + Travel"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-the-beach/category/beach-fun",
                                                    "text": "Beach Towels + Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-bbq-accessories/category/bbq-accessories",
                                                    "text": "Grilling Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-insulated-totes/category/insulated-bags",
                                                    "text": "Insulated Bags"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-luggage-tags-business-card-cases/category/luggage-tags",
                                                    "text": "Luggage Tags"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-acrylic-outdoor-drinkware/category/outdoor-entertaining",
                                                    "text": "Outdoor Entertaining"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-travel-mugs/category/travel-mugs-water-bottles",
                                                    "text": "Travel Mugs + Water Bottles"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/barware-for-homebar/category/barware-for-homebar",
                                            "text": "Barware"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-bar-accessories-tools/category/bar-accessories",
                                                    "text": "Bar Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-bar-whisky-glasses/category/bar-glasses",
                                                    "text": "Bar Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-beer-gifts/category/beer-glasses",
                                                    "text": "Beer Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-champagne-flutes/category/champagne-flutes",
                                                    "text": "Champange Flutes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-coasters/category/coasters",
                                                    "text": "Coasters"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-decanters-carafes/category/decanters",
                                                    "text": "Decanters"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-flasks/category/flasks",
                                                    "text": "Flasks"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-ice-buckets-drink-coolers/category/ice-buckets-drink-coolers",
                                                    "text": "Ice Buckets + Coolers"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-bottle-openers-wine-stoppers/category/bottle-openers-wine-stoppers",
                                                    "text": "Openers + Wine Stoppers"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-shot-glasses/category/shot-glasses",
                                                    "text": "Shot Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-whiskey-glasses/category/whiskey-glasses",
                                                    "text": "Whiskey Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-wine-glasses/category/wine-glasses",
                                                    "text": "Wine Glasses"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/homebar/category/homebar",
                                            "text": "Home"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-door-knockers/category/door-knockers",
                                                    "text": "Door Knockers"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-picture-frames/category/frames",
                                                    "text": "Frames"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-games/category/games",
                                                    "text": "Games"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-clocks/category/home-clocks",
                                                    "text": "Home Clocks"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/keepsakes-figurines-for-kids/category/keepsakes-figurines-for-kids",
                                                    "text": "Keepsakes + Figurines"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-photo-albums/category/photo-albums",
                                                    "text": "Photo Albums"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-shadow-boxes/category/shadow-boxes",
                                                    "text": "Shadow Boxes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-snow-globes/category/snow-globes",
                                                    "text": "Snow Globes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/table-top-decor/category/table-top-decor",
                                                    "text": "Table Top Decor"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-throws-pillows/category/throws-pillows",
                                                    "text": "Throws + Pillows"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-candle-holders-vases/category/vases",
                                                    "text": "Vases"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-wall-art-gifts/category/wall-art",
                                                    "text": "Wall Art"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/kitchen-homebar/category/kitchen-homebar",
                                            "text": "Kitchen + Dining"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-coffee-mugs-tea-cups/category/coffee-mugs-tea-cups",
                                                    "text": "Coffee Mugs + Tea Cups"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-containers-canisters/category/containers-canisters",
                                                    "text": "Containers + Canisters"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-cutting-boards/category/cutting-boards",
                                                    "text": "Cutting Boards"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-pitchers-carafes/category/pitchers-carafes",
                                                    "text": "Pitchers + Carafes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-plates-glassware/category/plates-glasses",
                                                    "text": "Plate + Glasses"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-plates-glassware/category/plates-glasses",
                                                    "text": "Serving Trays + Platters"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/home-office-homebar/category/home-office-homebar",
                                            "text": "Home Office"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-business-briefcases-totes/category/business-briefcases-totes",
                                                    "text": "Breifcases + Totes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-clocks/category/home-clocks",
                                                    "text": "Clocks"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-desk-accessories/category/desk-accessories",
                                                    "text": "Desk Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-globe-gifts/category/globes",
                                                    "text": "Globes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-padfolios-journals/category/padfolios-journals",
                                                    "text": "Padfolios + Journals"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-paperweights/category/paperweights",
                                                    "text": "Paperweights"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-pens-pencils/category/pens-pencils",
                                                    "text": "Pens + Pencils"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
            }
        ]
    },

    {
        "element": "li",
        "contents" : [
            {
                "element": "a",
                "class": "primaryMenuItem",
                "href": "/personalized-wedding-gifts",
                "tabindex": "1",
                "title": "Personalized Wedding Gifts",
                "text": "WEDDING"
            },
            {
                "element": "div",
                "class": "menu has-submenu",
                "contents": [
                    {
                    "element": "div",
                    "class": "secondaryMenu",
                    "contents": [
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/day-of-wedding/category/day-of-wedding",
                                            "text": "Wedding Day"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-toasting-flutes/category/toasting-flutes",
                                                    "text": "Toasting Flutes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-cake-servers/category/cake-servers",
                                                    "text": "Cake Knives + Server Sets"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-wedding-guest-books-favors-accessories/category/guest-book-favors-accessories",
                                                    "text": "Guestbook + Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-unity-candles-cake-toppers/category/unity-items",
                                                    "text": "Untiy Items"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/milestone-wedding/category/milestone-wedding",
                                            "text": "Milestones"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engagement-gifts/category/engagement",
                                                    "text": "Engagement"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-bridal-shower-gifts/category/bridal-shower-gifts",
                                                    "text": "Bridal Shower"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-honeymoon-gifts/category/honeymoon-gifts",
                                                    "text": "Honeymoon Gifts"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-anniversary-gifts",
                                                    "text": "Anniversary"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/participants-wedding/category/participants-wedding",
                                            "text": "Wedding Party"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-bride-gifts/category/for-the-bride",
                                                    "text": "Bride"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-groom/category/for-the-groom",
                                                    "text": "Groom"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/gifts-for-bridesmaids-wedding/category/gifts-for-bridesmaids-wedding",
                                                    "text": "Bridesmaids"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/gifts-for-groomsmen-wedding/category/gifts-for-groomsmen-wedding",
                                                    "text": "Groomsmen"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/gifts-for-the-bridal-party/category/for-the-bridal-party",
                                                    "text": "Bridal Party"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-wedding-gifts-for-parents/category/for-the-parents",
                                                    "text": "Parents"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-flower-girl-gifts/category/for-the-flower-girl",
                                                    "text": "Flower Girl"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-ring-bearer-gifts/category/for-the-ring-bearer",
                                                    "text": "Ring Bearer"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-wedding-officiant-helpers/category/for-the-wedding-helpers-officiants",
                                                    "text": "Wedding Helper + Officiants"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/bride-groom-wedding/category/bride-groom-wedding",
                                            "text": "For The Couple"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/top-wedding-gifts/category/top-wedding-gifts",
                                                    "text": "Top Wedding Gifts"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/cut-crystal-glassware/category/cut-crystal-drinkware",
                                                    "text": "Crystal Glassware"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/wedding-drinkware/category/wedding-drinkware",
                                                    "text": "Wedding Drinkware"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-wedding-picture-frames/category/wedding-frames-photo-gifts",
                                                    "text": "Wedding Frames + Photo Gifts"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/wedding-gifts-for-the-home/category/wedding-home-gifts",
                                                    "text": "Wedding Home Gifts"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-wedding-keepsakes/category/wedding-keepsakes",
                                                    "text": "Wedding Keepsakes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/wedding-anniversary-snow-globes/category/wedding-anniversary-snow-globes",
                                                    "text": "Wedding Snow Globes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/wedding-anniversary-throws/category/wedding-anniversary-throws",
                                                    "text": "Wedding Throws"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-candle-holders-vases/category/vases",
                                                    "text": "Wedding Vases"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/brands-for-wedding/category/brands-for-wedding",
                                            "text": "Brands We Love"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-luigi-bormioli-glassware/category/luigi-bormioli",
                                                    "text": "Luigi Bormioli"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-mariposa-gifts/category/mariposa-collection",
                                                    "text": "Mariposa Collection"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-riedel-drinkware/category/riedel-drinkware",
                                                    "text": "Riedel Drinkware"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/vera-wang-wedgewood-collection/category/vera-wang-wedgewood-collection",
                                                    "text": "Vera Wang Wedgewood"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-waterford-crystal-gifts/category/waterford-collection",
                                                    "text": "Waterford Collection"
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/bridal-catalog",
                                            "text": "Wedding Catalog"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/bridal-catalog",
                                                    "text": "View Catalog"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/wedding-catalog-email-signup",
                                                    "text": "Request A Catelog"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
            }
        ]
    }, 

    {
        "element": "li",
        "contents" : [
            {
                "element": "a",
                "class": "primaryMenuItem",
                "href": "/personalized-business-gifts",
                "tabindex": "1",
                "title": "Shop Business",
                "text": "BUSINESS"
            },
            {
                "element": "div",
                "class": "menu has-submenu",
                "contents": [
                    {
                    "element": "div",
                    "class": "secondaryMenu",
                    "contents": [
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/gifts-for-business/category/gifts-for-business",
                                            "text": "Business Gifts"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/top-business-gifts/category/top-business-gifts",
                                                    "text": "Top Business Gifts"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-business-card-holders-cases/category/business-card-holders-cases",
                                                    "text": "Business Card Holders"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/embroidered-corporate-apparel/category/corporate-apparel",
                                                    "text": "Coporate Apparel"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-crystal-gifts/category/crystal-business-gifts",
                                                    "text": "Crystal Business Gifts"
                                                },

                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-desk-accessories/category/desk-accessories",
                                                    "text": "Desk Accessories"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-name-plates-tags/category/engraving-plates-name-tags",
                                                    "text": "Engraving Name Plates"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-fine-writing-instruments/category/fine-writing-instruments",
                                                    "text": "Fine Writing Instruments"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/business-picture-frames/category/business-frames-albums",
                                                    "text": "Frames + Albums"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-motivational-office-decor/category/motivational-office-decor",
                                                    "text": "Motivational Office Decor"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-padfolios-journals/category/padfolios-journals",
                                                    "text": "Padfolios + Journals"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-paperweights/category/paperweights",
                                                    "text": "Paperweights"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-pens-pencils/category/pens-pencils",
                                                    "text": "Pens + Penicls"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-professional-drinkware/category/professional-drinkware",
                                                    "text": "Professional Drinkware"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/recognition-business/category/recognition-business",
                                            "text": "Recognition Gifts"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-plaques-awards/category/awards",
                                                    "text": "Awards"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-business-clocks/category/business-clocks",
                                                    "text": "Clocks"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-globe-gifts/category/globes",
                                                    "text": "Globes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-plaques-awards/category/plaques",
                                                    "text": "Plaques"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-vases-for-office/category/vases-for-office",
                                                    "text": "Vases"
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/travel-business/category/travel-business",
                                            "text": "Travel"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-business-briefcases-totes/category/business-briefcases-totes",
                                                    "text": "Breifcases + Totes"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-business-luggage-tags/category/business-luggage-tags",
                                                    "text": "Luggage Tags"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-padfolios-journals/category/padfolios-journals",
                                                    "text": "Padfolios + Journals"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-pens-pencils/category/pens-pencils",
                                                    "text": "Pens + Pencils"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-travel-mugs/category/travel-mugs-water-bottles",
                                                    "text": "Travel Mugs + Water Bottles"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/recipients-business/category/recipients-business",
                                            "text": "Recipients"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-office-gifts-for-her/category/business-gifts-for-her",
                                                    "text": "Business Gifts For Her"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-business-gifts-for-him/category/business-gifts-for-him",
                                                    "text": "Business Gifts For Him"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-client-gifts/category/gifts-for-clients",
                                                    "text": "Gifts For Clients"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-coworkers/category/gifts-for-coworkers",
                                                    "text": "Gifts For Coworkers"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-gifts-for-boss/category/for-the-boss",
                                                    "text": "Gifts For The Boss"
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/occasions-business/category/occasions-business",
                                            "text": "Occasions"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-administrative-professional-gifts/category/administrative-professional-gifts",
                                                    "text": "Administrative Professionals"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-boss-day-gifts/category/boss-day",
                                                    "text": "Boss Day"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/employee-recognition-gifts-for-years-of-service/category/employee-appreciation-gifts",
                                                    "text": "Employee Application"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-new-job-gifts/category/new-job-gifts",
                                                    "text": "New Job"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/inspirational-cards/category/inspirational-cards",
                                                    "text": "Office Stationary"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-retirement-gifts/category/top-retirement-gifts",
                                                    "text": "Retirement"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/name-brands-business/category/name-brands-business",
                                            "text": "Business Brands"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-aurora-pens/category/aurora-pens",
                                                    "text": "Aurora"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/bulova-clocks-watches/category/bulova-clocks-watches",
                                                    "text": "Bulova"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/citizen-clocks/category/citizen-clocks",
                                                    "text": "Citizen"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-conklin-pens/category/conklin-pens",
                                                    "text": "Conklin"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/contigo-mugs-bottles/category/contigo",
                                                    "text": "Contigo"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/corkcicle/category/corkcicle-drinkware",
                                                    "text": "Corkcicle"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/cross-pens/category/cross-pens",
                                                    "text": "Cross"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-faber-castell-pens/category/faber-castell-pens",
                                                    "text": "Faber-Castell"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/fossil/category/fossil",
                                                    "text": "Fossil"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-howard-miller-clocks/category/howard-miller-clocks",
                                                    "text": "Howard Miller"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-lamy-pens/category/lamy",
                                                    "text": "Lamy"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-reflections-pens/category/reflections-pens",
                                                    "text": "Reflections"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/engraved-visconti-pens/category/visconti-pens",
                                                    "text": "Visconti"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/personalized-waterman-pens/category/waterman-pens",
                                                    "text": "Waterman"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/ystudio-pens/category/ystudio-pens",
                                                    "text": "ystudio"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/business-accounts",
                                            "text": "Business Class"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/business-account-specialists#LargeOrder",
                                                    "text": "Large Volume Discounts"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/your-company-logo-our-gifts",
                                                    "text": "Free Logo"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/request-contact",
                                                    "text": "Contact Us"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/business-account-specialists",
                                                    "text": "FAQ"
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem header has-submenu",
                                            "href": "/thingsremembered-business-catalog",
                                            "text": "Business Catalog"
                                        },
                                        {
                                            "element": "div",
                                            "class": "tertiaryMenu",
                                            "contents":[
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/thingsremembered-business-catalog",
                                                    "text": "View Catalog"
                                                },
                                                {
                                                    "element": "a",
                                                    "class": "tertiaryMenuItem",
                                                    "href": "/business-catalog-email-signup",
                                                    "text": "Request A Catelog"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
            }
        ]
    }, 

    {
        "element": "li",
        "contents" : [
            {
                "element": "a",
                "class": "primaryMenuItem",
                "href": "/semi-annual-sale",
                "tabindex": "1",
                "title": "Sale",
                "text": "SALE"
            },
            {
                "element": "div",
                "class": "menu has-submenu",
                "contents": [
                    {
                    "element": "div",
                    "class": "secondaryMenu",
                    "contents": [
                        {
                            "element": "ul",
                            "contents": [
                                {
                                    "element": "li",
                                    "class": "tertiaryHeader",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem",
                                            "href": "/semi-annual-sale-gifts-for-her/category/semi-annual-sale-gifts-for-her",
                                            "text": "Her"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "class": "tertiaryHeader",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem",
                                            "href": "/semi-annual-sale-gifts-for-him/category/semi-annual-sale-gifts-for-him",
                                            "text": "Him"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "class": "tertiaryHeader",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem",
                                            "href": "/sale-baby-kids/category/sale-baby-kids",
                                            "text": "Baby + Kids"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "class": "tertiaryHeader",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem",
                                            "href": "/semi-annual-sale-gifts-for-home-bar/category/semi-annual-sale-gifts-for-home-bar",
                                            "text": "Home + Bar"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "class": "tertiaryHeader",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem",
                                            "href": "/sale-gifts-wedding/category/sale-gifts-wedding",
                                            "text": "Wedding"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "class": "tertiaryHeader",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem",
                                            "href": "/office-on-sale/category/office-on-sale",
                                            "text": "Business"
                                        }
                                    ]
                                },
                                {
                                    "element": "li",
                                    "class": "tertiaryHeader",
                                    "contents":[
                                        {
                                            "element": "a",
                                            "class": "secondaryMenuItem",
                                            "href": "/clearance-items/category/clearance-items",
                                            "text": "Clearance"
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
            }
        ]
    }
]