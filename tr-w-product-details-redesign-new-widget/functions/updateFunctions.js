//This method updates the selection value for the options wiht single option values.
			updateSingleSelection: function (selectedOptionID) {
				var variantOptions = this.variantOptionsArray();
				for (var i = 0; i < variantOptions.length; i++) {
					var optionId = variantOptions[i].optionId;
					if (variantOptions[i].countVisibleOptions() == 1 && variantOptions[i].selectedOption() == undefined && optionId != selectedOptionID) {
						var isValidForSingleSelection = this.validForSingleSelection(optionId);
						var optionValues = variantOptions[i].originalOptionValues();
						for (var j = 0; j < optionValues.length; j++) {
							if (optionValues[j].visible() == true) {
								variantOptions[i].selectedOption(optionValues[j]);
								break;
							}
						}
					}
				}
			},

			updateQuantity: function () {
				item.updatableCustomQuantity(this.itemQty());
			},