//this method is triggered whenever there is a change to the selected option.
			filterOptionValues: function (selectedOptionId) {
				if (this.filtered()) {
					return;
				}
				var variantOptions = this.variantOptionsArray();
				for (var i = 0; i < variantOptions.length; i++) {
					var currentOption = variantOptions[i];
					var matchingSkus = this.getMatchingSKUs(variantOptions[i].optionId);
					// var optionValues = this.updateOptionValuesFromSku(matchingSkus, selectedOptionId, currentOption);
					// variantOptions[i].optionValues(optionValues);
					this.filtered(true);
				}
				this.updateSingleSelection(selectedOptionId);
			},