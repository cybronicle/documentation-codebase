addPersonalizationItemToCart: function (productId, index) {
    var a = {};
    ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_GET_PRODUCT, a, function (product) {
        //var product = products[i];

        var quantity = 1;
        var t = [];
        var result = {};
        var skuItem = product.childSKUs[0];
        for (var variant in product.productVariantOptions) {
            t.push({
                optionName: product.productVariantOptions[variant].optionName,
                optionValue: skuItem[product.productVariantOptions[variant].optionName],
                optionId: product.productVariantOptions[variant].optionId,
                optionValueId: skuItem.dynamicPropertyMapLong[product.productVariantOptions[variant].mapKeyPropertyAttribute]
            });
        }

        result = {
            selectedOptions: t
        };


        var s = $.extend(true, {}, product, result);
        s.childSKUs = [skuItem], s.orderQuantity = parseInt(quantity, 10);
        $.Topic(pubsub.topicNames.CART_ADD).publishWith(s, [{
            message: "success"
        }]);
    }, function (output) {}, productId);
},