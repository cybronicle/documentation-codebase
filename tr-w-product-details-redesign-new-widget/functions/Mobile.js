


			checkIsMobile: function (viewportWidth) {
				var widget = this;

				if (viewportWidth > 991) {
					widget.isMobile(false);
				} else if (viewportWidth <= 991) {
					widget.isMobile(true);
				}
			},
			mobileImageZoomEvents: function () {
				$("#prodDetails-mobileCarousel").on("click", ".item.active", function () {
					var imgUrl = $(this).find("img").attr("src");
					$("#mobileImageZoomedImg").attr("src", imgUrl.replace(/512/g, "712"));
					$("#imageMobileZoomModal").modal("show");
				});
			},