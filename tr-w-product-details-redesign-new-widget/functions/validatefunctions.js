// this method validates if all the options of the product are selected before allowing
			// add to space. Unlike validateAddToCart, however, it does not take into account inventory.
			validateAddToSpace: function () {
				var allOptionsSelected = true;
				if (this.variantOptionsArray().length > 0) {
					var variantOptions = this.variantOptionsArray();
					for (var i = 0; i < variantOptions.length; i++) {
						if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
							allOptionsSelected = false;
							break;
						}
					}
					if (allOptionsSelected) {
						// get the selected sku based on the options selected by the user
						var selectedSKUObj = this.getSelectedSku(variantOptions);


						if (selectedSKUObj === null) {
							return false;
						}
					}
				}

				// get quantity input value
				var quantityInput = this.itemQty();
				if (quantityInput.toString() != "") {
					if (!quantityInput.toString().match(/^\d+$/) || Number(quantityInput) < 0) {
						return false;
					}
				}

				var addToSpaceButtonFlag = allOptionsSelected;
				if (!addToSpaceButtonFlag) {
					$('#cc-prodDetailsAddToSpace').attr("aria-disabled", "true");
				}

				return addToSpaceButtonFlag;
			},

			//check whether all the variant options are selected and if so, populate selectedSku with the correct sku of the product.
			//this is generic method, can be reused in validateAddToSpace and validateAddToCart in future
			validateAndSetSelectedSku: function (refreshRequired) {
				var allOptionsSelected = true;
				if (this.variantOptionsArray().length > 0) {
					var variantOptions = this.variantOptionsArray();
					for (var i = 0; i < variantOptions.length; i++) {
						if (!variantOptions[i].selectedOption.isValid() && !variantOptions[i].disable()) {
							allOptionsSelected = false;
							this.selectedSku(null);
							break;
						}
					}
					if (allOptionsSelected) {
						// get the selected sku based on the options selected by the user
						var selectedSKUObj = this.getSelectedSku(variantOptions);
						if (selectedSKUObj === null) {
							return false;
						}
						this.selectedSku(selectedSKUObj);
					}
					if (refreshRequired) {
						this.refreshSkuData(this.selectedSku());
					}
				}
				return allOptionsSelected;
            },
            // this method validated if all the options of the product are selected
			validateAddToCart: function () {

				var AddToCartButtonFlag = this.allOptionsSelected() && this.stockStatus() && this.quantityIsValid();
				if (!AddToCartButtonFlag) {
					$('#cc-prodDetailsAddToCart').attr("aria-disabled", "true");
				}


                
				return AddToCartButtonFlag;

            },
            
            quantityIsValid: function () {
				var cartQty = 0;
				cartQty = Number($('#CC-shoppingCart-productQuantity-' + this.product().id() + this.product().repositoryId()).text());
				return this.itemQty() > 0 && parseInt(this.itemQty()) + cartQty <= this.stockAvailable();
            },
            
            //This method returns true if the option passed is the only one not selected
			//and all other options are either selected or disabled.
			validForSingleSelection: function (optionId) {
				var variantOptions = this.variantOptionsArray();
				for (var j = 0; j < variantOptions.length; j++) {
					if (variantOptions[j].disable() || (variantOptions[j].optionId != optionId && variantOptions[j].selectedOption() != undefined)) {
						return true;
					}
					if (variantOptions[j].optionId != optionId && variantOptions[j].selectedOption() == undefined && variantOptions[j].countVisibleOptions() == 1) {
						return true;
					}
				}
				return false;
			},

			function isSelectedBomArrayValid(widget) {
				return widget.bomArray().length == widget.selectedBomArray().length &&
					widget.selectedBomArray().every(function (selectedBomItem) {
						return selectedBomItem.isInStock() == true;
					});
			}