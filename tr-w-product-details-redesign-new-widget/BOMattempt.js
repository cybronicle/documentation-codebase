/*
going through, You can access the variant based on product.childSKUs()
tr_billOfMaterial can be found in the zones of each childSKU. 

CSS classes for each birthstone (12) and ring size ()
633341,633354,633367,633370,633383,633396,633406,633419,633422,633435,633448,633451

var widget.bomArray =
    {
        bom_variant_arr: [
            0 : {
                    slot_bom: [
                        0 : {
                            skuId: referencedComponentGroup.sku(),
                            name: referencedComponentGroup.name(),
                            imgurl: "/file/products/" + referencedComponentGroup.sku() + ".jpg",
                            slot: "slot" + i + 1,
                            prodsku: widget.product().id
                        }
                    ]
                }
        ]
    }

var widget.skuBOM[sku_id] = 
    { 
        bom_variant_arr: [
            0 : {
                    slot_bom: [
                        0 : {
                            skuId: referencedComponentGroup.sku(),
                            name: referencedComponentGroup.name(),
                            imgurl: "/file/products/" + referencedComponentGroup.sku() + ".jpg",
                            slot: "slot" + i + 1,
                            prodsku: widget.product().id
                        }
                    ]
                }
        ]
    }

*/

function billOfMaterialAjax(){
    // product being rendered in from OCC
    var productOCC = widget.product();

    //all the parent product's childSKUs
    var productChildSkus = productOCC.childSKUs();

    //used to hold the index of the child product
    var currentChildProduct;

    //used to hold the kets for referencedComponentGroup
    var componentGroupReferences = [];

    var slot_bom = [];

    // loop used to find the correct child product selected and set that index
    for(i = 0; i < productChildSkus.length; i++){
        if (productChildSkus[i].repositoryId() == product_obj.id){
            currentChildProduct = i;
            break;
        }
    }
    
    //used to grab the specific child skus' BOM. currentChildProduct needs to be set
    var billOfMaterial = JSON.parse(productChildSkus[currentChildProduct].tr_billOfMaterialJson());
    var displayNames = [];
    //used to grab the child products BOM referenced component group
    var referencedComponentGroup = billOfMaterial.referencedComponentGroups;
    console.log("referencedComponentGroup: ", referencedComponentGroup);
    //got child sku and its tr_BOM, needs to be fully created 
    for(i = 0; i < billOfMaterial.slots.length; i++){
        componentGroupReferences.push(billOfMaterial.slots[i].componentGroupReference);
        displayNames.push(billOfMaterial.slots[i].displayName);
    }

    //fill out the dummy objects
    for(i=0; i < componentGroupReferences.length; i++){
        for(j=0; j < referencedComponentGroup[componentGroupReferences[i]].length; j++){
            var obj = {};

            obj.skuid = referencedComponentGroup[componentGroupReferences[i]][j].sku;

            obj.name = referencedComponentGroup[componentGroupReferences[i]][j].description;

            obj.imgurl = "/file/products/" + obj.skuid + ".jpg";
            
            obj.slot = "slot" + (i+1);

            obj.displayName = displayNames[i];
            
            obj.prodsku = sku_id;

            slot_bom.push(obj);
        }
    }
    //fills bom_variant_arr with slot_bom
    bom_variant_arr[bom_variant_arr.length] = slot_bom;

    //populating empty bomArray with bom_variant_arr
    widget.bomArray(bom_variant_arr);

    //populates widget skuBOM with parent sku
    widget.skuBOM[sku_id] = bom_variant_arr;

    var owl = $(".bom-row.owl-carousel")[$(".bom-row.owl-carousel").length - 1]

    //sets up bom carousel
    $(".bom-row.owl-carousel").each(function() {
        $(this).owlCarousel({
            items: 5,
            pagination: false,
            navigation: true,
            navigationText: false,
            rewindNav: false,
            itemsMobile: [479, 5],
            itemsDesktop: [1199, 5],
            itemsDesktopSmall: [980, 5],
            itemsTablet: [768, 5],
            itemsTabletSmall: false
        });
    });
}