addPersonalizationToCart: function () {
    var productIds = [];
    var productIdsList = {};
    var zoneCookie = _this.getCookie("zoneProduct");
    var zoneData = {};
    if (zoneCookie != '') {
        zoneData = $.parseJSON(zoneCookie);
        _this.setCookie("zoneProduct", "");
    } else {
        productIds = ['194071', '574015', '336476', '607652', '574015', '587031'];
    }
    for (var key in zoneData) {
        var data = $.parseJSON(zoneData[key]);

        for (var key1 in data) {
            var data1 = data[key1];
            if ($.isArray(data1)) {
                productIds.concat(data1);
            } else {
                productIds.push(data1);
            }
        }
    }

    //productIdsList[CCConstants.PRODUCT_IDS] = productIds;

    var delay = 0;
    for (var i in productIds) {
        var productId = productIds[i];
        setTimeout(_this.addPersonalizationItemToCart.bind(null, productId, i), delay);
        delay = delay + 1000;
    }
},