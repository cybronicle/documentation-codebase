Variables used in template: 
    - giftModalData
    - videoURL
    - variantOptionsArray
    - otherStores
    - myStore
    - itemQty
    - selectedSku (commented out)
    - disableOptions
    - isAddToCartClicked
    - imgGroups
    - activeImgIndex
    - listPrice
    - salePrice
    - imgMetadata
    - isMobile
    - availableTodayXs
    - otherStore
    - availableTodayStore
    - availableCity
    - availableState
    - availableQuantity
    - buttonVisibilityFlag
    - personalizeVisibilityFlag
    - personalizeDisabledFlag
    - uiDisplayAttr
    - isForUpdating
    - checkPersonalization
    - bomArray
    - giftTitle
    - occasionsArr
    - deliveryDate
    - virtual
    - bwyFlag
    - airFlag
    - two_ndFlag
    - prmFlag
    - bwyFlagDate
    - airFlagDate
    - two_ndFlagDate
    - prmFlagDate
    - availableToday
    - availableDateText
    - displayOpus
    - displaySts
    - displayShipToHome
    - displayOnlyWarning
    - displayWarningMsg
    - stsStore
    - stsAvailableText
    - stsDisable
    - skuSts
    - backOrderFlag
    - backOrderMessage
    - standardDeliveryDate
    - seoReviews
    - deliveryDateHeadingFlag
    - giftText
    - selectedBomArray
    - prodTitleNew
    - skuidNew
    - skulistPriceNew
    - skusalePriceNew
    - opusDisabled
    - productGroups
    - products
    - spanClass
    - relatedProductGroups
    - relatedProducts
    - onSelectBOMElement (function, calls isSelectedBomArrayVaild)


Variables not used in template: 
    - availableTodayVisibility
    - stockStatus
    - showStockStatus
    - itemQuantity
    - stockAvailable
    - isSkuSelected
    - priceRange
    - filtered
    - WIDGET_ID
    - containerImage
    - mainImgUrl
    - viewportWidth
    - skipTheContent
    - personalizationCost
    - backLinkActive
    - variantName
    - variantValue
    - skuBackorderFlag
    - listingVariant
    - shippingSurcharge
    - prodAvlFlag
    - itemWisePersonalizationCostArr
    - scene7BaseUrl
    - occasionCode
    - storeCode
    - urlObjArr
    - skuBOM
    - bomPrice
    - bomTotalPrice
    - showSWM
    - isAddToSpaceClicked
    - disableAddToSpace
    - spaceOptionsArray
    - spaceOptionsGrpMySpacesArr
    - spaceOptionsGrpJoinedSpacesArr
    - mySpaces
    - siteFbAppId
    - isSFS
    - sfsStoreId
    - skuRepoId
    - skuOpus
    - skuData
    - pageStoreLat
    - pageStoreLon
    - pageStore
    - aggregateRating
    - giftData
    - giftDisplayName
    - hasGift
    - personaliztionObj
    - isPhotoUpload
    - finalBackOrderFlag
    - dcQuantity
    - isConfigReady
    - backOrderFlag
    - DCInventory
    - maxQuantityStore
    - availableSTHInventory
    - mobileDefaultOccassionId
    - mobileDefaultOccassionIndex
    - storeStockErrorMessage
    - itemsPerRowInLargeDesktopView
    - itemsPerRowInDesktopView
    - itemsPerRowInTabletView
    - itemsPerRowInPhoneView
    - itemsPerRow
    - viewportWidth
    - viewportMode
    - isDeskTopMode
    - activeProdIndex
    - numberOfRelatedProductsToShow
    - numberOfRelatedProducts
    - relatedProdsloaded
    - isSelectedBomArrayValid (function, is called by a onSelectBOMElement that is used)

Variables used in JS:
    - videoUrl
    - availableTodayVisibility
    - stockStatus
    - showStockStatus
    - variantOptionsArray
    - otherStores
    - myStore
    - itemQty
    - stockAvailable
    - selectedSku
    - disableOptions
    - priceRange
    - filtered
    - WIDGET_ID
    - isAddToCartClicked
    - imgGroups
    - mainImgUrl
    - activeImgIndex
    - viewportWidth
    - skipTheContent
    - listPrice
    - salePrice 
    - personalizationCost (used but is never changed to anything other than 0)
    - backLinkActive
    - variantName
    - variantValue
    - skuBackorderFlag
    - listingVariant
    - shippingSurcharge
    - imgMetadata
    - isMobile
    - availableTodayStore
    - availableCity
    - availableState
    - availableQuantity
    - buttonVisibilityFlag
    - personalizeVisibilityFlag
    - personalizeDisabledFlag
    - prodAvlFlag
    - uiDisplayAttr
    - scene7BaseUrl
    - occasionCode
    - checkPersonalization
    - storeCode
    - urlObjArr
    - skuBOM
    - bomArray
    - giftTitle
    - showSWM
    - isAddToSpaceClicked
    - disableAddToSpace
    - spaceOptionsArray
    - spaceOptionsGrpMySpacesArr
    - spaceOptionsGrpJoinedSpacesArr
    - mySpaces
    - siteFbAppId
    - occasionsArr
    - deliveryDate
    - virtual
    - bwyFlag
    - airFlag
    - two_ndFlag
    - prmFlag
    - bwyFlagDate
    - airFlagDate
    - two_ndFlagDate
    - prmFlagDate
    - availableToday
    - displayOpus
    - displaySts
    - isSFS
    - sfsStoreId
    - displayShipToHome
    - stsAvailableText ( 1 occurance ) 
    - skuRepoId
    - skuOpus 
    - skuSts
    - skuData
    - pageStoreLat
    - pageStoreLon
    - pageStore
    - backOrderFlag 
    - backOrderMessage ( 1 occurance )
    - standardDeliveryDate
    - deliveryDateHeadingFlag
    - giftText
    - giftData
    - giftDisplayName
    - hasGift
    - personaliztionObj
    - isPhotoUpload
    - finalBackOrderFlag (1 uncommented occurance but is also unset)
    - dcQuantity(1 occuance)
    - selectedBomArray 
    - DCInventory
    - maxQuantityStore ( 1 occurance )
    - availableSTHInventory
    - mobileDefaultOccassionId
    - mobileDefaultOccassionIndex
    - prodTitleNew
    - skuidNew
    - skulistPriceNew
    - skusalePriceNew
    - storeStockErrorMessage
    - opusDisabled ( 1 occurance, but is a flag and looks like correct usage )
    - itemsPerRowInLargeDesktopView
    - itemsPerRowInDesktopView
    - itemsPerRowInTabletView
    - itemsPerRowInPhoneView
    - itemsPerRow
    - viewportMode
    - isDeskTopMode
    - products ( not sure if used or if parameters are the same name)
    - spanClass
    - relatedProductGroups ( function )
    - relatedProducts
    - numberOfRelatedProductsToShow
    - numberOfRelatedProducts
    - checkPersonalizationExist ( function )
    - loadCarousel ( function )
    - giftboxModalShown ( function, 1 commented out occurance )
    - getCartInventoryDetailsArray ( function )

Variables not used in JS: 
    - giftModalData  
    - itemQuantity
    - isSkuSelected
    - containerImage
    - availableTodayXs
    - otherStore
    - itemWisePersonalizationCostArr
    - isForUpdating
    - bomPrice
    - bomTotalPrice
    - availableDateText ( commented out )
    - displayOnlyWarning
    - displayWarningMsg
    - stsStore
    - stsDisable
    - aggregateRating
    - seoReviews
    - isConfigReady
    - productGroups
    - activeProdIndex
    - relatedProdsloaded
    - resourcesLoaded ( function )
    - updateQuantity ( function )
    - handleMobileCheckout ( function )
    - onClickMoreStores ( function )

Variables not used in JS but in Template:
    - giftModalData ( used to check to see if greater than 0 )
    - availableTodayXs ( used for a visibility attribute value assigning )
    - isForUpdating ( used for if and ifnot checks )
    - availableDateText ( used for a if check and then set to text data-bind )
    - displayOnlyWarning ( only used for visibility value )
    - displayWarningMsg ( used for the text attribute based on displayOnlyWarning)
    - stsStore ( used for text attribute )
    - stsDisable ( used for a disable attribute value )
    - seoReviews ( used for a text attribute value )
    - updateQuantity ( used for 2 click events )
    - handleMobileCheckout ( used for a click event )
    - onClickMoreStores ( used for a click event ))

Variables not used in JS but in Template: 
    - itemQuantity
    - isSkuSelected
    - containerImage 
    - otherStore ( but otherStores is used )
    - itemWisePersonalizationCostArr
    - bomPrice
    - bomTotalPrice 
    - aggregateRating
    - isConfigReady
    - productGroups
    - activeProdIndex
    - relatedProdsloaded
    - resourcesLoaded