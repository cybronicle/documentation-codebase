getProductImages: function (size, ProductObj) {

    var imgArr = [];

    var productObj = ProductObj || this.product(),
        productId, alternateImgCount = 0;

    if (typeof productObj.AlternateImageCount !== "undefined") {
        alternateImgCount = parseInt(this.getActualValue(productObj.AlternateImageCount));
    } else if (typeof productObj.alternateImageCount !== "undefined") {
        alternateImgCount = parseInt(this.getActualValue(productObj.alternateImageCount));
    }

    productId = this.getActualValue(productObj.id);

    var zeroPaddedProdId = this.getZeroPaddedSkuId(productId);

    switch (size) {
        case "L":
            for (var j = 0; j < alternateImgCount; j++) {
                imgArr.push(_this.scene7BaseUrl + zeroPaddedProdId + "_" + (j + 1) + "_lg?wid=" + imageRes);
            }
            break;

        case "S":
            for (var k = 0; k < alternateImgCount; k++) {

                imgArr.push(_this.scene7BaseUrl + zeroPaddedProdId + "_" + (k + 1) + "_lg?wid=" + imageRes);

            }
            break;
    }

    return imgArr;
},
getZeroPaddedSkuId: function (productId) {


    var zeroPaddedProdId = "";


    for (var i = 0; i < (9 - productId.length); i++) {
        zeroPaddedProdId += "0";
    }
    zeroPaddedProdId += productId;


    return zeroPaddedProdId;
},
getActualValue: function (obj) {

    if (typeof obj === "function") {


        return obj();
    } else {
        return obj;
    }
},
/**
			 * Retrieve list of spaces for a user
			 */
			getSpaces: function (callback) {
				var widget = this;
				var successCB = function (result) {
					var mySpaceOptions = [];
					var joinedSpaceOptions = [];
					if (result.response.code.indexOf("200") === 0) {

						//spaces
						var spaces = result.items;
						spaces.forEach(function (space, index) {
							var spaceOption = {
								spaceid: space.spaceId,
								spaceNameFull: ko.observable(space.spaceName),
								spaceNameFormatted: ko.computed(function () {
									return space.spaceName + " (" + space.creatorFirstName + " " + space.creatorLastName + ")";
								}, widget),
								creatorid: space.creatorId,
								accessLevel: space.accessLevel,
								spaceOwnerFirstName: space.creatorFirstName,
								spaceOwnerLastName: space.creatorLastName
							};

							// if user created the space, add it to My Spaces, otherwise add it to Joined Spaces
							if (space.creatorId == swmRestClient.apiuserid) {
								mySpaceOptions.push(spaceOption);
							} else {
								joinedSpaceOptions.push(spaceOption);
							}
						});

						// sort each group alphabetically
						mySpaceOptions.sort(mySpacesComparator);
						joinedSpaceOptions.sort(joinedSpacesComparator);

						widget.spaceOptionsGrpMySpacesArr(mySpaceOptions);
						widget.spaceOptionsGrpJoinedSpacesArr(joinedSpaceOptions);

						var groups = [];
						var mySpacesGroup = {
							label: widget.translate('mySpacesGroupText'),
							children: ko.observableArray(widget.spaceOptionsGrpMySpacesArr())
						};
						var joinedSpacesGroup = {
							label: widget.translate('joinedSpacesGroupText'),
							children: ko.observableArray(widget.spaceOptionsGrpJoinedSpacesArr())
						};

						var createOptions = [];
						var createNewOption = {
							spaceid: "createnewspace",
							spaceNameFull: ko.observable(widget.translate('createNewSpaceOptText'))
						};
						createOptions.push(createNewOption);
						var createNewSpaceGroup = {
							label: "",
							children: ko.observableArray(createOptions)
						};

						groups.push(mySpacesGroup);
						groups.push(joinedSpacesGroup);
						groups.push(createNewSpaceGroup);
						widget.spaceOptionsArray(groups);
						widget.mySpaces(mySpaceOptions);

						if (callback) {
							callback();
						}
					}
				};
				var errorCB = function (resultStr, status, errorThrown) {};

				swmRestClient.request('GET', '/swm/rs/v1/sites/{siteid}/spaces', '', successCB, errorCB, {});
            },
            
            // this method  returns a map of all the options selected by the user for the product
			getSelectedSkuOptions: function (variantOptions) {
				var selectedOptions = [];

				for (var i = 0; i < variantOptions.length; i++) {
					if (!variantOptions[i].disable()) {
						selectedOptions.push({
							'optionName': variantOptions[i].optionDisplayName,
							'optionValue': variantOptions[i].selectedOption().key,
							'optionId': variantOptions[i].actualOptionId,
							'optionValueId': variantOptions[i].selectedOption().value
						});

					}
				}
				return selectedOptions;
			},
			getSelectedProductVariant: function (variantOptions) {
				var selectedOptions = [];
				for (var i = 0; i < variantOptions.length; i++) {
					if (!variantOptions[i].disable() && variantOptions[i].selectedOption()) {
						var tempOptionValue = variantOptions[i].selectedOption().key;
						if (tempOptionValue.indexOf("|") != -1) {
							tempOptionValue = tempOptionValue.split("|");
							tempOptionValue = tempOptionValue[0];
						}
						selectedOptions.push({
							'optionName': variantOptions[i].optionDisplayName,
							'optionValue': variantOptions[i].selectedOption().key,
							'optionId': variantOptions[i].actualOptionId,
							'optionValueId': variantOptions[i].selectedOption().value,
							'optionText': tempOptionValue
						});

					}
				}
				return selectedOptions;
            },
            //this method returns the selected sku in the product, Based on the options selected
			getSelectedSku: function (variantOptions) {
				var childSkus = this.product().product.childSKUs;
				var selectedSKUObj = {};
				if (variantOptions.length === 0) {
					return null;
				}
				try {
					for (var i = 0; i < childSkus.length; i++) {
						selectedSKUObj = childSkus[i];
						for (var j = 0; j < variantOptions.length; j++) {
							if (!variantOptions[j].disable() && childSkus[i].dynamicPropertyMapLong[variantOptions[j].optionId] != variantOptions[j].selectedOption().value) {
								selectedSKUObj = null;
								break;
							}
						}
						if (selectedSKUObj !== null) {
							// $.Topic('SKU_SELECTED').publish(this.product(), selectedSKUObj, variantOptions);
							return selectedSKUObj;
						}
					}
				} catch (e) {};
				return null;
            },
            
            // get all the matching SKUs
			getMatchingSKUs: function (optionId) {
				var childSkus = this.product().childSKUs();
				var matchingSkus = [];
				var variantOptions = this.variantOptionsArray();
				var selectedOptionMap = {};
				for (var j = 0; j < variantOptions.length; j++) {
					if (variantOptions[j].optionId != optionId && variantOptions[j].selectedOption() != undefined) {
						selectedOptionMap[variantOptions[j].optionId] = variantOptions[j].selectedOption().value;
					}
				}
				for (var i = 0; i < childSkus.length; i++) {
					var skuMatched = true;
					for (var key in selectedOptionMap) {
						if (selectedOptionMap.hasOwnProperty(key)) {
							if (!childSkus[i].dynamicPropertyMapLong[key] ||
								childSkus[i].dynamicPropertyMapLong[key]() != selectedOptionMap[key]) {
								skuMatched = false;
								break;
							}
						}
					}
					if (skuMatched) {
						matchingSkus.push(childSkus[i]);
					}
				}
				return matchingSkus;
            },
            getReviewStar: function () {
				var getStartInterval = setInterval(function () {
					if ($('.bv-rating-stars-container').length > 0) {
					    
					    if(($('.bv-rating-stars-container').attr('title').indexOf('Read 0 Reviews') === 0) == true){
					        $('.bv-rating-stars-container').attr('style','display:none !important');
					    }
					    else{
					       // if ($('.bv-rating-stars-container').find('.bv-off-screen').text().indexOf('No rating value') === 0) {
    						    $('.bv-rating-stars-container').click(function () {
    								$("#review-container").modal('show');
    							});
    							$('.bv-rating-label').click(function () {
    								$("#review-container").modal('show');
    							});
    							window.clearInterval(getStartInterval);
						  //  }
					    }
					    
					}
				}, 100);


            },
            getCartInventoryDetailsArray: function () {
				var widget = this;

				var itemShippingInventoryDetail = {};
				itemShippingInventoryDetail.quantity = 0;
				for (var i = 0; i < widget.cart().items().length; i++) {
					var item = widget.cart().items()[i];
					var trDeliveryDetails = JSON.parse(item.tr_delivery_details());
					if (item.catRefId === widget.skuRepoId()) {
						if (trDeliveryDetails.deliveryType === "2") {
							itemShippingInventoryDetail.storeId = "shipping";
							itemShippingInventoryDetail.quantity = Number(itemShippingInventoryDetail.quantity) + Number(item.quantity());
						} else {
							var found = false;
							for (var j = 0; j < cartInventoryDetailsArray.length; j++) {
								if (cartInventoryDetailsArray[j].storeId === trDeliveryDetails.store_inventory) {
									cartInventoryDetailsArray[j].quantity = cartInventoryDetailsArray[j].quantity + item.quantity();
									found = true;
									break;
								}
							}
							if (found) {
								continue;
							}
							var cartInventoryDetails = {};
							cartInventoryDetails.storeId = trDeliveryDetails.store_inventory;
							cartInventoryDetails.quantity = item.quantity();
							cartInventoryDetailsArray.push(cartInventoryDetails);
						}
					}
				}
				if (itemShippingInventoryDetail.hasOwnProperty('storeId')) {
					cartInventoryDetailsArray.push(itemShippingInventoryDetail);
				}
				return cartInventoryDetailsArray;
            },
            
            function getfallBackDatafromSku(widget){
                if(widget.product().childSKUs().length === 1){
                    widget.createInventorySpinner();
                    var skuId = "personalizationFallback_"+widget.product().childSKUs()[0].repositoryId().split("_")[0];
                    ccRestClient.authenticatedRequest('/ccstoreui/v1/products/'+skuId+'?fields=tr_personalizationJson', {},
                        function (data) {
                             if(data.hasOwnProperty('tr_personalizationJson')){
                                var data = JSON.parse(data.tr_personalizationJson);
                                isPersonalizationPropertyExist = data.zones.length;
                                if(data.zones.length > 0){
                                    newPersonalizationData = data;
                                    widget.generateExpressPhase(newPersonalizationData);
                                    widget.generateFonts(newPersonalizationData);
                                    widget.generalFonts(newPersonalizationData);
                                    widget.generateColorFills(newPersonalizationData);
                                    widget.generateDesigns(newPersonalizationData);
                                    widget.generateNographic(newPersonalizationData);
                                    widget.checkPersonalizationExist();
                                }else if($('.error-msg').is(':visible') === false){
                                    $('.tr-xs-personalize-block').removeClass('hide');
                                    $(".personalizationBtn").hide();
                                    $(".addToCartBtn").show();
                                    $(".personalizationBtns").removeClass('hide');
                                }
                                }
                                widget.destroyInventorySpinner();
                        },
                                function (data) {
    
                                });
                }else{
                    if(widget.selectedSku() !== null){
                        skuId = widget.selectedSku().repositoryId;
                        ccRestClient.authenticatedRequest('/ccstoreui/v1/products/'+skuId+'?fields=tr_personalizationJson', {},
                        function (data) {
                             if(data.hasOwnProperty('tr_personalizationJson')){
                                var data = JSON.parse(data.tr_personalizationJson);
                            isPersonalizationPropertyExist = data.zones.length;
                            if(data.zones.length > 0){
                                newPersonalizationData = data;
                                widget.generateExpressPhase(newPersonalizationData);
                                widget.generateFonts(newPersonalizationData);
                                widget.generalFonts(newPersonalizationData);
                                widget.generateColorFills(newPersonalizationData);
                                widget.generateDesigns(newPersonalizationData);
                                widget.generateNographic(newPersonalizationData);
                                widget.checkPersonalizationExist();
                            }else if($('.error-msg').is(':visible') === false){
                                $('.tr-xs-personalize-block').removeClass('hide');
                                $(".personalizationBtn").hide();
                                $(".addToCartBtn").show();
                                $(".personalizationBtns").removeClass('hide');
                            }
                            }
                            widget.destroyInventorySpinner();
                        },
                        function (data) {
    
                                });
                        }
                }
        }

        function getParametrFromUrl(urlParams, param) {
			for (var i = 0, lth = urlParams.length; i < lth; i++) {
				var oneParametr = urlParams[i];
				if (oneParametr.key == param) {
					return oneParametr.value;
				}
			}
			return "";
		}