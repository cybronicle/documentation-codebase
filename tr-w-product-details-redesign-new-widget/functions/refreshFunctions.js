
			//refreshes the prices based on the variant options selected
			refreshSkuPrice: function (selectedSKUObj) {
				if (selectedSKUObj === null) {
					if (this.product().hasPriceRange) {
						this.priceRange(true);
					} else {
						this.listPrice(this.product().listPrice());
						this.salePrice(this.product().salePrice());
						this.priceRange(false);
					}
				} else {
					this.priceRange(false);
					var skuPriceData = this.product().getSkuPrice(selectedSKUObj);
					this.listPrice(skuPriceData.listPrice);
					this.salePrice(skuPriceData.salePrice);
				}
			},

			//refreshes the stockstatus based on the variant options selected
			refreshSkuStockStatus: function (selectedSKUObj) {
				var key;
				if (selectedSKUObj === null) {
					key = 'stockStatus';
				} else {
					key = selectedSKUObj.repositoryId;
				}
				var stockStatusMap = this.product().stockStatus();
				for (var i in stockStatusMap) {
					if (i == key) {
						if (stockStatusMap[key] == 'IN_STOCK') {
							this.stockStatus(true);
							if (selectedSKUObj === null) {
								this.stockAvailable(1);
							} else {
								this.stockAvailable(selectedSKUObj.quantity);
							}
						} else {
							this.stockStatus(false);
							this.stockAvailable(0);
						}
						return;
					}
				}
			},

			refreshSkuData: function (selectedSKUObj) {
				this.refreshSkuPrice(selectedSKUObj);
				this.refreshSkuStockStatus(selectedSKUObj);
			},