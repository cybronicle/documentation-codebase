beforeAppear: function (page) {
				var widget = this;
				widget.giftTitle('');
				var opusGlobalOffFlag = site.getInstance().extensionSiteSettings['externalSiteSettings']['opusGlobalOffFlag'];
				if (opusGlobalOffFlag && opusGlobalOffFlag.toUpperCase() === "TRUE") {
					widget.opusDisabled(true);
				}
				widget.imgMetadata = [];
				widget.selectedBomArray([]);
				widget.bomArray([]);
				
				cartInventoryDetailsArray = [];
				processingTime = 0;
				currentPersonalizationData = [];
				var storeItemObj = storageApi.getInstance().getItem("storeItems");
				if (storeItemObj && storeItemObj !== '') {
					storeItems = JSON.parse(storeItemObj);
					window.storeItems = storeItems;
				}
				if (location.search.substr(1).indexOf("basketLineId") != -1) {
					$("#tr-w-product-details").addClass("hide");
				}

				$(window).on("hashchange", function () {
					if (window.location.hash === '') {
						$('body').removeClass("modal-open");
					}
				});

				if (location.search.substr(1).indexOf("basketLineId") == -1) {
					var e = document.getElementById("refreshBack");
					if (!e || e.value === "yes") {
						//location.reload();
					} else {
						e.value = "yes";
					}
				}

				inventoryObject = {};

				function detectIE() {
					var ua = window.navigator.userAgent;

					var msie = ua.indexOf('MSIE ');
					if (msie > 0) {
						// IE 10 or older => return version number
						return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
					}

					var trident = ua.indexOf('Trident/');
					if (trident > 0) {
						// IE 11 => return version number
						var rv = ua.indexOf('rv:');
						return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
					}

					var edge = ua.indexOf('Edge/');
					if (edge > 0) {
						// Edge (IE 12+) => return version number
						return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
					}

					// other browser
					return false;
				}

				var isIEbrowser = detectIE();
				if (isIEbrowser) {
					setTimeout(function () {
						if (location.search.substr(1).indexOf("basketLineId") != -1) {
							return;
						}
					}, 1500)
				} else {
					if (location.search.substr(1).indexOf("basketLineId") != -1) {
						return;
					}
				}


				document.onclick = function (event) {
					//$('#at15s').css('visibility','hidden');
				};

				widget.prodTitleNew(widget.product().displayName());
				widget.skuidNew(-1);
				widget.skulistPriceNew(0);
				widget.skusalePriceNew(0);
				widget.occasionCode('');
				if ($(window).width() < 767) {
					widget.createInventorySpinner();
				}
				$('#main').wrap('<div itemscope itemtype="http://schema.org/Product">');

				var reviewsClicked = sessionStorage.getItem('ReviewsClicked');

				var extraPixels;
				if ((navigator.userAgent.match(/iPhone/i))) {
					extraPixels = 1200;
				} else {
					extraPixels = 500;
				}

				if (reviewsClicked == 'True') {

					var checkExist = setInterval(function () {

						if ($('.bv-content-container').length) {

							$('#tr-w-product-details .tr-e-review>a').click();
							$('html, body').animate({
								scrollTop: $('.bv-content-container').position().top + extraPixels
							}, 'slow');
							sessionStorage.removeItem('ReviewsClicked');
							clearInterval(checkExist);

						} else {

							$('#tr-w-product-details .tr-e-review>a').click();
							$('html, body').animate({
								scrollTop: $('#reviews').position().top + extraPixels
							}, 'slow');
							sessionStorage.removeItem('ReviewsClicked');
							clearInterval(checkExist);

						}

					}, 100);

				}

				var widget = this;
				if ($(window).width() < 767) {
					widget.createInventorySpinner();
				}
				this.setCookie("personalizationProducts", "");
				this.setCookie("zoneProduct", "");
				$("html, body").animate({
					scrollTop: 0
				});
				var addThisSrc = "//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58206e7d9dbcbcd1";
				$('script[src="' + addThisSrc + '"]').remove();
				$('<script>').attr('src', addThisSrc).appendTo('body');

				setTimeout(function () { // IE fix for accessibility to avoid focus on SVG
					$("svg").attr("focusable", false);
				}, 2000)

				widget.skuRepoId(null);
				//widget.personalizeVisibilityFlag(true);
				$(".tr-e-delivery").find(".error-msg").hide();

				widget.giftText('');
				widget.giftData('');
				widget.hasGift('');
				widget.giftDisplayName(false);
				//Free Gift Code
				if (widget.product().hasGift && typeof widget.product().hasGift == 'function') {
					widget.hasGift(widget.product().hasGift());
				}
				if (widget.product().giftSkuId && typeof widget.product().giftSkuId == 'function') {
					if (widget.hasGift() && widget.product().giftSkuId()) {
						var data = [];
						data[CCConstants.PRODUCT_IDS] = widget.product().giftSkuId();
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, data, function handleGift(data) {
							var giftItemName = data[0].displayName;
							var giftText = "Minimum personalization charge required on promotional item:" + "<i>" + giftItemName + "</i>";
							widget.giftText(giftText);
							widget.giftData(data[0]);
							widget.giftDisplayName(widget.product().displayName() + ' with Free ' + giftItemName);
							$.Topic("GIFT_ITEM").publish(data);
							widget.prodTitleNew(widget.product().displayName());
							widget.giftTitle(' with Free ' + giftItemName);
							var publishedZone = data[0].ProductZones || data[0].productZones;
							var productZonesIds = {};
							if (publishedZone) {
								productZonesIds[CCConstants.PRODUCT_IDS] = publishedZone;
								var productZonesData = storageApi.getInstance().getItem(data[0].id + '-productZones');
								if (refreshData || !productZonesData) {
									ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, productZonesIds, function (productZonesArray) {
										storageApi.getInstance().setItem(data[0].id + '-productZones', JSON.stringify(productZonesArray));
										var zoneIds = '';
										for (i = 0; i < productZonesArray.length; i++) {
											if (zoneIds.indexOf(productZonesArray[i].zone) === -1) {
												if (i === 0) {
													zoneIds = productZonesArray[i].zone;
												} else {
													zoneIds = zoneIds + ',' + productZonesArray[i].zone;
												}
											}
										}
										var zone = {};
										if (zoneIds !== '') {
											zone[CCConstants.PRODUCT_IDS] = zoneIds;
											ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, zone, function (zoneData) {
												storageApi.getInstance().setItem('zones' + data[0].id, JSON.stringify(zoneData));
											});
										}
									});
								}
							}
						}, function (data) {});
					}
				}
				//End Free Gift Code
				//Hide the Available for today and OPUS/STS Data from previous product.
				$(".tr-pickup-availability").hide();
				widget.displayOpus(false);
				widget.displaySts(false);
				widget.buttonVisibilityFlag(false);
				//Display personalization by default
				//widget.personalizeVisibilityFlag(true);
				widget.checkResponsiveFeatures($(window).width());
				this.backLinkActive(true);
				if (!widget.isPreview() && !widget.historyStack.length) {
					this.backLinkActive(false);
				}

				/* reset active img index to 0 */
				widget.shippingSurcharge(null);
				widget.activeImgIndex(0);
				widget.bomArray([]);
				widget.firstTimeRender = true;
				this.populateVariantOptions(widget);
				this.populateUIDisplayAttr(widget);
				this.populateOccasions(widget);
				this.loadCarousel();
				if (widget.product()) {

					var productId = widget.product().id().split('_')[0],
						zeroPaddedProdId = "";


					for (var i = 0; i < (9 - productId.length); i++) {
						zeroPaddedProdId += "0";
					}
					zeroPaddedProdId += productId;


					var productImageArr = widget.getProductImages("S");

					productImageArr.unshift(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);


					//widget.mainImgUrl(widget.product().primaryFullImageURL());
					widget.product().primaryFullImageURL(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					widget.product().primaryMediumImageURL(_this.scene7BaseUrl + zeroPaddedProdId + "?wid=" + imageRes);
					widget.product().mediumImageURLs(productImageArr);
					widget.product().fullImageURLs(productImageArr);

					widget.imgGroups(widget.groupImages(productImageArr));
					widget.product().thumbImageURLs(productImageArr);

					productImageArr.forEach(function () {

						widget.product().product.productImagesMetadata.push({
							altText: "Alt # " + widget.product().displayName()
						});

					});
				}
				widget.loaded(true);

				// the dropdown values should be pre-selected if there is only one sku
				if ((widget.product() && widget.product().childSKUs().length == 1) || storageApi.getInstance().getItem("test")) { // TODO
					this.filtered(false);
					this.filterOptionValues(null);
					var sku_id = widget.product().childSKUs()[0].repositoryId(),
						publishedSkuData = widget.product().childSKUs()[0];

					var skuToSelect;
					if (storageApi.getInstance().getItem("test")) {
						skuToSelect = widget.product().childSKUs().filter(function (item) {
							return item.repositoryId() === "793252"; // TODO
						})[0];
						publishedSkuData = skuToSelect;
						sku_id = skuToSelect.repositoryId();
					}

					if (widget.product().backorder && widget.product().backorder()) {
						widget.skuBackorderFlag(widget.product().backorder());
					}
					widget.skuRepoId(sku_id);
					// widget.skuSts(widget.product().STS());
					widget.skuSts(true);
					if (widget.product().OPUS && typeof widget.product().OPUS == 'function') {
						widget.skuOpus(widget.product().OPUS());
					}
					$(".tr-e-product-description").find('a').find('div.id span').text(sku_id);

					$.Topic('SKU_SELECTED').publish(widget.product(), publishedSkuData, [], widget.itemQty());


					//Checks if product has zones and then displays the appropriate button
					var initData = widget.product();
					if (widget.product().ItemType !== undefined) {
						var initType = widget.product().ItemType();
					} else if (widget.product().itemType !== undefined) {
						var initType = widget.product().itemType();
					}

					if (initType === 'p' || initType === 'P') {
						if (typeof initData.ProductZones === 'function') {
							var initZone = initData.ProductZones();
						} else {
							var initZone = initData.ProductZones
						}

						var initPhotoZone = initData.photoUpload();
						var initGift = initData.hasGift();
						var initGiftId = initData.giftSkuId();
						if (!initZone) {
							widget.buttonVisibilityFlag(true); //Display add to cart button
							widget.personalizeVisibilityFlag(false);
						} else {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
						if (initPhotoZone === true || initPhotoZone === false) {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
						if (initGift && initGiftId) {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
					}
					//End Code to toggle Add to Cart and Personalize
				} else {

					//Checks if product has zones and then displays the appropriate button

					var initData = widget.product();
					if (widget.product().ItemType !== undefined) {
						var initType = widget.product().ItemType();
					} else if (widget.product().itemType !== undefined) {
						var initType = widget.product().itemType();
					}
					if (initType === 'v' || initType === 'V') {
						var initZone = initData.ProductZones || initData.productZones;
						if (typeof initZone === "function") {
							initZone = initZone();
						}
						if (typeof initPhotoZone === "function") {
							initPhotoZone = initPhotoZone();
						}
						var initPhotoZone = initData.photoUpload || initData.PhotoUpload;
						if ((initZone === null) || (typeof initZone === "undefined")) {
							widget.buttonVisibilityFlag(true); //Display add to cart button
							widget.personalizeVisibilityFlag(false);
						} else {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
						if (initPhotoZone === true || initPhotoZone === false) {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
						if (initData.hasGift() && initData.giftSkuId()) {
							widget.buttonVisibilityFlag(false); //Display personalize button
							widget.personalizeVisibilityFlag(true);
						}
					}


				}


				notifier.clearSuccess(this.WIDGET_ID);
				var catalogId = null;
				if (widget.user().catalog) {
					catalogId = widget.user().catalog.repositoryId;
				}
				widget.listPrice(widget.product().listPrice());
				widget.salePrice(widget.product().salePrice());

				if (widget.product()) {
					widget.product().stockStatus.subscribe(function (newValue) {
						if (widget.product().stockStatus().stockStatus === CCConstants.IN_STOCK) {
							if (widget.product().stockStatus().orderableQuantity) {
								widget.stockAvailable(widget.product().stockStatus().orderableQuantity);
							} else {
								widget.stockAvailable(1);
							}
							widget.disableOptions(false);
							widget.stockStatus(true);
						} else {
							widget.stockAvailable(0);
							widget.disableOptions(true);
							widget.stockStatus(false);
						}
						widget.showStockStatus(true);
					});
					var firstchildSKU = widget.product().childSKUs()[0];
					if (firstchildSKU) {
						var skuId = firstchildSKU.repositoryId();
						if (this.variantOptionsArray().length > 0) {
							skuId = '';
						}
						this.showStockStatus(false);
						widget.product().getAvailability(widget.product().id(), skuId, catalogId);
						widget.product().getPrices(widget.product().id(), skuId);
					} else {
						widget.stockStatus(false);
						widget.disableOptions(true);
						widget.showStockStatus(true);
					}
					this.priceRange(this.product().hasPriceRange);
					widget.mainImgUrl(widget.product().primaryFullImageURL());

					$.Topic(pubsub.topicNames.PRODUCT_VIEWED).publish(widget.product());
					$.Topic(pubsub.topicNames.PRODUCT_PRICE_CHANGED).subscribe(function () {
						widget.listPrice(widget.product().listPrice());
						widget.salePrice(widget.product().salePrice());
						widget.shippingSurcharge(widget.product().shippingSurcharge());
					});
				}

				// Load spaces
				if (widget.user().loggedIn()) {
					widget.getSpaces(function () {});
				}

				// Delivery Date
				if (widget.product().BWYBackorderDeliveryDates && typeof widget.product().BWYBackorderDeliveryDates == 'function') {
					var fullDate = widget.product().BWYBackorderDeliveryDates();
					if (fullDate !== '' && fullDate !== null) {
						var index = fullDate.indexOf('-');
						var newDate = fullDate.substring(index + 1);
						newDate = newDate.replace('-', '/');
						newDate = "Estimated Arrival " + newDate;
						widget.standardDeliveryDate(newDate);
					}
				}

				//Flash Video

				if (widget.product().videoFlag && typeof widget.product().videoFlag == 'function' && widget.product().videoFlag() === true) {
					var params = {
						wmode: "transparent"
					};
					var skuID = widget.product().id();

					widget.videoUrl(window.location.protocol + "//s7d1.scene7.com/s7viewers/html5/VideoViewer.html?videoserverurl=https://s7d1.scene7.com/is/content&asset=ThingsRemembered/000" + skuID + "_MP4")

				}


				$(document).on("click", ".occasion-btn", function (e) {
					if ($(this).parent(".dropdown").hasClass("open")) {} else {
						$(this).parent(".dropdown").addClass("open");
					}

				});
				$(document).on("click", ".occasion-dropdown", function () {
					$(this).parent(".dropdown").removeClass("open");
				});

				$(document).on("click", ".occasion-dropdown li", function () {

					widget.occasionCode($(this).attr('occasion_index'));
					$.Topic('OCCASION').publish(widget.occasionCode());

				});


				if (storageApi.getInstance().getItem("test")) { // TODO
					location.hash = "personalize";
					$(".tr-qty-input").change();
					$('html,body').animate({
						scrollTop: 0
					}, 600);
					// $('#personalization-zone-container').empty();
					var variantOptions = this.variantOptionsArray();
					notifier.clearSuccess(this.WIDGET_ID);
					//get the selected options, if all the options are selected.
					var selectedOptions = this.getSelectedSkuOptions(variantOptions);
					//Check if gift item is present. And send it to personalization
					if (this.hasGift() && this.giftData()) {
						$.Topic('PERSONALIZE_GIFT_ITEM').publish(this.giftData());
					} else {
						$.Topic('PERSONALIZE_GIFT_ITEM').publish(false);
					}
					var selectedOptionsObj = {
						'selectedOptions': selectedOptions
					};

					var newProduct = $.extend(true, {}, this.product().product, selectedOptionsObj);
					if (this.variantOptionsArray().length > 0) {
						//assign only the selected sku as child skus
						newProduct.childSKUs = [this.selectedSku()];
					}
					newProduct.orderQuantity = parseInt(this.itemQty(), 10);

					var l_windowWidth = $(window).width();
					if (l_windowWidth <= 767) {
						var pdpTitleInfo = $('.tr-personlization-pdp-header-info');
						if (selectedOptions[0] && selectedOptions[0].optionName === "Color") {
							var colorVal = selectedOptions[0].optionValue.split('|')[0];
							pdpTitleInfo.find('#tr-persona-color-xs').text(colorVal);
						}
						if (selectedOptions[1] && selectedOptions[1].optionName === "Size") {
							var sizeVal = selectedOptions[1].optionValue.split('|')[0];
							pdpTitleInfo.find('#tr-persona-size-xs').text(sizeVal);
						}
						pdpTitleInfo.css({
							'display': 'block'
						});
						$('.tr-personlization-pdp-header-section').append(pdpTitleInfo);
					}

					$.Topic("SENT_TO_PERSONALIZE").publish({
						qty: parseInt(this.itemQty(), 10),
						opusPhotoUpload: this.isPhotoUpload
					});

					// To disable Add to cart button for three seconds when it is clicked and enabling again
					this.isAddToCartClicked(true);
					var self = this;
					setTimeout(enableAddToCartButton, 3000);

					function enableAddToCartButton() {
						self.isAddToCartClicked(false);
					};
				}

				// added for to handle TROCC-3448 starts
				
				$('#tr-e-available-store').on('shown.bs.modal', function () {
					$('body').css('overflow', 'hidden');
					$('body').css('position', 'fixed');
				});
				
				widget.selectOccassionForMobile();

				var productID = widget.product().id();
				var productName = widget.product().displayName();
				var orderQuantity = 1;
				var productBrand = widget.product().brand();
				var productListPrice = widget.product().productListPrice;
				var saleprice = widget.product().salePrice();
				var productVariant = widget.product().variantName();
				var productCategory;

				if (widget.product().parentCategories().length > 1) {
					productCategory = ko.toJS(widget.product().parentCategories()["1"].id);
				}

				productPrice = productListPrice;

				if (saleprice !== null && saleprice < productListPrice)
					productPrice = saleprice;

				dataLayer.push({
					'event': 'productView',
					'ecommerce': {
						'detail': {
							'products': [{
								'name': productName,
								'id': productID,
								'quantity': orderQuantity, // TODO: Needed?
								'price': productPrice,
								'category': productCategory, // TODO: Unknown for now
								'brand': productBrand, // TODO: always null
								'variant': productVariant // TODO: always undefined
							}]
						}
					}
				});

				$.getScript("https://display.ugc.bazaarvoice.com/static/Thingsremembered/en_US/bvapi.js", function () {
					$.Topic(pubsub.topicNames.PRODUCT_VIEWED).subscribe(function (v) {
						$BV.ui('rr', 'show_reviews', {
							productId: widget.product().id(),
							doShowContent: function () {
								$('#tr-w-product-details .tr-e-review>a').click();
							}
						});
					});

				});
				var skuId = "";
				var giftSku = "";
				if(widget.product().hasGift() && widget.product().giftSkuId() && widget.product().giftSkuId() !== "") {
				    giftSku = "?giftSku=" + widget.product().giftSkuId();
				}
				if(widget.product().childSKUs().length === 0 || widget.product().childSKUs().length === 1){
				    widget.personalizeDisabledFlag(true);
				    widget.createInventorySpinner();
				    if(widget.product().childSKUs().length === 0){
				        skuId = widget.product().id();
				    }else{
				        skuId = widget.product().childSKUs()[0].repositoryId().split("_")[0];
				    }
				    $.ajax({
                        url : widget.site().extensionSiteSettings.externalSiteSettings.awsUrl + skuId + giftSku,
                        method: "GET",
                        success : function(data) {
							isPersonalizationPropertyExist = data.zones.length;
                            if(data.zones.length > 0){
                                newPersonalizationData = data;
                                widget.generateExpressPhase(newPersonalizationData);
            					widget.generateFonts(newPersonalizationData);
            					widget.generalFonts(newPersonalizationData);
            					widget.generateColorFills(newPersonalizationData);
            					widget.generateDesigns(newPersonalizationData);
            					widget.generateNographic(newPersonalizationData);
            					var viewModel = ko.mapping.fromJS(newPersonalizationData);
            					$.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
                            }else if($('.error-msg').is(':visible') === false){
                                $('.tr-xs-personalize-block').removeClass('hide');
                                $(".personalizationBtn").hide();
								$(".addToCartBtn").show();
								$(".personalizationBtns").removeClass('hide');
                            }
                            
        					widget.destroyInventorySpinner();
                        },
                        error: function() {
                            getfallBackDatafromSku(widget);
                        }
				    });
				}
				
				$("body").delegate(".minicart-button", "click", function(){
				    $("body").removeClass('modal-open');
				});
			}