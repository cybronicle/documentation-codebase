checkIfPersonalizationSkuPresent: function () {
				var cartItems = getWidget.cart().items();
				for (var i in cartItems) {
					if (cartItems[i].catRefId === "personalization1") {
						return true;
					}
				}
				return false;
            },
            //this method is triggered to check if the option value is present in all the child Skus.
			checkOptionValueWithSkus: function (optionId, value) {
				var childSkus = this.product().childSKUs();
				var childSkusLength = childSkus.length;
				for (var i = 0; i < childSkusLength; i++) {
					if (!childSkus[i].dynamicPropertyMapLong[optionId] || childSkus[i].dynamicPropertyMapLong[optionId]() === undefined) {
						return true;
					}
				}
				return false;
            },
            checkPersonalizationExist: function () {
				var widget = this;
				if (isPersonalizationPropertyExist > 0) {
					widget.personalizeVisibilityFlag(true);
					widget.buttonVisibilityFlag(false);
					widget.checkPersonalization(true);
				} else {
					widget.personalizeVisibilityFlag(false);
					widget.buttonVisibilityFlag(true);
					widget.checkPersonalization(false);
					widget.personalizeDisabledFlag(true);
				}
			},