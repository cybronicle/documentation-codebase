onClickSetStore: function () {},

			onClickShowMore: function (e) {
				e.cancelBubble = true;
				if (e.stopPropagation) e.stopPropagation();

				if ($(".tr-prod-descr-show-more a").hasClass("js-hidden")) {
					$("#descr-container").addClass("overflown");
					$(".tr-prod-descr-show-more a").text("less...");
					$(".tr-prod-descr-show-more a").removeClass("js-hidden").addClass("js-shown");
				} else {
					$("#descr-container").removeClass("overflown");
					$(".tr-prod-descr-show-more a").text("more...");
					$(".tr-prod-descr-show-more a").addClass("js-hidden").removeClass("js-shown");
				}
			},
            onChangeBOMTypes: function () {},
            
            onClickZipSubmit: function (data, event) {
				if (event.type == "click" || (event.type == "keyup" && event.keyCode == 13)) {
					var searchZipCode = $("#zip-code-text-available").val().trim();
					if (searchZipCode.length === 0) {
						return;
					}
					$('body').css('overflow', 'hidden');
					$('body').css('position', 'fixed');

					function validateZipCode(elementValue) {
						var zipCodePattern = /^\d{5}$|^\d{5}-\d{4}$/;
						return zipCodePattern.test(elementValue);
					}
					$('.new-zip-section .error-msg').hide();
					/*if (validateZipCode(searchZipCode)) {
					    $('.new-zip-section .error-msg').show();
					} else {
					    $('.new-zip-section .error-msg').hide();
					}*/

					var widget = arguments[0];
					$.ajax({
						url: "https://api.tomtom.com/search/2/search/"+searchZipCode+".json?key=iDGYNjVRhRq1hFJFf3bJXm886ieFlYKR&countrySet=US&limit=1",
						method: "GET",
						success: function (data) {
							$(".new-zip-section").find(".error-msg").hide();
							if (data.status === "ZERO_RESULTS") {
								$(".new-zip-section").find(".error-msg").show().text("Entered Zip code is not valid.");
								return;

							}
							var storeNumber;

							var latitude = data.results["0"].position.lat,
								longitude = data.results["0"].position.lon;
								productData = widget.skuData();
							if (!productData) {
								productData = widget.product().product;
							}
							var opusProductFlag = productData.OPUS;


							//TRV - 1917 - E
							var stsProductFlag = widget.skuSts();

							var skuId = productData.repositoryId;
							var qty = widget.itemQty();
							var cartQuantity = 0;

							if (!reqData) reqData = {
								"skuId": skuId,
								"stsProductFlag": "",
								"storeNumber": storeNumber,
								"location": {
									"latitude": latitude,
									"longitude": longitude
								},
								"quantity": qty,
								"radiusLimit": 400,
								"limit": 4
							}
							qty = Number(qty);
							//widget.itemQty(qty);


							//var qty = widget.itemQty();
							var reqData = {
								"skuId": skuId,
								"stsProductFlag": "",
								"storeNumber": storeNumber,
								"location": {
									"latitude": latitude,
									"longitude": longitude
								},
								"quantity": qty,
								"radiusLimit": 400,
								"limit": 4
							};
							//_this.getSkuInventoryFromRepository(reqData, 400, widget);
							var stores = helpers.storeLocatorUtils().getStores(latitude, longitude, storeItems);
							var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);

							var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();
							helpers.storeLocatorUtils().getInventoryResponseIR(stores, qty, storeNumber, 4, skuId, storeIds, reqData, "changeStorePDPZip", cartInventoryDetailsArrayObj);
						}
					});
				}
			},
			onClickDelivery: function (e, event) {
				var widget = this;
				$(".tr-e-delivery").find(".error-msg").hide();
				if (!quantityChangeDeliveryTriggered) {
					$(".tr-store-stock-error").hide().text("");
					widget.quantityChange();
				} else {
					quantityChangeDeliveryTriggered = false;

				}
            },
            onClickMoreStores: function (data, event) {
				var widget = this;
				var storeNumber = widget.myStore()[0].storeNumber;
				var lat = widget.myStore()[0].latitude;
				var long = widget.myStore()[0].longitude;
				var skuId = widget.skuRepoId();
				var quantity = widget.itemQty();
				var stores = helpers.storeLocatorUtils().getStores(lat, long, window.storeItems);
				var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);

				var reqData = {
					"skuId": skuId,
					"stsProductFlag": "",
					"storeNumber": storeNumber,
					"location": {
						"latitude": lat,
						"longitude": long
					},
					"quantity": quantity,
					"radiusLimit": 400,
					"limit": 4
				};

				var stores = helpers.storeLocatorUtils().getStores(lat, long, storeItems);
				var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);

				var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();
				helpers.storeLocatorUtils().getInventoryResponseIR(stores, quantity, storeNumber, 4, skuId, storeIds, reqData, "changeStorePDP", cartInventoryDetailsArrayObj);
				
				$('#tr-e-available-store').on('hidden.bs.modal', function () {
					$('body').css('overflow', 'unset');
					$('body').css('position', 'unset');
				});
			},