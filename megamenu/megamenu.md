Menu keyboard navigation
    - the menu is triggered to be visible when the primaryMenuAnchor is focused or the anchor has the class isFocused. 
    - the tabbing navigation is custom and based on an element attriute data_place.
    - data_place represents an a,b,c coordinate menu where a is the primaryMenuAnchor, b is the column number, and c is the row number.
    - the secondary menu is navigated using the arrow keys when inside.

Reminders: 
    - data_place needs to be the second attribute in the anchor tags



Menu HTML Order: 
1. nav - whole navigation 
2. ul - list containing all the menus starting with primary
3. li - first li of ul is the mobile menu title. Second and more contain the primary menus and their secondary/tertiary menus
4a. a - anchor tag for the primary menu link that contains a data_place attribute referencing which number the menu is
4b. span - used for the text itself inside the anchor tag
5. div - these divs are used as a container for all the secondary menus under the primary menu
6. div - these divs are used as a container for each secondary menu and their tertiary menus
7. ul - these ul elements are used to signify the column in the secondary row for the desktop menu. These ul also contain a data_place property that holds 2 numbers, the primary menu and the column number starting with 0
8. li - these li elements are used to represent separate secondary menus and their tertiary menus. To extend the number of rows in a column, add another secondary menu with a separate li tag under the same ul parent
9a. a - these anchor tags are used for the secondary menu link like 4a except these anchor tags contain a data_place attribute that holds 3 values, the primary menu number, the column number and the row number.
9b. span - these span tags are used for the secondary menu title like 4b. 
10. div - these divs are the container for the tertiary menus
11a. a - these anchor tags are used in the same manner as 9a, still containing 3 values in the data_place, the primary menu number, the column number and the row number.
11b. span - these spans are used in the same manner as 4b and 9b but for the tertiary menu.



Example menu HTML Order (will need to beautify it): 

`<nav class="tr-nav-menu">
    <ul>
        <li class="mobileMenuTitle">
            <span>Menu</span>
        </li>
        <li class="node">
            <a class="primaryMenuItem" data_place="0" href="..." tabindex="1">
                <span>PRIMARY1</span>
            </a>
            <div class="menu has-submenu" role="menu">
                <div class="secondaryMenu">
                    <ul data_place="0,0">
                        <li>
                            <a class="secondaryMenuItem header" data_place="0,0,0"  href="..." >
                                <span>SECONDARY0-0</span>
                            </a>
                            <div class="tertiaryMenu">
                                <a class="tertiaryMenuItem" data_place="0,0,1" href="...">
                                    <span>TERTIARY0-1</span>
                                </a>
                                <a class="tertiaryMenuItem" data_place="0,0,2" href="...">
                                    <span>TERTIARY0-2</span>
                                </a>
                                <a class="tertiaryMenuItem" data_place="0,0,3" href="...">
                                    <span>TERTIARY0-3</span>
                                </a>
                            </div>
                        </li>
                        <li>
                            <a class="secondaryMenuItem header" data_place="0,0,4"  href="..." >
                                <span>SECONDARY0-4</span>
                            </a>
                            <div class="tertiaryMenu">
                                <a class="tertiaryMenuItem" data_place="0,0,5" href="...">
                                    <span>TERTIARY0-5</span>
                                </a>
                                <a class="tertiaryMenuItem" data_place="0,0,6" href="...">
                                    <span>TERTIARY0-6</span>
                                </a>
                                <a class="tertiaryMenuItem" data_place="0,0,7" href="...">
                                    <span>TERTIARY0-7</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
</nav>`