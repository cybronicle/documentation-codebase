//TRV - 2162 - S
setProductData: function (value) {
    _this.g_productData = value;
},
getProductData: function () {
    return _this.g_productData;
},
setSelectedSKU: function (value) {
    _this.g_selectedSKUData = value;
},
getSelectedSKU: function () {
    return _this.g_selectedSKUData;
},
setSelectedSKUQty: function (value) {
    _this.g_selectedSKUQty = value;
},
getSelectedSKUQty: function () {
    return _this.g_selectedSKUQty;
},
timeStringToFloat: function (p_time) {
    var hoursMinutes = p_time.split(/[.:]/);
    var hours = parseInt(hoursMinutes[0], 10);
    var minutes = hoursMinutes[1] ? parseInt(hoursMinutes[1], 10) : 0;
    return hours + minutes / 60;
},