generateExpressPhase:function(expressData){
			    var widget = this;
		        var getProductData = expressData;
		         var skuDetails = [];
		          var getDesignSku = [];
		          var specialDesignAssociations = [];
		          var specialTempDesignAvailable = false;
		        if(getProductData.zones.length > 0){
		            $.each(getProductData.zones, function(k,v){
		                $.each(v.expressPhraseReferences, function(m,n){
		                    if(getProductData.referencedExpressPhrases[n].id === n){
		                        v.expressPhraseReferences[m] = JSON.parse(JSON.stringify(getProductData.referencedExpressPhrases[n]));
		                    }
		                    if(v.expressPhraseReferences[m].priceModel.price === null){
		                        v.expressPhraseReferences[m].priceModel.price = {"amount": 0,
                                                                                "currency": "USD"}
		                    }
		                    if(v.expressPhraseReferences[m].type === "CYO"){
		                        if(typeof(v.vertical) !== "undefined" && v.vertical){
		                            v.expressPhraseReferences[m].details[0].characterCount = v.height;
		                            v.expressPhraseReferences[m].details[0].lineNumber = v.width;
		                        }else{
		                            v.expressPhraseReferences[m].details[0].characterCount = v.width;
		                            v.expressPhraseReferences[m].details[0].lineNumber = v.height;
		                        }
		                        
		                    }
		                    if(v.expressPhraseReferences[m].type === "DYNAMIC"){
		                        $.each(v.expressPhraseReferences[m].details, function(b,x){
		                            if(x.type === "TEXT"){
		                                x.characterCount = v.width;
		                            }
		                            if(x.type === "DESIGN"){
		                                if(x.availableDesignClassReferences.length > 0){
		                                    specialTempDesignAvailable = true;
		                                    $.each(x.availableDesignClassReferences, function(a,q){
    		                                    x.availableDesignClassReferences[a] = JSON.parse(JSON.stringify(getProductData.referencedDesignClasses[q]));
    		                                    $.each(x.availableDesignClassReferences[a].classMemberReferences, function(e,r){
    		                                        var id = parseInt(r);
    		                                        skuDetails.push(getProductData.referencedDesigns[r]);
    		                                    });
    		                                    x.availableDesignClassReferences[a]['skuDetails'] = skuDetails;
                		                        skuDetails = [];
                		                        getDesignSku = [];
		                                    });
		                                    x.availableDesignClassReferences.unshift({
                    		                        "classMemberReferences":"",
                    		                        "displayName":"No Graphics",
                    		                        "id":"catNo",
                    		                });
                    		                specialDesignAssociations = x.availableDesignClassReferences;
		                                }
		                                
		                            }
		                        });
		                    }
		                });
		                if(getProductData.photoUpload.required === true){
		                    v['minimumPhotoResolution'] = '576,720';
		                    v['photoUrl'] = "/file/general/no-image.jpg";
		                }
		                v['specialDesignAssociations'] = specialDesignAssociations;
		                v['specialTemplate'] = false;
		                v['zoneCost'] = 0;
		                v['personalizeCompleted'] = false;
		                v['isPhotoUploaded'] = false;
		                v['personalizeImage'] = 'https://thingsremembered.scene7.com/ir/render/ThingsRememberedRender/'+v.zoneAttributes.vignette;
		            });
		        }
		        if($('.error-msg').is(':visible') === false){
		            $('.tr-xs-personalize-block').removeClass('hide');
            		$(".addToCartBtn").hide();
					$(".personalizationBtn").show();
					$(".personalizationBtns").removeClass('hide');
		        }
		        
		        
		    },
		    generateFonts:function(expressData){
		        var getProductData = expressData;
		        if(getProductData.zones.length > 0){
		            $.each(getProductData.zones, function(k,v){
						if(v.fontOptions && v.fontOptions.defaultSelectionReference) {
		                	v.fontOptions.defaultSelectionReference = getProductData.referencedFonts[v.fontOptions.defaultSelectionReference];
							$.each(v.fontOptions.groupMemberReferences, function(m,n){
								if(getProductData.referencedFonts[n].id === n){
									v.fontOptions.groupMemberReferences[m] = getProductData.referencedFonts[n];
								}
							});
						}
		            });
		        }
		        
		    },
		    generalFonts: function(expressData){
		       var getProductData = expressData;
		       var CYOFonts = [];
		       var monogram1 = [];
		       var monogram2 = [];
		       var monogram3 = []; 
		       if(getProductData.zones.length > 0){
		            $.each(getProductData.zones, function(k,v){
		                CYOFonts = [];
		                monogram1 = [];
		                monogram2 = [];
						monogram3 = [];
						if(v.fontOptions && v.fontOptions.groupMemberReferences) {
							$.each(v.fontOptions.groupMemberReferences, function(m,n){
								n.groupId=v.fontOptions.groupId;
								n.optionType = v.fontOptions.optionType;
								if(n.type === "STANDARD"){
									CYOFonts.push(n);
								}
								if(n.type === "MONOGRAM1" || n.type === "INITIAL1"){
									monogram1.push(n);
								}
								if(n.type === "MONOGRAM2" || n.type === "INITIAL2"){
									monogram2.push(n);
								}
								if(n.type === "MONOGRAM3" || n.type === "INITIAL3"){
									monogram3.push(n);
								}
							});
							v.fontOptions["cyoFonts"] = CYOFonts;
							v.fontOptions["monogram1Fonts"] = monogram1;
							v.fontOptions["monogram2Fonts"] = monogram2;
							v.fontOptions["monogram3Fonts"] = monogram3;
						}
		            });
		        }
		    },
		    generateColorFills:function(expressData){
		        var getProductData = expressData;
		        if(getProductData.zones.length > 0){
		            $.each(getProductData.zones, function(k,v){
		                if(v.colorOptions !== null){
		                    v.colorOptions.defaultSelectionReference = getProductData.referencedColors[v.colorOptions.defaultSelectionReference];
		                    $.each(v.colorOptions.groupMemberReferences, function(m,n){
		                    if(getProductData.referencedColors[n].id === n){
		                        v.colorOptions.groupMemberReferences[m] = getProductData.referencedColors[n];
		                    }
		                    if(v.colorOptions.groupMemberReferences[m].upcharge === null){
		                        v.colorOptions.groupMemberReferences[m].upcharge = {
		                            "price": {
		                                "amount":0,
		                                "currency":"USD"
		                            },
		                            "sku":"0000"
		                        }
		                    }
		                });
		                }
		                
		            });
		        }
		    },
		    generateDesigns:function(expressData){
		            var getProductData = expressData;
		            var skuDetails = [];
		            var getDesignSku = [];
		            if(getProductData.zones.length > 0){
		                $.each(getProductData.zones, function(k,v){
		                    $.each(v.designClassAssociations, function(m,n){
		                        var getData = getProductData.referencedDesignClasses[n.designClassReference];
		                        v.designClassAssociations[m] = getData;
		                        $.each(getData.classMemberReferences, function(l,s){
		                            var id = parseInt(s);
		                            skuDetails.push(getProductData.referencedDesigns[id]);
		                        });
		                        v.designClassAssociations[m]['skuDetails'] = skuDetails;
		                        skuDetails = [];
		                        getDesignSku = [];
		                    });
		                });
		            }
		        },
		        
		        generateNographic:function(expressData){
		            if(expressData.zones.length > 0){
		                
		                $.each(expressData.zones, function(k,v){
		                    if(v.designClassAssociations.length > 0){
		                        if(v.designClassAssociations[0].id !== "catNo"){
		                             v.designClassAssociations.unshift({
        		                        "classMemberReferences":"",
        		                        "displayName":"No Graphics",
        		                        "id":"catNo"
        		                    });
		                        }
		                    }
		                   
		                   // return false;
		                });
		            }
		        },
		    