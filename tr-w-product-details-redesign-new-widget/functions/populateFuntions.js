populateOccasions: function (widget) {
				if (!widget.product().occasions) {
					return;
				}
				var occasions = widget.product().occasions().replace(/<p>/g, ""),
					occasions_arr = occasions.split("</p>");
				occasions_arr.unshift("00000|Select Occasion");
				widget.occasionsArr.removeAll();
				for (var i = 0; i < occasions_arr.length; i++) {
					if (occasions_arr[i].indexOf('No Thanks') != -1) {
						widget.mobileDefaultOccassionId(occasions_arr[i].split('|')[0]);
						widget.mobileDefaultOccassionIndex(i);
					}
					widget.occasionsArr().push(occasions_arr[i] + '~' + i)
				}
				if (widget.mobileDefaultOccassionId() == '' && occasions_arr.length > 0) {
					widget.mobileDefaultOccassionId(occasions_arr[1].split('|')[0]);
					widget.mobileDefaultOccassionIndex(1);
				}
			},
			populateUIDisplayAttr: function (widget) {
				if (!widget.product().UIDisplayAttribute || !widget.product().UIDisplayAttribute()) {
					widget.uiDisplayAttr("true");
					return;
				}
                console.log("uiDisplayAttr");
			    console.log(widget.product().UIDisplayAttribute().replaceAll(" ", "").split(","));
				widget.uiDisplayAttr(widget.product().UIDisplayAttribute().split(","));

			},