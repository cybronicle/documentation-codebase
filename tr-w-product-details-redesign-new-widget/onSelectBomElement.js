// widget variable setting to a function
// function is data-bound to a click event on the template
widget.onSelectBOMElement = function(selectedBomItem, event, slotIndex, parentContext, parent) {
   // 
    var data = {},
        skuid = selectedBomItem.skuid,
        slot = selectedBomItem.slot;
    
    //setting variables to values
    selectedBomItem.slotNumber = slotIndex;
    selectedBomItem.isInStock = ko.observable(true);
    // sets a boolean after a check to see if the first selectedBomArray item is selectedBomItem.slot
    var match = ko.utils.arrayFirst(widget.selectedBomArray(), function(item) {
        return selectedBomItem.slot === item.slot;
    });
    // if false
    if (!match) {
        // pushes selected gem into an array 
        widget.selectedBomArray.push(selectedBomItem);
    } else {
        //gets the index of the selected gem and 
        var index = widget.selectedBomArray.indexOf(match);
        // replaces the element n the selectedBomArray
        widget.selectedBomArray.splice(index, 1, selectedBomItem);
    }
    /// sets the month text on the details
    $(event.target).closest(".bom-content").find(".bom-value").text(selectedBomItem.name);
    // removes class off the previously selected gem
    $(event.target).closest(".bom-row").find(".selected").removeClass("selected");
    // sets selected to the current gem
    $(event.currentTarget).addClass("selected");

    // overwrting previous data variable
    var data = [];
    // sets parent product id
    var productId = widget.product().id();
    //sets dummy parent product id
    data[CCConstants.PRODUCT_IDS] = widget.product().id();
    //sets child product id
    data[CCConstants.SKU_ID] = selectedBomItem.skuid.trim();
    data[CCConstants.CATREF_ID] = selectedBomItem.skuid.trim();

    //stupid html add
    var preloader = $("<div id='global-preloader'><div class='content'>Processing Request..</div></div>");
    preloader.appendTo('body');

    //rest call, probably not needed
    ccRestClient.request(CCConstants.ENDPOINT_GET_PRODUCT_AVAILABILITY, data, function(data) {

        //checks stock status
        if (data.stockStatus === "OUT_OF_STOCK") {
            data.orderableQuantity = 0;
        }

        //sets stockavailable and a flag
        var stockavailable = data.orderableQuantity;
        var availableflag = 1;
        //if quantity is less than the 
        if (stockavailable < widget.itemQty()) {
            $(event.target).closest(".bom-content").find(".error-msg").text(selectedBomItem.name + " is out of stock").show();
            availableflag = 0;
        }
        // smae check as above
        if (data.orderableQuantity < widget.itemQty()) {
            availableflag = 0;
        }
        // duplicated assignment from line 40
        data[CCConstants.PRODUCT_IDS] = selectedBomItem.skuid.trim();
        //rest call
        ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, data, function(products) {
            // checks to see if products exist
            if (products && products.length === 0) {
                //will always be true
                if (preloader) {
                    preloader.remove();
                }
                //dummy object
                var obj = {};
                //set passed in variable + 1
                obj.slotnum = slotIndex + 1;
                //sets child product
                obj.prodsku = skuid;
                //sets an error message to inform that the product is out of stock
                $(event.target).closest(".bom-content").find(".error-msg").text(selectedBomItem.name + " is out of stock").show();
                // changes flag to 0
                availableflag = 0;

                // posts to topic for bom removal
                $.Topic('REMOVE_BOM').publish(obj);
                // sets flag 
                widget.personalizeVisibilityFlag(isSelectedBomArrayValid(widget));
                return;
            }
            //will always be true
            if (preloader) {
                preloader.remove();
            }
            // if not available
            if (availableflag === 0) {
                // sets to sale price first or list price if not sale
                var price = products[0].salePrice || products[0].listPrice;

                //dummy object creation for removal
                var obj = {};
                obj.slotnum = Number(slotIndex) + 1;
                obj.color = "130,20,30";
                obj.skuid = products[0].repositoryId;
                obj.color = products[0].birthstoneColor || "1302030";
                obj.prodsku = skuid;
                $.Topic('REMOVE_BOM').publish(obj);

            } else {
                //hides error message
                $(event.target).closest(".bom-content").find(".error-msg").hide();
                //check for a sale price
                if (products[0].salePrice === null) {
                    // ser sale price to list price
                    products[0].salePrice = products[0].listPrice;
                }

                var price = products[0].salePrice || products[0].listPrice;
                // dummy product creation for the BOM_URL
                var obj = {};
                obj.slotnum = slotIndex + 1;
                obj.skuid = products[0].repositoryId;
                obj.color = products[0].birthstoneColor || "1302030";
                obj.price = price;
                obj.name = selectedBomItem.name;
                obj.slot = "Slot " + obj.slotnum;
                obj.prodsku = skuid;
                $.Topic('BOM_URL').publish(obj);
            }
            // sets visibility flag
            widget.personalizeVisibilityFlag(isSelectedBomArrayValid(widget, parentContext));

        });

        // second half of the rest call to endpoint ENDPOINT_GET_PRODUCT_AVAILABILITY 
    }, function(data) {
        //will always be ttrue
        if (preloader) {
            preloader.remove();
        }
        // dummy object 
        var obj = {};
        obj.slotnum = slotIndex + 1;
        obj.prodsku = skuid;
        // pushes out of stock message and sets flag
        $(event.target).closest(".bom-content").find(".error-msg").text(selectedBomItem.name + " is out of stock").show();
        availableflag = 0;

        // publishes to remove bom
        $.Topic('REMOVE_BOM').publish(obj);

        //check for valid bom array, returns boolean, variable is used
        widget.personalizeVisibilityFlag(isSelectedBomArrayValid(widget));

    }, productId);
}