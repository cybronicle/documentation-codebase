JS FILES COMPLETED:
[addFunctions, addPersonalizationItemToCart, optionsFunctions,addPersonalizationToCart, checkFunctions, cookie, fetchFunctions, filterFunctions, getServerTime, images, optionsFunctions, validatefunctions, ]



file addFunctions.js:

    function addToSpaceClick: takes in widget
        
        1. this.variantOptionsArray() -> KO observable array
        2. notifier.clearSuccess(this.Wdget_ID) -> widget dependency
        3. this.getSelectedSkuOptions(varientOptions) -> this method  returns a map of all the options selected by the user for the product (file = getter.js) -> returns selectedOptions
        4. this.itemQty() -> KO observable int -> used in topic "SKU_SELECTED"
        5. $.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD).publishWith(newProduct, [{message:"success"}]) -> 
        
        Overview: takes and fills in the variantOptions and then publishes to topic pubsub.topicNames.SOCIAL_SPACE_ADD

    function addToSpaceSelectorClick: takes in widget
        
        1. this.variantOptionsArray() 
        2. notifier.clearSuccess(this.WIDGET_ID)
        3. getSelectedSkuOptions(variantOptions)
        4. $.Topic(pubsub.topicNames.SOCIAL_SPACE_SELECTOR_ADD.publishWith(newProduct, [{message: "success"}]);
        
        Overview: takes and fills in the variantOptions and then publishes to topic pubsub.topicNames.SOCIAL_SPACE_SELECTOR_ADD

    function addToSpaceSelect: takes in widget and spaceId
        
        1. $.Topic(pubsub.topicNames.SOCIAL_SPACE_ADD_TO_SELECTED_SPACE).publishWith(newProduct, [spaceId]);
        
        Overview: takes and fills in the variantOptions and then publishes to topic pubsub.topicNames.SOCIAL_SPACE_ADD_TO_SELECTED_SPACE
        


    File Overview. All functions have duplicated LOC up until topic. Can convert into separate functions for setting up calls

file addPersonalizationItemToCart.js:
    
    function addPersonalizationItemToCart: takes in productId and index

        Overview: This file can be taken out. 


file addPersonalizationToCart.js:

    function addPersonalizationToCart: takes in nothing and is not referenced in the whole JS collection

    File Overview: This file can be taken out.


file checkFunctions.js: 

    function checkIfPersonalizationSkuPresent: takes in nothing and is referenced in handleFunctions.js, and twice in onLoad.js

        1. NO TRUE FUNCTION CALLS

        Overview: Grabs to see if any cart items are personalized. Returns boolean.

    function checkOptionValueWithSkus: takes in optionId and value. Is referenced in variables_miscFunctions.js

        1. NO TRUE FUNCTION CALLS

        Overview: iterates through all childSkus to see if dynamicPropertyMapLong is false OR or undefined. If True, returns True. Else, returns False.

    function checkPersonalizationExist: takes in nothing and is referenced in onLoad.js and twice in getter.js

        1. NO TRUE FUNCTION CALLS


        Overview: checks to see if there are any personalization properties. If > 0, then sets widgets personalizeVisibilityFlag and checkPersonalization to true, and buttonVisibilityFlag to false. If < 0, then sets buttonVisibilityFlag and personalizeDisabledFlag to true while setting checkPersonalization and personalizeVisibilityFlag to false.

    File Overview: this file seems relevant in terms of its overall use. 


file cookie.js: 

    function getCookie: takes in cname but is called in a function that is never called. 

    function setCookie: takes in cname and cvalue and is called twice in beforeAppear.js. Also has the same implementation twice.

        1. NO TRUE FUNCTION CALL

        Overview: sets document.cookie to equal "cname=cvalue" where cname and cvalue are the variables taken in. 

    File Overview: getCookie is not needed and not fully sure if setCookie is needed yet. setCookie is only called at beforeAppear and no cookies are used execpt in functions that are never called.


file fetchFunctions.js: 

    function fetchFacebookAppId: takes in nothing and is referenced in the onConfigReady.js file.

        1. ccRestClient.request(CCConstants.ENDPOINT_MERCHANT_GET_EXTERNALDATA,null, widget.fetchFacebookAppIdSuccessHandler.bind(widget),widget.fetchFacebookAppIdErrorHandler.bind(widget),serverType); -> a REST call to the CCConstants.ENDPOINT_MERCHANT_GET_EXTERNALDATA endpoint. 

        Overview: Checks to see if the serverType is production or preview. Next makes a request REST call CCConstants.ENDPOINT_MERCHANT_GET_EXTERNALDATA endpoint. 

    function fetchFacebookAppIdSuccessHandler: takes in pResult and is referenced in fetchFacebookAppId above. 

        1: NO TRUE FUNCTION CALLS
        
        Overview: sets the siteFbAppId to the pResult applicationId.

    function fetchFacebookAppIdErrorHandler: takes in pResult and is called but has no functionality inside.

    File Overview: fetchFacebookAppIdErrorHandler is called in other places but does nothing. The other two functions seem to have good functionality. 

file filterFunctions.js:

    function filterOptionValues: takes in selectedOptionId,
                                 called in variables_miscFunction.js and beforeAppear.js

        1. getMatchingSKUs() -> in getter.js file. self titled functionality. 
        2. updateSingleSelection() -> found in updateFunctions.js. this method updates the selection value for the options with single option values.

        Overview: Starts off with check to see if the selectedOptionId is filtered. If True, return. If False, itterates through the variantOptions and gets matching skus, setting the filtered flag to true for each variant option. Lastly, updates the selectedOptionId.

    File Overview: Seems to hold value.

(NOT DONE) file generateFunctions.js: 

    function generateExpressPhase:  takes in expressData,
                                    called in beforeAppear.js, twice in getter.js, called in onLoad.js.

        1. $.each(getProductData.zones, function(k,v) -> jQuery 
        2. $.each(v.expressPhraseReferences, function(m,n) -> jQuery
        3. JSON.parse(JSON.stringify(getProductData.referencedExpressPhrases[n])) -> Not sure why a stringify is used to then parse. 
        4. $.each(v.expressPhraseReferences[m].details, function(b,x) -> jQuery
        5. $.each(x.availableDesignClassReferences, function(a,q) -> jQuery 
        6. JSON.parse(JSON.stringify(getProductData.referencedDesignClasses[q])) -> Not sure why a stringify is used to then parse. 
        7. $.each(x.availableDesignClassReferences[a].classMemberReferences, function(e,r) -> jQuery
        8. x.availableDesignClassReferences.unshift -> used to add 3 objects and returns length
        9. $('.tr-xs-personalize-block').removeClass('hide'); -> removes attribute
        10. $(".addToCartBtn").hide(); -> hides element
		11. $(".personalizationBtn").show(); -> shows element 
		12. $(".personalizationBtns").removeClass('hide'); -> removes attribute

        Overview: Checks to see if the personalization data has more than one zone. If true, go through and normalize the express phrases for each zone. Also, check to see if the price is null. If true, set a sample object to the price. Also, check to see if the expressPhrase type is "CYO" if they are equal. If true, a check to see if the vertical flag has been set and is true. If both conditions are met, set the characterCount to zone height and lineNumber to zone width. If both are not met, do flip the zone height and width assignments. Next, if expressPhraseReferences type check is dynamic then iterate through each expressPhraseReferences details. While in the itertation of the details, if the type of details is equal to text, then set the character count to the zone width. If the details type is design, check to see if the details availableDesignClassReferences is longer than 0. If true, iterate through the availableDesignClassReferences and set their value to the personalizaton referencedDesignClasses. After the assignment, iterate through the classMemberReferences and push the referenced design to the skuDetails. After iterating through the availableDesignClassReferences, adds a template object to the start of the array. Now, after the iteration through the expressPhraseReferences, check to see if there is a required photoUpload. If true, set the minimumPhotoResolution and photoUrl to 576x720 and the no-image.jpg file. Then set specialDesignAssociations to the availableDesignClassReferences, specialTemplate to false, zoneCost to 0, personalizeCompleted to false, isPhotoUploaded to false, and personalizeImage to 'https://thingsremembered.scene7.com/ir/render/ThingsRememberedRender/'+v.zoneAttributes.vignette. After the check to see if there are product zones, chck to see if the error message is not visible. If it is visable, then, perform #'s 9-12. 
        
        Note: Can get as deep as 5 for each loops. 

    function generateFonts: takes in personalization data (expressData), found in same locations as generateExpressPhase()
        
        1. $.each(getProductData.zones, function(k,v) -> goes through product zones
        2. $.each(v.fontOptions.groupMemberReferences, function(m,n) -> goes through zones font options and their groupMemberReferences

        Overview: Checks to see if there are product zones. If true, iterate through the product zones and check to see if the fontOptions and defaultSelectionReference are set. If they are both set, then iterate through the fontOptions.groupMemberReferences and check to see if the id matches the referencedFonts id. If this is also true, then set the fontOptions.groupMemberReference to personalization referenced font.

    function generalFonts: takes in personalization data(expressData), found in all same locations as generateExpressPhase

        1. $.each(getProductData.zones, function(k,v) -> goes through product zones
        2. $.each(v.fontOptions.groupMemberReferences, function(m,n) -> oes through zones font options and their groupMemberReferences
        
        Overview: Checks to see if there are product zones. If true, iterate through the product zones and check to see if the fontOptions and defaultSelectionReference are set. If they are both set, then iterate through the fontOptions.groupMemberReferences. In the iteration, there type checks on the groupMemberReferences to see if the type is standard, monogram1 to mongram3, and intial1 to intial4. If the check is true, then it pushes the groupMemberReference to the corrct fontOptions.

    function generateColorFills: takes in personalization data(expressData), found in all same locations as generateExpressPhase

        1. $.each(getProductData.zones, function(k,v) -> goes through product zones
        2. $.each(v.colorOptions.groupMemberReferences, function(m,n) -> iterate through colorOptions.groupMemberReferences. 
        
        Overview: 

    


    File Overview: A lot of iteration functions can be created to simplify the code. 






file getServerTime.js: 

    function getServerTime: takes in nothing and is called in onConfigReady.js

        1. XMLHttpRequest() -> creates new request
        2. ActiveXObject('Msxml2.XMLHTTP') -> creates IE object
        3. ActiveXObject('Microsoft.XMLHTTP') -> creates microsoft object
        4. xmlHttp.open('HEAD', window.location.href.toString(), false) -> opens a request
        5. xmlHttp.setRequestHeader("Content-Type", "text/html") -> sets header
        6. xmlHttp.send('') -> sends request
        
        Overview: Creates a random request and sends it to get the Server time. Can be done a different way instead. Returns the response header date.
    
    File Overview: Very weird way to grab the time from the user.
    

(NOT DONE) file getter.js:

(NOT DONE) file handleFunctions.js:

file images.js: 

    function handleCycleImages: takes in data, event, index and parentIndex. However is never called in the whole JS collection.

    function loadImageToMain: takes in data, event and index. Called in onConfigReady.js.

        1. this.activeImgIndex(index); -> KO variable
        2. $(document).find(".cc-viewer-pane").removeClass("hide"); -> removes class
        3. $(document).find("#display-video").addClass("hide"); -> adds class

        Overview: Sets the activeImgIndex to index param. Removes "hide" attribute from elements with ".cc-viewer-pane" class. Adds "hide" attribute to the "#display-video" element. returns false.

    function assignImagesToProduct: takes in pInput and is called twice in variables_miscFunctions.js 

        1. NO TRUE FUNCTION CALLS

        Overview: Checks to see if the product is rendered. If true, sets initial primary image urls. Next, sets new urls for all other image variables on product. Last does a valueHasMutated call which is in OCC documentation to update JS array.

    function loadImage: is not called outside of its own fuction body.

    function groupImages: called once in variables_miscFunctions.js, twice in onConfigReady.js, and once in beforeAppear.js. takes in imageSrc

        1. NO REAL FUNCTION CALL

        Overview: If checks imageSrc, but not sure if imageSrc is a boolean or an array based on the next logic. Next, is an iteration through is length, pushing an array with imageSrc[i] into images. If not divisible by 4, imageSrc[i] is appended to the end. Returns images, an array of the imagesSrc.

    File Overview: two functions can be removed. two functions appear to be useful. 


file optionsFunctions.js: 

    function allOptionsSelectedF: takes in nothing
        1. this.variantOptionsArray() 
        
        Overview: checks to see if there are variantOptions. If true, then checks to see if the selectedOption is valid and not disabled. If valid and not diabled, then allOptionsSelected is false and the selected sku is set to null. Returns either true or false for allOptions selected.
    
    function opusOption: takes in the data and event
        
        1. $(event.currentTarget).find('#storePickupWarngModal .store-warning input[type=radio]:checked').attr('id') -> grabs an id based on the storePickupModal that is checked
        2. data.isPhotoUpload(false); -> only sets isPhotoUplad to false if "s-decline-upload" === sWarnOptid
        3. $('#available-today').prop('checked', false) -> sets the property to false for the available-today element only if "s-accept-upload" === sWarnOptid
        4. $('#ship-to-home').prop('checked', true)-> sets the property to true for the ship-to-home element only if "s-accept-upload" === sWarnOptid
        5. data.isPhotoUpload(true) -> sets isPhotoUpload to true only if "s-accept-upload" === sWarnOptid
        6. $.Topic("opusIsPhotoUpload").publish(data.isPhotoUpload()); -> publishes the isPhotoUpload variable to the "opusIsPhotoUpload" topic
        
        Overview: Finds the id of store warning that is checked. Does an if else if statement to see if the sWarnOptid variable is equal to "s-decline-upload" or "s-accept-upload"

    function allOptionsSelected: takes in nothing

        1. Lines 1 - 10 are the exact same as the allOptionsSelectedF function
        2. this.getSelectedSku(variantOptions); -> method returns the selected sku in the product, Based on the options selected. Found in getter.js
        3. this.selectedSku(selectedSKUObj); -> fills in the selectedSKUObj with t
        4. this.refreshSkuData(this.selectedSku()); -> sets the selectedSku to the variantOptions. 

        Overview: Starts off checking to see if there are variantOptions. If true, then checks to see if the selectedOption is valid and not disabled. If valid and not diabled, then allOptionsSelected is false and the selected sku is set to null. Next, if allOptionsSelect is set to true, grabs this.getSelectedSku(variantOptions) and does a null check. Next, calls refreshSkuData with the selectedSku. Returns allOptionsSelected boolean.

    funciton mapOptionsToArray:takes in the variantOptions and order

        1. variantOptions.hasOwnProperty(order[idx])
        2. optionArray.push -> puts an K:V pair into the optionArray where K = order[idx] and value = variantOptions[order[idx]], then sets visible to true
        
        Overview: this method converts the map to array of key value object and sorts them based on the enum value to use it in the select binding of knockout

    function populateVariantOptions: takes in widget

        1. widget.productVariantOptions(); -> grabs the productVariantOptions, can be found at the widget level using console.log(widget) 
        2. this.mapOptionsToArray(options[i].optionValueMap, productLevelOrder ? productLevelOrder : productTypeLevelVariantOrder[options[i].optionId]); -> function can be found above.
        3. this.productVariantModel(options[i].optionName, options[i].mapKeyPropertyAttribute, optionValues, widget, options[i].optionId); -> function can be found in the variables_miscFunctions.js file

        Overview: Method populates productVariantOption model to display the variant options of the product. Starts with a null check on productVariantOptions. If null, the imgMedata is set to the productImagesMetadata and then the variantOptionsArray is fed a null array. If not null, there are 6 variables set up (optionsArray, productLevelOrder, productTypeLevelVariantOrder, optionValues,productVariantOption, variants) and uses a for loop until typeIdx (starts at 0) is less than typeLen. Inside the for loop, there is an if equals comparison between the widget.productTypes()[typeIdx].id and widget.product().type(). If true, the variants variable is populated with widget.productTypes()[typeIdx].variants along with another for loop to set the productTypeLevelVariantOrder[variants[variantIdx].id] = variants[variantIdx].values. Once the for loop with the typeIdx less than typeLen condtion is over, another for loop is added to iterate through all the options. To start this for loop, we check to see if widget.product().variantValuesOrder[options[i].optionId] is true. If true, productLevelOrder is set to widget.product().variantValuesOrder[options[i].optionId](). If false, the for loop moves to set the optionsValues variable to the result of the mapOptionsToArray function above, with parameters (options[i].optionValueMap, productLevelOrder ? productLevelOrder : productTypeLevelVariantOrder[options[i].optionId]). After the mapping function call, The produductVariantModel function is called. The result is then pushed into the optionsArray and sets a timeout on the widget.loadCarousel() for 100 milliseconds.

    File Overview: Only duplicated LOC is found in the first 10 lines of allOptionsSelected and allOptionsSelectedF functions.

file validatefunctions.js: 
    
    function validateAddToSpace: takes in nothing is not referenced in the whole JS collection
        
        1. getSelectedSku() -> referenced in getter.js, "this method returns the selected sku in the product, Based on the options selected"

        Overview: Does a check to see if, all the variantOptions are valid and not disabled. Next, a quantity check is done to see if itemQty() is not null. Then, if not all of the options are selected, an aria-disabled attribute is set to true on the id '#cc-prodDetailsAddToSpace'

    function validateAndSetSelectedSku: takes in refreshRequired but is not called anywhere in the whole JS collection.

    function validateAddToCart: is not called anywhere in the whole JS collection

    function quantityIsValid: is referenced in validateAddToCart but validateAddToCart is not used ever.

    function validForSingleSelection: takes in optionId and is referenced in updateSingleSelection found in updateFunctions.js

        1. NO FUNCTIONS CALLS

        Overview: Iterates through the variantOptionsArray to see if the variant option is disabled, OR variant option id doesnt match passed in optionId, OR selectedOption is not undefined. If one is met, Return True. If false, a check to see if the variantOption.optionIf not equal to passed in optionId AND selectedOption is undefined AND variantOption's countVisibleOptions is equal to 1. If True, return True. Failing both if checks will result in a return of false.

    function isSelectedBomArrayValid: takes in widget and is referenced in onConfigReady.js

        1. No real function calls

        Overview: Returns a boolean. However, it appears to check to see if bomArray length matches selectedBomArray length AND does a check to see if all selectedBomItems are in stock.

    File Overview: 4 functions are never used. 2 are being called somewhere else but not sure of their full purpose in the program.







(NOT DONE) file updateFunctions.js: 
    
    function updateSingleSelection: takes in selectedOptionID and is referenced in filterOptionValues found in filterFunctions.js 

(NOT DONE) file filterFunctions.js: 

    function 

(NOT DONE) file onConfigReady.js: 

    function onConfigReady: takes in widget and is referenced in onLoad.js.

(NOT DONE) file beforeappear.js:

(NOT DONE) file getter.js: 

    function getfallBackDatafromSku: takes in widget, called in onLoad.js and beforeAppear.js

        1. 

        Overview: Makes the same REST call in both cases where the only change is the skuId