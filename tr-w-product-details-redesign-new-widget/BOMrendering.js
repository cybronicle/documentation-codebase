// current input = the old bill of material format
$.Topic('SKU_OBJ_BOM').subscribe(function(product_obj) {
    // making an copy of the old product BOM
    publishedSkuData = product_obj;
    // getting stockAvailable
    skuInventory = widget.stockAvailable();
    // setting up variables
    var slot_variant, curr_index,
        sku_id = product_obj.id;
    // KO variable
    widget.selectedBomArray([]);

    //is used for the BOM id
    if (product_obj.BOMKey) {
        //checks to see if there is any data in the skuBOM base on the componentGroupReference
        if (widget.skuBOM[sku_id]) {
            widget.bomArray(widget.skuBOM[sku_id]);
            $(".bom-row.owl-carousel").each(function() {
                $(this).owlCarousel({
                    items: 5,
                    pagination: false,
                    navigation: true,
                    navigationText: false,
                    rewindNav: false,
                    itemsMobile: [479, 5],
                    itemsDesktop: [1199, 5],
                    itemsDesktopSmall: [980, 5],
                    itemsTablet: [768, 5],
                    itemsTabletSmall: false
                });
            });
            return;
        }

        var bom_variant_arr = [];

        var value = 0;

        //will have 0 or i+1 passed in based on iteration of call
        function executeBOMAjax(value) {

            //passes in the slot variant ids, correlates to the slot[i].componentGroupReference
            //rest call
            ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, function(
            products) {
                //array
                var slot_bom = [];
                //check to see if there is a product variant component group
                if (products.length > 0) {

                    // descr_arr is equivalent to sku, can be called with JSON.parse(widget.product().childSKUs[i].tr_billOfMaterialJson().referencedComponentGroups.*slots[i].componentGroupReference*[i].sku)
                    // name_arr is equivalent to month, can be called with JSON.parse(widget.product().childSKUs[i].tr_billOfMaterialJson().referencedComponentGroups.*slots[i].componentGroupReference*[i].description)
                    // urls, not sure the use of longDescription. Have only seen null.
                    var descr_arr = products[0].description.split(','),
                        name_arr = products[0].names.split(',');

                    // iterating through the componentGroupReference skus and 
                    // creates objects based on the content for slot_bom
                    for (var i = 0; i < descr_arr.length; i++) {
                        
                        // creates dummy object
                        var obj = {};

                        // trims & sets the month off. 
                        obj.skuid = descr_arr[i].trim();

                        // sets the sku.
                        obj.name = name_arr[i];

                        // sets url based on sku
                        obj.imgurl = "/file/products/" + obj.skuid + ".jpg";

                        //not sure why this is set
                        obj.slot = "slot" + (value + 1);

                        //sets prodsku to parent id
                        obj.prodsku = sku_id;

                        //populating array with object
                        slot_bom.push(obj);
                    }


                    //fills bom_variant_arr with slot_bom
                    bom_variant_arr[bom_variant_arr.length] = slot_bom;

                    //populating empty bomArray with bom_variant_arr
                    widget.bomArray(bom_variant_arr);

                    // populates widget skuBOM with parent sku
                    widget.skuBOM[sku_id] = bom_variant_arr;

                    //sets each row with 5 options for the variant row and some context to the slider
                    $(".bom-row .owl-carousel").each(function() {
                        $(this).owlCarousel({
                            items: 5,
                            pagination: false,
                            navigation: true,
                            navigationText: false,
                            rewindNav: false,
                            itemsMobile: [479, 5],
                            itemsDesktop: [1199, 5],
                            itemsDesktopSmall: [980, 5],
                            itemsTablet: [768, 5],
                            itemsTabletSmall: false
                        });
                    });

                }
            });
        }
        //calls this function in order for this to function work on publish
        executeBOMAjax(value);
    //end of if componentgroupreferences
    }

    //checks to see if the parent sku is backordered and 
    //if either the stockAvailable is zero or if the itemQty
    //is greater than the stockAvailable
    if ((publishedSkuData.backorder && (widget.stockAvailable() === 0 || widget.itemQty() > widget
            .stockAvailable()))) {

    } else {
        //sets two flags for the product
        widget.deliveryDateHeadingFlag(true);
        widget.backOrderFlag(false);
        
        // sets a delivery date
        var fullDate = product_obj.BWYDeliveryDates || '';

        // checks if the the delivery date is not empty or null.
        if (fullDate !== '' && fullDate !== null) {
            // finds the dash in the date
            var index = fullDate.indexOf('-');
            // finds grabs the contents after the dash
            var newDate = fullDate.substring(index + 1);
            //replaces the dash with a slash
            newDate = newDate.replace('-', '/');
            //sets deliveryDate
            newDate = "Estimated Arrival " + newDate;
            widget.standardDeliveryDate(newDate);
        }

        // checks to see if the height of the container is 10 pixels 
        // less than the height of the string 
        if ($(".tr-e-product-description #descr-container").height() + 10 < $(
                ".tr-e-product-description #descr-container .tr-prod-descr-string").height()) {
            $(".tr-prod-descr-show-more").show();
        } else {
            $(".tr-prod-descr-show-more").hide();
        }

        // checks to see if the product delivery date is not undefined and true.
        // sets bwyFlag and bwyFlagDate
        // can be found at widget.product().BWYDeliverDates()
        if (typeof product_obj.BWYDeliveryDates !== "undefined" && product_obj.BWYDeliveryDates) {
            widget.bwyFlag(true);
            widget.bwyFlagDate(product_obj.BWYDeliveryDates);
        } else {
            widget.bwyFlag(false);
        }

        // checks to see if the product delivery date is not undefined and true.
        // sets two_ndFlag and two_ndFlagDate
        // can be found at widget.product().two2NDDeliverDates()
        if (typeof product_obj.two2NDDeliveryDates !== "undefined" && product_obj
            .two2NDDeliveryDates) {
            widget.two_ndFlag(true);
            widget.two_ndFlagDate(product_obj.two2NDDeliveryDates);
        } else {
            widget.two_ndFlag(false);
        }

        // checks to see if the product delivery date is not undefined and true.
        // sets airFlag and airFlagDate
        // can be found at widget.product().AIRDeliverDates()
        if (typeof product_obj.AIRDeliveryDates !== "undefined" && product_obj.AIRDeliveryDates) {
            widget.airFlag(true);
            widget.airFlagDate((product_obj.AIRDeliveryDates));

        } else {
            widget.airFlag(false);
        }

        // checks to see if the product delivery date is not undefined and true.
        // sets prmFlag and prmFlagDate
        // can be found at widget.product().PRMDeliverDates()
        if (typeof product_obj.PRMDeliveryDates !== "undefined" && product_obj.PRMDeliveryDates) {
            widget.prmFlag(true);
            // calls getPRMDate, found in onConfigReady
            widget.prmFlagDate(getPRMDate(product_obj.PRMDeliveryDates));
        } else {
            widget.prmFlag(false);
        }
    }
    //publishes destroy inventory spinner
    $.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
});