processPersonalizationCost: function (personalizationCostObj) {
				var item_wise_cost_arr = [];
				for (var prop in personalizationCostObj) {
					var obj = {};
					if (personalizationCostObj.hasOwnProperty()) {
						continue;
					}
					var zone_obj = personalizationCostObj[prop];
					for (var zone_prop in zone_obj) {


						var inernal_obj = zone_obj[zone_prop];
						for (var per_props in inernal_obj) {
							if (obj[inernal_obj[per_props]] && obj[inernal_obj[per_props]["name"]]) {
								obj[inernal_obj[per_props]["name"]] = Number(obj[inernal_obj[per_props]["name"]]) + Number(inernal_obj[per_props]["price"])
							} else {
								if (obj[inernal_obj[per_props]])
									obj[inernal_obj[per_props]["name"]] = inernal_obj[per_props]["price"];
							}
						}
					}
					item_wise_cost_arr.push(obj);
				}
				return item_wise_cost_arr;
            },
            function processInventory(reqData, stores, quantity, storeNumber, locationInventoryInfo) {
                var sthSelected = $('#ship-to-home').prop('checked');

                widget.prodAvlFlag(1);
                var responseData = helpers.storeLocatorUtils().getInventoryResponse(stores, locationInventoryInfo, quantity, storeNumber, 4);
                var data_obj = reqData;
                var data2 = func2();


                if (!data_obj.skuId) {
                    return;
                }

                var data = responseData,
                    opusProductFlag = data_obj.opusProductFlag,
                    stsProductFlag = data_obj.stsProductFlag,
                    skuid = data_obj.skuId;

                var selectedStore = data.selectedStore,
                    displaySTSflag, store_obj, curr_store_obj,
                    my_store_arr = [],
                    other_store_arr = [];

                if (data.dcInventory === 0 && data.storePickupEligibleQty === false) {
                    if (data_obj.quantity > 1) {
                        $(".tr-e-delivery").find(".error-msg").show().text("The requested quantity is not available for purchase.")
                    } else {
                        $(".tr-e-delivery").find(".error-msg").show().text("This product is no longer available for Purchase.")
                    }
                    $("#inventory-error").text("");
                    widget.displayOpus(false);
                    widget.displaySts(false);
                    widget.displayShipToHome(false);
                    widget.prodAvlFlag(0);
                    widget.personalizeVisibilityFlag(false);
                    return;
                }

                widget.sfsStoreId(data.sfsStoreId);
                widget.isSFS(data.isSFS);

                // var dc_qty = data2[0].productSkuInventoryStatus[Number(skuid)];
                // var dc_qty = data2;
                var dc_qty = 0;
                widget.DCInventory(data.dcInventory);
                widget.maxQuantityStore(data.maxQuantityStore);
                /*if(data.dcInventory < data.maxQuantityInStore){
                    dc_qty = data.maxQuantityInStore;
                }
                else{

                }*/

                dc_qty = data.dcInventory;
                widget.availableSTHInventory(dc_qty);


                for (var i = 0; i < data.otherStores.length; i++) {
                    store_obj = {};
                    curr_store_obj = data.otherStores[i];
                    store_obj.storeNumber = curr_store_obj.storeNumber || "";
                    store_obj.threshold = curr_store_obj.threshold || 0;
                    store_obj.storeName = curr_store_obj.storeName || "";
                    store_obj.storeHours = curr_store_obj.storeHours || "";
                    store_obj.shipToStoreAvailable = curr_store_obj.shipToStoreAvailable || false;
                    store_obj.shipToActive = curr_store_obj.shipToActive || "N";
                    store_obj.isAvailableToday = curr_store_obj.isAvailableToday || 0;
                    store_obj.distanceInMiles = curr_store_obj.distanceInMiles || 0;
                    store_obj.deliveryDate = curr_store_obj.deliveryDate || "Today";
                    store_obj.closeTime = curr_store_obj.closeTime || 0;
                    store_obj.isAvailableForPickup = curr_store_obj.isAvailableForPickup || 0;
                    store_obj.onHandQuantity = curr_store_obj.onHandQuantity || 0;
                    store_obj.address = curr_store_obj.addressLine1 + " " + curr_store_obj.addressLine2;
                    store_obj.address1 = curr_store_obj.addressLine1;
                    store_obj.address2 = curr_store_obj.addressLine2;
                    store_obj.formattedDeliveryDate = curr_store_obj.formattedDeliveryDate;
                    store_obj.phone = curr_store_obj.phone;
                    store_obj.openTime = curr_store_obj.openTime ? curr_store_obj.openTime + " - " + store_obj.closeTime : 0;
                    store_obj.longitude = curr_store_obj.longitude;
                    store_obj.latitude = curr_store_obj.latitude;
                    store_obj.zipCode = curr_store_obj.zipCode;
                    store_obj.city = curr_store_obj.city;
                    store_obj.state = curr_store_obj.state;

                    other_store_arr.push(store_obj);
                }

                widget.otherStores(other_store_arr);
                var my_store_obj = {};

                if (selectedStore) {
                    my_store_obj.storeNumber = selectedStore.storeNumber || "";
                    my_store_obj.threshold = selectedStore.threshold || 0;
                    my_store_obj.storeName = selectedStore.storeName || "";
                    my_store_obj.storeHours = selectedStore.storeHours || "";
                    my_store_obj.shipToStoreAvailable = selectedStore.shipToStoreAvailable || false;
                    my_store_obj.shipToActive = selectedStore.shipToActive || "N";
                    my_store_obj.isAvailableToday = selectedStore.isAvailableToday || 0;
                    my_store_obj.distanceInMiles = selectedStore.distanceInMiles || 0;
                    my_store_obj.deliveryDate = selectedStore.deliveryDate || "Today";
                    my_store_obj.closeTime = selectedStore.closeTime || 0;
                    my_store_obj.isAvailableForPickup = selectedStore.isAvailableForPickup || 0;
                    my_store_obj.onHandQuantity = selectedStore.onHandQuantity || 0;
                    my_store_obj.address = selectedStore.addressLine1 + " " + selectedStore.addressLine2;
                    my_store_obj.address1 = selectedStore.addressLine1;
                    my_store_obj.address2 = selectedStore.addressLine2;
                    my_store_obj.formattedDeliveryDate = selectedStore.formattedDeliveryDate;
                    my_store_obj.phone = selectedStore.phone;
                    my_store_obj.openTime = selectedStore.openTime ? selectedStore.openTime + " - " + my_store_obj.closeTime : 0;
                    my_store_obj.longitude = selectedStore.longitude;
                    my_store_obj.latitude = selectedStore.latitude;
                    my_store_obj.zipCode = selectedStore.zipCode;
                    my_store_obj.city = selectedStore.city;
                    my_store_obj.state = selectedStore.state;


                    my_store_arr.push(my_store_obj);
                    widget.myStore(my_store_arr);
                }

                widget.displayOpus(false);

                widget.displayShipToHome(false);
                widget.dcQuantity(dc_qty);
                if (widget.availableSTHInventory() >= widget.itemQty()) {
                    // display ship to home
                    widget.displayShipToHome(true);
                    widget.backOrderFlag(false);
                    $("#ship-to-home").attr("checked", true);
                }
                $(".tr-pickup-availability").hide();
                /////////// New OPUS-Logic
                //if(opusProductFlag) {
                if (selectedStore && selectedStore.isAvailableForPickup) {
                    widget.availableTodayStore(selectedStore.storeName);
                    widget.availableState(selectedStore.state);
                    widget.availableCity(selectedStore.city);
                    widget.availableQuantity(selectedStore.onHandQuantity);
                    widget.displayOpus(true);
                    widget.displaySts(false);
                    widget.availableTodayVisibility(true);
                    if (selectedStore.isAvailableToday) {
                        // widget.availableDateText("Pick Up " + selectedStore.deliveryDate);
                        $(".tr-pickup-availability").show();
                    } else {
                        widget.availableTodayStore(selectedStore.storeName);
                        widget.availableState(selectedStore.state);
                        widget.availableCity(selectedStore.city);

                        widget.availableQuantity(selectedStore.onHandQuantity);
                        // widget.availableDateText("Pick Up " + selectedStore.deliveryDate);
                    }
                    if (widget.displayShipToHome() === false) {
                        $("#available-today").prop("checked", true);
                    }
                } else if (selectedStore && selectedStore.shipToStoreAvailable) {

                    if (productSts) {
                        widget.displayOpus(false);
                        widget.displaySts(true);
                        widget.availableTodayVisibility(false);
                        widget.stsStore(selectedStore.storeName);
                        widget.stsAvailableText("Available for store pickup " + selectedStore.storeHours[5].date);
                        if (widget.displayShipToHome() === false) {
                            $("#available-sts").prop("checked", true);
                        }
                    }
                } else {
                    if (data.storePickupEligibleQty === true && selectedStore) {
                        widget.displayOpus(true);
                    } else {
                        widget.displayOpus(false);
                    }
                    widget.displaySts(false);
                }

                if (widget.displayOpus() === false &&
                    widget.displaySts() === false &&
                    widget.displayShipToHome() === false) {

                    if (widget.finalBackOrderFlag()) {
                        widget.displayShipToHome(true);
                        widget.handleBackOrderDate();
                    } else {
                        $(".tr-e-delivery").find(".error-msg").show().text("This product is no longer available for Purchase.")
                        $("#inventory-error").text("");
                        widget.prodAvlFlag(0);
                        widget.personalizeVisibilityFlag(false);
                    }
                }

                $.Topic("displayOpus").publish(widget.displayOpus());
                $.Topic("displaySts").publish(widget.displaySts());
                $.Topic("displayShipToHome").publish(widget.displayShipToHome());

                $("[id^=personalize-button]").prop("disabled", false);
                $("#inventory-error").text("");
                if ($("#available-today").prop("checked")) {
                    if (!widget.myStore()[0].isAvailableForPickup && widget.displaySts()) {
                        widget.availableTodayVisibility(false);
                    }
                    if (!widget.myStore()[0].isAvailableForPickup && !widget.displaySts()) {
                        $("#inventory-error").text("Sorry, only “" + widget.myStore()[0].onHandQuantity + "” available. Please reselect quantity to proceed")
                    } else {
                        $("#inventory-error").text("");
                    }
                } else if ($("#available-sts").prop("checked")) {
                    if (widget.myStore()[0].onHandQuantity < widget.itemQty()) {
                        $("#inventory-error").text("Sorry, only “" + widget.myStore()[0].onHandQuantity + "” available. Please reselect quantity to proceed")
                    } else {
                        $("#inventory-error").text("");

                    }

                } else if (sthSelected) {
                    if (widget.itemQty() > widget.availableSTHInventory() && widget.availableSTHInventory() > 0) {
                        widget.displayShipToHome(true);
                        widget.personalizeVisibilityFlag(true);

                        $("[id^=personalize-button]").prop("disabled", true);
                        $('#ship-to-home').click();
                        $("#inventory-error").text("Sorry, the selected quantity is not available .Please reselect the quantity to proceed.")
                        $(".tr-e-delivery").find(".error-msg").text('');
                    } else {
                        $("#inventory-error").text("");
                        $("[id^=personalize-button]").prop("disabled", false);

                    }

                }
                widget.destroyInventorySpinner();


            }