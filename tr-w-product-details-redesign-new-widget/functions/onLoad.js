onLoad: function (widget) {
			    $.Topic('SKU_SELECTED').subscribe(function(){
					if(widget.product().childSKUs().length > 1){
						var skuId = "";
						var giftSku = "";
						if(widget.product().hasGift() && widget.product().giftSkuId() && widget.product().giftSkuId() !== "") {
							giftSku = "?giftSku=" + widget.product().giftSkuId();
						}
						widget.personalizeDisabledFlag(true);
						if(widget.selectedSku() !== null){
							skuId = widget.selectedSku().repositoryId;
							$.ajax({
									url : widget.site().extensionSiteSettings.externalSiteSettings.awsUrl + skuId + giftSku,
									method: "GET",
									success : function(data) {
										isPersonalizationPropertyExist = data.zones.length;
										if(data.zones.length > 0){
											newPersonalizationData = data;
											widget.generateExpressPhase(newPersonalizationData);
											widget.generateFonts(newPersonalizationData);
											widget.generalFonts(newPersonalizationData);
											widget.generateColorFills(newPersonalizationData);
											widget.generateDesigns(newPersonalizationData);
											widget.generateNographic(newPersonalizationData);
											var viewModel = ko.mapping.fromJS(newPersonalizationData);
											$.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
										}else if($('.error-msg').is(':visible') === false){
											$('.tr-xs-personalize-block').removeClass('hide');
											$(".personalizationBtn").hide();
											$(".addToCartBtn").show();
											$(".personalizationBtns").removeClass('hide');
										}
										widget.destroyInventorySpinner();
									},
									error: function() {
										getfallBackDatafromSku(widget);
									}
								});
						}
					}
			    });
				
				
				$.Topic('PERSONALIZATION_BACK_BUTTON').subscribe(function(){
					var viewModel = ko.mapping.fromJS(newPersonalizationData);
					$.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
				});

				$.Topic('TR_PDP_NEW_PERSONALIZATION_DATA_LOAD_SUCCESS.memory').subscribe(function(){
					widget.checkPersonalizationExist();
					if(isPersonalizationPropertyExist > 0) {
						widget.personalizeDisabledFlag(false);
						widget.personalizeVisibilityFlag(true);
					} else {
						widget.personalizeDisabledFlag(true);
						widget.personalizeVisibilityFlag(false);
					}
				});
			    
			    widget.cart().addItem = function (data) {
			        var self = this;
                    if (data && data.childSKUs) {
                      var productFound = false;
                      var newQuantity;
                      // Create sub-items as CartItems, if they exist.
                      var childItems;
                      if (self.isConfigurableItem(data)) {
                        childItems = [];
                        childItems = self.getChildItemsDataAsCartItems(data);
                      }
                      if(data.selectedAddOnProducts && data.selectedAddOnProducts.length > 0) {
                        var addonChildItems = self.getAddonDataAsCartItems(data);
                        if(childItems && childItems.length > 0) {
                          childItems = childItems.concat(addonChildItems);
                        } else {
                          childItems = [];
                          childItems = addonChildItems;
                        }
                      }
                      // Check for line item only if there are no child items.
                      var cartItem = null;
                      if (!data.commerceItemId && self.isConfigurableItem(data)) {
                        cartItem = self.getConfigurableCartItem(data.id, data.childSKUs[0].repositoryId, data.commerceItemId);
                      } else {
                        if(data.selectedSku) {
                          cartItem = self.getCartItem(data.id, data.selectedSku.repositoryId, data.commerceItemId);
                        } else {
                          cartItem = self.getCartItem(data.id, data.childSKUs[0].repositoryId, data.commerceItemId);
                        }
                        //Search for corresponding cart item only if line items are to be combined, else proceed just like a new item being added to cart.
                        if(!(self.combineLineItems == CCConstants.COMBINE_YES || (cartItem && cartItem.isUpdate()))){
                          cartItem = null;
                        }
                      }
                      if(self.isProductWithAddons(data) && cartItem && !cartItem.isUpdate()) {
                           // In case of Product with add-ons, and if Product is not a CPQ product, always create
                           // a new cart item if its not under update, as shopper may have different shopperInputs and need not decide based
                           // on the actual ShopperInput data
                        cartItem = null;
                      }
                      if (cartItem !== null ) {
                        if(!cartItem.isUpdate()) {
                          newQuantity = cartItem.quantity() + data.orderQuantity;
                          cartItem.quantity(newQuantity);
                          cartItem.updatableQuantity(cartItem.quantity());
            
                          // Add giftWithPurchaseSelections of the cart item
                          if (data.giftProductData) {
                            cartItem.giftWithPurchaseSelections = [
                              {
                                "giftWithPurchaseIdentifier": data.giftProductData.giftWithPurchaseIdentifier,
                                "promotionId": data.giftProductData.promotionId,
                                "giftWithPurchaseQuantity": data.giftProductData.giftWithPurchaseQuantity
                              }];
                          }
                          $.Topic(pubsub.topicNames.CART_UPDATE_QUANTITY_SUCCESS).publishWith(null,
                                  [{message:"success", cartItem: cartItem, prodData:data, createNewSGR: true}]);
                            if(this.storeConfiguration.isLargeCart() === true){
                              self.updateItemPriceForLargeCart(data,cartItem);
                              self.updateItemShippingGroupRelationShipForLargeCart(data,cartItem);
                            }
                        } else {
                            if (cartItem.shippingGroupRelationships().length > 1) {
                                cartItem.productData(data);
                                cartItem.updatableQuantity(data.orderQuantity);
                                $.Topic(pubsub.topicNames.CART_UPDATE_QUANTITY_SUCCESS).publishWith(null,
                                        [{message:"success", createNewSGR: false, data: cartItem, prodData: data}]);
                                cartItem.quantity(data.orderQuantity);
                            }
                            else {
                              var sum =0;
                              cartItem.productData(data);
                              cartItem.shippingGroupRelationships()[0].quantity(data.orderQuantity);
                              cartItem.shippingGroupRelationships()[0].updatableQuantity(data.orderQuantity);
                              cartItem.updatableQuantity(data.orderQuantity);
                              for(var index=0; index< cartItem.shippingGroupRelationships().length; index++) {
                                  sum+= parseFloat(cartItem.shippingGroupRelationships()[index].quantity());
                                  cartItem.shippingGroupRelationships()[index].catRefId = data.childSKUs[0].id;
                              }
                              cartItem.quantity(sum);
                              cartItem.catRefId = data.childSKUs[0].id;
                            }
                            if(data.selectedAddOnProducts && data.selectedAddOnProducts.length > 0) {
                                var addonChildItems = self.getAddonDataAsCartItems(data);
                                //to check if the cartItem.childItems exists or not
                                if(cartItem.childItems && cartItem.childItems.length > 0) {
                                  cartItem.childItems = cartItem.childItems.filter(function(childItem) {
                                    return !childItem.addOnItem;
                                  });
                                }
                               /* duplicate condition checks because above we are filtering the cartItem.childItems
                                 and need to check the length again. This will be the case when the product has a
                                 CPQ childItems and add on products. If CPQ childitems exists, the new addOns will
                                 be concatinated, otherwise the value can be over-written. */
                                if(cartItem.childItems && cartItem.childItems.length > 0) {
                                  cartItem.childItems = cartItem.childItems.concat(addonChildItems);
                                } else {
                                  cartItem.childItems = [];
                                  cartItem.childItems = addonChildItems;
                                }
                            }
                        }
                        productFound = true;
                      }
                      // Adding a condition for reconfiguration flow. In this case the commerceItemId will
                      // be null. However the configuratorId should be set for the item.
                      if ((data.commerceItemId == null) && self.isConfigurableItem(data)) {
                        cartItem = self.getCartItemForReconfiguration(data.id, data.childSKUs[0].repositoryId, data.configuratorId);
                        if (cartItem !== null) {
                          // Update the childItems
                          // Do not overwrite the childItems directly, as there could be add-on product childItems
                          // existing with the CPQ main product item
                          var addOnProductItems = [];
                          for(var index=0; index<cartItem.childItems.length; index++) {
                            if (cartItem.childItems[index].addOnItem) {
                              // If the childItem contains another level of childItems, as in multi level
                              // add-ons then it will be automatically handled as we are persisting the
                              // entire childItem object
                              addOnProductItems.push(cartItem.childItems[index]);
                            }
                          }
                          if (addOnProductItems.length > 0) {
                            childItems = childItems.concat(addOnProductItems);
                          }
                          cartItem.childItems = childItems;
                          cartItem.externalPrice(data.price);
                          cartItem.externalData((data.externalData || []).map(function (data) {
                            return new CartItemExternalData(data);
                          }));
                          cartItem.actionCode(data.actionCode);
                          cartItem.externalRecurringCharge(data.externalRecurringCharge);
                          cartItem.externalRecurringChargeFrequency(data.externalRecurringChargeFrequency);
                          cartItem.externalRecurringChargeDuration(data.externalRecurringChargeDuration);
                          cartItem.assetId(data.assetId);
                          cartItem.serviceId(data.serviceId);
                          cartItem.customerAccountId(data.customerAccountId);
                          cartItem.billingAccountId(data.billingAccountId);
                          cartItem.serviceAccountId(data.serviceAccountId);
                          cartItem.billingProfileId(data.billingProfileId);
                          cartItem.activationDate(data.activationDate);
                          cartItem.deactivationDate(data.deactivationDate);
                          cartItem.transactionDate(data.transactionDate);
                          cartItem.clearUnpricedError();
                          cartItem.updatableQuantity.rules.remove( function(updatableQuantity) {
                            return updatableQuantity.rule == 'max';
                          });
                          self.getCartAvailability();
                          // Add giftWithPurchaseSelections of the cart item
                          if (data.giftProductData) {
                            cartItem.giftWithPurchaseSelections = [
                              {
                                "giftWithPurchaseIdentifier": data.giftProductData.giftWithPurchaseIdentifier,
                                "promotionId": data.giftProductData.promotionId,
                                "giftWithPurchaseQuantity": data.giftProductData.giftWithPurchaseQuantity
                              }
                            ];
                          }
                          productFound = true;
                        }
                      }
                      // If product is not in the cart then add it with the quantity set on the new product.
                      if (!productFound) {
                        newQuantity = data.orderQuantity;
            
                        var cartItemData = {
                          productId: data.id,
                          productData: data,
                          quantity: newQuantity,
                          catRefId: data.childSKUs[0].repositoryId, 
                          selectedOptions: data.selectedOptions,
                          currency: self.currency,
                          externalData: data.externalData,
                          actionCode: data.actionCode,
                          lineAttributes: self.lineAttributes,
                          externalRecurringCharge: data.externalRecurringCharge,
                          externalRecurringChargeFrequency: data.externalRecurringChargeFrequency,
                          externalRecurringChargeDuration: data.externalRecurringChargeDuration,
                          assetId: data.assetId,
                          serviceId: data.serviceId,
                          customerAccountId: data.customerAccountId,
                          billingAccountId: data.billingAccountId,
                          serviceAccountId: data.serviceAccountId,
                          billingProfileId: data.billingProfileId,
                          activationDate: data.activationDate,
                          deactivationDate: data.deactivationDate,
                          transactionDate: data.transactionDate,
                          addOnItem: data.addOnItem,
                          shopperInput: data.shopperInput,
                          selectedStore: typeof data.selectedStore == "function" ? data.selectedStore() : data.selectedStore,
                          availablePickupDateTime : data.availablePickupDateTime
                        };
            
                        if (self.isConfigurableItem(data)) {
                          // Handle configurable items. Expect to get external prices for
                          // the configurable items.
                          $.extend(cartItemData, {
                            configuratorId: data.configuratorId,
                            childItems: childItems,
                            externalPrice: data.price,
                            externalPriceQuantity: -1
                          })
                        } else if (self.isProductWithAddons(data)) {
                          $.extend(cartItemData, {
                            childItems: childItems
                          })
                          // If the product contains add-ons and if the product is externally priced,
                          // then as per the existing restriction on the ExternalPricingCalculator only
                          // -1 can be passed to the externalPriceQuantity
                          var externalQuantity = data.externalPriceQuantity;
                          // Iterate over the childItems and check if any childItem is externally Priced
                          // such that the externalQuantity should be -1 in that case
                          for(var i=0; i<childItems.length; i++) {
                            if(childItems[i].externalPrice) {
                              externalQuantity = -1;
                              break;
                            }
                          }
            
                          if(data.externalPrice) {
                            $.extend(cartItemData, {
                              externalPrice: data.externalPrice,
                              externalPriceQuantity: externalQuantity
                            })
                          }
                        } else if (data.externalPrice && data.externalPriceQuantity) {
                          $.extend(cartItemData, {
                            externalPrice: data.externalPrice,
                            externalPriceQuantity: data.externalPriceQuantity
                          })
                        }
            
                        var productItem = new CartItem(cartItemData);
                        //Update item total for large cart
                        if(this.storeConfiguration.isLargeCart() === true){
                          self.updateItemPriceForLargeCart(data,productItem);
                          self.updateItemShippingGroupRelationShipForLargeCart(data,productItem);
                          /*var price = data.childSKUs[0].salePrice ? data.childSKUs[0].salePrice :data.childSKUs[0].listPrice;
                          productItem.itemTotal(productItem.itemTotal()+price*data.orderQuantity);   */
                        }
                        // SKU properties of the product item.
                        productItem.skuProperties = data.skuProperties;
                        productItem.selectedSkuProperties = data.selectedSkuProperties;
            
                        self.items.push(productItem);
                      }
                      self.isDirty(false);
                      if(this.storeConfiguration.isLargeCart() === true){
                        self.updateCartItemDataForLargeCart(data);
                        self.updateAllItemsArray();
                      }
                      self.markDirty();
                    }
            
                  };

				widget.formatRelatedProducts = function (pProducts) {
					var formattedProducts = [];
					var productsLength = pProducts.length;
					for (var index = 0; index < productsLength; index++) {
						if (pProducts[index]) {
							formattedProducts.push(new product(pProducts[index]));
						}
					}
					return formattedProducts;
				};

				widget.updateSpanClass = function () {
					var classString = "";
					var phoneViewItems = 0,
						tabletViewItems = 0,
						desktopViewItems = 0,
						largeDesktopViewItems = 0;
					if (this.itemsPerRow() == this.itemsPerRowInPhoneView()) {
						phoneViewItems = 12 / this.itemsPerRow();
					}
					if (this.itemsPerRow() == this.itemsPerRowInTabletView()) {
						tabletViewItems = 12 / this.itemsPerRow();
					}
					if (this.itemsPerRow() == this.itemsPerRowInDesktopView()) {
						desktopViewItems = 12 / this.itemsPerRow();
					}
					if (this.itemsPerRow() == this.itemsPerRowInLargeDesktopView()) {
						largeDesktopViewItems = 12 / this.itemsPerRow();
					}

					if (phoneViewItems > 0) {
						classString += "col-xs-" + phoneViewItems;
					}
					if ((tabletViewItems > 0) && (tabletViewItems != phoneViewItems)) {
						classString += " col-sm-" + tabletViewItems;
					}
					if ((desktopViewItems > 0) && (desktopViewItems != tabletViewItems)) {
						classString += " col-md-" + desktopViewItems;
					}
					if ((largeDesktopViewItems > 0) && (largeDesktopViewItems != desktopViewItems)) {
						classString += " col-lg-" + largeDesktopViewItems;
					}

					widget.spanClass(classString);

				};

				widget.checkResponsiveFeatures = function (viewportWidth) {
					widget.isDeskTopMode(false);
					if (viewportWidth > CCConstants.VIEWPORT_LARGE_DESKTOP_LOWER_WIDTH) {
						if (widget.viewportMode() != CCConstants.LARGE_DESKTOP_VIEW) {
							widget.viewportMode(CCConstants.LARGE_DESKTOP_VIEW);
							widget.itemsPerRow(widget.itemsPerRowInLargeDesktopView());
						}
					} else if (viewportWidth > CCConstants.VIEWPORT_TABLET_UPPER_WIDTH &&
						viewportWidth <= CCConstants.VIEWPORT_LARGE_DESKTOP_LOWER_WIDTH) {
						if (widget.viewportMode() != CCConstants.DESKTOP_VIEW) {
							widget.viewportMode(CCConstants.DESKTOP_VIEW);
							widget.itemsPerRow(widget.itemsPerRowInDesktopView());
						}
					} else if (viewportWidth >= CCConstants.VIEWPORT_TABLET_LOWER_WIDTH &&
						viewportWidth <= CCConstants.VIEWPORT_TABLET_UPPER_WIDTH) {
						if (widget.viewportMode() != CCConstants.TABLET_VIEW) {
							widget.viewportMode(CCConstants.TABLET_VIEW);
							widget.itemsPerRow(widget.itemsPerRowInTabletView());
						}
					} else if (widget.viewportMode() != CCConstants.PHONE_VIEW) {
						widget.viewportMode(CCConstants.PHONE_VIEW);
						widget.itemsPerRow(widget.itemsPerRowInPhoneView());
					}
					widget.updateSpanClass();
				};

				widget.checkResponsiveFeatures($(window)[0].innerWidth || $(window).width());
				$(window).resize(
					function () {
						widget.checkResponsiveFeatures($(window)[0].innerWidth || $(window).width());
						widget.viewportWidth($(window)[0].innerWidth || $(window).width());
					});


				widget.relatedProductGroups = ko.computed(function () {
					var groups = [];
					if (widget.product().relatedProducts != null) {

						widget.numberOfRelatedProductsToShow = widget.numberOfRelatedProducts() < widget.product().relatedProducts.length ?
							widget.numberOfRelatedProducts() : widget.product().relatedProducts.length;
						for (var index = 0; index < widget.numberOfRelatedProductsToShow; index++) {
							if (index % widget.itemsPerRow() == 0) {
								groups.push(ko.observableArray([widget.product().relatedProducts[index]]));
							} else {
								groups[groups.length - 1]().push(widget.product().relatedProducts[index]);
							}
						}
					}

					return groups;

				}, widget);

				$("#color").click(function () {
					$("a").css("color", "white");
				});

				getWidget = widget;
				getWidget.cart().combineLineItems = CCConstants.COMBINE_NO;
				CCConstants.COMBINE_YES = "yes";

				$.Topic(pubsub.topicNames.CART_ADD_SUCCESS).subscribe(function () {
					displayStoreChangeMessage = false;
				});

				$.Topic("PROCESS_INVENTORY_PDP_ZIP_SERACH_IR").subscribe(function (inventoryData, callSource) {

					$("#tr-more-store .tr-row").hide();
					$(".service-error-msg").text("");

					widget.otherStores([]);

					if (inventoryData.pickup[0].available) {

						var otherStoreArr = [];
						var x = inventoryData.pickup.length;
						if (callSource == "changeStorePDP") {
							for (var i = 1; i < x; i++) {
								otherStoreArr.push(inventoryData.pickup[i].store);
							}
						} else if (callSource == "changeStorePDPZip") {
							for (var i = 0; i < x; i++) {
								otherStoreArr.push(inventoryData.pickup[i].store);
							}
						}
						widget.otherStores(otherStoreArr);
					}
					var displayStores = widget.otherStores();

					if (callSource == "changeStorePDP") {
						$("#tr-e-available-store").modal('show');
					}
					if (displayStores.length === 0) {
						$("#tr-more-store .tr-row").hide();
						$(".service-error-msg").text("No nearby stores are found for the selected product and quantity.");
						return;
					} else {
						$("#tr-more-store .tr-row").show();
					}


				});

				$.Topic("STORE_STOCK_ERROR_MESSAGE").subscribe(function (errorType) {
					widget.storeStockErrorMessage(errorType);
				});

				$.Topic("VARIANT_NO_STOCK_ERROR_MESSAGE").subscribe(function (errorType) {
					if (widget.variantOptionsArray().length > 0) {
						$(".tr-store-stock-error").hide().text("");
						$(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
						$(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);
						$(".personalizationBtns").addClass('hide');
                        $('.tr-xs-personalize-block').addClass('hide');

						if (widget.variantOptionsArray().length === 1) {
							$(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
							$(".personalizationBtns").addClass('hide');
                            $('.tr-xs-personalize-block').addClass('hide');
						}
					}
					$("[id^=personalize-button]").prop("disabled", true);
					$("[id^=addToCart-button]").prop("disabled", true);
				});

				$.Topic("PROCESS_INVENTORY_PERSONALIZE_QTY_IR").subscribe(function (requestObj, locationStock) {
					widget.storeStockErrorMessage("");
					var cartQuantity = 0;
					for (var i = 0; i < widget.cart().items().length; i++) {
						var item = widget.cart().items()[i];
						var trDeliveryDetails = JSON.parse(item.tr_delivery_details());
						if (item.catRefId === requestObj.skuNumber && trDeliveryDetails.store_inventory == requestObj.storeNumber) {
							cartQuantity += item.quantity();
						}
					}
					var computedqty = Number(requestObj.skuQuantity) + Number(cartQuantity);
					var computedStock = locationStock.stockLevel - locationStock.stockThreshold - computedqty;

					$(".tr-store-stock-error").hide().text("");
					var shippingType = $("[name=optradio]:checked").val();
					if (shippingType == 2) {
						computedStock = widget.availableSTHInventory() - computedqty;
					}


					var variantOptions = widget.variantOptionsArray(),
						sku_obj = widget.getSelectedSku(variantOptions);
					if (widget.product().childSKUs().length == 1) {
						sku_obj = widget.product().childSKUs()[0];
					}

					if (computedStock < 0) {
						if (shippingType == 1) { //pickup
							$(".tr-store-stock-error").show();
							$.Topic("STORE_STOCK_ERROR_MESSAGE").publish("noSelectedStore");
						} else {
							$(".tr-store-stock-error").show();
							$.Topic("STORE_STOCK_ERROR_MESSAGE").publish("noProduct");
						}

						quantityChangeDeliveryTriggered = false;
						//$.Topic('SKU_SELECTED').publish(widget.product(), sku_obj, variantOptions, requestObj.skuQuantity);
					} else {
						quantityChangeDeliveryTriggered = false;
					}

					var skuId = widget.skuRepoId();
					var skuQuantity = widget.itemQty();
					var latitude = $('.my-store-lat').text();
					var longitude = $('.my-store-lon').text();
					var storeNumber = $('.my-store-id').text();
					var reqData = {
						"skuId": skuId,
						"storeNumber": storeNumber,
						"location": {
							"latitude": latitude,
							"longitude": longitude
						},
						"quantity": skuQuantity,
						"radiusLimit": 400,
						"limit": 4
					};
					var stores = helpers.storeLocatorUtils().getStores(latitude, longitude, storeItems);

					var storeIds = helpers.storeLocatorUtils().getStoreIds(stores);
					var cartInventoryDetailsArrayObj = widget.getCartInventoryDetailsArray();


					helpers.storeLocatorUtils().getInventoryResponseIR(stores, computedqty, storeNumber, 4, skuId, storeIds, reqData, "pdpLoad", cartInventoryDetailsArrayObj);

				});

				$.Topic("GIFT_ITEM").subscribe(updateHeader);

				function updateHeader(giftData) {
					//var giftName = giftData[0].displayName;
					widget.giftTitle(" with Free " + giftData[0].displayName);
				}


				$.Topic("PROCESS_INVENTORY_PDP_IR").subscribe(function (data) {
					var shippingType = $("[name=optradio]:checked").val();
					var storeNumber = "920";
					widget.storeStockErrorMessage("");

					if (!data.pickup[0].available && !data.shipping[0].available) {

						$(".tr-store-stock-error").show();
						widget.storeStockErrorMessage("noProduct");

					} else {
						if (shippingType == 1) { //pickup
						    if (data.pickup.length > 0 && !data.pickup[0].available) {
						        $(".tr-store-stock-error").text('');
						        $(".tr-store-stock-error").show();
								widget.storeStockErrorMessage("noSelectedStore");
						        //$("#ship-to-home").click();
						        setTimeout(function(){
						            $("#ship-to-home").prop('checked', true);
						            $('.personalizationBtns').removeClass('hide'); 
						        },200);
						    } else {
    							storeNumber = widget.myStore()[0] ? widget.myStore()[0].storeNumber : $('.my-store-id').text();
    							var responseStoreId = data.pickup.length > 0 && data.pickup[0].available ? data.pickup[0].store.id : null;
    							if ((!responseStoreId || responseStoreId !== storeNumber) && displayStoreChangeMessage) {
    								if (widget.myStore()[0].onHandQuantity > widget.itemQty()) {
    									$(".tr-store-stock-error").show();
    									$(".tr-store-stock-error").text("The nearest store available for pickup today is " + data.pickup[0].store.name + ".")
    								} else {
    									$(".tr-store-stock-error").show();
    									widget.storeStockErrorMessage("noSelectedStore");
    								}
    							}
						    }
						} else {
							if (!data.shipping[0].available) {
								//$(".tr-store-stock-error").show();
								//widget.storeStockErrorMessage("noProduct");
								widget.displayShipToHome(false);
								$('#available-today').click();
							}
						}
					}

					displayStoreChangeMessage = true;
					inventoryDataIR = data;
					//$(".tr-store-stock-error").text("");
					$.Topic("INVENTORY_PERSONALIZE_STH_QTY.memory").publish(0);
					if (data.shipping[0].available) {
						widget.displayShipToHome(true);
						widget.availableSTHInventory(data.shipping[0].skuQuantity);
						$.Topic("INVENTORY_PERSONALIZE_STH_QTY.memory").publish(data.shipping[0].skuQuantity);
					}
					if (data.pickup[0].available) {
						widget.displayOpus(true);
						widget.availableTodayStore(data.pickup[0].store.name);
						widget.availableState(data.pickup[0].store.state);
						widget.availableCity(data.pickup[0].store.city);

						widget.availableQuantity(data.pickup[0].skuQuantity);
						// widget.availableDateText("Pick Up " + data.pickup[0].store.deliveryDate);

						var myStoreArr = [];
						myStoreArr.push(data.pickup[0].store);
						widget.myStore(myStoreArr);

						var otherStoreArr = [];
						var x = data.pickup.length;
						for (var i = 1; i < x; i++) {
							otherStoreArr.push(data.pickup[i].store);
						}
						widget.otherStores(otherStoreArr);
					}
                    
                    $(".personalizationBtns").removeClass('hide');
				    $('.tr-xs-personalize-block').removeClass('hide');
						        
					if ($(".tr-store-stock-error").is(':visible')) {
						if (widget.storeStockErrorMessage() == "noSelectedStore") {
							if (data.pickup[0].available) {
								$(".tr-store-stock-error").text("Due to low inventory, the nearest store available for pickup today is " + data.pickup[0].store.name + ". Select ADD TO BAG to continue, or choose Change Store or Ship to Home.")
							} else if (data.shipping[0].available) {
								$(".tr-store-stock-error").text("This product is no longer available for Purchase In Store, Please change to Ship to home.")
								$(".personalizationBtns").addClass('hide');
						        $('.tr-xs-personalize-block').addClass('hide');
							}
                            
						} else if (widget.storeStockErrorMessage() == "noProduct") {
							if (!data.shipping[0].available && data.pickup[0].available) {
								$(".tr-store-stock-error").text("This product is no longer available for Ship to Home.Please change to Pickup in Store.");
							} else if (!data.shipping[0].available && !data.pickup[0].available) {
								$(".tr-store-stock-error").text("This product is no longer available for Purchase.");
								$(".personalizationBtns").addClass('hide');
						        $('.tr-xs-personalize-block').addClass('hide');
							}

						}
						//$(".personalizationBtns").addClass('hide');
                        //$('.tr-xs-personalize-block').addClass('hide');
					} else {
						widget.storeStockErrorMessage('');
					}

					if (data.pickup[0].available === false && data.shipping[0].available === false && widget.variantOptionsArray().length == 0) {
						$(".tr-button").attr("disabled", "disabled");
						$(".tr-store-stock-error").text("This product is no longer available for Purchase.");
						$(".tr-store-stock-error").show();
						$(".personalizationBtns").addClass('hide');
                        $('.tr-xs-personalize-block').addClass('hide');
					} else if (data.pickup[0].available === false && data.shipping[0].available === false && widget.variantOptionsArray().length > 0) {
						$(".tr-store-stock-error").hide().text("");
						$(".tr-e-product-variation").find(".error-msg").text("Selected combination doesn't have stock, please choose different options").show();
						$(".tr-e-product-variation").find(".error-msg").attr("data-val", 2);
						$(".personalizationBtns").addClass('hide');
                        $('.tr-xs-personalize-block').addClass('hide');

						if (widget.variantOptionsArray().length === 1) {
							$(".tr-e-product-variation").find(".error-msg").text("There is no stock available for the selected variant.");
							$(".personalizationBtns").addClass('hide');
                            $('.tr-xs-personalize-block').addClass('hide');
						}
					}

					var shippingType = $("[name=optradio]:checked").val();
					if ((shippingType == 2 && data.shipping[0].available === true) || (shippingType == 1 && data.pickup[0].available === true)) {
						$("[id^=personalize-button]").prop("disabled", false);
					} else {
						$("[id^=personalize-button]").prop("disabled", true);
					}

					// 

					widget.destroyInventorySpinner();
					$.Topic("INVENTORY_PERSONALIZE_STORE_DATA_IR.memory").publish(widget.myStore(), widget.otherStores());
				});
				$.Topic("CURRENT_PERSONALIZATION_DATA").subscribe(function (data) {
					currentPersonalizationData = data;
					widget.handleCustomAddToCart();
				});
				_this = this;
				widget.loadCarousel();
				var lastDataCacheTimeStamp = storageApi.getInstance().getItem("lastDataCacheTimeStamp");
				if (!lastDataCacheTimeStamp) {
					lastDataCacheTimeStamp = 0;
				}
				refreshData = false;

				/*function checkAndCacheData(refreshData) {
					if (refreshData) {
						var d = new Date();
						var n = d.getTime();
						storageApi.getInstance().setItem("lastDataCacheTimeStamp", n);
						var arr = [];
						for (var i = 0; i < localStorage.length; i++) {
							if ((localStorage.key(i).indexOf('productZones') !== -1) || (localStorage.key(i).indexOf('zones') !== -1)) {
								arr.push(localStorage.key(i));
							}
						}
						for (var j = 0; j < arr.length; j++) {
							storageApi.getInstance().removeItem(arr[j]);
						}
					}
					if (refreshData || !storageApi.getInstance().getItem("siteGraphicCats")) {
						var siteGraphicCatIds = {};
						siteGraphicCatIds['fields'] = "items.displayName,items.designSKUs,items.id";
						siteGraphicCatIds[CCConstants.PRODUCT_IDS] = "catTopHolidayDesigns,catHolidaySeason,catLoveRomance,catGradEdAcademics,catReligious,catWedding,catCelebration,catBabiesChildren,catSports,catMustache,catSymbols,catChineseSymbols,catScrolls,catMonogramDiamondO,catMonogramDiamondS,catMonogramGiraffePrint,catMonogramLeopardPrint,catMonogramZebraPrint,catMonogramTigerPrint,catAnimals,catBugsInsects,catArtMusic,catCustomWeddingLogo,catDragonsSnakesSkulls,catEarthMoonStars,catFlags,catMascots,catOccupations,catTransportationTravel,catUSCitiesStates,catWestern,catZodiac,catHalloween";
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, siteGraphicCatIds, function (graphicCats) {
							storageApi.getInstance().setItem("siteGraphicCats", JSON.stringify(graphicCats));
						});
					}
					if (refreshData || !storageApi.getInstance().getItem("siteColors")) {
						var siteClolorIds = {};
						siteClolorIds['fields'] = "items.id,items.listPrice,items.salePrice,items.upchargeSKU,items.blue,items.red,items.green,items.description";
						siteClolorIds[CCConstants.PRODUCT_IDS] = "Color_WIT_H,Color_VIL_H,Color_TLP_H,Color_SLV_H,Color_NOC_H,Color_MNT_H,Color_IRB_H,Color_HLB_H,Color_GOL_H,Color_EMD_H,Color_CPR_H,Color_BCK_H,Color_RBY_H";
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, siteClolorIds, function (colors) {
							storageApi.getInstance().setItem("siteColors", JSON.stringify(colors));
						});
					}
					if (refreshData || !storageApi.getInstance().getItem("siteFonts")) {
						var siteFontIds = {};
						siteFontIds["fields"] = "items.listPrice,items.salePrice,items.id,items.description,items.subCode,items.upchargeSKU,items.fontLeft,items.fontRight,items.fontCenter,items.tr_pulsePreviewName";
						siteFontIds[CCConstants.PRODUCT_IDS] = "Font_AAC_S,Font_AMZ_S,Font_ARE_S,Font_AS2_H,Font_ASH_H,Font_ASI_H,Font_AVG_H,Font_BAL_H,Font_BBB_S,Font_BBL_S,Font_BDP_S,Font_BLC_H,Font_BLE_S,Font_BLK_H,Font_BMO_H,Font_BOC_H,Font_BOD_H,Font_BOK_H,Font_BOO_H,Font_BOY_H,Font_BRS_S,Font_CAB_H,Font_CAE_S,Font_CCM_H,Font_CDM_H,Font_CEM_H,Font_CES_H,Font_CHE_S,Font_CHF_H,Font_CHS_H,Font_CLB_S,Font_CLK_H,Font_CLM_H,Font_CLN_H,Font_CLS_H,Font_CMA_H,Font_CME_S,Font_CMG_H,Font_CMH_H,Font_CML_H,Font_CMM_H,Font_CMX_H,Font_CNT_S,Font_COM_H,Font_COR_H,Font_COS_H,Font_COU_H,Font_CPB_H,Font_CPM_H,Font_CPT_H,Font_CRL_S,Font_CSE_S,Font_CSL_H,Font_CSM_H,Font_CSX_H,Font_CTE_S,Font_CTM_H,Font_CTN_H,Font_CTX_H,Font_CUE_S,Font_CUR_H,Font_CVM_H,Font_DIA_H,Font_DID_S,Font_DIE_S,Font_DIM_S,Font_DMO_H,Font_DMS_S,Font_EDW_S,Font_EGM_H,Font_ELG_H,Font_ENG_H,Font_FAS_H,Font_FCM_H,Font_FCM_S,Font_FDL_H,Font_FGT_S,Font_FSC_H,Font_FUT_H,Font_FUV_H,Font_FUX_H,Font_GAB_H,Font_GIL_H,Font_GIR_H,Font_GLS_H,Font_GOR_H,Font_GRE_S,Font_GRL_H,Font_GVB_H,Font_H1_S,Font_HDS_H,Font_HHS_H,Font_HMA_S,Font_HMD_S,Font_HMM_S,Font_HPB_H,Font_HPG_H,Font_HS2_H,Font_HSC_H,Font_IMG_H,Font_IMV_H,Font_ISA_H,Font_ISB_H,Font_ISM_H,Font_ISW_H,Font_ISX_H,Font_JAX_S,Font_KBL_H,Font_KDS_S,Font_KLB_H,Font_KMK_H,Font_KSC_H,Font_KZN_H,Font_LAL_H,Font_LAU_H,Font_LBL_H,Font_LCS_H,Font_LEO_H,Font_LNO_H,Font_LSC_H,Font_LSM_H,Font_LVD_H,Font_MLD_S,Font_MME_S,Font_MNS_S,Font_MST_H,Font_MYR_H,Font_NGS_H,Font_NTB_H,Font_NWE_S,Font_OE2_H,Font_OE5_H,Font_OEI_H,Font_OMO_H,Font_ONM_H,Font_ONX_H,Font_OPT_H,Font_PLB_H,Font_PLO_H,Font_PSC_H,Font_PSM_S,Font_QRY_H,Font_RCW_H,Font_RMG_H,Font_RMN_H,Font_ROM_H,Font_RSM_H,Font_RYM_S,Font_SCE_S,Font_SCR_H,Font_SFL_H,Font_SHD_H,Font_SIG_H,Font_SMS_S,Font_SOM_H,Font_SPE_H,Font_SPT_H,Font_STL_H,Font_TBB_H,Font_TCM_H,Font_TDM_H,Font_TDX_H,Font_TGB_H,Font_TGE_H,Font_THA_H,Font_TI2_H,Font_TIG_H,Font_TIM_H,Font_TIS_H,Font_TIX_H,Font_TMC_H,Font_TMM_H,Font_TMX_H,Font_TS2_H,Font_TSG_H,Font_TSM_H,Font_TSS_H,Font_TSX_H,Font_TTC_H,Font_VAN_H,Font_VIM_S,Font_VIN_H,Font_VLC_H,Font_VMM_H,Font_VMX_H,Font_VNA_H,Font_VTS_H,Font_WBK_H,Font_WFT_H,Font_WLG_S,Font_WME_S,Font_WRG_H,Font_WSR_H,Font_ZEB_H";
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, siteFontIds, function (fonts) {
							storageApi.getInstance().setItem("siteFonts", JSON.stringify(fonts));
						});
					}
					if (refreshData || !storageApi.getInstance().getItem("siteExpressPhrases")) {
						var expressPhraseIds = {};
						expressPhraseIds["fields"] = "items.listPrice,items.salePrice,items.expressPhraseDetailsSKUs,items.hardlineSoftline,items.displayName,items.itemType,items.repositoryId,items.discountMultipleInstances,items.firstWordDiscountable,items.fullImageURLs,items.description,items.expressPhraseExample,items.id,items.tr_expressPhraseType";
						expressPhraseIds[CCConstants.PRODUCT_IDS] = "194071,607665,607652,685137,713245,713258,718897,334684,337047,480798,337061,337054,531003,640020,795920,644754,343869,347488,219396,562593,335392,634340,634353,712851,685137,735760,337050,doNotPersonalize";
						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, expressPhraseIds, function (expressPhrases) {
							storageApi.getInstance().setItem("siteExpressPhrases", JSON.stringify(expressPhrases));
							var siteExpressPhraseDetailsIds = '';
							for (var k = 0; k < expressPhrases.length; k++) {
								if (siteExpressPhraseDetailsIds.indexOf(expressPhrases[k].expressPhraseDetailsSKUs) === -1) {
									if (k === 0) {
										siteExpressPhraseDetailsIds = expressPhrases[k].expressPhraseDetailsSKUs;
									} else {
										siteExpressPhraseDetailsIds = siteExpressPhraseDetailsIds + ',' + expressPhrases[k].expressPhraseDetailsSKUs;
									}
								}
							}
							var expressPhraseDetailsId = {};
							expressPhraseDetailsId['fields'] = "items.description,items.lineNumber,items.characterCount,items.id,items.inputCapable,items.spaceBefore,items.spaceAfter,items.tr_sequence,items.displayName,items.tr_designList,items.tr_designListType,items.required,items.tr_additionalDesignCost,items.tr_previewOverrideFontId,items.tr_previewOverrideColorId";
							expressPhraseDetailsId[CCConstants.PRODUCT_IDS] = siteExpressPhraseDetailsIds;
							ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, expressPhraseDetailsId, function (expressPhraseDetails) {
								storageApi.getInstance().setItem("siteExpressPhraseDetails", JSON.stringify(expressPhraseDetails));
							});
						});
					} */
					if (refreshData || !storageApi.getInstance().getItem("siteWordCost")) {
						var wordsArrayIds = [];
						wordsArrayIds["fields"] = "items.childSKUs,items.listPrice,items.salePrice,items.id";
						wordsArrayIds[CCConstants.PRODUCT_IDS] = "firstWord,anyWord,personalizationWords";

						ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, wordsArrayIds, function (wordSKUs) {
							storageApi.getInstance().setItem("siteWordCost", JSON.stringify(wordSKUs));
						});
					}
					/*if (refreshData || !storageApi.getInstance().getItem("siteDesignSkus")) {
						ccRestClient.authenticatedRequest('/ccstoreui/v1/products?fields=items.description,items.id,items.displayName,items.designClass,items.itemType,items.hardlineSoftline,items.listPrice,items.salePrice&q=type eq "DesignSKU"', {},
							function (data) {
								storageApi.getInstance().setItem("siteDesignSkus", JSON.stringify(data));
							},
							function (data) {

							});
					}
				}*/

				ccRestClient.authenticatedRequest("/ccstoreui/v1/publish", {},
					function (data) {
						var clearCache = false;
						if ((data.lastPublishedTimeStamp * 1) > (lastDataCacheTimeStamp * 1)) {
							clearCache = true;
						}
						//checkAndCacheData(clearCache);
					},
					function (data) {
						//checkAndCacheData(true);
					});
				if (!storeItems || storeItems.length === 0) {
					$.ajax({
						url: "/storelocator/stores.json",
						type: "get",
						async: false,
						success: function (data) {
							storeItems = data;
							window.storeItems = storeItems;
						},
						error: function () {}
					});
				}
				_this.onConfigReady(widget);
				$.Topic(pubsub.topicNames.CART_PRICE_COMPLETE).subscribe(function () {
					setTimeout(function () {

						if (!setDetailsToItem) {
							return;
						}
						setDetailsToItem = false;
						var localCart = getWidget.cart();
						var cartItems = localCart.allItems();
						var itemIndex = 0;
						var cartUpdated = false;


						if (currentPersonalizationData && currentPersonalizationData.length > 0) {
							for (var i = 0; i < cartItems.length; i++) {
								if (cartItems[i].catRefId !== 'personalization1') {
									if (cartItems[i].tr_isPersonalized() && cartItems[i].tr_delivery_details()) {
										var deliveryDetails = JSON.parse(cartItems[i].tr_delivery_details());
										var currentDeliveryDetails = JSON.parse(currentPersonalizationData[itemIndex]['data']['deliveryDetails']);
										if (currentDeliveryDetails && currentDeliveryDetails.skuid === cartItems[i].catRefId && currentDeliveryDetails.deliveryType === "2" && deliveryDetails && deliveryDetails.deliveryType === "2") {
											cartItems[i]['tr_delivery_details'] = ko.observable(currentPersonalizationData[itemIndex]['data']['deliveryDetails']);
											var personalizationData = JSON.parse(cartItems[i].tr_personalization());
											personalizationData.deliveryDetails = currentPersonalizationData[itemIndex]['data']['deliveryDetails'];
											cartItems[i]['tr_personalization'] = ko.observable(JSON.stringify(personalizationData));
										}
									}
									if (!cartItems[i].tr_isPersonalized()) {
										cartItems[i]['tr_personalization_hash'] = ko.observable(currentPersonalizationData[itemIndex]['hash']);
										cartItems[i]['tr_personalization'] = ko.observable(JSON.stringify(currentPersonalizationData[itemIndex]['data']));
										var cost = Number(currentPersonalizationData[itemIndex]['data']['personalizationCost']);
										if (isNaN(cost)) {
											cost = 0;
										}
										cartItems[i]['tr_personalization_cost'] = ko.observable(cost);
										cartItems[i]['tr_image_url'] = ko.observable(helpers.personalizationUtils().generateScene7URL(cartItems[i].catRefId.split("_")[0]));
										cartItems[i]['tr_total_with_personalization'] = ko.observable(cost + Number(cartItems[i].originalPrice()));
										cartItems[i]['tr_delivery_details'] = ko.observable(currentPersonalizationData[itemIndex]['data']['deliveryDetails']);
										cartItems[i]['tr_isPersonalized'] = ko.observable(true);
										cartItems[i].isPersonalized(true);
										cartUpdated = true;
										currentPersonalizationData.shift();
									}
								}
							}
							// Fix for moving cart items at the top //

							getWidget.cart().items().splice(0, 0, widget.cart().items().splice(cartItems.length - 1, 1)[0]);

							// end fix //

						}
						if (cartUpdated && currentPersonalizationData.length === 0) {
							if (helpers.orderUtils.checkIfPersonalizationSkuPresent(localCart)) {
								setDetailsToItem = false;
								if (widget.user().loggedIn()) {
									localCart.updateCurrentProfileOrder();
								} else {
									localCart.cartUpdated();
								}
								
							    widget.destroySpinner();
								$('#addToCartSuccess').modal('show');
								if(newPersonalizationData && newPersonalizationData !== '') {
    								$.Topic('TR_RESET_DATA.memory').publish();
    								var viewModel = ko.mapping.fromJS(newPersonalizationData);
            					    $.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
								}
								//$.Topic("Invoke_Gift_Modal.memory").publish(giftSkuFlag);
								$.Topic("Invoke_External_Pricing").publish(true,false,true);  
								$.Topic(pubsub.topicNames.CART_ADD_SUCCESS).publish(currentProduct);
								$("#select-quantity").val("1").trigger('change');
							} else {
								setDetailsToItem = true;
								helpers.orderUtils.checkAndAddPersonalizationCost(localCart, true);
							}
						} else {
							setDetailsToItem = true;
							if (currentPersonalizationData.length === 0) {
								if (_this.checkIfPersonalizationSkuPresent() === false) {
									helpers.orderUtils.checkAndAddPersonalizationCost(localCart, true);
								} else {
									setDetailsToItem = false;
									if (widget.user().loggedIn()) {
										localCart.updateCurrentProfileOrder();
									} else {
										localCart.cartUpdated();
									}
								    widget.destroySpinner();
									$('#addToCartSuccess').modal('show');
									if(newPersonalizationData && newPersonalizationData !== '') {
        								$.Topic('TR_RESET_DATA.memory').publish();
        								var viewModel = ko.mapping.fromJS(newPersonalizationData);
                					    $.Topic("TR_PDP_NEW_PERSONALIZATION_DATA_LOAD.memory").publish(viewModel);
    								}
									//$.Topic("Invoke_Gift_Modal.memory").publish(giftSkuFlag);
								    $.Topic("Invoke_External_Pricing").publish(true,false,true);
									$.Topic(pubsub.topicNames.CART_ADD_SUCCESS).publish(currentProduct);
									$("#select-quantity").val("1").trigger('change');
								}
							} else if (currentPersonalizationData.length === 1) {
								/*$.Topic(pubsub.topicNames.CART_ADD).publishWith(currentProduct, [{
								    message: "success"
								}]);*/
								getWidget.cart().addItem(currentProduct);
							} else {
								getWidget.cart().addItem(currentProduct);
							}
						}
					}, processingTime);

				});

				//Enable the radio buttons by default
				widget.availableTodayRadio = ko.computed(function () {
					if (widget.displayOpus() === true && widget.displayShipToHome() === false) {
						return true;
					} else {
						return false;
					}
				});

				widget.totalCost = ko.computed(function () {

					return (widget.salePrice() || widget.listPrice()) + (widget.personalizationCost() || 0);
				});

				$.Topic('TR_PDP_NEW_PRICE.memory').subscribe(function (skuid, skulistprice, skusalePrice) {
					widget.skuidNew(skuid);
					widget.skulistPriceNew(skulistprice);
					widget.skusalePriceNew(skusalePrice);
				});

				$.Topic('TR_CREATE_INVENTORY_SPINNER').subscribe(function () {
					widget.createInventorySpinner();
				});

				$.Topic('TR_DESTROY_INVENTORY_SPINNER').subscribe(function () {
					widget.destroyInventorySpinner();
				});

				$('body').delegate('.tr-color-swatch', 'keydown', function (e) {
					var keyCode = e.keyCode || e.which;
					var that = this;
					if (keyCode == 13) {
						setTimeout(function () {
							if ($(that).parents(".tr-carousel-wrapper").next().next(".tr-carousel-wrapper").length > 0) {
								$(that).parents(".tr-carousel-wrapper").next().next(".tr-carousel-wrapper").find(".owl-item:first a").focus();
							} else if ($(that).parents(".tr-e-product-variation").next(".tr-e-bom").find(".bom-content").length > 0) {
								$(that).parents(".tr-e-product-variation").next(".tr-e-bom").find(".bom-content:first").find(".owl-item:first a").focus();
							} else {
								$(".quantity-dropdown-wrap").focus();
							}
						}, 500)
					}
				});

				$('body').delegate('.tr-bom-item', 'keydown', function (e) {
					var keyCode = e.keyCode || e.which;
					var that = this;
					if (keyCode == 13) {
						setTimeout(function () {
							if ($(that).parents(".bom-content").next(".bom-content").length > 0) {
								$(that).parents(".bom-content").next(".bom-content").find(".owl-item:first a").focus();
							} else {
								$(".quantity-dropdown-wrap").focus();
							}
						}, 500)
					}
				});

				widget.checkIsMobile($(window).width());
				$(window).resize(function () {
					widget.checkIsMobile($(window).width());
				});
				
				
				
				/*$.Topic("Trigger_Gift_Modal").subscribe(function(data){
				    var prodData = data.split(', ');
				    ccRestClient.authenticatedRequest("/ccstoreui/v1/products?productIds="+prodData+ "&fields=items.id,items.displayName,items.route,items.tr_externalImageUrl",{}, function (data) {
                    widget.giftModalData(data);  
                    widget.destroySpinner();
                    //widget.giftboxModalShown();
                    $('#giftModalPopUp').modal('show'); 
                    $("#giftModalPopUp").on("hidden.bs.modal", function () {
                            window.location.reload();
                    });
				}, function (data) {}, "GET");
				})*/
				
				
				$.Topic('SKU_SELECTED').subscribe(checkNearby);
            
            function checkNearby(product, selectedSKUObj, variantOptions, qnty){
                $.Topic('TR_CREATE_INVENTORY_SPINNER').publish();
               h_product=product;
               h_selectedSKUObj= selectedSKUObj;
               h_variantOptions=variantOptions;
               h_qnty=qnty;
                
                
                if((typeof selectedSKUObj !== "undefined") && (selectedSKUObj !== null)){
                     var data = {};
                     
                     var repoId;
                     
                     if (typeof selectedSKUObj.repositoryId === "function"){
                         repoId = selectedSKUObj.repositoryId();
                     } else {
                         repoId = selectedSKUObj.repositoryId;
                     }

                    data[CCConstants.PRODUCT_IDS] = [repoId];
                    ccRestClient.request(CCConstants.ENDPOINT_PRODUCTS_LIST_PRODUCTS, data, function(products) {
						$(".dotwhackPDP").html("");
                         if(products.length > 0){
							$(".dotwhackPDP").html(products[0].promotionMessage);
                            $.Topic('SKU_OBJ1').publish(products[0]);
                            $.Topic('SKU_OBJ_BOM').publish(products[0]);
                            skuOPUS=products[0].OPUS;
                            var listPrice, salePrice;
                            if (typeof selectedSKUObj.listPrice === "function"){
                                listPrice = selectedSKUObj.listPrice();
                            } else {
                                listPrice = selectedSKUObj.listPrice;
                            }
                            
                            if (typeof selectedSKUObj.salePrice === "function"){
                                salePrice = selectedSKUObj.salePrice();
                            } else {
                                salePrice = selectedSKUObj.salePrice;
                            }
                            if(product.childSKUs().length === 1){
                                
                            }
                           
                            $.Topic('TR_PDP_NEW_PRICE.memory').publish(repoId,listPrice,salePrice);    
                           if(skuOPUS){
                               

								request_obj.skuId = repoId;
								request_obj.storeNumber=store_id;
								request_obj.location = {};
								request_obj.location.latitude = parseFloat(latitude);
								request_obj.location.longitude = parseFloat(longitude);
								request_obj.quantity = qnty || 1;
								request_obj.radiusLimit =400; 
								request_obj.limit =400; 
								var request_json = JSON.stringify(request_obj);
							
							}
							else{
								$.Topic('AVAILABLE_TODAY').publish(false);
								
							}
							setTimeout(function(){
								$.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
							},5000)   
                    }else {
						$.Topic('VARIANT_NO_STOCK_ERROR_MESSAGE').publish();
                        $.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
                    }
                        
                        
                    });
                 
                }
                else{
                    $.Topic('TR_DESTROY_INVENTORY_SPINNER').publish();
                    $.Topic('TR_PDP_NEW_PRICE.memory').publish(-1,product.listPrice(),product.salePrice());
                    $.Topic('AVAILABLE_TODAY').publish(false);
                }
            }


			}